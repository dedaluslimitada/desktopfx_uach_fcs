<%@ page contentType="text/plain" pageEncoding="iso-8859-1" session="false" %>
<%
/*
 * File generated by OBCOM Record Wizard 5.1.120
 *
 * Copyright (c) OBCOM INGENIERIA S.A. (Chile). All rights reserved.
 *
 * All rights to this product are owned by OBCOM INGENIERIA S.A. and may only be
 * used  under  the  terms of its associated license document. You may NOT copy,
 * modify, sublicense, or distribute this source file or portions of  it  unless
 * previously  authorized in writing by OBCOM INGENIERIA S.A. In any event, this
 * notice and above copyright must always be included verbatim with this file.
 *
 * File Name:       ACC_CLAVE.jsp
 * Creation Date:   15-jul-2015 19:00:11
 * Source Database: ECUBAS at localhost, User:EcuAdmin
 * Library Name:    ACCSER
 * Record Name:     ACC-CLAVE
 * Last Edition:    12-jul-2005 17:15:47, OSTERTAG@OSTERTAGLP
 * Description:     Pide nueva clave (niveles zero)
 */
    response.addHeader("Pragma", "no-cache");
    response.addHeader("Cache-Control", "no-cache");
    response.setDateHeader("Expires", System.currentTimeMillis());
%>
VR:1.0:ACC-CLAVE:2015-07-15:19:00:11
N:7:CODIGO
X:6:NCLAVE
CS:13:ACC-CLAVE:fvjTJW+s88O81x+qBvgXqW
