<%@ page contentType="text/plain" pageEncoding="iso-8859-1" session="false" %>
<%
/*
 * File generated by OBCOM Record Wizard 5.1.120
 *
 * Copyright (c) OBCOM INGENIERIA S.A. (Chile). All rights reserved.
 *
 * All rights to this product are owned by OBCOM INGENIERIA S.A. and may only be
 * used  under  the  terms of its associated license document. You may NOT copy,
 * modify, sublicense, or distribute this source file or portions of  it  unless
 * previously  authorized in writing by OBCOM INGENIERIA S.A. In any event, this
 * notice and above copyright must always be included verbatim with this file.
 *
 * File Name:       ACA_USU.jsp
 * Creation Date:   15-jul-2015 19:00:06
 * Source Database: ECUBAS at localhost, User:EcuAdmin
 * Library Name:    ACASER
 * Record Name:     ACA-USU
 * Last Edition:    12-jul-2005 17:15:46, OSTERTAG@OSTERTAGLP
 * Description:     Usuario ECU ADI
 */
    response.addHeader("Pragma", "no-cache");
    response.addHeader("Cache-Control", "no-cache");
    response.setDateHeader("Expires", System.currentTimeMillis());
%>
VR:1.0:ACA-USU:2015-07-15:19:00:06
X:16:CODIGO_ADI
X:20:APELL_PAT
X:20:APELL_MAT
X:30:NOMBRES
X:10:FAMILIA
X:1:TIPO_FACT
N:2:DEMO_DIAS
X:16:DEMO_AUTO
D:8:DEMO_F_INI
N:2:DEMO_VECES
N:4:DEMO_DIAST
X:2:USU_ESTADO:NH
D:8:VIG_DESDE
D:8:VIG_HASTA
X:20:PASSWORD
X:3:TIPO_PASSW:CAD
N:6:DIAS_CAD_P
X:3:ESTAD_PASS:EXP
X:2:ESTAD_CERT:NV
X:26:ID_CERT
X:19:F_CRE_USU
X:19:F_PRI_LOG
X:19:F_ULT_LOG
X:1:SOL_CERT
LB:940.20:PERFILES
N:7:CODIGO_ECU
X:40:NOMBRE_ECU
LE:940:PERFILES
R:10.1:RUT
R:10.1:RUT_INST
X:40:CARGO
X:40:DIRECCION
X:20:COMUNA
X:20:CIUDAD
X:20:ESTADO
X:20:PAIS
X:20:FONO1
X:20:FONO2
X:20:FAX
X:40:EMAIL
D:8:FEC_REG
X:20:COD_CONTR
X:10:ANEX_CONTR
X:8:COD_FACTUR
X:1:TIPO_ADM
X:1:USUARIOIM
CS:1533:ACA-USU:Ov-G4BqixXIw9adkZqgOr4
