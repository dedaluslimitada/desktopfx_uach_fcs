@rem ---------------------------------------------------------------------------
@rem Copyright (c) DEDALUS Limitada (Chile). All rights reserved.
@rem
@rem All  rights  to  this product are owned by DEDALUS Limitada and may only be
@rem used under the terms of its associated license document. You may NOT  copy,
@rem modify, sublicense, or distribute this source file or portions of it unless
@rem previously  authorized  in  writing by DEDALUS Limitada. In any event, this
@rem notice and above copyright must always be included verbatim with this file.
@rem ---------------------------------------------------------------------------

@goto error

@rem ---------------------------------------------------------------------------
@rem Generate self-signed certificate and private key: cakey.pem, cacert.cer

openssl.exe req -x509 -config ..\ca.conf -newkey rsa:2048 -sha256 -days 9000 ^
-keyout cakey.pem -out cacert.cer -passout pass:DemoTrustPassPhrase
@if %errorlevel% neq 0 goto error
@if exist .rnd del /q .rnd

@rem ---------------------------------------------------------------------------
@rem Display the contents of the generated certificate: cacert.cer.txt

openssl.exe x509 -in cacert.cer -noout -text > cacert.cer.txt
@if %errorlevel% neq 0 goto error

@rem ---------------------------------------------------------------------------
@rem Convertimos certificado de PEM a DER: cacert.cer -> cacert.der

openssl.exe x509 -in cacert.cer -inform PEM -out cacert.der -outform DER
@if %errorlevel% neq 0 goto error

@rem ---------------------------------------------------------------------------
@rem Import Root Certificate store into a JKS store: cacert.der -> cacert.jks

"C:\Java\jdk1.8.0_40\bin\keytool.exe" -importcert -noprompt -alias CACERT ^
-file cacert.der -keystore cacert.jks -storepass DemoTrustKeyStorePassPhrase
@if %errorlevel% neq 0 goto error

@rem ---------------------------------------------------------------------------
@rem Display the contents of the JKS store: cacert.jks -> cacert.jks.txt

"C:\Java\jdk1.8.0_40\bin\keytool.exe" -list -v -keystore cacert.jks ^
-storepass DemoTrustKeyStorePassPhrase > cacert.jks.txt
@if %errorlevel% neq 0 goto error

@rem ---------------------------------------------------------------------------

:error
@pause
