@rem ---------------------------------------------------------------------------
@rem Copyright (c) DEDALUS Limitada (Chile). All rights reserved.
@rem
@rem All  rights  to  this product are owned by DEDALUS Limitada and may only be
@rem used under the terms of its associated license document. You may NOT  copy,
@rem modify, sublicense, or distribute this source file or portions of it unless
@rem previously  authorized  in  writing by DEDALUS Limitada. In any event, this
@rem notice and above copyright must always be included verbatim with this file.
@rem ---------------------------------------------------------------------------

@goto error

@set alias=s1as
@set cfile=domeyko
@set cpass=changeit
@set cname=domeyko.dedalus.cl

@rem ---------------------------------------------------------------------------
@rem CN=%cname%
@rem OU=Security
@rem O=DEDALUS
@rem L=Valdivia
@rem S=Los Rios
@rem C=CL
@rem ---------------------------------------------------------------------------

@rem ---------------------------------------------------------------------------
@rem Generamos certificado auto-firmado en almacen: %cfile%.jks

"C:\Java\jdk1.8.0_40\bin\keytool.exe" -genkeypair ^
-keysize 2048 -keyalg RSA -sigalg SHA256withRSA -validity 9000 ^
-keystore %cfile%.jks -storepass %cpass% -keypass %cpass% -alias %alias% ^
-dname "CN=%cname%,OU=Security,O=DEDALUS,L=Valdivia,S=Los Rios,C=CL"
@if %errorlevel% neq 0 goto error

@rem ---------------------------------------------------------------------------
@rem Generamos solicitud de certificado (CSR): %cfile%.jks -> %cfile%.csr

"C:\Java\jdk1.8.0_40\bin\keytool.exe" -certreq ^
-keystore %cfile%.jks -storepass %cpass% -keypass %cpass% ^
-file %cfile%.csr -alias %alias%
@if %errorlevel% neq 0 goto error

@rem ---------------------------------------------------------------------------
@rem Firmamos la solicitud con certificado raiz: %cfile%.csr -> %cfile%.cer

openssl.exe ca -config ..\ca.conf -batch ^
-in %cfile%.csr -passin pass:DemoTrustPassPhrase -out %cfile%.cer
@if %errorlevel% neq 0 goto error

@rem ---------------------------------------------------------------------------
@rem Convertimos certificado de PEM a DER: %cfile%.cer -> %cfile%.der

openssl.exe x509 -in %cfile%.cer -inform PEM -out %cfile%.der -outform DER
@if %errorlevel% neq 0 goto error

@rem ---------------------------------------------------------------------------
@rem Importamos certificado raiz a almacen: cacert.der -> %cfile%.jks

"C:\Java\jdk1.8.0_40\bin\keytool.exe" -importcert -noprompt -trustcacerts ^
-file ..\CA\cacert.der -alias CACERT -keystore %cfile%.jks -storepass %cpass%
@if %errorlevel% neq 0 goto error

@rem ---------------------------------------------------------------------------
@rem Importamos certificado firmado a almacen: %cfile%.der -> %cfile%.jks

"C:\Java\jdk1.8.0_40\bin\keytool.exe" -importcert -noprompt -trustcacerts ^
-file %cfile%.der -alias %alias% -keystore %cfile%.jks -storepass %cpass% -keypass %cpass%
@if %errorlevel% neq 0 goto error

@rem ---------------------------------------------------------------------------
@rem Eliminamos certificado raiz del almacen: %cfile%.jks - cacert.der

"C:\Java\jdk1.8.0_40\bin\keytool.exe" -delete ^
-keystore %cfile%.jks -storepass %cpass% -alias CACERT
@if %errorlevel% neq 0 goto error

@rem ---------------------------------------------------------------------------
@rem Mostramos el contenido del almacen: %cfile%.jks -> %cfile%.jks.txt

"C:\Java\jdk1.8.0_40\bin\keytool.exe" -list -v ^
-keystore %cfile%.jks -storepass %cpass% > %cfile%.jks.txt
@if %errorlevel% neq 0 goto error

@rem ---------------------------------------------------------------------------
@rem Convertimos store de formato JKS a P12: %cfile%.jks -> %cfile%.p12

@if exist %cfile%.p12 del /q %cfile%.p12
"C:\Java\jdk1.8.0_40\bin\keytool.exe" -importkeystore ^
-srckeystore %cfile%.jks -srcstoretype JKS -srcalias %alias% ^
-srcstorepass %cpass% -srckeypass %cpass% ^
-destkeystore %cfile%.p12 -deststoretype PKCS12 -destalias %alias% ^
-deststorepass %cpass% -destkeypass %cpass%
@if %errorlevel% neq 0 goto error

@rem ---------------------------------------------------------------------------
@rem Extraemos llave privada del almacen P12: %cfile%.p12 -> %cfile%.key

openssl.exe pkcs12 -nocerts ^
-in  %cfile%.p12 -passin pass:%cpass% ^
-out %cfile%.key -passout pass:%cpass%
@if %errorlevel% neq 0 goto error

@rem ---------------------------------------------------------------------------
@rem Eliminamos el archivo de semilla random de OpenSSL: .rnd

@if exist .rnd del /q .rnd

@rem ---------------------------------------------------------------------------

:error
@pause
