@rem ---------------------------------------------------------------------------
@rem Copyright (c) OBCOM INGENIERIA S.A. (Chile). All rights reserved.
@rem
@rem All  rights to this product are owned by OBCOM INGENIERIA S.A. and may only
@rem be used under the terms of its associated license  document.  You  may  NOT
@rem copy,  modify, sublicense, or distribute this source file or portions of it
@rem unless previously authorized in writing by OBCOM  INGENIERIA  S.A.  In  any
@rem event, this notice and the above copyright must always be included verbatim
@rem with this file.
@rem ---------------------------------------------------------------------------

@goto errors

"C:\Java\jdk1.8.0_45\bin\keytool.exe" -genkeypair ^
-keystore dedalus.jks -storepass changeit -keypass changeit ^
-keyalg rsa -keysize 2048 -alias DEDALUS -validity 1095 ^
-dname "CN=DesktopFX,O=DEDALUS,L=Santiago,S=RM,C=CL"
@if %errorlevel% neq 0 goto errors

"C:\Java\jdk1.8.0_45\bin\keytool.exe" -list -v  ^
-keystore dedalus.jks -storepass changeit > dedalus.txt
@if %errorlevel% neq 0 goto errors

"C:\Java\jdk1.8.0_45\bin\keytool.exe" -importkeystore ^
-srcstoretype JKS -srckeystore dedalus.jks -srcstorepass changeit ^
-deststoretype PKCS12 -destkeystore dedalus.p12 -deststorepass changeit
@if %errorlevel% neq 0 goto errors

:errors
@pause
