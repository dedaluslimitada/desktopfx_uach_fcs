<!--
 * Copyright (c) OBCOM INGENIERIA S.A. (Chile). All rights reserved.
 *
 * All rights to this product are owned by OBCOM INGENIERIA S.A. and may only be
 * used  under  the  terms of its associated license document. You may NOT copy,
 * modify, sublicense, or distribute this source file or portions of  it  unless
 * previously  authorized in writing by OBCOM INGENIERIA S.A. In any event, this
 * notice and above copyright must always be included verbatim with this file.
-->
<project name="jar-build" default="jar-deploy-all" basedir=".">
    <!-- Import common ant definitions -->
    <import file="build-def.xml"/>

    <!-- Default jar specification version -->
    <property name="jar.version" value="1.1"/>

    <!-- Directory that contains the jar java sources -->
    <property name="src.dir" location="../src"/>

    <!-- Directory that contains the jar class files -->
    <property name="bin.dir" location="../bin"/>

    <!-- Location of the application jar file -->
    <property name="jar.file" location="${ant.project.name}.jar"/>

    <!-- Location of the application jar file in deploys directory -->
    <property name="dep.file" location="${deploy.libs}/${ant.project.name}.jar"/>

    <!-- Display package classes and members in javadoc -->
    <property name="doc.show.package" location="yes"/>

    <!-- Location of the overview document for javadoc -->
    <property name="doc.overview" location=""/>

    <!-- Location of the application javadoc file -->
    <property name="doc.file" location="${deploy.javadoc}/${ant.project.name}-javadoc.jar"/>

    <!-- Application javadoc patternset -->
    <patternset id="javadoc.packageset"/>

    <!-- Application jar classpath -->
    <path id="class.path"/>
    
    <!-- Builds the application jar depend proyects -->
    <target name="jar-depends"/>

    <!-- Create and initialize application jar bin dir -->
    <target name="jar-init">
        <mkdir dir="${bin.dir}"/>
        <copy todir="${bin.dir}" includeemptydirs="false">
            <fileset dir="${src.dir}">
                <exclude name="**/*.java"/>
            </fileset>
        </copy>
    </target>

    <!-- Compiles the jar source files if newer than class files -->
    <target name="jar-compile" depends="jar-depends,jar-init">
        <javac destdir="${bin.dir}" includeantruntime="false"
            debug="true" debuglevel="lines,vars,source" encoding="UTF-8">
            <src path="${src.dir}"/>
            <classpath refid="class.path"/>
        </javac>
    </target>

    <!-- Builds the application jar file -->
    <target name="jar-build" depends="jar-compile">
        <tstamp>
            <format property="version" pattern="${version.pattern}"/>
            <format property="build.datetime" pattern="${build.datetime.pattern}"/>
        </tstamp>
        <replaceregexp file="MANIFEST.MF" match="Ant-Version:.*\n" flags="g" replace=""/>
        <replaceregexp file="MANIFEST.MF" match="Created-By:.*\n" flags="g" replace=""/>
        <manifest file="MANIFEST.MF" mode="update">
            <attribute name="Codebase" value="*"/>
            <attribute name="Permissions" value="all-permissions"/>
            <attribute name="Build-DateTime" value="${build.datetime}"/>
            <attribute name="Specification-Version" value="${jar.version}"/>
            <attribute name="Implementation-Version" value="${jar.version}.${version}"/>
        </manifest>
        <jar destfile="${jar.file}" manifest="MANIFEST.MF" compress="true">
            <fileset dir="${bin.dir}" excludes="**/*.java,**/*.html"/>
        </jar>
    </target>

    <!-- Optimize jar file in the current directory -->
    <target name="jar-optimize" depends="jar-build">
        <proguard 
            configuration="${proguard.pro}" 
            printseeds="${ant.project.name}.seeds"
            printmapping="${ant.project.name}.map">
            <injar file="${jar.file}" filter="!**/*.java"/>
            <outjar file="${ant.project.name}0.jar"/>
            <libraryjar refid="class.path"/>
        </proguard>
        <move file="${ant.project.name}0.jar" tofile="${jar.file}"/>
    </target>

    <!-- Sign the jar file to shared libs directory -->
    <target name="jar-sign" depends="jar-optimize">
        <signjar jar="${jar.file}" digestalg="SHA-256" lazy="false"
            sigfile="${sign.sigfile}" tsaurl="${sign.tsaurl}"
            keystore="${sign.keystore}" alias="${sign.alias}"
            storepass="${sign.storepass}" keypass="${sign.keypass}">
            <sysproperty key="jsse.enableSNIExtension" value="true"/>
        </signjar>
        <move file="${jar.file}" tofile="${dep.file}"/>
    </target>

    <!-- Deploys the application jar file to jnlp library (if any) -->
    <target name="jar-deploy" depends="jar-sign" if="jnlp.dir">
        <copy file="${dep.file}" todir="${jnlp.dir}"/>
    </target>

    <!-- Checks if the application javadoc file is up-to-date -->
    <target name="jar-build-doc-check">
        <condition property="doc.uptodate">
            <or>
                <not><isset property="doc.header"/></not>
                <uptodate targetfile="${doc.file}">
                    <srcfiles dir="${src.dir}" includes="**/*">
                    	<patternset refid="javadoc.packageset"/>
                    </srcfiles>
                </uptodate>
            </or>
        </condition>
    </target>

    <!-- Builds the application javadoc file if not up-to-date -->
    <target name="jar-deploy-doc" depends="jar-build-doc-check" unless="doc.uptodate">
        <delete dir="javadoc"/>
        <tstamp>
            <format property="doc.date" pattern="yyyy-MM-dd"/>
        </tstamp>
        <javadoc destdir="javadoc" author="no" verbose="no"
        	encoding="UTF-8" docencoding="UTF-8" charset="UTF-8"
        	classpathref="class.path" overview="${doc.overview}" package="${doc.show.package}" 
            doctitle="${doc.header} v${jar.version}" windowtitle="${doc.header} v${jar.version}">
            <header><![CDATA[<p>${doc.header}&nbsp;v${jar.version}&nbsp;(${doc.date})</p>]]></header>
            <bottom><![CDATA[Copyright &copy; <a href="http://www.obcom.cl/" target="_blank">OBCOM INGENIERIA S.A. (Chile)</a>. All Rights Reserved.]]></bottom>
            <link href="http://docs.oracle.com/javase/8/docs/api/" offline="true" packagelistloc="${deploy.links}/javase"/>
            <link href="http://docs.oracle.com/javase/8/javafx/api/" offline="true" packagelistloc="${deploy.links}/javafx"/>
            <link href="http://docs.oracle.com/javase/8/docs/jre/api/javaws/jnlp/" offline="true" packagelistloc="${deploy.links}/javaws"/>
            <link href="http://docs.oracle.com/javaee/7/api/" offline="true" packagelistloc="${deploy.links}/javaee"/>
        	<packageset dir="${src.dir}" defaultexcludes="yes">
        		<patternset refid="javadoc.packageset"/>
        	</packageset>
        </javadoc>
        <jar destfile="${doc.file}" manifest="MANIFEST.MF" compress="true">
            <fileset dir="javadoc"/>
        </jar>
        <delete dir="javadoc"/>
    </target>

    <!-- Deploys the application jar and javadoc files -->
    <target name="jar-deploy-all" depends="jar-deploy,jar-deploy-doc"/>
</project>