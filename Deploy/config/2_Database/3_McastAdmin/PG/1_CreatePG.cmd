@rem ---------------------------------------------------------------------------
@rem Copyright (c) OBCOM INGENIERIA S.A. (Chile). All rights reserved.
@rem
@rem All  rights to this product are owned by OBCOM INGENIERIA S.A. and may only
@rem be used under the terms of its associated license  document.  You  may  NOT
@rem copy,  modify, sublicense, or distribute this source file or portions of it
@rem unless previously authorized in writing by OBCOM  INGENIERIA  S.A.  In  any
@rem event, this notice and the above copyright must always be included verbatim
@rem with this file.
@rem ---------------------------------------------------------------------------

@if not defined PGSQL set PGSQL=C:\PostgreSQL\9.3.4\bin\psql.exe
@if not defined PGCON set PGCON=-d ecubas -h localhost -p 5432 -U ecuadmin

@rem ---------------------------------------------------------------------------
@for %%s in (MCASTADMIN$*.pg) do @call :load_pg %%s

@rem ---------------------------------------------------------------------------
%PGSQL% %PGCON% -q -c "REVOKE EXECUTE ON ALL FUNCTIONS IN SCHEMA public FROM public"
@if %errorlevel% neq 0 goto errors

@rem ---------------------------------------------------------------------------
%PGSQL% %PGCON% -q -c "GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA public TO ecuadmin"
@if %errorlevel% neq 0 goto errors

@rem ---------------------------------------------------------------------------
@echo Database objects loaded successfully
@pause
@exit /b 0

@rem ---------------------------------------------------------------------------
:load_pg
    %PGSQL% %PGCON% -q -f %1%
    @if %errorlevel% neq 0 goto errors
@goto :eof

@rem ---------------------------------------------------------------------------
:errors
@echo Error loading database objects
@pause
@exit /b 1
