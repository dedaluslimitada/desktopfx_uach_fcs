#!/bin/sh
#-------------------------------------------------------------------------------
# Copyright (c) OBCOM INGENIERIA S.A. (Chile). All rights reserved.
#
# All  rights to this product are owned by OBCOM INGENIERIA S.A. and may only be
# used under the terms of its associated license document.  You  may  NOT  copy,
# modify,  sublicense,  or  distribute this source file or portions of it unless
# previously authorized in writing by OBCOM INGENIERIA S.A. In any  event,  this
# notice  and  the  above  copyright  must always be included verbatim with this
# file.
#-------------------------------------------------------------------------------
PGCLIENTENCODING=utf8
PSQL="/Library/PostgreSQL/9.4/bin/psql -d ecubas -h domeyko -p 5432 -U ecuadmin"

#-------------------------------------------------------------------------------
for file in MCASTNEWS\$*.pg; do
    $PSQL -q -f $file
    if [ "$?" -ne 0 ]; then echo "psql failed"; exit 1; fi
done

#-------------------------------------------------------------------------------
$PSQL -q -c "REVOKE EXECUTE ON ALL FUNCTIONS IN SCHEMA public FROM public"
if [ "$?" -ne 0 ]; then echo "psql failed"; exit 1; fi

#-------------------------------------------------------------------------------
$PSQL -q -c "GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA public TO ecuadmin"
if [ "$?" -ne 0 ]; then echo "psql failed"; exit 1; fi

#-------------------------------------------------------------------------------
echo "Database objects loaded successfully"
