@rem ---------------------------------------------------------------------------
@rem Copyright (c) OBCOM INGENIERIA S.A. (Chile). All rights reserved.
@rem
@rem All  rights to this product are owned by OBCOM INGENIERIA S.A. and may only
@rem be used under the terms of its associated license  document.  You  may  NOT
@rem copy,  modify, sublicense, or distribute this source file or portions of it
@rem unless previously authorized in writing by OBCOM  INGENIERIA  S.A.  In  any
@rem event, this notice and the above copyright must always be included verbatim
@rem with this file.
@rem ---------------------------------------------------------------------------

@set URL="jdbc:postgresql://localhost:5432/ecubas?user=ecuadmin&password=ecuadmin"
@set DIR=.

@for /r %%f in (%DIR%/menus/*.xml) do java -cp %DIR%/lib/*;. -Durl=%URL% ^
-Dmenu=%%~nf -Dfile=%DIR%/menus/%%~nxf -DdeleteAll=false cl.obcom.desktopfx.loader.Main

@pause
