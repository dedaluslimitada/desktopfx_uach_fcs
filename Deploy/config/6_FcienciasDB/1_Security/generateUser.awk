#!/usr/bin/awk -f
BEGIN {
	FS=";";
}
{
username = substr($3,x+2,length($3)-2);
firstName = substr($6,x+2,length($6)-2);
lastName = substr($7,x+2,length($7)-2);

print "INSERT INTO ecuaccusu (";
print "    usu_codigo,";
print "    nombres,";
print "    usu_estado,";
print "    psw_tipo,";
print "    password,";
print "    psw_vig_desd,";
print "    psw_estado,";
print "    familia,";
print "    fec_vig_desd,";
print "    fec_vig_hast,";
print "    fec_cre_usu,";
print "    fec_pri_log,";
print "    fec_ult_log,";
print "    fec_err_log,";
print "    facturacion,";
print "    usuarioim";
print ") VALUES (";
print "    '" username  "',";
print "    '" firstName " " lastName "',";
print   " 'HA',";
print "    'NCA',";
print "    '138474693594455360904252160475958409042521604759584',";
print "    0,";
print "    'EXP',";
print "    'GENERAL',";
print "    20010101,";
print "    29991231,";
print "    '0001-01-01:00:00:00.000000',";
print "    '0001-01-01:00:00:00.000000',";
print "    '0001-01-01:00:00:00.000000',";
print "    '0001-01-01:00:00:00.000000',";
print "    'B',";
print "    ''";
print ");";

}
