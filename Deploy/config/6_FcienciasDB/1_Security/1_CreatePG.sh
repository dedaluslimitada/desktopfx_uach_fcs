#!/bin/sh
#-------------------------------------------------------------------------------
# Copyright (c) OBCOM INGENIERIA S.A. (Chile). All rights reserved.
#
# All  rights to this product are owned by OBCOM INGENIERIA S.A. and may only be
# used under the terms of its associated license document.  You  may  NOT  copy,
# modify,  sublicense,  or  distribute this source file or portions of it unless
# previously authorized in writing by OBCOM INGENIERIA S.A. In any  event,  this
# notice  and  the  above  copyright  must always be included verbatim with this
# file.
#-------------------------------------------------------------------------------
export PGPASSWORD=ecuadmin
PSQL="/Library/PostgreSQL/9.4/bin/psql -d ecubas -h domeyko -p 5432 -U ecuadmin"
PSQL="/Library/PostgreSQL/9.4/bin/psql -d ecubas -h ferre2015.cloudapp.net -p 5432 -U ecuadmin"

#-------------------------------------------------------------------------------
$PSQL -q -f 2_SecurityTables.pg
if [ "$?" -ne 0 ]; then echo "psql failed"; exit 1; fi

#-------------------------------------------------------------------------------
echo "Database objects loaded successfully"
