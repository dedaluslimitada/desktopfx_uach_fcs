CREATE OR REPLACE FUNCTION fcsinformes$calcula_hrs_adm (
	_codi_docente		IN	VARCHAR,	
    _codi_semestre		IN	VARCHAR,
    _codi_agno			IN 	VARCHAR	
) RETURNS void
AS $BODY$
--------------------------------------------------------------------------------
-- Copyright (c) DEDALUS S.A. (Chile). All rights reserved.
--------------------------------------------------------------------------------
DECLARE
		v_adm		REFCURSOR;
		v_ext		REFCURSOR;
		v_rec		RECORD;	
		v_semanas	INTEGER;
		v_horas		INTEGER;
		v_total_a	INTEGER;
		v_total_e	INTEGER;
BEGIN
--==============================================================================================================
--======================================= Calculo las horas de Administracion CO y AA
--==============================================================================================================
	OPEN v_adm FOR
	SELECT
		a.codi_docente, 	
		a.fech_actividad					AS	C_FECH_INI,			-- Fecha inicial
			CASE WHEN((to_char(a.fech_actividad,'MM'))::int >=1 AND (to_char(a.fech_actividad,'MM'))::int<=6) THEN '1'
				ELSE '2'
			END AS C_SEMESTRE_INI,									-- Semestre inicial
		to_char(a.fech_actividad,'YYYY')		AS	C_AGNO_INI,			-- Agno inicial
		a.fech_termino						AS	C_FECH_FIN,			-- Fecha final
			CASE WHEN((to_char(a.fech_termino,'MM'))::int >=1 AND (to_char(a.fech_termino,'MM'))::int<=6) THEN '1'
				ELSE '2'
			END AS C_SEMESTRE_FIN,									-- Semestre final
		to_char(a.fech_termino,'YYYY')		AS	C_AGNO_FIN,			-- Agno final
		(a.fech_termino-a.fech_actividad)/7	AS	C_SEMANAS,			
		a.cant_horas_semana * ((a.fech_termino-a.fech_actividad)/7)				AS	C_HRS_TOTAL,
		a.cant_horas_semana   	AS 	C_HRS_SEMANA  --horas/semana	
	FROM 
		fct_otrasactividades a
	WHERE
		a.fech_actividad <> '1900-01-01' AND a.fech_termino <> '1900-01-01' AND
		a.fech_termino <> a.fech_actividad  AND
		a.cant_horas_semana <> 0 	AND 
		(to_char(a.fech_actividad,'YYYY')=_codi_agno OR to_char(a.fech_termino,'YYYY')=_codi_agno) AND
		a.codi_docente=_codi_docente AND	a.iden_actividad in ('CO','AA')
	order by
		a.id;

	v_total_a:=0;		
	LOOP
		v_horas:=0;
		FETCH v_adm INTO v_rec;
		IF (NOT FOUND) THEN
			exit;
		END IF;
		
		--- Primer caso si ini_semestre=fin_semestre del año de ingreso, proyecto esta en un semestre (1 o 2).
		IF ((v_rec.C_AGNO_INI=_codi_agno) AND (v_rec.C_SEMESTRE_INI=v_rec.C_SEMESTRE_FIN) AND (v_rec.C_SEMESTRE_INI=_codi_semestre)) THEN
				v_horas := v_rec.C_HRS_TOTAL;
		END IF;
		
		--  Si fin_semestre=2 y y ini_semestre=1 and fin_semestre=2 and _codi_semestre=2
		IF ((v_rec.C_AGNO_INI=_codi_agno) AND (v_rec.C_SEMESTRE_INI='1') AND (v_rec.C_SEMESTRE_FIN='2') AND(_codi_semestre='2')) THEN
				v_semanas := (v_rec.C_FECH_FIN - TO_DATE('30-06-'||v_rec.C_AGNO_INI,'DD-MM-YYYY') ) / 7;
				v_horas := v_rec.C_HRS_SEMANA * v_semanas;
		END IF;

		--  Si fin_semestre=2 y y ini_semestre=1 and fin_semestre=2 and _codi_semestre=1
		IF ((v_rec.C_AGNO_INI=_codi_agno) AND (v_rec.C_SEMESTRE_INI='1') AND (v_rec.C_SEMESTRE_FIN='2') AND (_codi_semestre='1')) THEN
				v_semanas := ( TO_DATE('30-06-'||v_rec.C_AGNO_INI,'DD-MM-YYYY' ) - v_rec.C_FECH_INI  ) / 7;
				v_horas := v_rec.C_HRS_SEMANA * v_semanas;
		END IF;
		
		-- Si ini_semestre=2 y fin_semestre=1 y agno_ini=_agno_ini  y _codi_semestre='2'
		IF ((v_rec.C_AGNO_INI=_codi_agno) AND (v_rec.C_SEMESTRE_INI='2') AND (v_rec.C_SEMESTRE_FIN='1') AND (_codi_semestre='2')) THEN
				v_semanas := ( TO_DATE('31-12-'||v_rec.C_AGNO_INI,'DD-MM-YYYY') - v_rec.C_FECH_INI ) / 7;
				v_horas := v_rec.C_HRS_SEMANA * v_semanas;
		END IF;
		
		--Si ini_semestre=2 y fin_semestre=1 y agno_fin=_agno_ini
		IF ((v_rec.C_AGNO_FIN=_codi_agno) AND (v_rec.C_SEMESTRE_INI='2') AND (v_rec.C_SEMESTRE_FIN='1')) AND (_codi_semestre='1')THEN
				v_semanas := (v_rec.C_FECH_FIN - TO_DATE('01-01-'||v_rec.C_AGNO_FIN,'DD-MM-YYYY') ) / 7;
				v_horas := v_rec.C_HRS_SEMANA * v_semanas;
		END IF;
		
		-- Si se debe incluir completo el semestre 2
		IF ((v_rec.C_FECH_INI < TO_DATE('30-06-'||_codi_agno,'DD-MM-YYYY')) AND (_codi_semestre='2')
				AND (v_rec.C_FECH_FIN > TO_DATE('31-12-'||_codi_agno,'DD-MM-YYYY')) ) THEN
				v_horas := v_rec.C_HRS_SEMANA * 24;   -- 1 Semestre tiene 24 semanas
		END IF;

		-- Si se debe incluir completo el semestre 1
		IF ((v_rec.C_FECH_INI < TO_DATE('01-01-'||_codi_agno,'DD-MM-YYYY')) AND (_codi_semestre='1')
				AND (v_rec.C_FECH_FIN > TO_DATE('30-06-'||_codi_agno,'DD-MM-YYYY')) ) THEN
				v_horas := v_rec.C_HRS_SEMANA * 24;   -- 1 Semestre tiene 24 semanas
		END IF;
		
		v_total_a := v_total_a + v_horas;
	END LOOP;
	CLOSE v_adm;
			
	-------------------------------------------------------------
	IF (v_total_a<>0) THEN
		UPDATE FCT_COLABXASIGNATURA
		SET
			tota_hr_adm = v_total_a
		WHERE
			codi_asignatura='ADM' 	AND
			codi_docente=_codi_docente		AND
			codi_semestre=_codi_semestre	AND
			codi_agno=_codi_agno;
		IF (NOT FOUND) THEN
			INSERT INTO FCT_COLABXASIGNATURA (
				codi_asignatura,
				codi_semestre,
				codi_agno,
				codi_docente,
				tota_hr_adm
			) VALUES (
				'ADM',
				_codi_semestre,
				_codi_agno,
				_codi_docente,
				v_total_a	
			);
		END IF;
	END IF;
	
--==============================================================================================================
--======================================= Calculo las horas de Extension
--==============================================================================================================
	OPEN v_ext FOR
	SELECT
		a.codi_docente, 	
		a.fech_actividad					AS	C_FECH_INI,			-- Fecha inicial
			CASE WHEN((to_char(a.fech_actividad,'MM'))::int >=1 AND (to_char(a.fech_actividad,'MM'))::int<=6) THEN '1'
				ELSE '2'
			END AS C_SEMESTRE_INI,									-- Semestre inicial
		to_char(a.fech_actividad,'YYYY')		AS	C_AGNO_INI,			-- Agno inicial
		a.fech_termino						AS	C_FECH_FIN,			-- Fecha final
			CASE WHEN((to_char(a.fech_termino,'MM'))::int >=1 AND (to_char(a.fech_termino,'MM'))::int<=6) THEN '1'
				ELSE '2'
			END AS C_SEMESTRE_FIN,									-- Semestre final
		to_char(a.fech_termino,'YYYY')		AS	C_AGNO_FIN,			-- Agno final
		(a.fech_termino-a.fech_actividad)/7	AS	C_SEMANAS,			
		a.cant_horas_semana * ((a.fech_termino-a.fech_actividad)/7)				AS	C_HRS_TOTAL,
		a.cant_horas_semana   	AS 	C_HRS_SEMANA  --horas/semana	
	FROM 
		fct_otrasactividades a
	WHERE
		a.fech_actividad <> '1900-01-01' AND a.fech_termino <> '1900-01-01' AND
		a.fech_actividad <> a.fech_termino AND
		a.cant_horas_semana <> 0 	AND 
		(to_char(a.fech_actividad,'YYYY')=_codi_agno OR to_char(a.fech_termino,'YYYY')=_codi_agno) AND
		a.codi_docente=_codi_docente AND	a.iden_actividad in ('EU')
	order by
		a.id;

	v_total_e:=0;		
	LOOP
		v_horas:=0;
		FETCH v_ext INTO v_rec;
		IF (NOT FOUND) THEN
			exit;
		END IF;
		
		--- Primer caso si ini_semestre=fin_semestre del año de ingreso, proyecto esta en un semestre (1 o 2).
		IF ((v_rec.C_AGNO_INI=_codi_agno) AND (v_rec.C_SEMESTRE_INI=v_rec.C_SEMESTRE_FIN) AND (v_rec.C_SEMESTRE_INI=_codi_semestre)) THEN
				v_horas := v_rec.C_HRS_TOTAL;
		END IF;
		
		--  Si fin_semestre=2 y y ini_semestre=1 and fin_semestre=2 and _codi_semestre=2
		IF ((v_rec.C_AGNO_INI=_codi_agno) AND (v_rec.C_SEMESTRE_INI='1') AND (v_rec.C_SEMESTRE_FIN='2') AND(_codi_semestre='2')) THEN
				v_semanas := (v_rec.C_FECH_FIN - TO_DATE('30-06-'||v_rec.C_AGNO_INI,'DD-MM-YYYY') ) / 7;
				v_horas := v_rec.C_HRS_SEMANA * v_semanas;
		END IF;

		--  Si fin_semestre=2 y y ini_semestre=1 and fin_semestre=2 and _codi_semestre=1
		IF ((v_rec.C_AGNO_INI=_codi_agno) AND (v_rec.C_SEMESTRE_INI='1') AND (v_rec.C_SEMESTRE_FIN='2') AND (_codi_semestre='1')) THEN
				v_semanas := ( TO_DATE('30-06-'||v_rec.C_AGNO_INI,'DD-MM-YYYY' ) - v_rec.C_FECH_INI  ) / 7;
				v_horas := v_rec.C_HRS_SEMANA * v_semanas;
		END IF;
		
		-- Si ini_semestre=2 y fin_semestre=1 y agno_ini=_agno_ini  y _codi_semestre='2'
		IF ((v_rec.C_AGNO_INI=_codi_agno) AND (v_rec.C_SEMESTRE_INI='2') AND (v_rec.C_SEMESTRE_FIN='1') AND (_codi_semestre='2')) THEN
				v_semanas := ( TO_DATE('31-12-'||v_rec.C_AGNO_INI,'DD-MM-YYYY') - v_rec.C_FECH_INI ) / 7;
				v_horas := v_rec.C_HRS_SEMANA * v_semanas;
		END IF;
		
		--Si ini_semestre=2 y fin_semestre=1 y agno_fin=_agno_ini
		IF ((v_rec.C_AGNO_FIN=_codi_agno) AND (v_rec.C_SEMESTRE_INI='2') AND (v_rec.C_SEMESTRE_FIN='1')) AND (_codi_semestre='1')THEN
				v_semanas := (v_rec.C_FECH_FIN - TO_DATE('01-01-'||v_rec.C_AGNO_FIN,'DD-MM-YYYY') ) / 7;
				v_horas := v_rec.C_HRS_SEMANA * v_semanas;
		END IF;
		
		-- Si se debe incluir completo el semestre 2
		IF ((v_rec.C_FECH_INI < TO_DATE('30-06-'||_codi_agno,'DD-MM-YYYY')) AND (_codi_semestre='2')
				AND (v_rec.C_FECH_FIN > TO_DATE('31-12-'||_codi_agno,'DD-MM-YYYY')) ) THEN
				v_horas := v_rec.C_HRS_SEMANA * 24;   -- 1 Semestre tiene 24 semanas
		END IF;

		-- Si se debe incluir completo el semestre 1
		IF ((v_rec.C_FECH_INI < TO_DATE('01-01-'||_codi_agno,'DD-MM-YYYY')) AND (_codi_semestre='1')
				AND (v_rec.C_FECH_FIN > TO_DATE('30-06-'||_codi_agno,'DD-MM-YYYY')) ) THEN
				v_horas := v_rec.C_HRS_SEMANA * 24;   -- 1 Semestre tiene 24 semanas
		END IF;
		
		v_total_e := v_total_e + v_horas;
	END LOOP;
	CLOSE v_ext;
			
	-------------------------------------------------------------
	IF (v_total_e<>0) THEN
		UPDATE FCT_COLABXASIGNATURA
		SET
			tota_hr_ext = v_total_e
		WHERE
			codi_asignatura='EXT' 	AND
			codi_docente=_codi_docente		AND
			codi_semestre=_codi_semestre	AND
			codi_agno=_codi_agno;
		IF (NOT FOUND) THEN
			INSERT INTO FCT_COLABXASIGNATURA (
				codi_asignatura,
				codi_semestre,
				codi_agno,
				codi_docente,
				tota_hr_ext
			) VALUES (
				'EXT',
				_codi_semestre,
				_codi_agno,
				_codi_docente,
				v_total_e
			);
		END IF;
	END IF;
	-------------------------------------------------------------				

END;
$BODY$ LANGUAGE plpgsql;
