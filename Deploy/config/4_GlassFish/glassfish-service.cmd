@rem ---------------------------------------------------------------------------
@rem Copyright (c) OBCOM INGENIERIA S.A. (Chile). All rights reserved.
@rem
@rem All  rights to this product are owned by OBCOM INGENIERIA S.A. and may only
@rem be used under the terms of its associated license  document.  You  may  NOT
@rem copy,  modify, sublicense, or distribute this source file or portions of it
@rem unless previously authorized in writing by OBCOM  INGENIERIA  S.A.  In  any
@rem event, this notice and the above copyright must always be included verbatim
@rem with this file.
@rem ---------------------------------------------------------------------------

@rem create GlassFish Windows Service named "glassfish_4_1_13"
.\bin\asadmin.bat create-service --name glassfish_4_1_13

@rem change the display name to "GlassFish Server 4.1.13"
C:\Windows\System32\sc.exe config glassfish_4_1_13 DisplayName= "GlassFish Server 4.1.13"

@rem change the description to "GlassFish Server 4.1.13"
C:\Windows\System32\sc.exe description glassfish_4_1_13 "GlassFish Server 4.1.13"

@rem delete the GlassFish Windows Service "glassfish_4_1_13"
@rem C:\Windows\System32\sc.exe delete glassfish_4_1_13

:errors
@pause
