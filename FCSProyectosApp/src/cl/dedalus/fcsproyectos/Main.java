package cl.dedalus.fcsproyectos;

import java.awt.Desktop;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import cl.dedalus.fcsproyectos.ws.DocentesGetDocente;
import cl.dedalus.fcsproyectos.ws.DocentesGetResult;
import cl.dedalus.fcsproyectos.ws.FcsproyectosWeb;
import cl.dedalus.fcsproyectos.ws.FcsproyectosWebException;
import cl.dedalus.fcsproyectos.ws.FcsproyectosWebService;
import cl.dedalus.fcsproyectos.ws.ParamGetEstado;
import cl.dedalus.fcsproyectos.ws.ParamGetResult;
import cl.dedalus.fcsproyectos.ws.ParamGetTipofin;
import cl.dedalus.fcsproyectos.ws.ParamGetTipopry;
import cl.dedalus.fcsproyectos.ws.ParticipantesGetParti;
import cl.dedalus.fcsproyectos.ws.ParticipantesGetResult;
import cl.dedalus.fcsproyectos.ws.ProyectoGetResult;
import cl.dedalus.fcsproyectos.ws.ProyectosGetPry;
import cl.dedalus.fcsproyectos.ws.ProyectosGetResult;
import cl.obcom.desktopfx.core.DesktopTask;
import cl.obcom.desktopfx.jfx.Dialog;
import cl.obcom.desktopfx.jfx.DialogAction;
import cl.obcom.desktopfx.jfx.FxmlPane;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;

public class Main extends FxmlPane
{
    @FXML private TextField tx_codigo;
    @FXML private TextField tx_titulo;
    @FXML private TextField tx_filtro;
    @FXML private TextField tx_monto;
    @FXML private TextArea  tx_resumen;
    @FXML private DatePicker dp_fecha_ini_f;
    @FXML private DatePicker dp_fecha_fin_f;
    @FXML private DatePicker dp_fecha_ini_e;
    @FXML private DatePicker dp_fecha_fin_e;
    @FXML private Button bo_salir;
    @FXML private Button bo_mas;
    @FXML private Button bo_menos;
    @FXML private Button bo_ingresar;
    @FXML private Button bo_modificar;
    @FXML private Button bo_cierre;
    @FXML private Button bo_ins_part;
    @FXML private Button bo_responsable;
    @FXML private Button bo_coresponsable;
    @FXML private ComboBox<String> cb_estado;
    @FXML private ComboBox<String> cb_tipo;
    @FXML private ComboBox<String> cb_financiamiento;
    @FXML TableView<docentes> tb_docentes = new TableView<>();
    @FXML TableView<colabs> tb_colabs = new TableView<>();
    @FXML TableView<proyectos> tb_proyectos = new TableView<>();
    @FXML Label lb_prys;
    @FXML Label lb_facultad;
    @FXML Label lb_instituto;

    final FcsproyectosWeb port;

    final ObservableList<proyectos> dataProyecto = FXCollections.observableArrayList ();
    final ObservableList<docentes> dataDocente = FXCollections.observableArrayList ();
    final ObservableList<docentes> dataDocenteF = FXCollections.observableArrayList ();
    final ObservableList<colabs> dataColab = FXCollections.observableArrayList ();
    final ObservableList<String> dataEstado = FXCollections.observableArrayList ();
    final ObservableList<String> dataTipo = FXCollections.observableArrayList ();
    final ObservableList<String> dataFinanciamiento = FXCollections.observableArrayList ();

    public Main(final DesktopTask task) throws Exception
    {
        // Se carga fxml
        loadFxml();
        task.getStage().getScene().getStylesheets().add("resources/css/NuevoEstilo.css");

        final Callback<TableColumn<colabs,String>, TableCell<colabs,String>> cellFactory_2 =
            p -> new EditingCell_2();

        //Inicializar WS
        final FcsproyectosWebService service = new FcsproyectosWebService();
        port = service.getFcsproyectosWebPort();
        task.initWebServicePort(port,service,task.getCodeBase());

        //Init Grilla tb_proyectos
        final TableColumn<proyectos, String> tc_codigo = new TableColumn<>("Codigo");
        tc_codigo.setCellValueFactory(new PropertyValueFactory<proyectos, String>("codigo"));
        tb_proyectos.getColumns().add(tc_codigo);
        final TableColumn<proyectos, String> tc_titulo = new TableColumn<>("Titulo");
        tc_titulo.setCellValueFactory(new PropertyValueFactory<proyectos, String>("titulo"));
        tb_proyectos.getColumns().add(tc_titulo);
        final TableColumn<proyectos, String> tc_estado = new TableColumn<>("Estado");
        tc_estado.setCellValueFactory(new PropertyValueFactory<proyectos, String>("estado"));
        tb_proyectos.getColumns().add(tc_estado);
        tb_proyectos.setItems(dataProyecto);
        tc_codigo.prefWidthProperty().bind(tb_proyectos.widthProperty().multiply(0.25));
        tc_titulo.prefWidthProperty().bind(tb_proyectos.widthProperty().multiply(0.50));
        tc_estado.prefWidthProperty().bind(tb_proyectos.widthProperty().multiply(0.25));

        //Init Grilla tb_docentes
        final TableColumn<docentes, String> tc_codi_d = new TableColumn<>("Username");
        tc_codi_d.setCellValueFactory(new PropertyValueFactory<docentes, String>("codi_docente"));
        tb_docentes.getColumns().add(tc_codi_d);
        final TableColumn<docentes, String> tc_nomb_d = new TableColumn<>("Nombre");
        tc_nomb_d.setCellValueFactory(new PropertyValueFactory<docentes, String>("nomb_docente"));
        tb_docentes.getColumns().add(tc_nomb_d);
        tb_docentes.setItems(dataDocente);
        tc_codi_d.prefWidthProperty().bind(tb_docentes.widthProperty().multiply(0.30));
        tc_nomb_d.prefWidthProperty().bind(tb_docentes.widthProperty().multiply(0.65));

      //Init Grilla tb_colabs
        final TableColumn<colabs, String> tc_codi_p = new TableColumn<>("Username");
        tc_codi_p.setCellValueFactory(new PropertyValueFactory<colabs, String>("codi_colab"));
        tb_colabs.getColumns().add(tc_codi_p);
        final TableColumn<colabs, String> tc_nomb_p = new TableColumn<>("Nombre");
        tc_nomb_p.setCellValueFactory(new PropertyValueFactory<colabs, String>("nomb_colab"));
        tb_colabs.getColumns().add(tc_nomb_p);
        final TableColumn<colabs, String> tc_orig_p = new TableColumn<>("Orig");
        tc_orig_p.setCellValueFactory(new PropertyValueFactory<colabs, String>("orig_colab"));
        tb_colabs.getColumns().add(tc_orig_p);
        final TableColumn<colabs, String> tc_resp_p = new TableColumn<>("Resp");
        tc_resp_p.setCellValueFactory(new PropertyValueFactory<colabs, String>("flag_responsable"));
        tb_colabs.getColumns().add(tc_resp_p);
        final TableColumn<colabs, String> tc_sem_f = new TableColumn<>();  // El titulo va con el tooltip mas abajo.
        tc_sem_f.setCellValueFactory(new PropertyValueFactory<colabs, String>("cant_semanas_f"));
        tc_sem_f.setCellFactory(cellFactory_2);
        tc_sem_f.setOnEditCommit(
            t -> {
                final colabs tt = tb_colabs.getSelectionModel().getSelectedItem();
                final int v_tot = Integer.parseInt(t.getNewValue()) * Integer.parseInt(tt.getCant_hrsxsem_f());

                updateParticipante(t.getRowValue().getId() ,t.getNewValue(), t.getRowValue().getCant_hrsxsem_f()
                    ,Integer.toString(v_tot), t.getRowValue().getCant_semanas_e(),t.getRowValue().getCant_hrsxsem_e() );
                tt.setCant_semanas_f(t.getNewValue());
                tt.setCant_total_f(Integer.toString(v_tot));
            });
        tb_colabs.getColumns().add(tc_sem_f);

        final TableColumn<colabs, String> tc_hrs_f = new TableColumn<>();
        tc_hrs_f.setCellValueFactory(new PropertyValueFactory<colabs, String>("cant_hrsxsem_f"));
        tc_hrs_f.setCellFactory(cellFactory_2);
        tc_hrs_f.setOnEditCommit(
            t -> {
                final colabs tt = tb_colabs.getSelectionModel().getSelectedItem();
                final int v_tot = Integer.parseInt(tt.getCant_semanas_f()) * Integer.parseInt(t.getNewValue());

                updateParticipante(t.getRowValue().getId(), t.getRowValue().getCant_semanas_f()
                    ,t.getNewValue(),Integer.toString(v_tot), t.getRowValue().getCant_semanas_e(),t.getRowValue().getCant_hrsxsem_e() );
                tt.setCant_hrsxsem_f(t.getNewValue());
                tt.setCant_total_f(Integer.toString(v_tot));

            });
        tb_colabs.getColumns().add(tc_hrs_f);

        final TableColumn<colabs, String> tc_tot_f = new TableColumn<>();
        tc_tot_f.setCellValueFactory(new PropertyValueFactory<colabs, String>("cant_total_f"));
        tc_tot_f.setCellFactory(cellFactory_2);
        tc_tot_f.setOnEditCommit(
            t -> {
                final colabs tt = tb_colabs.getSelectionModel().getSelectedItem();
                //Si pone un valor en TotalF, entonces asume Sem F=1 y HorasSemF=TotalF
                updateParticipante(t.getRowValue().getId(), "1" ,t.getNewValue(),t.getNewValue()
                    ,t.getRowValue().getCant_semanas_e(),t.getRowValue().getCant_hrsxsem_e() );
                tt.setCant_semanas_f("1");
                tt.setCant_hrsxsem_f(t.getNewValue());
                tt.setCant_total_f(t.getNewValue());
            });
        tb_colabs.getColumns().add(tc_tot_f);

        final TableColumn<colabs, String> tc_sem_e = new TableColumn<>();
        tc_sem_e.setCellValueFactory(new PropertyValueFactory<colabs, String>("cant_semanas_e"));
        tc_sem_e.setCellFactory(cellFactory_2);
        tc_sem_e.setOnEditCommit(
            t -> {
                final colabs tt = tb_colabs.getSelectionModel().getSelectedItem();
                final int v_tot = Integer.parseInt(t.getNewValue()) * Integer.parseInt(tt.getCant_hrsxsem_e());

                updateParticipante(t.getRowValue().getId(), t.getRowValue().getCant_semanas_f()
                    , t.getRowValue().getCant_hrsxsem_f(),t.getRowValue().getCant_total_f()
                    ,t.getNewValue(),t.getRowValue().getCant_hrsxsem_e() );
                tt.setCant_semanas_e(t.getNewValue());
                tt.setCant_total_e(Integer.toString(v_tot));
            });
        tb_colabs.getColumns().add(tc_sem_e);

        final TableColumn<colabs, String> tc_hrs_e = new TableColumn<>();
        tc_hrs_e.setCellValueFactory(new PropertyValueFactory<colabs, String>("cant_hrsxsem_e"));
        tc_hrs_e.setCellFactory(cellFactory_2);
        tc_hrs_e.setOnEditCommit(
            t -> {
                final colabs tt = tb_colabs.getSelectionModel().getSelectedItem();
                final int v_tot = Integer.parseInt(tt.getCant_semanas_e()) * Integer.parseInt(t.getNewValue());

                updateParticipante(t.getRowValue().getId(),t.getRowValue().getCant_semanas_f()
                    , t.getRowValue().getCant_hrsxsem_f(),t.getRowValue().getCant_total_f()
                    ,t.getRowValue().getCant_semanas_e(),t.getNewValue() );
                tt.setCant_hrsxsem_e(t.getNewValue());
                tt.setCant_total_e(Integer.toString(v_tot));
            });
        tb_colabs.getColumns().add(tc_hrs_e);

        final TableColumn<colabs, String> tc_tot_e = new TableColumn<>();
        tc_tot_e.setCellValueFactory(new PropertyValueFactory<colabs, String>("cant_total_e"));
        tb_colabs.getColumns().add(tc_tot_e);
        tb_colabs.setItems(dataColab);
        tb_colabs.setEditable(true);
        tc_codi_p.prefWidthProperty().bind(tb_colabs.widthProperty().multiply(0.13));
        tc_nomb_p.prefWidthProperty().bind(tb_colabs.widthProperty().multiply(0.20));
        tc_orig_p.prefWidthProperty().bind(tb_colabs.widthProperty().multiply(0.08));
        tc_resp_p.prefWidthProperty().bind(tb_colabs.widthProperty().multiply(0.12));
        tc_sem_f.prefWidthProperty().bind(tb_colabs.widthProperty().multiply(0.08));
        tc_hrs_f.prefWidthProperty().bind(tb_colabs.widthProperty().multiply(0.07));
        tc_tot_f.prefWidthProperty().bind(tb_colabs.widthProperty().multiply(0.07));
        tc_sem_e.prefWidthProperty().bind(tb_colabs.widthProperty().multiply(0.08));
        tc_hrs_e.prefWidthProperty().bind(tb_colabs.widthProperty().multiply(0.07));
        tc_tot_e.prefWidthProperty().bind(tb_colabs.widthProperty().multiply(0.07));
        final Label lb_sem_f = new Label("Sem F");
        lb_sem_f.setTooltip(new Tooltip("Numero de semanas para la formulacion."));
        tc_sem_f.setGraphic(lb_sem_f);
        final Label lb_hrs_f = new Label("Hrs F");
        lb_hrs_f.setTooltip(new Tooltip("Numero de horas por semana\npara la formulacion."));
        tc_hrs_f.setGraphic(lb_hrs_f);
        final Label lb_tot_f = new Label("Tot F");
        lb_tot_f.setTooltip(new Tooltip("Numero total de horas\npara la formulacion.(Sem F * Hrs F)"));
        tc_tot_f.setGraphic(lb_tot_f);
        final Label lb_sem_e = new Label("Sem C");
        lb_sem_e.setTooltip(new Tooltip("Numero de semanas comprometidas."));
        tc_sem_e.setGraphic(lb_sem_e);
        final Label lb_hrs_e = new Label("Hrs C");
        lb_hrs_e.setTooltip(new Tooltip("Numero de horas por semana comprometidas."));
        tc_hrs_e.setGraphic(lb_hrs_e);
        final Label lb_tot_e = new Label("Tot C");
        lb_tot_e.setTooltip(new Tooltip("Numero total de horas comprometidas.(Sem C * Hrs C)"));
        tc_tot_e.setGraphic(lb_tot_e);

        //Click on tb_proyectos
        tb_proyectos.setOnMouseClicked(event -> {
            final proyectos t = dataProyecto.get(tb_proyectos.getSelectionModel().getSelectedIndex());
            cargaProyecto(t.getId());
            cargaParticipantes(t.getId());
            //dataColab.clear();
        });

        // escucha los cambios en tx_filtro para articulos
        tx_filtro.textProperty().addListener((ChangeListener<String>) (observable, oldValue, newValue) -> {
            dataDocenteF.clear();
            for (final docentes p : dataDocente) {
                if (matchesFilter(p)) {  dataDocenteF.add(p);  }
            }
           // tb_articulo.setItems(dataArticuloF);   // copio a la tabla, datos filtrados
            if (dataDocenteF.size()!=0) {
                tb_docentes.setItems(dataDocenteF);   // copio a la tabla, datos filtrados
            } else {
                Toolkit.getDefaultToolkit().beep();
                tx_filtro.setText(oldValue);
            }
         });
        
        dp_fecha_ini_f.setValue(LocalDate.now());
        dp_fecha_fin_f.setValue(LocalDate.now());
        

        cargaDocentes(task.getUser().getCode().trim());
        cargaParams();
        cargaProyectos(task.getUser().getCode().trim());
       
        bo_ingresar.setOnAction(evt -> ingresarProyecto(task));
        bo_salir.setOnAction(evt -> task.terminate(true));
        bo_modificar.setOnAction(evt -> updateProyecto(task));
        bo_responsable.setOnAction(evt -> updateParticipanteResp("RESP"));
        bo_coresponsable.setOnAction(evt -> updateParticipanteResp("CO-RESP"));
        bo_modificar.setOnAction(evt -> updateProyecto(task));
        bo_mas.setOnAction(evt -> ingresarParticipante());
        bo_menos.setOnAction(evt -> deleteParticipante());
        bo_ins_part.setOnAction(evt -> AuditShow(task));

    }
    
    public void updateParticipanteResp(String v_resp){
        DialogAction D;
        // si no existe un colabaraodr seleccionado entonces -> exit
        if 	(tb_colabs.getSelectionModel().getSelectedIndex() < 0) { return; } 
        
        final colabs tt = tb_colabs.getSelectionModel().getSelectedItem();
        D = Dialog.showConfirm(this, "Desea asignar responsabilidad a:"+tt.getCodi_colab());
        if ( D.toString() == "YES" ) {           
            try {
                port.responsableUpd(tt.getId(), v_resp);
                final proyectos t = dataProyecto.get(tb_proyectos.getSelectionModel().getSelectedIndex());              
                cargaParticipantes(t.getId());
            } catch (final FcsproyectosWebException e) {
                Dialog.showError(this, e);
            }
        }
    }
    
    boolean matchesFilter(final docentes t) {
        final String filterString = tx_filtro.getText();
        if (filterString == null || filterString.isEmpty()) {
            // No filter --> Add all.
            return true;
        }
        final String lowerCaseFilterString = filterString.toLowerCase();
        
        if (t.getCodi_docente().toLowerCase().indexOf(lowerCaseFilterString) != -1) {
            return true;
        } else if (t.getNomb_docente().toString().toLowerCase().indexOf(lowerCaseFilterString) != -1) {
            return true;
        } 
        return false; // Does not match
    }

    public void AuditShow(final DesktopTask T){
        final ANewPersonData personData=new ANewPersonData();
        if (ANewPersonDialog.show(T, personData)) {
            ingresarColabExterno(T.getUser().getCode().trim(), personData.getUserP(), personData.getNombresP() , personData.getApellidosP()
            		, personData.getProfesionP() , personData.getRutP(), personData.getMailP(), personData.getFonoP());
            cargaDocentes(T.getUser().getCode().trim());
        }
    }

    public void ingresarColabExterno(String v_usuario, final String v_username, final String v_nombres, final String v_apellidos, final String v_profesion,
    				String v_rut, String v_mail, String v_fono){
        //v_usuario es para obtener facultad e instituto
    	try {
            port.colabexternoSet(v_usuario, v_username, v_nombres, v_apellidos, v_profesion, v_rut, v_mail, v_fono);
        } catch (final FcsproyectosWebException e) {
            Dialog.showError(this,e);
        }
    }

    public void deleteParticipante(){
        final colabs t = dataColab.get(tb_colabs.getSelectionModel().getSelectedIndex());
        final proyectos tt = dataProyecto.get(tb_proyectos.getSelectionModel().getSelectedIndex());
        DialogAction D;

        D = Dialog.showConfirm(this, "Desea eliminar este participante: "+t.getCodi_colab()+" ?");
        if ( D.toString() == "YES" ) {
            try {
                port.participanteDel(t.getId());
                cargaParticipantes(tt.getId());
            } catch (final FcsproyectosWebException e) {
               Dialog.showError(this, e);
            }
        }
    }

    public void ingresarParticipante(){
        final proyectos tt = dataProyecto.get(tb_proyectos.getSelectionModel().getSelectedIndex());
        final docentes dd = dataDocenteF.get(tb_docentes.getSelectionModel().getSelectedIndex());
       // final ParticipanteSetResult r1 = null;

        try {
            port.participanteSet(tt.getId(), dd.getCodi_docente(), dd.getOrig_docente());
            cargaParticipantes(tt.getId());
        } catch (final FcsproyectosWebException e) {
            Dialog.showError(this,e);
        }

    }

    public void cargaDocentes(String v_user){
        dataDocente.clear();
        dataDocenteF.clear();
        DocentesGetResult res = null;
        try {
            res = port.docentesGet(v_user);
            for ( final DocentesGetDocente t : res.getDocentes()) {
                dataDocente.add(new docentes( t.getCodiFuncionario(), t.getDescNombre(), t.getOrigFuncionario() ));
            }
            tb_docentes.setItems(dataDocente);
            dataDocenteF.addAll(dataDocente);
        } catch (final FcsproyectosWebException e) {
            Dialog.showError(this, e);
        }
    }

    public void cargaProyecto(final Long v_id){
        ProyectoGetResult res = null;
        try {
            res = port.proyectoGet(v_id);
            final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            final LocalDate dateIniF = LocalDate.parse(res.getFechIniF(), formatter);
            final LocalDate dateFinF = LocalDate.parse(res.getFechFinF(), formatter);
            final LocalDate dateIniE = LocalDate.parse(res.getFechIniE(), formatter);
            final LocalDate dateFinE = LocalDate.parse(res.getFechFinE(), formatter);

            tx_codigo.setText(res.getCodiProyecto());
            tx_titulo.setText(res.getDescTitulo());
            tx_resumen.setText(res.getDescResumen());
            tx_monto.setText(res.getCantMonto());
            dp_fecha_ini_f.setValue(dateIniF);
            dp_fecha_fin_f.setValue(dateFinF);

            if (res.getFechIniE().equals("1900-01-01")){
                dp_fecha_ini_e.setValue(null);
            }else{
                dp_fecha_ini_e.setValue(dateIniE);
            }
            if (res.getFechFinE().equals("1900-01-01")){
                dp_fecha_fin_e.setValue(null);
            }else{
                dp_fecha_fin_e.setValue(dateFinE);
            }
            cb_estado.setValue(res.getDescEstado());
            cb_tipo.setValue(res.getDescTipop());
            cb_financiamiento.setValue(res.getDescFinancia());

        } catch (final FcsproyectosWebException e) {
            Dialog.showError(this, e);
        }
    }

    public void cargaParticipantes(final Long v_proyecto){
        dataColab.clear();
        ParticipantesGetResult res = null;
        try {
            res = port.participantesGet(v_proyecto);
            for ( final ParticipantesGetParti t : res.getPartis()) {
                dataColab.add(new colabs( t.getIdParticipante(),t.getCodiParticipante(),t.getNombParticipante()
                	,t.getCodiOrigen() , t.getFlagResponsable()
                    ,t.getCantSemForm(), t.getCantHorForm(),t.getCantTotForm()
                    ,t.getCantSemEjec(), t.getCantHorEjec(),t.getCantTotEjec()));
            }
            tb_colabs.setItems(dataColab);
        } catch (final FcsproyectosWebException e) {
            Dialog.showError(this, e);
        }
    }

    public void updateParticipante(final Long v_id, final String v_sem_f, final String v_hor_f
                                   , final String v_total_f, final String v_sem_e, final String v_hor_e){
        try {
            port.participanteUpd(v_id, v_sem_f, v_hor_f, v_total_f, v_sem_e, v_hor_e);
        } catch (final FcsproyectosWebException e) {
            Dialog.showError(this,e);
        }
    }

    public void openFile(final File myfile) {
        try {
            Desktop.getDesktop().open(myfile);
        } catch (final IOException ex) {
           Dialog.showError(this, ex);
        }
    }

    public void cargaProyectos(final String v_codi_docente){
        dataProyecto.clear();
        ProyectosGetResult resultado = null;
        try {       	
            resultado = port.proyectosGet(v_codi_docente);
            lb_prys.setText("Username:"+v_codi_docente);
            lb_facultad.setText("Facultad:"+resultado.getDescFacultad());
            lb_instituto.setText("Instituto:"+resultado.getDescInstituto());
            
            for ( final ProyectosGetPry t : resultado.getPrys()) {
                dataProyecto.add(new proyectos(t.getId(), t.getCodiProyecto(), t.getDescTitulo(), t.getDescEstado() ));
            }
            tb_proyectos.setItems(dataProyecto);
        } catch (final FcsproyectosWebException e) {
            Dialog.showError(this, e);
        }
    }

    public void updateProyecto(final DesktopTask T){
        String v_codi_pro, v_codi_doc, v_monto, v_desc_tit, v_desc_res, v_fech_ini_f, v_fech_fin_f, v_fech_ini_e, v_fech_fin_e;
        Integer v_iden_est,v_tipo, v_fin;
        DialogAction D;

        D = Dialog.showConfirm(this, "Desea actualizar este Proyecto?");
        if ( D.toString() == "YES" ) {
            final proyectos tt = dataProyecto.get(tb_proyectos.getSelectionModel().getSelectedIndex());
            v_codi_pro=tx_codigo.getText().trim();
            v_codi_doc= T.getUser().getCode();
            v_desc_tit=tx_titulo.getText().trim();
            v_desc_res=tx_resumen.getText().trim();
            v_fech_ini_f=dp_fecha_ini_f.getValue().toString();
            v_fech_fin_f=dp_fecha_fin_f.getValue().toString();
            v_monto=tx_monto.getText().trim();
            if (dp_fecha_ini_e.getValue() != null){
                v_fech_ini_e=dp_fecha_ini_e.getValue().toString();
            }else{
                v_fech_ini_e="1900-01-01";
            }
            if (dp_fecha_fin_e.getValue() != null){
                v_fech_fin_e=dp_fecha_fin_e.getValue().toString();
            }else{
                v_fech_fin_e="1900-01-01";
            }

            final int k = cb_estado.getSelectionModel().getSelectedItem().indexOf(" ");
            v_iden_est = Integer.parseInt(cb_estado.getSelectionModel().getSelectedItem().substring(0, k));
            final int l = cb_tipo.getSelectionModel().getSelectedItem().indexOf(" ");
            v_tipo = Integer.parseInt(cb_tipo.getSelectionModel().getSelectedItem().substring(0, l));
            final int m = cb_financiamiento.getSelectionModel().getSelectedItem().indexOf(" ");
            v_fin = Integer.parseInt(cb_financiamiento.getSelectionModel().getSelectedItem().substring(0, m));

            try {
                port.proyectoUpd(tt.getId(), v_codi_pro, v_codi_doc, v_desc_tit, v_desc_res, v_fech_ini_f, v_fech_fin_f
                    , v_fech_ini_e, v_fech_fin_e, v_iden_est, v_tipo, v_fin, v_monto);
                Dialog.showMessage(this, "Proyecto actualizado correctamente.");
                cargaProyectos(T.getUser().getCode().trim());
                //dataDoc.clear();
                //dataColab.clear();
                limpiarCampos();
            } catch (final FcsproyectosWebException e) {
                Dialog.showError(this, e);
            }
        }
    }

    public void ingresarProyecto(final DesktopTask T){
        String v_codi_pro, v_codi_doc, v_desc_tit, v_desc_res, v_fech_ini_f, v_fech_fin_f;
        String v_fech_ini_e,v_fech_fin_e,v_monto;
        Integer v_iden_est, v_fin, v_tipo;
        DialogAction D;

        D = Dialog.showConfirm(this, "Desea ingresar este nuevo Proyecto. ?");
        if ( D.toString() == "YES" ) {
            v_codi_pro=tx_codigo.getText().trim();
            v_codi_doc= T.getUser().getCode();
            v_desc_tit=tx_titulo.getText().trim();
            v_desc_res=tx_resumen.getText().trim();
            v_monto=tx_monto.getText().trim();
            v_fech_ini_f=dp_fecha_ini_f.getValue().toString();
            v_fech_fin_f=dp_fecha_fin_f.getValue().toString();

            
            if (dp_fecha_ini_e.getValue()!= null){
                v_fech_ini_e=dp_fecha_ini_e.getValue().toString();
            }else{
                v_fech_ini_e="1900-01-01";
            }
            if (dp_fecha_fin_e.getValue() != null){
                v_fech_fin_e=dp_fecha_fin_e.getValue().toString();
            }else{
                v_fech_fin_e="1900-01-01";
            }
            
            final int k = cb_estado.getSelectionModel().getSelectedItem().indexOf(" ");
            v_iden_est = Integer.parseInt(cb_estado.getSelectionModel().getSelectedItem().substring(0, k));
            final int l = cb_tipo.getSelectionModel().getSelectedItem().indexOf(" ");
            v_tipo = Integer.parseInt(cb_tipo.getSelectionModel().getSelectedItem().substring(0, l));
            final int m = cb_financiamiento.getSelectionModel().getSelectedItem().indexOf(" ");
            v_fin = Integer.parseInt(cb_financiamiento.getSelectionModel().getSelectedItem().substring(0, m));

            try {
                port.proyectoSet(v_codi_pro, v_codi_doc, v_desc_tit, v_desc_res, v_fech_ini_f, v_fech_fin_f
                        , v_fech_ini_e, v_fech_fin_e, v_iden_est, v_tipo, v_fin, v_monto);
                Dialog.showMessage(this, "Proyecto ingresado correctamente.");
                cargaProyectos(T.getUser().getCode().trim());
               // dataDoc.clear();
               // dataColab.clear();
                limpiarCampos();
            } catch (final FcsproyectosWebException e) {
                Dialog.showError(this, e);
            }
        }
    }

    public void limpiarCampos(){
        tx_codigo.setText("");
        tx_titulo.setText("");
        tx_resumen.setText("");
        tx_filtro.setText("");
        dp_fecha_ini_f.setValue(LocalDate.now());
        dp_fecha_fin_f.setValue(LocalDate.now());
        dp_fecha_ini_e.setValue(null);
        dp_fecha_fin_e.setValue(null);
        cb_estado.getSelectionModel().select(0); // Set valor por defecto
        cb_tipo.getSelectionModel().select(0); // Set valor por defecto
        cb_financiamiento.getSelectionModel().select(0); // Set valor por defecto
    }

    public void cargaParams(){
        dataEstado.clear();dataTipo.clear();dataFinanciamiento.clear();
        ParamGetResult r = null;
        try {
            r=port.paramGet();
            // Cargo los estaodos del proyecto
            for (final ParamGetEstado p : r.getEstados()){
                dataEstado.add(p.getEstado());
            }
            cb_estado.setItems(dataEstado);
            cb_estado.getSelectionModel().select(0);  // Set valor por defecto

            // Cargo los tipos del proyecto
            for (final ParamGetTipopry p : r.getTipoprys()){
                dataTipo.add(p.getTipoproyecto());
            }
            cb_tipo.setItems(dataTipo);
            cb_tipo.getSelectionModel().select(0);  // Set valor por defecto

            // Cargo financiamientos del proyecto
            for (final ParamGetTipofin p : r.getTipofins()){
                dataFinanciamiento.add(p.getTipofinancia());
            }
            cb_financiamiento.setItems(dataFinanciamiento);
            cb_financiamiento.getSelectionModel().select(0);  // Set valor por defecto

        } catch (final Exception e1) {
            Dialog.showError(this, e1);
        }
    }

} // Main - FXMPane
