package cl.dedalus.fcsproyectos;

import javafx.beans.property.SimpleStringProperty;

public class docs{
    private final   SimpleStringProperty id;
    private final   SimpleStringProperty fecha;
    private final   SimpleStringProperty titulo;
    private final   SimpleStringProperty nombre;

    public docs(final String id, final String fecha, final String titulo, final String nombre){
        this.id = new SimpleStringProperty(id);
        this.fecha = new SimpleStringProperty(fecha);
        this.titulo = new SimpleStringProperty(titulo);
        this.nombre = new SimpleStringProperty(nombre);
    }

    public String getId() {
        return id.get();
    }

    public String getFecha() {
        return fecha.get();
    }
    public void setFecha(final String ifecha) {
        fecha.set(ifecha);
    }

    public String getTitulo() {
        return titulo.get();
    }
    public void setTitulo(final String ititulo) {
        titulo.set(ititulo);
    }

    public String getNombre() {
        return nombre.get();
    }

}
