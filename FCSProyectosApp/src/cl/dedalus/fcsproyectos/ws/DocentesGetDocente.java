
package cl.dedalus.fcsproyectos.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DocentesGetDocente complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DocentesGetDocente">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codiFuncionario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descNombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="origFuncionario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocentesGetDocente", propOrder = {
    "codiFuncionario",
    "descNombre",
    "origFuncionario"
})
public class DocentesGetDocente {

    protected String codiFuncionario;
    protected String descNombre;
    protected String origFuncionario;

    /**
     * Obtiene el valor de la propiedad codiFuncionario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiFuncionario() {
        return codiFuncionario;
    }

    /**
     * Define el valor de la propiedad codiFuncionario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiFuncionario(String value) {
        this.codiFuncionario = value;
    }

    /**
     * Obtiene el valor de la propiedad descNombre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescNombre() {
        return descNombre;
    }

    /**
     * Define el valor de la propiedad descNombre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescNombre(String value) {
        this.descNombre = value;
    }

    /**
     * Obtiene el valor de la propiedad origFuncionario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrigFuncionario() {
        return origFuncionario;
    }

    /**
     * Define el valor de la propiedad origFuncionario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrigFuncionario(String value) {
        this.origFuncionario = value;
    }

}
