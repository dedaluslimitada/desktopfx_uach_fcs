
package cl.dedalus.fcsproyectos.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ProyectoGetResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ProyectoGetResult">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.fcsproyectos.dedalus.cl/}ProcedureResult">
 *       &lt;sequence>
 *         &lt;element name="cantMonto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codiProyecto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descEstado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descFinancia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descResumen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descTipop" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descTitulo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechFinE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechFinF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechIniE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechIniF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProyectoGetResult", propOrder = {
    "cantMonto",
    "codiProyecto",
    "descEstado",
    "descFinancia",
    "descResumen",
    "descTipop",
    "descTitulo",
    "fechFinE",
    "fechFinF",
    "fechIniE",
    "fechIniF"
})
public class ProyectoGetResult
    extends ProcedureResult
{

    protected String cantMonto;
    protected String codiProyecto;
    protected String descEstado;
    protected String descFinancia;
    protected String descResumen;
    protected String descTipop;
    protected String descTitulo;
    protected String fechFinE;
    protected String fechFinF;
    protected String fechIniE;
    protected String fechIniF;

    /**
     * Obtiene el valor de la propiedad cantMonto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantMonto() {
        return cantMonto;
    }

    /**
     * Define el valor de la propiedad cantMonto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantMonto(String value) {
        this.cantMonto = value;
    }

    /**
     * Obtiene el valor de la propiedad codiProyecto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiProyecto() {
        return codiProyecto;
    }

    /**
     * Define el valor de la propiedad codiProyecto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiProyecto(String value) {
        this.codiProyecto = value;
    }

    /**
     * Obtiene el valor de la propiedad descEstado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescEstado() {
        return descEstado;
    }

    /**
     * Define el valor de la propiedad descEstado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescEstado(String value) {
        this.descEstado = value;
    }

    /**
     * Obtiene el valor de la propiedad descFinancia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescFinancia() {
        return descFinancia;
    }

    /**
     * Define el valor de la propiedad descFinancia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescFinancia(String value) {
        this.descFinancia = value;
    }

    /**
     * Obtiene el valor de la propiedad descResumen.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescResumen() {
        return descResumen;
    }

    /**
     * Define el valor de la propiedad descResumen.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescResumen(String value) {
        this.descResumen = value;
    }

    /**
     * Obtiene el valor de la propiedad descTipop.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescTipop() {
        return descTipop;
    }

    /**
     * Define el valor de la propiedad descTipop.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescTipop(String value) {
        this.descTipop = value;
    }

    /**
     * Obtiene el valor de la propiedad descTitulo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescTitulo() {
        return descTitulo;
    }

    /**
     * Define el valor de la propiedad descTitulo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescTitulo(String value) {
        this.descTitulo = value;
    }

    /**
     * Obtiene el valor de la propiedad fechFinE.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechFinE() {
        return fechFinE;
    }

    /**
     * Define el valor de la propiedad fechFinE.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechFinE(String value) {
        this.fechFinE = value;
    }

    /**
     * Obtiene el valor de la propiedad fechFinF.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechFinF() {
        return fechFinF;
    }

    /**
     * Define el valor de la propiedad fechFinF.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechFinF(String value) {
        this.fechFinF = value;
    }

    /**
     * Obtiene el valor de la propiedad fechIniE.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechIniE() {
        return fechIniE;
    }

    /**
     * Define el valor de la propiedad fechIniE.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechIniE(String value) {
        this.fechIniE = value;
    }

    /**
     * Obtiene el valor de la propiedad fechIniF.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechIniF() {
        return fechIniF;
    }

    /**
     * Define el valor de la propiedad fechIniF.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechIniF(String value) {
        this.fechIniF = value;
    }

}
