
package cl.dedalus.fcsproyectos.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para participantesGet complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="participantesGet">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idProyecto" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "participantesGet", propOrder = {
    "idProyecto"
})
public class ParticipantesGet {

    protected long idProyecto;

    /**
     * Obtiene el valor de la propiedad idProyecto.
     * 
     */
    public long getIdProyecto() {
        return idProyecto;
    }

    /**
     * Define el valor de la propiedad idProyecto.
     * 
     */
    public void setIdProyecto(long value) {
        this.idProyecto = value;
    }

}
