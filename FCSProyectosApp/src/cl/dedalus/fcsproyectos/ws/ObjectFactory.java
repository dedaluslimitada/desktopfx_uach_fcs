
package cl.dedalus.fcsproyectos.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cl.dedalus.fcsproyectos.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ParticipantesGet_QNAME = new QName("http://ws.fcsproyectos.dedalus.cl/", "participantesGet");
    private final static QName _ParticipanteSet_QNAME = new QName("http://ws.fcsproyectos.dedalus.cl/", "participanteSet");
    private final static QName _ProyectoUpd_QNAME = new QName("http://ws.fcsproyectos.dedalus.cl/", "proyectoUpd");
    private final static QName _DocentesGetResponse_QNAME = new QName("http://ws.fcsproyectos.dedalus.cl/", "docentesGetResponse");
    private final static QName _ParamGet_QNAME = new QName("http://ws.fcsproyectos.dedalus.cl/", "paramGet");
    private final static QName _ParticipanteUpd_QNAME = new QName("http://ws.fcsproyectos.dedalus.cl/", "participanteUpd");
    private final static QName _ResponsableUpd_QNAME = new QName("http://ws.fcsproyectos.dedalus.cl/", "responsableUpd");
    private final static QName _ParticipanteDelResponse_QNAME = new QName("http://ws.fcsproyectos.dedalus.cl/", "participanteDelResponse");
    private final static QName _ParamGetResponse_QNAME = new QName("http://ws.fcsproyectos.dedalus.cl/", "paramGetResponse");
    private final static QName _ParticipantesGetResponse_QNAME = new QName("http://ws.fcsproyectos.dedalus.cl/", "participantesGetResponse");
    private final static QName _FaultInfo_QNAME = new QName("http://ws.fcsproyectos.dedalus.cl/", "faultInfo");
    private final static QName _ProyectosGet_QNAME = new QName("http://ws.fcsproyectos.dedalus.cl/", "proyectosGet");
    private final static QName _ColabexternoSetResponse_QNAME = new QName("http://ws.fcsproyectos.dedalus.cl/", "colabexternoSetResponse");
    private final static QName _ParticipanteUpdResponse_QNAME = new QName("http://ws.fcsproyectos.dedalus.cl/", "participanteUpdResponse");
    private final static QName _ResponsableUpdResponse_QNAME = new QName("http://ws.fcsproyectos.dedalus.cl/", "responsableUpdResponse");
    private final static QName _ProyectosGetResponse_QNAME = new QName("http://ws.fcsproyectos.dedalus.cl/", "proyectosGetResponse");
    private final static QName _ProyectoSetResponse_QNAME = new QName("http://ws.fcsproyectos.dedalus.cl/", "proyectoSetResponse");
    private final static QName _ProyectoUpdResponse_QNAME = new QName("http://ws.fcsproyectos.dedalus.cl/", "proyectoUpdResponse");
    private final static QName _ParticipanteDel_QNAME = new QName("http://ws.fcsproyectos.dedalus.cl/", "participanteDel");
    private final static QName _DocentesGet_QNAME = new QName("http://ws.fcsproyectos.dedalus.cl/", "docentesGet");
    private final static QName _ColabexternoSet_QNAME = new QName("http://ws.fcsproyectos.dedalus.cl/", "colabexternoSet");
    private final static QName _ProyectoGet_QNAME = new QName("http://ws.fcsproyectos.dedalus.cl/", "proyectoGet");
    private final static QName _ProyectoSet_QNAME = new QName("http://ws.fcsproyectos.dedalus.cl/", "proyectoSet");
    private final static QName _ProyectoGetResponse_QNAME = new QName("http://ws.fcsproyectos.dedalus.cl/", "proyectoGetResponse");
    private final static QName _ParticipanteSetResponse_QNAME = new QName("http://ws.fcsproyectos.dedalus.cl/", "participanteSetResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cl.dedalus.fcsproyectos.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ResponsableUpdResponse }
     * 
     */
    public ResponsableUpdResponse createResponsableUpdResponse() {
        return new ResponsableUpdResponse();
    }

    /**
     * Create an instance of {@link ParticipanteUpdResponse }
     * 
     */
    public ParticipanteUpdResponse createParticipanteUpdResponse() {
        return new ParticipanteUpdResponse();
    }

    /**
     * Create an instance of {@link ColabexternoSetResponse }
     * 
     */
    public ColabexternoSetResponse createColabexternoSetResponse() {
        return new ColabexternoSetResponse();
    }

    /**
     * Create an instance of {@link ProyectosGet }
     * 
     */
    public ProyectosGet createProyectosGet() {
        return new ProyectosGet();
    }

    /**
     * Create an instance of {@link ParamGetResponse }
     * 
     */
    public ParamGetResponse createParamGetResponse() {
        return new ParamGetResponse();
    }

    /**
     * Create an instance of {@link ParticipantesGetResponse }
     * 
     */
    public ParticipantesGetResponse createParticipantesGetResponse() {
        return new ParticipantesGetResponse();
    }

    /**
     * Create an instance of {@link ParticipanteDelResponse }
     * 
     */
    public ParticipanteDelResponse createParticipanteDelResponse() {
        return new ParticipanteDelResponse();
    }

    /**
     * Create an instance of {@link DocentesGetResponse }
     * 
     */
    public DocentesGetResponse createDocentesGetResponse() {
        return new DocentesGetResponse();
    }

    /**
     * Create an instance of {@link ParamGet }
     * 
     */
    public ParamGet createParamGet() {
        return new ParamGet();
    }

    /**
     * Create an instance of {@link ParticipanteUpd }
     * 
     */
    public ParticipanteUpd createParticipanteUpd() {
        return new ParticipanteUpd();
    }

    /**
     * Create an instance of {@link ResponsableUpd }
     * 
     */
    public ResponsableUpd createResponsableUpd() {
        return new ResponsableUpd();
    }

    /**
     * Create an instance of {@link ParticipanteSet }
     * 
     */
    public ParticipanteSet createParticipanteSet() {
        return new ParticipanteSet();
    }

    /**
     * Create an instance of {@link ProyectoUpd }
     * 
     */
    public ProyectoUpd createProyectoUpd() {
        return new ProyectoUpd();
    }

    /**
     * Create an instance of {@link ParticipantesGet }
     * 
     */
    public ParticipantesGet createParticipantesGet() {
        return new ParticipantesGet();
    }

    /**
     * Create an instance of {@link ParticipanteSetResponse }
     * 
     */
    public ParticipanteSetResponse createParticipanteSetResponse() {
        return new ParticipanteSetResponse();
    }

    /**
     * Create an instance of {@link ProyectoGetResponse }
     * 
     */
    public ProyectoGetResponse createProyectoGetResponse() {
        return new ProyectoGetResponse();
    }

    /**
     * Create an instance of {@link ProyectoSet }
     * 
     */
    public ProyectoSet createProyectoSet() {
        return new ProyectoSet();
    }

    /**
     * Create an instance of {@link ColabexternoSet }
     * 
     */
    public ColabexternoSet createColabexternoSet() {
        return new ColabexternoSet();
    }

    /**
     * Create an instance of {@link ProyectoGet }
     * 
     */
    public ProyectoGet createProyectoGet() {
        return new ProyectoGet();
    }

    /**
     * Create an instance of {@link DocentesGet }
     * 
     */
    public DocentesGet createDocentesGet() {
        return new DocentesGet();
    }

    /**
     * Create an instance of {@link ParticipanteDel }
     * 
     */
    public ParticipanteDel createParticipanteDel() {
        return new ParticipanteDel();
    }

    /**
     * Create an instance of {@link ProyectoUpdResponse }
     * 
     */
    public ProyectoUpdResponse createProyectoUpdResponse() {
        return new ProyectoUpdResponse();
    }

    /**
     * Create an instance of {@link ProyectoSetResponse }
     * 
     */
    public ProyectoSetResponse createProyectoSetResponse() {
        return new ProyectoSetResponse();
    }

    /**
     * Create an instance of {@link ProyectosGetResponse }
     * 
     */
    public ProyectosGetResponse createProyectosGetResponse() {
        return new ProyectosGetResponse();
    }

    /**
     * Create an instance of {@link DocentesGetResult }
     * 
     */
    public DocentesGetResult createDocentesGetResult() {
        return new DocentesGetResult();
    }

    /**
     * Create an instance of {@link ParamGetEstado }
     * 
     */
    public ParamGetEstado createParamGetEstado() {
        return new ParamGetEstado();
    }

    /**
     * Create an instance of {@link DocentesGetDocente }
     * 
     */
    public DocentesGetDocente createDocentesGetDocente() {
        return new DocentesGetDocente();
    }

    /**
     * Create an instance of {@link ParamGetTipofin }
     * 
     */
    public ParamGetTipofin createParamGetTipofin() {
        return new ParamGetTipofin();
    }

    /**
     * Create an instance of {@link ParticipantesGetResult }
     * 
     */
    public ParticipantesGetResult createParticipantesGetResult() {
        return new ParticipantesGetResult();
    }

    /**
     * Create an instance of {@link ProyectoSetResult }
     * 
     */
    public ProyectoSetResult createProyectoSetResult() {
        return new ProyectoSetResult();
    }

    /**
     * Create an instance of {@link ParticipantesGetParti }
     * 
     */
    public ParticipantesGetParti createParticipantesGetParti() {
        return new ParticipantesGetParti();
    }

    /**
     * Create an instance of {@link ParticipanteDelResult }
     * 
     */
    public ParticipanteDelResult createParticipanteDelResult() {
        return new ParticipanteDelResult();
    }

    /**
     * Create an instance of {@link ProyectoGetResult }
     * 
     */
    public ProyectoGetResult createProyectoGetResult() {
        return new ProyectoGetResult();
    }

    /**
     * Create an instance of {@link ParticipanteSetResult }
     * 
     */
    public ParticipanteSetResult createParticipanteSetResult() {
        return new ParticipanteSetResult();
    }

    /**
     * Create an instance of {@link ProyectosGetResult }
     * 
     */
    public ProyectosGetResult createProyectosGetResult() {
        return new ProyectosGetResult();
    }

    /**
     * Create an instance of {@link ResponsableUpdResult }
     * 
     */
    public ResponsableUpdResult createResponsableUpdResult() {
        return new ResponsableUpdResult();
    }

    /**
     * Create an instance of {@link ColabexternoSetResult }
     * 
     */
    public ColabexternoSetResult createColabexternoSetResult() {
        return new ColabexternoSetResult();
    }

    /**
     * Create an instance of {@link ParamGetResult }
     * 
     */
    public ParamGetResult createParamGetResult() {
        return new ParamGetResult();
    }

    /**
     * Create an instance of {@link ProyectosGetPry }
     * 
     */
    public ProyectosGetPry createProyectosGetPry() {
        return new ProyectosGetPry();
    }

    /**
     * Create an instance of {@link ParamGetTipopry }
     * 
     */
    public ParamGetTipopry createParamGetTipopry() {
        return new ParamGetTipopry();
    }

    /**
     * Create an instance of {@link ParticipanteUpdResult }
     * 
     */
    public ParticipanteUpdResult createParticipanteUpdResult() {
        return new ParticipanteUpdResult();
    }

    /**
     * Create an instance of {@link ProyectoUpdResult }
     * 
     */
    public ProyectoUpdResult createProyectoUpdResult() {
        return new ProyectoUpdResult();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ParticipantesGet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsproyectos.dedalus.cl/", name = "participantesGet")
    public JAXBElement<ParticipantesGet> createParticipantesGet(ParticipantesGet value) {
        return new JAXBElement<ParticipantesGet>(_ParticipantesGet_QNAME, ParticipantesGet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ParticipanteSet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsproyectos.dedalus.cl/", name = "participanteSet")
    public JAXBElement<ParticipanteSet> createParticipanteSet(ParticipanteSet value) {
        return new JAXBElement<ParticipanteSet>(_ParticipanteSet_QNAME, ParticipanteSet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProyectoUpd }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsproyectos.dedalus.cl/", name = "proyectoUpd")
    public JAXBElement<ProyectoUpd> createProyectoUpd(ProyectoUpd value) {
        return new JAXBElement<ProyectoUpd>(_ProyectoUpd_QNAME, ProyectoUpd.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocentesGetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsproyectos.dedalus.cl/", name = "docentesGetResponse")
    public JAXBElement<DocentesGetResponse> createDocentesGetResponse(DocentesGetResponse value) {
        return new JAXBElement<DocentesGetResponse>(_DocentesGetResponse_QNAME, DocentesGetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ParamGet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsproyectos.dedalus.cl/", name = "paramGet")
    public JAXBElement<ParamGet> createParamGet(ParamGet value) {
        return new JAXBElement<ParamGet>(_ParamGet_QNAME, ParamGet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ParticipanteUpd }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsproyectos.dedalus.cl/", name = "participanteUpd")
    public JAXBElement<ParticipanteUpd> createParticipanteUpd(ParticipanteUpd value) {
        return new JAXBElement<ParticipanteUpd>(_ParticipanteUpd_QNAME, ParticipanteUpd.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponsableUpd }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsproyectos.dedalus.cl/", name = "responsableUpd")
    public JAXBElement<ResponsableUpd> createResponsableUpd(ResponsableUpd value) {
        return new JAXBElement<ResponsableUpd>(_ResponsableUpd_QNAME, ResponsableUpd.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ParticipanteDelResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsproyectos.dedalus.cl/", name = "participanteDelResponse")
    public JAXBElement<ParticipanteDelResponse> createParticipanteDelResponse(ParticipanteDelResponse value) {
        return new JAXBElement<ParticipanteDelResponse>(_ParticipanteDelResponse_QNAME, ParticipanteDelResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ParamGetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsproyectos.dedalus.cl/", name = "paramGetResponse")
    public JAXBElement<ParamGetResponse> createParamGetResponse(ParamGetResponse value) {
        return new JAXBElement<ParamGetResponse>(_ParamGetResponse_QNAME, ParamGetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ParticipantesGetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsproyectos.dedalus.cl/", name = "participantesGetResponse")
    public JAXBElement<ParticipantesGetResponse> createParticipantesGetResponse(ParticipantesGetResponse value) {
        return new JAXBElement<ParticipantesGetResponse>(_ParticipantesGetResponse_QNAME, ParticipantesGetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsproyectos.dedalus.cl/", name = "faultInfo")
    public JAXBElement<String> createFaultInfo(String value) {
        return new JAXBElement<String>(_FaultInfo_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProyectosGet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsproyectos.dedalus.cl/", name = "proyectosGet")
    public JAXBElement<ProyectosGet> createProyectosGet(ProyectosGet value) {
        return new JAXBElement<ProyectosGet>(_ProyectosGet_QNAME, ProyectosGet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ColabexternoSetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsproyectos.dedalus.cl/", name = "colabexternoSetResponse")
    public JAXBElement<ColabexternoSetResponse> createColabexternoSetResponse(ColabexternoSetResponse value) {
        return new JAXBElement<ColabexternoSetResponse>(_ColabexternoSetResponse_QNAME, ColabexternoSetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ParticipanteUpdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsproyectos.dedalus.cl/", name = "participanteUpdResponse")
    public JAXBElement<ParticipanteUpdResponse> createParticipanteUpdResponse(ParticipanteUpdResponse value) {
        return new JAXBElement<ParticipanteUpdResponse>(_ParticipanteUpdResponse_QNAME, ParticipanteUpdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponsableUpdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsproyectos.dedalus.cl/", name = "responsableUpdResponse")
    public JAXBElement<ResponsableUpdResponse> createResponsableUpdResponse(ResponsableUpdResponse value) {
        return new JAXBElement<ResponsableUpdResponse>(_ResponsableUpdResponse_QNAME, ResponsableUpdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProyectosGetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsproyectos.dedalus.cl/", name = "proyectosGetResponse")
    public JAXBElement<ProyectosGetResponse> createProyectosGetResponse(ProyectosGetResponse value) {
        return new JAXBElement<ProyectosGetResponse>(_ProyectosGetResponse_QNAME, ProyectosGetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProyectoSetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsproyectos.dedalus.cl/", name = "proyectoSetResponse")
    public JAXBElement<ProyectoSetResponse> createProyectoSetResponse(ProyectoSetResponse value) {
        return new JAXBElement<ProyectoSetResponse>(_ProyectoSetResponse_QNAME, ProyectoSetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProyectoUpdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsproyectos.dedalus.cl/", name = "proyectoUpdResponse")
    public JAXBElement<ProyectoUpdResponse> createProyectoUpdResponse(ProyectoUpdResponse value) {
        return new JAXBElement<ProyectoUpdResponse>(_ProyectoUpdResponse_QNAME, ProyectoUpdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ParticipanteDel }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsproyectos.dedalus.cl/", name = "participanteDel")
    public JAXBElement<ParticipanteDel> createParticipanteDel(ParticipanteDel value) {
        return new JAXBElement<ParticipanteDel>(_ParticipanteDel_QNAME, ParticipanteDel.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocentesGet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsproyectos.dedalus.cl/", name = "docentesGet")
    public JAXBElement<DocentesGet> createDocentesGet(DocentesGet value) {
        return new JAXBElement<DocentesGet>(_DocentesGet_QNAME, DocentesGet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ColabexternoSet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsproyectos.dedalus.cl/", name = "colabexternoSet")
    public JAXBElement<ColabexternoSet> createColabexternoSet(ColabexternoSet value) {
        return new JAXBElement<ColabexternoSet>(_ColabexternoSet_QNAME, ColabexternoSet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProyectoGet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsproyectos.dedalus.cl/", name = "proyectoGet")
    public JAXBElement<ProyectoGet> createProyectoGet(ProyectoGet value) {
        return new JAXBElement<ProyectoGet>(_ProyectoGet_QNAME, ProyectoGet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProyectoSet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsproyectos.dedalus.cl/", name = "proyectoSet")
    public JAXBElement<ProyectoSet> createProyectoSet(ProyectoSet value) {
        return new JAXBElement<ProyectoSet>(_ProyectoSet_QNAME, ProyectoSet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProyectoGetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsproyectos.dedalus.cl/", name = "proyectoGetResponse")
    public JAXBElement<ProyectoGetResponse> createProyectoGetResponse(ProyectoGetResponse value) {
        return new JAXBElement<ProyectoGetResponse>(_ProyectoGetResponse_QNAME, ProyectoGetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ParticipanteSetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsproyectos.dedalus.cl/", name = "participanteSetResponse")
    public JAXBElement<ParticipanteSetResponse> createParticipanteSetResponse(ParticipanteSetResponse value) {
        return new JAXBElement<ParticipanteSetResponse>(_ParticipanteSetResponse_QNAME, ParticipanteSetResponse.class, null, value);
    }

}
