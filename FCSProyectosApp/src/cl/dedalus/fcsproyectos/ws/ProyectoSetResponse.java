
package cl.dedalus.fcsproyectos.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para proyectoSetResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="proyectoSetResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="proyectoSetResult" type="{http://ws.fcsproyectos.dedalus.cl/}ProyectoSetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "proyectoSetResponse", propOrder = {
    "proyectoSetResult"
})
public class ProyectoSetResponse {

    protected ProyectoSetResult proyectoSetResult;

    /**
     * Obtiene el valor de la propiedad proyectoSetResult.
     * 
     * @return
     *     possible object is
     *     {@link ProyectoSetResult }
     *     
     */
    public ProyectoSetResult getProyectoSetResult() {
        return proyectoSetResult;
    }

    /**
     * Define el valor de la propiedad proyectoSetResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ProyectoSetResult }
     *     
     */
    public void setProyectoSetResult(ProyectoSetResult value) {
        this.proyectoSetResult = value;
    }

}
