
package cl.dedalus.fcsproyectos.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ParticipantesGetParti complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ParticipantesGetParti">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cantHorEjec" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantHorForm" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantSemEjec" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantSemForm" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantTotEjec" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantTotForm" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codiOrigen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codiParticipante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="flagResponsable" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idParticipante" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="nombParticipante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ParticipantesGetParti", propOrder = {
    "cantHorEjec",
    "cantHorForm",
    "cantSemEjec",
    "cantSemForm",
    "cantTotEjec",
    "cantTotForm",
    "codiOrigen",
    "codiParticipante",
    "flagResponsable",
    "idParticipante",
    "nombParticipante"
})
public class ParticipantesGetParti {

    protected String cantHorEjec;
    protected String cantHorForm;
    protected String cantSemEjec;
    protected String cantSemForm;
    protected String cantTotEjec;
    protected String cantTotForm;
    protected String codiOrigen;
    protected String codiParticipante;
    protected String flagResponsable;
    protected long idParticipante;
    protected String nombParticipante;

    /**
     * Obtiene el valor de la propiedad cantHorEjec.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantHorEjec() {
        return cantHorEjec;
    }

    /**
     * Define el valor de la propiedad cantHorEjec.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantHorEjec(String value) {
        this.cantHorEjec = value;
    }

    /**
     * Obtiene el valor de la propiedad cantHorForm.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantHorForm() {
        return cantHorForm;
    }

    /**
     * Define el valor de la propiedad cantHorForm.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantHorForm(String value) {
        this.cantHorForm = value;
    }

    /**
     * Obtiene el valor de la propiedad cantSemEjec.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantSemEjec() {
        return cantSemEjec;
    }

    /**
     * Define el valor de la propiedad cantSemEjec.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantSemEjec(String value) {
        this.cantSemEjec = value;
    }

    /**
     * Obtiene el valor de la propiedad cantSemForm.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantSemForm() {
        return cantSemForm;
    }

    /**
     * Define el valor de la propiedad cantSemForm.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantSemForm(String value) {
        this.cantSemForm = value;
    }

    /**
     * Obtiene el valor de la propiedad cantTotEjec.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantTotEjec() {
        return cantTotEjec;
    }

    /**
     * Define el valor de la propiedad cantTotEjec.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantTotEjec(String value) {
        this.cantTotEjec = value;
    }

    /**
     * Obtiene el valor de la propiedad cantTotForm.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantTotForm() {
        return cantTotForm;
    }

    /**
     * Define el valor de la propiedad cantTotForm.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantTotForm(String value) {
        this.cantTotForm = value;
    }

    /**
     * Obtiene el valor de la propiedad codiOrigen.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiOrigen() {
        return codiOrigen;
    }

    /**
     * Define el valor de la propiedad codiOrigen.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiOrigen(String value) {
        this.codiOrigen = value;
    }

    /**
     * Obtiene el valor de la propiedad codiParticipante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiParticipante() {
        return codiParticipante;
    }

    /**
     * Define el valor de la propiedad codiParticipante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiParticipante(String value) {
        this.codiParticipante = value;
    }

    /**
     * Obtiene el valor de la propiedad flagResponsable.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlagResponsable() {
        return flagResponsable;
    }

    /**
     * Define el valor de la propiedad flagResponsable.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlagResponsable(String value) {
        this.flagResponsable = value;
    }

    /**
     * Obtiene el valor de la propiedad idParticipante.
     * 
     */
    public long getIdParticipante() {
        return idParticipante;
    }

    /**
     * Define el valor de la propiedad idParticipante.
     * 
     */
    public void setIdParticipante(long value) {
        this.idParticipante = value;
    }

    /**
     * Obtiene el valor de la propiedad nombParticipante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombParticipante() {
        return nombParticipante;
    }

    /**
     * Define el valor de la propiedad nombParticipante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombParticipante(String value) {
        this.nombParticipante = value;
    }

}
