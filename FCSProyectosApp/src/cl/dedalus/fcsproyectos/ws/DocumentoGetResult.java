
package cl.dedalus.fcsproyectos.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DocumentoGetResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DocumentoGetResult">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.fcsproyectos.dedalus.cl/}ProcedureResult">
 *       &lt;sequence>
 *         &lt;element name="contDocumento" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocumentoGetResult", propOrder = {
    "contDocumento"
})
public class DocumentoGetResult
    extends ProcedureResult
{

    protected byte[] contDocumento;

    /**
     * Obtiene el valor de la propiedad contDocumento.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getContDocumento() {
        return contDocumento;
    }

    /**
     * Define el valor de la propiedad contDocumento.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setContDocumento(byte[] value) {
        this.contDocumento = value;
    }

}
