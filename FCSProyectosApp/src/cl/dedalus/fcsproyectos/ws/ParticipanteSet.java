
package cl.dedalus.fcsproyectos.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para participanteSet complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="participanteSet">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idProyecto" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="codiParticipante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codiOrigen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "participanteSet", propOrder = {
    "idProyecto",
    "codiParticipante",
    "codiOrigen"
})
public class ParticipanteSet {

    protected long idProyecto;
    protected String codiParticipante;
    protected String codiOrigen;

    /**
     * Obtiene el valor de la propiedad idProyecto.
     * 
     */
    public long getIdProyecto() {
        return idProyecto;
    }

    /**
     * Define el valor de la propiedad idProyecto.
     * 
     */
    public void setIdProyecto(long value) {
        this.idProyecto = value;
    }

    /**
     * Obtiene el valor de la propiedad codiParticipante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiParticipante() {
        return codiParticipante;
    }

    /**
     * Define el valor de la propiedad codiParticipante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiParticipante(String value) {
        this.codiParticipante = value;
    }

    /**
     * Obtiene el valor de la propiedad codiOrigen.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiOrigen() {
        return codiOrigen;
    }

    /**
     * Define el valor de la propiedad codiOrigen.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiOrigen(String value) {
        this.codiOrigen = value;
    }

}
