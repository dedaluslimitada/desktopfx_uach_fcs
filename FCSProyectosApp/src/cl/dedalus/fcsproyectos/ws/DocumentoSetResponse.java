
package cl.dedalus.fcsproyectos.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para documentoSetResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="documentoSetResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="documentoSetResult" type="{http://ws.fcsproyectos.dedalus.cl/}DocumentoSetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "documentoSetResponse", propOrder = {
    "documentoSetResult"
})
public class DocumentoSetResponse {

    protected DocumentoSetResult documentoSetResult;

    /**
     * Obtiene el valor de la propiedad documentoSetResult.
     * 
     * @return
     *     possible object is
     *     {@link DocumentoSetResult }
     *     
     */
    public DocumentoSetResult getDocumentoSetResult() {
        return documentoSetResult;
    }

    /**
     * Define el valor de la propiedad documentoSetResult.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentoSetResult }
     *     
     */
    public void setDocumentoSetResult(DocumentoSetResult value) {
        this.documentoSetResult = value;
    }

}
