
package cl.dedalus.fcsproyectos.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para participanteDelResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="participanteDelResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="participanteDelResult" type="{http://ws.fcsproyectos.dedalus.cl/}ParticipanteDelResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "participanteDelResponse", propOrder = {
    "participanteDelResult"
})
public class ParticipanteDelResponse {

    protected ParticipanteDelResult participanteDelResult;

    /**
     * Obtiene el valor de la propiedad participanteDelResult.
     * 
     * @return
     *     possible object is
     *     {@link ParticipanteDelResult }
     *     
     */
    public ParticipanteDelResult getParticipanteDelResult() {
        return participanteDelResult;
    }

    /**
     * Define el valor de la propiedad participanteDelResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ParticipanteDelResult }
     *     
     */
    public void setParticipanteDelResult(ParticipanteDelResult value) {
        this.participanteDelResult = value;
    }

}
