
package cl.dedalus.fcsproyectos.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para proyectoUpd complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="proyectoUpd">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="codiProyecto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codiDocente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descTitulo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descResumen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechIniF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechFinF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechIniE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechFinE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idenEstado" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="idenTipop" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="idenFinancia" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="cantMonto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "proyectoUpd", propOrder = {
    "id",
    "codiProyecto",
    "codiDocente",
    "descTitulo",
    "descResumen",
    "fechIniF",
    "fechFinF",
    "fechIniE",
    "fechFinE",
    "idenEstado",
    "idenTipop",
    "idenFinancia",
    "cantMonto"
})
public class ProyectoUpd {

    protected long id;
    protected String codiProyecto;
    protected String codiDocente;
    protected String descTitulo;
    protected String descResumen;
    protected String fechIniF;
    protected String fechFinF;
    protected String fechIniE;
    protected String fechFinE;
    protected int idenEstado;
    protected int idenTipop;
    protected int idenFinancia;
    protected String cantMonto;

    /**
     * Obtiene el valor de la propiedad id.
     * 
     */
    public long getId() {
        return id;
    }

    /**
     * Define el valor de la propiedad id.
     * 
     */
    public void setId(long value) {
        this.id = value;
    }

    /**
     * Obtiene el valor de la propiedad codiProyecto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiProyecto() {
        return codiProyecto;
    }

    /**
     * Define el valor de la propiedad codiProyecto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiProyecto(String value) {
        this.codiProyecto = value;
    }

    /**
     * Obtiene el valor de la propiedad codiDocente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiDocente() {
        return codiDocente;
    }

    /**
     * Define el valor de la propiedad codiDocente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiDocente(String value) {
        this.codiDocente = value;
    }

    /**
     * Obtiene el valor de la propiedad descTitulo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescTitulo() {
        return descTitulo;
    }

    /**
     * Define el valor de la propiedad descTitulo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescTitulo(String value) {
        this.descTitulo = value;
    }

    /**
     * Obtiene el valor de la propiedad descResumen.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescResumen() {
        return descResumen;
    }

    /**
     * Define el valor de la propiedad descResumen.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescResumen(String value) {
        this.descResumen = value;
    }

    /**
     * Obtiene el valor de la propiedad fechIniF.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechIniF() {
        return fechIniF;
    }

    /**
     * Define el valor de la propiedad fechIniF.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechIniF(String value) {
        this.fechIniF = value;
    }

    /**
     * Obtiene el valor de la propiedad fechFinF.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechFinF() {
        return fechFinF;
    }

    /**
     * Define el valor de la propiedad fechFinF.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechFinF(String value) {
        this.fechFinF = value;
    }

    /**
     * Obtiene el valor de la propiedad fechIniE.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechIniE() {
        return fechIniE;
    }

    /**
     * Define el valor de la propiedad fechIniE.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechIniE(String value) {
        this.fechIniE = value;
    }

    /**
     * Obtiene el valor de la propiedad fechFinE.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechFinE() {
        return fechFinE;
    }

    /**
     * Define el valor de la propiedad fechFinE.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechFinE(String value) {
        this.fechFinE = value;
    }

    /**
     * Obtiene el valor de la propiedad idenEstado.
     * 
     */
    public int getIdenEstado() {
        return idenEstado;
    }

    /**
     * Define el valor de la propiedad idenEstado.
     * 
     */
    public void setIdenEstado(int value) {
        this.idenEstado = value;
    }

    /**
     * Obtiene el valor de la propiedad idenTipop.
     * 
     */
    public int getIdenTipop() {
        return idenTipop;
    }

    /**
     * Define el valor de la propiedad idenTipop.
     * 
     */
    public void setIdenTipop(int value) {
        this.idenTipop = value;
    }

    /**
     * Obtiene el valor de la propiedad idenFinancia.
     * 
     */
    public int getIdenFinancia() {
        return idenFinancia;
    }

    /**
     * Define el valor de la propiedad idenFinancia.
     * 
     */
    public void setIdenFinancia(int value) {
        this.idenFinancia = value;
    }

    /**
     * Obtiene el valor de la propiedad cantMonto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantMonto() {
        return cantMonto;
    }

    /**
     * Define el valor de la propiedad cantMonto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantMonto(String value) {
        this.cantMonto = value;
    }

}
