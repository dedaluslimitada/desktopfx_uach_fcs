
package cl.dedalus.fcsproyectos.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para participanteSetResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="participanteSetResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="participanteSetResult" type="{http://ws.fcsproyectos.dedalus.cl/}ParticipanteSetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "participanteSetResponse", propOrder = {
    "participanteSetResult"
})
public class ParticipanteSetResponse {

    protected ParticipanteSetResult participanteSetResult;

    /**
     * Obtiene el valor de la propiedad participanteSetResult.
     * 
     * @return
     *     possible object is
     *     {@link ParticipanteSetResult }
     *     
     */
    public ParticipanteSetResult getParticipanteSetResult() {
        return participanteSetResult;
    }

    /**
     * Define el valor de la propiedad participanteSetResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ParticipanteSetResult }
     *     
     */
    public void setParticipanteSetResult(ParticipanteSetResult value) {
        this.participanteSetResult = value;
    }

}
