
package cl.dedalus.fcsproyectos.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para estadosGetResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="estadosGetResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="estadosGetResult" type="{http://ws.fcsproyectos.dedalus.cl/}EstadosGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "estadosGetResponse", propOrder = {
    "estadosGetResult"
})
public class EstadosGetResponse {

    protected EstadosGetResult estadosGetResult;

    /**
     * Obtiene el valor de la propiedad estadosGetResult.
     * 
     * @return
     *     possible object is
     *     {@link EstadosGetResult }
     *     
     */
    public EstadosGetResult getEstadosGetResult() {
        return estadosGetResult;
    }

    /**
     * Define el valor de la propiedad estadosGetResult.
     * 
     * @param value
     *     allowed object is
     *     {@link EstadosGetResult }
     *     
     */
    public void setEstadosGetResult(EstadosGetResult value) {
        this.estadosGetResult = value;
    }

}
