
package cl.dedalus.fcsproyectos.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ParamGetResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ParamGetResult">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.fcsproyectos.dedalus.cl/}ProcedureResult">
 *       &lt;sequence>
 *         &lt;element name="estados" type="{http://ws.fcsproyectos.dedalus.cl/}ParamGetEstado" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="tipofins" type="{http://ws.fcsproyectos.dedalus.cl/}ParamGetTipofin" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="tipoprys" type="{http://ws.fcsproyectos.dedalus.cl/}ParamGetTipopry" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ParamGetResult", propOrder = {
    "estados",
    "tipofins",
    "tipoprys"
})
public class ParamGetResult
    extends ProcedureResult
{

    @XmlElement(nillable = true)
    protected List<ParamGetEstado> estados;
    @XmlElement(nillable = true)
    protected List<ParamGetTipofin> tipofins;
    @XmlElement(nillable = true)
    protected List<ParamGetTipopry> tipoprys;

    /**
     * Gets the value of the estados property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the estados property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEstados().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ParamGetEstado }
     * 
     * 
     */
    public List<ParamGetEstado> getEstados() {
        if (estados == null) {
            estados = new ArrayList<ParamGetEstado>();
        }
        return this.estados;
    }

    /**
     * Gets the value of the tipofins property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tipofins property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTipofins().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ParamGetTipofin }
     * 
     * 
     */
    public List<ParamGetTipofin> getTipofins() {
        if (tipofins == null) {
            tipofins = new ArrayList<ParamGetTipofin>();
        }
        return this.tipofins;
    }

    /**
     * Gets the value of the tipoprys property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tipoprys property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTipoprys().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ParamGetTipopry }
     * 
     * 
     */
    public List<ParamGetTipopry> getTipoprys() {
        if (tipoprys == null) {
            tipoprys = new ArrayList<ParamGetTipopry>();
        }
        return this.tipoprys;
    }

}
