
package cl.dedalus.fcsproyectos.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para proyectoUpdResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="proyectoUpdResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="proyectoUpdResult" type="{http://ws.fcsproyectos.dedalus.cl/}ProyectoUpdResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "proyectoUpdResponse", propOrder = {
    "proyectoUpdResult"
})
public class ProyectoUpdResponse {

    protected ProyectoUpdResult proyectoUpdResult;

    /**
     * Obtiene el valor de la propiedad proyectoUpdResult.
     * 
     * @return
     *     possible object is
     *     {@link ProyectoUpdResult }
     *     
     */
    public ProyectoUpdResult getProyectoUpdResult() {
        return proyectoUpdResult;
    }

    /**
     * Define el valor de la propiedad proyectoUpdResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ProyectoUpdResult }
     *     
     */
    public void setProyectoUpdResult(ProyectoUpdResult value) {
        this.proyectoUpdResult = value;
    }

}
