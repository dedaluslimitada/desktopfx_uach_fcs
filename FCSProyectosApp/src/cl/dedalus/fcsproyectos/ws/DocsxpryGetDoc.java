
package cl.dedalus.fcsproyectos.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DocsxpryGetDoc complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DocsxpryGetDoc">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="descTitulo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechDocumento" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="idenDocumento" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="nombDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocsxpryGetDoc", propOrder = {
    "descTitulo",
    "fechDocumento",
    "idenDocumento",
    "nombDocumento"
})
public class DocsxpryGetDoc {

    protected String descTitulo;
    protected int fechDocumento;
    protected int idenDocumento;
    protected String nombDocumento;

    /**
     * Obtiene el valor de la propiedad descTitulo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescTitulo() {
        return descTitulo;
    }

    /**
     * Define el valor de la propiedad descTitulo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescTitulo(String value) {
        this.descTitulo = value;
    }

    /**
     * Obtiene el valor de la propiedad fechDocumento.
     * 
     */
    public int getFechDocumento() {
        return fechDocumento;
    }

    /**
     * Define el valor de la propiedad fechDocumento.
     * 
     */
    public void setFechDocumento(int value) {
        this.fechDocumento = value;
    }

    /**
     * Obtiene el valor de la propiedad idenDocumento.
     * 
     */
    public int getIdenDocumento() {
        return idenDocumento;
    }

    /**
     * Define el valor de la propiedad idenDocumento.
     * 
     */
    public void setIdenDocumento(int value) {
        this.idenDocumento = value;
    }

    /**
     * Obtiene el valor de la propiedad nombDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombDocumento() {
        return nombDocumento;
    }

    /**
     * Define el valor de la propiedad nombDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombDocumento(String value) {
        this.nombDocumento = value;
    }

}
