
package cl.dedalus.fcsproyectos.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para colabexternoSet complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="colabexternoSet">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codiUser" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codiColab" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nombColab" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="apelColab" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="profColab" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rutColab" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mailColab" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fonoColab" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "colabexternoSet", propOrder = {
    "codiUser",
    "codiColab",
    "nombColab",
    "apelColab",
    "profColab",
    "rutColab",
    "mailColab",
    "fonoColab"
})
public class ColabexternoSet {

    protected String codiUser;
    protected String codiColab;
    protected String nombColab;
    protected String apelColab;
    protected String profColab;
    protected String rutColab;
    protected String mailColab;
    protected String fonoColab;

    /**
     * Obtiene el valor de la propiedad codiUser.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiUser() {
        return codiUser;
    }

    /**
     * Define el valor de la propiedad codiUser.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiUser(String value) {
        this.codiUser = value;
    }

    /**
     * Obtiene el valor de la propiedad codiColab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiColab() {
        return codiColab;
    }

    /**
     * Define el valor de la propiedad codiColab.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiColab(String value) {
        this.codiColab = value;
    }

    /**
     * Obtiene el valor de la propiedad nombColab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombColab() {
        return nombColab;
    }

    /**
     * Define el valor de la propiedad nombColab.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombColab(String value) {
        this.nombColab = value;
    }

    /**
     * Obtiene el valor de la propiedad apelColab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApelColab() {
        return apelColab;
    }

    /**
     * Define el valor de la propiedad apelColab.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApelColab(String value) {
        this.apelColab = value;
    }

    /**
     * Obtiene el valor de la propiedad profColab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProfColab() {
        return profColab;
    }

    /**
     * Define el valor de la propiedad profColab.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProfColab(String value) {
        this.profColab = value;
    }

    /**
     * Obtiene el valor de la propiedad rutColab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRutColab() {
        return rutColab;
    }

    /**
     * Define el valor de la propiedad rutColab.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRutColab(String value) {
        this.rutColab = value;
    }

    /**
     * Obtiene el valor de la propiedad mailColab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMailColab() {
        return mailColab;
    }

    /**
     * Define el valor de la propiedad mailColab.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMailColab(String value) {
        this.mailColab = value;
    }

    /**
     * Obtiene el valor de la propiedad fonoColab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFonoColab() {
        return fonoColab;
    }

    /**
     * Define el valor de la propiedad fonoColab.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFonoColab(String value) {
        this.fonoColab = value;
    }

}
