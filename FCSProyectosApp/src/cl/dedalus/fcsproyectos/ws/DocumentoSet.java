
package cl.dedalus.fcsproyectos.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para documentoSet complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="documentoSet">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codiProyecto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nombDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="documento" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "documentoSet", propOrder = {
    "codiProyecto",
    "nombDocumento",
    "documento"
})
public class DocumentoSet {

    protected String codiProyecto;
    protected String nombDocumento;
    @XmlElementRef(name = "documento", namespace = "http://ws.fcsproyectos.dedalus.cl/", type = JAXBElement.class, required = false)
    protected JAXBElement<byte[]> documento;

    /**
     * Obtiene el valor de la propiedad codiProyecto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiProyecto() {
        return codiProyecto;
    }

    /**
     * Define el valor de la propiedad codiProyecto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiProyecto(String value) {
        this.codiProyecto = value;
    }

    /**
     * Obtiene el valor de la propiedad nombDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombDocumento() {
        return nombDocumento;
    }

    /**
     * Define el valor de la propiedad nombDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombDocumento(String value) {
        this.nombDocumento = value;
    }

    /**
     * Obtiene el valor de la propiedad documento.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     *     
     */
    public JAXBElement<byte[]> getDocumento() {
        return documento;
    }

    /**
     * Define el valor de la propiedad documento.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     *     
     */
    public void setDocumento(JAXBElement<byte[]> value) {
        this.documento = value;
    }

}
