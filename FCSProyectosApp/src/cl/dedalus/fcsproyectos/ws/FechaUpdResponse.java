
package cl.dedalus.fcsproyectos.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para fechaUpdResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="fechaUpdResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="fechaUpdResult" type="{http://ws.fcsproyectos.dedalus.cl/}FechaUpdResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "fechaUpdResponse", propOrder = {
    "fechaUpdResult"
})
public class FechaUpdResponse {

    protected FechaUpdResult fechaUpdResult;

    /**
     * Obtiene el valor de la propiedad fechaUpdResult.
     * 
     * @return
     *     possible object is
     *     {@link FechaUpdResult }
     *     
     */
    public FechaUpdResult getFechaUpdResult() {
        return fechaUpdResult;
    }

    /**
     * Define el valor de la propiedad fechaUpdResult.
     * 
     * @param value
     *     allowed object is
     *     {@link FechaUpdResult }
     *     
     */
    public void setFechaUpdResult(FechaUpdResult value) {
        this.fechaUpdResult = value;
    }

}
