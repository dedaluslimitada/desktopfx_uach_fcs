
package cl.dedalus.fcsproyectos.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para tituloUpdResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tituloUpdResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="tituloUpdResult" type="{http://ws.fcsproyectos.dedalus.cl/}TituloUpdResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tituloUpdResponse", propOrder = {
    "tituloUpdResult"
})
public class TituloUpdResponse {

    protected TituloUpdResult tituloUpdResult;

    /**
     * Obtiene el valor de la propiedad tituloUpdResult.
     * 
     * @return
     *     possible object is
     *     {@link TituloUpdResult }
     *     
     */
    public TituloUpdResult getTituloUpdResult() {
        return tituloUpdResult;
    }

    /**
     * Define el valor de la propiedad tituloUpdResult.
     * 
     * @param value
     *     allowed object is
     *     {@link TituloUpdResult }
     *     
     */
    public void setTituloUpdResult(TituloUpdResult value) {
        this.tituloUpdResult = value;
    }

}
