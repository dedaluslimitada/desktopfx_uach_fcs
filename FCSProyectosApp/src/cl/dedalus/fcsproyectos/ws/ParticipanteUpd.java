
package cl.dedalus.fcsproyectos.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para participanteUpd complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="participanteUpd">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idParticipante" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="cantSemForm" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantHorForm" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantTotalForm" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantSemEjec" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantHorEjec" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "participanteUpd", propOrder = {
    "idParticipante",
    "cantSemForm",
    "cantHorForm",
    "cantTotalForm",
    "cantSemEjec",
    "cantHorEjec"
})
public class ParticipanteUpd {

    protected long idParticipante;
    protected String cantSemForm;
    protected String cantHorForm;
    protected String cantTotalForm;
    protected String cantSemEjec;
    protected String cantHorEjec;

    /**
     * Obtiene el valor de la propiedad idParticipante.
     * 
     */
    public long getIdParticipante() {
        return idParticipante;
    }

    /**
     * Define el valor de la propiedad idParticipante.
     * 
     */
    public void setIdParticipante(long value) {
        this.idParticipante = value;
    }

    /**
     * Obtiene el valor de la propiedad cantSemForm.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantSemForm() {
        return cantSemForm;
    }

    /**
     * Define el valor de la propiedad cantSemForm.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantSemForm(String value) {
        this.cantSemForm = value;
    }

    /**
     * Obtiene el valor de la propiedad cantHorForm.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantHorForm() {
        return cantHorForm;
    }

    /**
     * Define el valor de la propiedad cantHorForm.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantHorForm(String value) {
        this.cantHorForm = value;
    }

    /**
     * Obtiene el valor de la propiedad cantTotalForm.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantTotalForm() {
        return cantTotalForm;
    }

    /**
     * Define el valor de la propiedad cantTotalForm.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantTotalForm(String value) {
        this.cantTotalForm = value;
    }

    /**
     * Obtiene el valor de la propiedad cantSemEjec.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantSemEjec() {
        return cantSemEjec;
    }

    /**
     * Define el valor de la propiedad cantSemEjec.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantSemEjec(String value) {
        this.cantSemEjec = value;
    }

    /**
     * Obtiene el valor de la propiedad cantHorEjec.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantHorEjec() {
        return cantHorEjec;
    }

    /**
     * Define el valor de la propiedad cantHorEjec.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantHorEjec(String value) {
        this.cantHorEjec = value;
    }

}
