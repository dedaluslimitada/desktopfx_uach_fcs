
package cl.dedalus.fcsproyectos.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para responsableUpdResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="responsableUpdResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="responsableUpdResult" type="{http://ws.fcsproyectos.dedalus.cl/}ResponsableUpdResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "responsableUpdResponse", propOrder = {
    "responsableUpdResult"
})
public class ResponsableUpdResponse {

    protected ResponsableUpdResult responsableUpdResult;

    /**
     * Obtiene el valor de la propiedad responsableUpdResult.
     * 
     * @return
     *     possible object is
     *     {@link ResponsableUpdResult }
     *     
     */
    public ResponsableUpdResult getResponsableUpdResult() {
        return responsableUpdResult;
    }

    /**
     * Define el valor de la propiedad responsableUpdResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponsableUpdResult }
     *     
     */
    public void setResponsableUpdResult(ResponsableUpdResult value) {
        this.responsableUpdResult = value;
    }

}
