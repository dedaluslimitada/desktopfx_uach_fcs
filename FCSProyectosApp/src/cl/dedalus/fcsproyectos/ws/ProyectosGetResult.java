
package cl.dedalus.fcsproyectos.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ProyectosGetResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ProyectosGetResult">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.fcsproyectos.dedalus.cl/}ProcedureResult">
 *       &lt;sequence>
 *         &lt;element name="descFacultad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descInstituto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="prys" type="{http://ws.fcsproyectos.dedalus.cl/}ProyectosGetPry" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProyectosGetResult", propOrder = {
    "descFacultad",
    "descInstituto",
    "prys"
})
public class ProyectosGetResult
    extends ProcedureResult
{

    protected String descFacultad;
    protected String descInstituto;
    @XmlElement(nillable = true)
    protected List<ProyectosGetPry> prys;

    /**
     * Obtiene el valor de la propiedad descFacultad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescFacultad() {
        return descFacultad;
    }

    /**
     * Define el valor de la propiedad descFacultad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescFacultad(String value) {
        this.descFacultad = value;
    }

    /**
     * Obtiene el valor de la propiedad descInstituto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescInstituto() {
        return descInstituto;
    }

    /**
     * Define el valor de la propiedad descInstituto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescInstituto(String value) {
        this.descInstituto = value;
    }

    /**
     * Gets the value of the prys property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the prys property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPrys().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProyectosGetPry }
     * 
     * 
     */
    public List<ProyectosGetPry> getPrys() {
        if (prys == null) {
            prys = new ArrayList<ProyectosGetPry>();
        }
        return this.prys;
    }

}
