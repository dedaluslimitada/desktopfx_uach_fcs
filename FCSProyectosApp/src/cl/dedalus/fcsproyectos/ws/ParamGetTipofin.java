
package cl.dedalus.fcsproyectos.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ParamGetTipofin complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ParamGetTipofin">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="tipofinancia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ParamGetTipofin", propOrder = {
    "tipofinancia"
})
public class ParamGetTipofin {

    protected String tipofinancia;

    /**
     * Obtiene el valor de la propiedad tipofinancia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipofinancia() {
        return tipofinancia;
    }

    /**
     * Define el valor de la propiedad tipofinancia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipofinancia(String value) {
        this.tipofinancia = value;
    }

}
