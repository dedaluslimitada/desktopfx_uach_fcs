
package cl.dedalus.fcsproyectos.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para proyectoSet complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="proyectoSet">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codiProyecto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codiDocente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descTitulo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descResumen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechIniForm" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechFinForm" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechIniEjec" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechFinEjec" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idenEstado" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="idenTipoproyecto" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="idenFinancia" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="cantMonto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "proyectoSet", propOrder = {
    "codiProyecto",
    "codiDocente",
    "descTitulo",
    "descResumen",
    "fechIniForm",
    "fechFinForm",
    "fechIniEjec",
    "fechFinEjec",
    "idenEstado",
    "idenTipoproyecto",
    "idenFinancia",
    "cantMonto"
})
public class ProyectoSet {

    protected String codiProyecto;
    protected String codiDocente;
    protected String descTitulo;
    protected String descResumen;
    protected String fechIniForm;
    protected String fechFinForm;
    protected String fechIniEjec;
    protected String fechFinEjec;
    protected int idenEstado;
    protected int idenTipoproyecto;
    protected int idenFinancia;
    protected String cantMonto;

    /**
     * Obtiene el valor de la propiedad codiProyecto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiProyecto() {
        return codiProyecto;
    }

    /**
     * Define el valor de la propiedad codiProyecto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiProyecto(String value) {
        this.codiProyecto = value;
    }

    /**
     * Obtiene el valor de la propiedad codiDocente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiDocente() {
        return codiDocente;
    }

    /**
     * Define el valor de la propiedad codiDocente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiDocente(String value) {
        this.codiDocente = value;
    }

    /**
     * Obtiene el valor de la propiedad descTitulo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescTitulo() {
        return descTitulo;
    }

    /**
     * Define el valor de la propiedad descTitulo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescTitulo(String value) {
        this.descTitulo = value;
    }

    /**
     * Obtiene el valor de la propiedad descResumen.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescResumen() {
        return descResumen;
    }

    /**
     * Define el valor de la propiedad descResumen.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescResumen(String value) {
        this.descResumen = value;
    }

    /**
     * Obtiene el valor de la propiedad fechIniForm.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechIniForm() {
        return fechIniForm;
    }

    /**
     * Define el valor de la propiedad fechIniForm.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechIniForm(String value) {
        this.fechIniForm = value;
    }

    /**
     * Obtiene el valor de la propiedad fechFinForm.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechFinForm() {
        return fechFinForm;
    }

    /**
     * Define el valor de la propiedad fechFinForm.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechFinForm(String value) {
        this.fechFinForm = value;
    }

    /**
     * Obtiene el valor de la propiedad fechIniEjec.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechIniEjec() {
        return fechIniEjec;
    }

    /**
     * Define el valor de la propiedad fechIniEjec.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechIniEjec(String value) {
        this.fechIniEjec = value;
    }

    /**
     * Obtiene el valor de la propiedad fechFinEjec.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechFinEjec() {
        return fechFinEjec;
    }

    /**
     * Define el valor de la propiedad fechFinEjec.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechFinEjec(String value) {
        this.fechFinEjec = value;
    }

    /**
     * Obtiene el valor de la propiedad idenEstado.
     * 
     */
    public int getIdenEstado() {
        return idenEstado;
    }

    /**
     * Define el valor de la propiedad idenEstado.
     * 
     */
    public void setIdenEstado(int value) {
        this.idenEstado = value;
    }

    /**
     * Obtiene el valor de la propiedad idenTipoproyecto.
     * 
     */
    public int getIdenTipoproyecto() {
        return idenTipoproyecto;
    }

    /**
     * Define el valor de la propiedad idenTipoproyecto.
     * 
     */
    public void setIdenTipoproyecto(int value) {
        this.idenTipoproyecto = value;
    }

    /**
     * Obtiene el valor de la propiedad idenFinancia.
     * 
     */
    public int getIdenFinancia() {
        return idenFinancia;
    }

    /**
     * Define el valor de la propiedad idenFinancia.
     * 
     */
    public void setIdenFinancia(int value) {
        this.idenFinancia = value;
    }

    /**
     * Obtiene el valor de la propiedad cantMonto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantMonto() {
        return cantMonto;
    }

    /**
     * Define el valor de la propiedad cantMonto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantMonto(String value) {
        this.cantMonto = value;
    }

}
