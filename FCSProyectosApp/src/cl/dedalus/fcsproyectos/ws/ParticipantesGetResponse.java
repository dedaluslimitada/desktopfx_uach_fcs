
package cl.dedalus.fcsproyectos.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para participantesGetResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="participantesGetResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="participantesGetResult" type="{http://ws.fcsproyectos.dedalus.cl/}ParticipantesGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "participantesGetResponse", propOrder = {
    "participantesGetResult"
})
public class ParticipantesGetResponse {

    protected ParticipantesGetResult participantesGetResult;

    /**
     * Obtiene el valor de la propiedad participantesGetResult.
     * 
     * @return
     *     possible object is
     *     {@link ParticipantesGetResult }
     *     
     */
    public ParticipantesGetResult getParticipantesGetResult() {
        return participantesGetResult;
    }

    /**
     * Define el valor de la propiedad participantesGetResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ParticipantesGetResult }
     *     
     */
    public void setParticipantesGetResult(ParticipantesGetResult value) {
        this.participantesGetResult = value;
    }

}
