
package cl.dedalus.fcsproyectos.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DocsxpryGetResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DocsxpryGetResult">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.fcsproyectos.dedalus.cl/}ProcedureResult">
 *       &lt;sequence>
 *         &lt;element name="docs" type="{http://ws.fcsproyectos.dedalus.cl/}DocsxpryGetDoc" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocsxpryGetResult", propOrder = {
    "docs"
})
public class DocsxpryGetResult
    extends ProcedureResult
{

    @XmlElement(nillable = true)
    protected List<DocsxpryGetDoc> docs;

    /**
     * Gets the value of the docs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the docs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDocs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DocsxpryGetDoc }
     * 
     * 
     */
    public List<DocsxpryGetDoc> getDocs() {
        if (docs == null) {
            docs = new ArrayList<DocsxpryGetDoc>();
        }
        return this.docs;
    }

}
