
package cl.dedalus.fcsproyectos.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para colabexternoSetResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="colabexternoSetResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="colabexternoSetResult" type="{http://ws.fcsproyectos.dedalus.cl/}ColabexternoSetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "colabexternoSetResponse", propOrder = {
    "colabexternoSetResult"
})
public class ColabexternoSetResponse {

    protected ColabexternoSetResult colabexternoSetResult;

    /**
     * Obtiene el valor de la propiedad colabexternoSetResult.
     * 
     * @return
     *     possible object is
     *     {@link ColabexternoSetResult }
     *     
     */
    public ColabexternoSetResult getColabexternoSetResult() {
        return colabexternoSetResult;
    }

    /**
     * Define el valor de la propiedad colabexternoSetResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ColabexternoSetResult }
     *     
     */
    public void setColabexternoSetResult(ColabexternoSetResult value) {
        this.colabexternoSetResult = value;
    }

}
