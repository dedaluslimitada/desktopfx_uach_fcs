
package cl.dedalus.fcsproyectos.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para participanteUpdResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="participanteUpdResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="participanteUpdResult" type="{http://ws.fcsproyectos.dedalus.cl/}ParticipanteUpdResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "participanteUpdResponse", propOrder = {
    "participanteUpdResult"
})
public class ParticipanteUpdResponse {

    protected ParticipanteUpdResult participanteUpdResult;

    /**
     * Obtiene el valor de la propiedad participanteUpdResult.
     * 
     * @return
     *     possible object is
     *     {@link ParticipanteUpdResult }
     *     
     */
    public ParticipanteUpdResult getParticipanteUpdResult() {
        return participanteUpdResult;
    }

    /**
     * Define el valor de la propiedad participanteUpdResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ParticipanteUpdResult }
     *     
     */
    public void setParticipanteUpdResult(ParticipanteUpdResult value) {
        this.participanteUpdResult = value;
    }

}
