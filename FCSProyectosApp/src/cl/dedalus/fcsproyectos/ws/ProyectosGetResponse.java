
package cl.dedalus.fcsproyectos.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para proyectosGetResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="proyectosGetResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="proyectosGetResult" type="{http://ws.fcsproyectos.dedalus.cl/}ProyectosGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "proyectosGetResponse", propOrder = {
    "proyectosGetResult"
})
public class ProyectosGetResponse {

    protected ProyectosGetResult proyectosGetResult;

    /**
     * Obtiene el valor de la propiedad proyectosGetResult.
     * 
     * @return
     *     possible object is
     *     {@link ProyectosGetResult }
     *     
     */
    public ProyectosGetResult getProyectosGetResult() {
        return proyectosGetResult;
    }

    /**
     * Define el valor de la propiedad proyectosGetResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ProyectosGetResult }
     *     
     */
    public void setProyectosGetResult(ProyectosGetResult value) {
        this.proyectosGetResult = value;
    }

}
