
package cl.dedalus.fcsproyectos.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para responsableUpd complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="responsableUpd">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idParticipante" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="descResp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "responsableUpd", propOrder = {
    "idParticipante",
    "descResp"
})
public class ResponsableUpd {

    protected long idParticipante;
    protected String descResp;

    /**
     * Obtiene el valor de la propiedad idParticipante.
     * 
     */
    public long getIdParticipante() {
        return idParticipante;
    }

    /**
     * Define el valor de la propiedad idParticipante.
     * 
     */
    public void setIdParticipante(long value) {
        this.idParticipante = value;
    }

    /**
     * Obtiene el valor de la propiedad descResp.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescResp() {
        return descResp;
    }

    /**
     * Define el valor de la propiedad descResp.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescResp(String value) {
        this.descResp = value;
    }

}
