
package cl.dedalus.fcsproyectos.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para documentoGet complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="documentoGet">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idenDocumento" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "documentoGet", propOrder = {
    "idenDocumento"
})
public class DocumentoGet {

    protected int idenDocumento;

    /**
     * Obtiene el valor de la propiedad idenDocumento.
     * 
     */
    public int getIdenDocumento() {
        return idenDocumento;
    }

    /**
     * Define el valor de la propiedad idenDocumento.
     * 
     */
    public void setIdenDocumento(int value) {
        this.idenDocumento = value;
    }

}
