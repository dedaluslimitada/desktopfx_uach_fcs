
package cl.dedalus.fcsproyectos.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para docsxpryGetResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="docsxpryGetResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="docsxpryGetResult" type="{http://ws.fcsproyectos.dedalus.cl/}DocsxpryGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "docsxpryGetResponse", propOrder = {
    "docsxpryGetResult"
})
public class DocsxpryGetResponse {

    protected DocsxpryGetResult docsxpryGetResult;

    /**
     * Obtiene el valor de la propiedad docsxpryGetResult.
     * 
     * @return
     *     possible object is
     *     {@link DocsxpryGetResult }
     *     
     */
    public DocsxpryGetResult getDocsxpryGetResult() {
        return docsxpryGetResult;
    }

    /**
     * Define el valor de la propiedad docsxpryGetResult.
     * 
     * @param value
     *     allowed object is
     *     {@link DocsxpryGetResult }
     *     
     */
    public void setDocsxpryGetResult(DocsxpryGetResult value) {
        this.docsxpryGetResult = value;
    }

}
