
package cl.dedalus.fcsproyectos.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para participanteDel complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="participanteDel">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idParticipante" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "participanteDel", propOrder = {
    "idParticipante"
})
public class ParticipanteDel {

    protected long idParticipante;

    /**
     * Obtiene el valor de la propiedad idParticipante.
     * 
     */
    public long getIdParticipante() {
        return idParticipante;
    }

    /**
     * Define el valor de la propiedad idParticipante.
     * 
     */
    public void setIdParticipante(long value) {
        this.idParticipante = value;
    }

}
