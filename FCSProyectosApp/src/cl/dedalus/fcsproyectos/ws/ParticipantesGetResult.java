
package cl.dedalus.fcsproyectos.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ParticipantesGetResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ParticipantesGetResult">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.fcsproyectos.dedalus.cl/}ProcedureResult">
 *       &lt;sequence>
 *         &lt;element name="partis" type="{http://ws.fcsproyectos.dedalus.cl/}ParticipantesGetParti" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ParticipantesGetResult", propOrder = {
    "partis"
})
public class ParticipantesGetResult
    extends ProcedureResult
{

    @XmlElement(nillable = true)
    protected List<ParticipantesGetParti> partis;

    /**
     * Gets the value of the partis property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the partis property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPartis().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ParticipantesGetParti }
     * 
     * 
     */
    public List<ParticipantesGetParti> getPartis() {
        if (partis == null) {
            partis = new ArrayList<ParticipantesGetParti>();
        }
        return this.partis;
    }

}
