
package cl.dedalus.fcsproyectos.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ParamGetTipopry complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ParamGetTipopry">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="tipoproyecto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ParamGetTipopry", propOrder = {
    "tipoproyecto"
})
public class ParamGetTipopry {

    protected String tipoproyecto;

    /**
     * Obtiene el valor de la propiedad tipoproyecto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoproyecto() {
        return tipoproyecto;
    }

    /**
     * Define el valor de la propiedad tipoproyecto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoproyecto(String value) {
        this.tipoproyecto = value;
    }

}
