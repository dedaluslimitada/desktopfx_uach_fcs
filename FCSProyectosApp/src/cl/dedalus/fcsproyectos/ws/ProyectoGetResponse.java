
package cl.dedalus.fcsproyectos.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para proyectoGetResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="proyectoGetResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="proyectoGetResult" type="{http://ws.fcsproyectos.dedalus.cl/}ProyectoGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "proyectoGetResponse", propOrder = {
    "proyectoGetResult"
})
public class ProyectoGetResponse {

    protected ProyectoGetResult proyectoGetResult;

    /**
     * Obtiene el valor de la propiedad proyectoGetResult.
     * 
     * @return
     *     possible object is
     *     {@link ProyectoGetResult }
     *     
     */
    public ProyectoGetResult getProyectoGetResult() {
        return proyectoGetResult;
    }

    /**
     * Define el valor de la propiedad proyectoGetResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ProyectoGetResult }
     *     
     */
    public void setProyectoGetResult(ProyectoGetResult value) {
        this.proyectoGetResult = value;
    }

}
