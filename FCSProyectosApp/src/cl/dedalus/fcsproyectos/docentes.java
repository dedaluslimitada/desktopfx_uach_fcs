package cl.dedalus.fcsproyectos;

import javafx.beans.property.SimpleStringProperty;

public class docentes{
    private final SimpleStringProperty codi_docente;
    private final SimpleStringProperty nomb_docente;
    private final SimpleStringProperty orig_docente;

    public docentes(final String codi_docente, final String nomb_docente, final String orig_docente){
        this.codi_docente = new SimpleStringProperty(codi_docente);
        this.nomb_docente = new SimpleStringProperty(nomb_docente);
        this.orig_docente = new SimpleStringProperty(orig_docente);
    }

    public String getCodi_docente() {
        return codi_docente.get();
    }
    public String getNomb_docente() {
        return nomb_docente.get();
    }
    public String getOrig_docente() {
        return orig_docente.get();
    }

}
