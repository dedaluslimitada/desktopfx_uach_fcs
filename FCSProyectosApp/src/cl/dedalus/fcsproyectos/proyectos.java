package cl.dedalus.fcsproyectos;

import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;

public class proyectos{
    private final SimpleLongProperty id;
    private final SimpleStringProperty codigo;
    private final SimpleStringProperty titulo;
    private final SimpleStringProperty estado;


    public proyectos(final Long id, final String codigo, final String titulo, final String estado){
        this.id = new SimpleLongProperty(id);
        this.codigo = new SimpleStringProperty(codigo);
        this.titulo = new SimpleStringProperty(titulo);
        this.estado = new SimpleStringProperty(estado);
    }

    public Long getId() {
        return id.get();
    }
    public String getCodigo() {
        return codigo.get();
    }
    public String getTitulo() {
        return titulo.get();
    }
    public String getEstado() {
        return estado.get();
    }
}
