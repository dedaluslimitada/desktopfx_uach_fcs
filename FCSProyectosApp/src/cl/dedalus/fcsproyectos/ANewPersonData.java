/*
 * Copyright (c) DEDALUS Limitada. All rights reserved.
 *
 * All rights to this product are owned by DEDALUS Limitada and may only be used
 * under the terms of its associated license document. You may NOT copy, modify,
 * sublicense,  or  distribute  this  source  file  or  portions  of  it  unless
 * previously authorized in writing by  DEDALUS  Limitada. In  any  event,  this
 * notice and above copyright must always be included verbatim with this file.
 */

package cl.dedalus.fcsproyectos;

/**
 * @author Aulloa
 */
public class ANewPersonData {
    //variables visibility set to private
    private String userP;
    private String nombresP;
    private String apellidosP;
    private String profesionP;
    private String rutP;
    private String mailP;
    private String fonoP;

    //constructor takes String and Integer as parameters
    public ANewPersonData(){
        this.userP="";
        this.nombresP="";
        this.apellidosP="";
        this.profesionP="";
        this.rutP="";
        this.mailP="";
        this.fonoP="";
    }

    //public method that gets/set
    public String getUserP(){
        return userP;
    }
    public void setUserP(final String user){
        this.userP=user;
    }

    //public method that gets/set
    public String getNombresP(){
        return nombresP;
    }
    public void setNombresP(final String name){
        this.nombresP=name;
    }

    //public method that gets/set
    public String getApellidosP(){
        return apellidosP;
    }
    public void setApellidosP(final String apellidos){
        this.apellidosP=apellidos;
    }

    //public method that gets/set
    public String getProfesionP(){
        return profesionP;
    }
    public void setProfesionP(final String profesion){
        this.profesionP=profesion;
    }
    
    //public method that gets/set
    public String getRutP(){
        return rutP;
    }
    public void setRutP(final String irut){
        this.rutP=irut;
    }
    
    //public method that gets/set
    public String getMailP(){
        return mailP;
    }
    public void setMailP(final String imail){
        this.mailP=imail;
    }
    
    //public method that gets/set
    public String getFonoP(){
        return fonoP;
    }
    public void setFonoP(final String ifono){
        this.fonoP=ifono;
    }
}
