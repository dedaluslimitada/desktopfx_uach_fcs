package cl.dedalus.fcsproyectos;

import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;

public class colabs{
    private final SimpleLongProperty id;
    private final SimpleStringProperty codi_colab;
    private final SimpleStringProperty nomb_colab;
    private final SimpleStringProperty orig_colab ;
    private final SimpleStringProperty flag_responsable;
    private final SimpleStringProperty cant_semanas_f;
    private final SimpleStringProperty cant_hrsxsem_f;
    private final SimpleStringProperty cant_total_f;
    private final SimpleStringProperty cant_semanas_e;
    private final SimpleStringProperty cant_hrsxsem_e;
    private final SimpleStringProperty cant_total_e;


    public colabs(final Long id, final String codi_colab,final String nomb_colab, final String orig_colab
    				, String flag_responsable, final String cant_semanas_f, final String cant_hrsxsem_f , final String cant_total_f
    				, final String cant_semanas_e, final String cant_hrsxsem_e,final String cant_total_e){
        this.id = new SimpleLongProperty(id);
        this.codi_colab = new SimpleStringProperty(codi_colab);
        this.nomb_colab = new SimpleStringProperty(nomb_colab);
        this.orig_colab = new SimpleStringProperty(orig_colab);
        this.flag_responsable = new SimpleStringProperty(flag_responsable);
        this.cant_semanas_f = new SimpleStringProperty(cant_semanas_f);
        this.cant_hrsxsem_f = new SimpleStringProperty(cant_hrsxsem_f);
        this.cant_total_f = new SimpleStringProperty(cant_total_f);
        this.cant_semanas_e = new SimpleStringProperty(cant_semanas_e);
        this.cant_hrsxsem_e = new SimpleStringProperty(cant_hrsxsem_e);
        this.cant_total_e = new SimpleStringProperty(cant_total_e);
    }

    public Long getId() {
        return id.get();
    }

    public String getCodi_colab() {
        return codi_colab.get();
    }

    public String getNomb_colab() {
        return nomb_colab.get();
    }

    public String getOrig_colab() {
        return orig_colab.get();
    }
    
    public String getFlag_responsable() {
        return flag_responsable.get();
    }
  //-------------------------------------------------------------
    public String getCant_semanas_f(){
        return cant_semanas_f.get();
    }
    public void setCant_semanas_f(final String isem_f) {
        cant_semanas_f.set(isem_f);
    }
    public SimpleStringProperty cant_semanas_fProperty(){
        return cant_semanas_f;
    }
  //-------------------------------------------------------------
    public String getCant_hrsxsem_f(){
        return cant_hrsxsem_f.get();
    }
    public void setCant_hrsxsem_f(final String ihrs_f) {
        cant_hrsxsem_f.set(ihrs_f);
    }
    public SimpleStringProperty cant_hrsxsem_fProperty(){
        return cant_hrsxsem_f;
    }
//-------------------------------------------------------------
    public String getCant_total_f(){
        return cant_total_f.get();
    }
    public void setCant_total_f(final String itot_f) {
        cant_total_f.set(itot_f);
    }
    public SimpleStringProperty cant_total_fProperty(){
        return cant_total_f;
    }
  //-------------------------------------------------------------
    public String getCant_semanas_e(){
        return cant_semanas_e.get();
    }
    public void setCant_semanas_e(final String isem_e) {
        cant_semanas_e.set(isem_e);
    }

    public String getCant_hrsxsem_e(){
        return cant_hrsxsem_e.get();
    }
    public void setCant_hrsxsem_e(final String ihrs_e) {
        cant_hrsxsem_e.set(ihrs_e);
    }

    public String getCant_total_e(){
        return cant_total_e.get();
    }
    public void setCant_total_e(final String itot_e) {
        cant_total_e.set(itot_e);
    }
    public SimpleStringProperty cant_total_eProperty(){
        return cant_total_e;
    }
}
