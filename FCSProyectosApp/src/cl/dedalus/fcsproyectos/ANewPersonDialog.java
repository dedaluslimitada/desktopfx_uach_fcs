/*
 * Copyright (c) OBCOM INGENIERIA S.A. (Chile). All rights reserved.
 *
 * All rights to this product are owned by OBCOM INGENIERIA S.A. and may only be
 * used  under  the  terms of its associated license document. You may NOT copy,
 * modify, sublicense, or distribute this source file or portions of  it  unless
 * previously  authorized in writing by OBCOM INGENIERIA S.A. In any event, this
 * notice and above copyright must always be included verbatim with this file.
 */

package cl.dedalus.fcsproyectos;

import cl.obcom.desktopfx.core.DesktopTask;
import cl.obcom.desktopfx.jfx.Dialog;
import javafx.event.ActionEvent;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import javafx.stage.WindowEvent;

/**
 * Implements the NetSecurity Search Audit Dialog.
 *
 * @author Eduardo Ostertag
 */
final class ANewPersonDialog extends Stage {
    private final TextField nombresText;
    private final TextField apellidosText;
    private final TextField profesionText;
    private final TextField userText;
    private final TextField rutText;
    private final TextField fonoText;
    private final TextField mailText;
    private final ANewPersonData personData;

    private boolean cancel;

    //--------------------------------------------------------------------------
    //-- Constructor Methods ---------------------------------------------------
    //--------------------------------------------------------------------------

    /**
     * Creates the new {@code AuditDialog} instance.
     *
     * @param  task the desktop task instance.
     * @param  fromDate the initial from date.
     * @param  toDate the initial to date.
     * @throws NullPointerException if an argument is {@code null}.
     */
    private ANewPersonDialog(final DesktopTask task, final ANewPersonData data)
    {
        // Check supplied arguments
        if (task == null)
            throw new NullPointerException("task is null");
        if (data == null)
            throw new NullPointerException("data is null");

        // Save supplied arguments
        this.cancel = true;
        this.personData = data;

        // Initialize dialog properties
        setResizable(false);
        initOwner(task.getStage());
        initStyle(StageStyle.UTILITY);
        setOnShown(e -> onDialogShown(e));
        setTitle("Ingresar Participante Externo");
        initModality(Modality.APPLICATION_MODAL);


        // Create and initialize root rootVBox
        final VBox rootVBox = new VBox(10);
        {
            // Initialize rootVBox properties
            rootVBox.setPadding(new Insets(10, 10, 10, 10));

            // Create and initialize centerGrid
            final GridPane centerGrid = new GridPane();
            {
                // Initialize centerGrid properties
                centerGrid.setHgap(5);
                centerGrid.setVgap(5);
                // Create and initialize Rut --------------------------------------
                final Label rutLabel = new Label("Rut");
                {
                    GridPane.setHalignment(rutLabel, HPos.RIGHT);
                }
                centerGrid.add(rutLabel, 0, 0);
                // Textfield participante code
                rutText = new TextField("Rut");
                {
                    rutText.setPrefColumnCount(20);
                    GridPane.setHgrow(rutText, Priority.ALWAYS);
                }
                centerGrid.add(rutText, 1, 0);
                
                
                // Create and initialize Username --------------------------------------
                final Label fromDateLabel = new Label("Username");
                {
                    GridPane.setHalignment(fromDateLabel, HPos.RIGHT);
                }
                centerGrid.add(fromDateLabel, 0, 1);
                // Textfield participante code
                userText = new TextField("Username");
                {
                    userText.setPrefColumnCount(20);
                    GridPane.setHgrow(userText, Priority.ALWAYS);
                }
                centerGrid.add(userText, 1, 1);

                // Create and initialize campo nombres --------------------------------------
                final Label nombresLabel = new Label("Nombres");
                {
                    GridPane.setHalignment(nombresLabel, HPos.RIGHT);
                }
                centerGrid.add(nombresLabel, 0, 2);
                nombresText = new TextField("Nombres");
                {
                    userText.setPrefColumnCount(20);
                    GridPane.setHgrow(nombresText, Priority.ALWAYS);
                }
                centerGrid.add(nombresText, 1, 2);

                // Create and initialize ApellidosLabel --------------------------------------
                final Label apellidosLabel = new Label("Apellidos");
                {
                    GridPane.setHalignment(apellidosLabel, HPos.RIGHT);
                }
                centerGrid.add(apellidosLabel, 0, 3);
               // Create and initialize apellidosText
                apellidosText = new TextField("Apellidos");
                {
                    apellidosText.setPrefColumnCount(20);
                    GridPane.setHgrow(apellidosText, Priority.ALWAYS);
                }
                centerGrid.add(apellidosText, 1, 3);

                // Create and initialize profesionLabel ----------------------------------------
                final Label profesionLabel = new Label("Profesion");
                {
                    GridPane.setHalignment(profesionLabel, HPos.RIGHT);
                }
                centerGrid.add(profesionLabel, 0, 4);
                profesionText = new TextField("Profesion");
                {
                    profesionText.setPrefColumnCount(20);
                    GridPane.setHgrow(profesionText, Priority.ALWAYS);
                }
                centerGrid.add(profesionText, 1, 4);
                
                // Create and initialize Fono ----------------------------------------
                final Label fonoLabel = new Label("Fono");
                {
                    GridPane.setHalignment(fonoLabel, HPos.RIGHT);
                }
                centerGrid.add(fonoLabel, 0, 5);
                fonoText = new TextField("Fono");
                {
                    fonoText.setPrefColumnCount(20);
                    GridPane.setHgrow(fonoText, Priority.ALWAYS);
                }
                centerGrid.add(fonoText, 1, 5);
            
             // Create and initialize Mail ----------------------------------------
                final Label mailLabel = new Label("Mail");
                {
                    GridPane.setHalignment(mailLabel, HPos.RIGHT);
                }
                centerGrid.add(mailLabel, 0, 6);
                mailText = new TextField("Mail");
                {
                    mailText.setPrefColumnCount(20);
                    GridPane.setHgrow(mailText, Priority.ALWAYS);
                }
                centerGrid.add(mailText, 1, 6);
            }
            rootVBox.getChildren().add(centerGrid);

            // Create and initialize buttonsHBox
            final HBox buttonsHBox = new HBox(5);
            {
                // Initialize buttonsHBox properties
                buttonsHBox.setAlignment(Pos.CENTER_RIGHT);

                // Create and initialize acceptButton
                final Button acceptButton = new Button(("Aceptar"));
                {
                    acceptButton.setOnAction(e -> onAcceptButtonClick(e));
                    acceptButton.setDefaultButton(true);
                    acceptButton.setMinWidth(70);
                }
                buttonsHBox.getChildren().add(acceptButton);

                // Create and initialize cancelButton
                final Button cancelButton = new Button(("Cancelar"));
                {
                    cancelButton.setOnAction(e -> onCancelButtonClick(e));
                    cancelButton.setCancelButton(true);
                    cancelButton.setMinWidth(70);
                }
                buttonsHBox.getChildren().add(cancelButton);
            }
            rootVBox.getChildren().add(buttonsHBox);
        }

        // Create and initialize scene
        setScene(new Scene(rootVBox));
        sizeToScene();
    }

    //--------------------------------------------------------------------------
    //-- Handler Methods -------------------------------------------------------
    //--------------------------------------------------------------------------

    /**
     * Called just after the Window is shown: center to owner.
     *
     * @param event describes the window event.
     */
    private void onDialogShown(final WindowEvent event)
    {
        final Window owner = getOwner();
        setX(owner.getX() + ((owner.getWidth() - getWidth()) / 2));
        setY(owner.getY() + ((owner.getHeight() - getHeight()) / 2));
    }

    /**
     * Called when the "Accept" button is clicked.
     *
     * @param event describes the action event.
     */
    private void onAcceptButtonClick(final ActionEvent event)
    {
        try {
            // Obtain data
            this.personData.setUserP(userText.getText().toUpperCase().trim());
            this.personData.setNombresP(nombresText.getText().toString().trim());
            this.personData.setApellidosP(apellidosText.getText().toString().trim());
            this.personData.setProfesionP(profesionText.getText().trim());
            this.personData.setRutP(rutText.getText().trim());
            this.personData.setFonoP(fonoText.getText().trim());
            this.personData.setMailP(mailText.getText().trim());
            // Accept and clode dialog
            cancel = false;
            hide();
        } catch (final Throwable thrown) {
            Dialog.showError(this, thrown);
        }
    }

    /**
     * Called when the "Cancel" button is clicked.
     *
     * @param event describes the action event.
     */
    private void onCancelButtonClick(final ActionEvent event)
    {
        cancel = true;
        hide();
    }

    //--------------------------------------------------------------------------
    //-- AuditDialog Methods ---------------------------------------------------
    //--------------------------------------------------------------------------
    /**
     * Returns {@code true} if the dialog was accepted.
     *
     * @return {@code true} if the dialog was accepted.
     */
    private boolean getAccepted()
    {
        return !cancel;
    }

    //--------------------------------------------------------------------------
    //-- Static Methods --------------------------------------------------------
    //--------------------------------------------------------------------------

    /**
     * Displays the dialog and returns the accepted message.
     *
     * @param  task the desktop task instance.
     * @param  auditData the initial values of the filters.
     * @return {@code true} if the dialog was accepted.
     * @throws NullPointerException if an argument is {@code null}.
     */
    static boolean show(final DesktopTask task, final ANewPersonData adata)
    {
        final ANewPersonDialog dialog = new ANewPersonDialog(task,adata);
        dialog.showAndWait();
        return dialog.getAccepted();
    }
}
