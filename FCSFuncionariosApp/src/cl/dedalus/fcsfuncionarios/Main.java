package cl.dedalus.fcsfuncionarios;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import cl.dedalus.fcsfuncionarios.ws.FacultadesGetFac;
import cl.dedalus.fcsfuncionarios.ws.FacultadesGetResult;
import cl.dedalus.fcsfuncionarios.ws.FcsfuncionariosWeb;
import cl.dedalus.fcsfuncionarios.ws.FcsfuncionariosWebException;
import cl.dedalus.fcsfuncionarios.ws.FcsfuncionariosWebService;
import cl.dedalus.fcsfuncionarios.ws.FuncionarioGetResult;
import cl.dedalus.fcsfuncionarios.ws.FuncionariosGetFunc;
import cl.dedalus.fcsfuncionarios.ws.FuncionariosGetResult;
import cl.dedalus.fcsfuncionarios.ws.GradosdocenteGetGrado;
import cl.dedalus.fcsfuncionarios.ws.GradosdocenteGetResult;
import cl.dedalus.fcsfuncionarios.ws.HistoriadocGetHistoria;
import cl.dedalus.fcsfuncionarios.ws.HistoriadocGetResult;
import cl.dedalus.fcsfuncionarios.ws.InstitutosGetInstit;
import cl.dedalus.fcsfuncionarios.ws.InstitutosGetResult;
import cl.dedalus.fcsfuncionarios.ws.ParamGetCategoria;
import cl.dedalus.fcsfuncionarios.ws.ParamGetContrato;
import cl.dedalus.fcsfuncionarios.ws.ParamGetEdificio;
import cl.dedalus.fcsfuncionarios.ws.ParamGetFuncionario;
import cl.dedalus.fcsfuncionarios.ws.ParamGetGrado;
import cl.dedalus.fcsfuncionarios.ws.ParamGetResult;
import cl.obcom.desktopfx.core.DesktopTask;
import cl.obcom.desktopfx.jfx.Dialog;
import cl.obcom.desktopfx.jfx.DialogAction;
import cl.obcom.desktopfx.jfx.FxmlPane;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;

public class Main extends FxmlPane
{
    @FXML TableView<funcionario> tb_funcionarios = new TableView<>();
    @FXML TableView<gradosxdocente> tb_gradosxdocente = new TableView<>();
    @FXML TableView<grado> tb_grados = new TableView<>();
    @FXML TableView<historiadoc> tb_historiadoc = new TableView<>();
    @FXML private ComboBox<String> cb_facultad;
    @FXML private ComboBox<String> cb_instituto;
    @FXML private ComboBox<String> cb_tipofunc;
    @FXML private ComboBox<String> cb_tipocont;
    @FXML private ComboBox<String> cb_edificio;
    @FXML private ComboBox<String> cb_categoria;
    @FXML private ComboBox<String> cb_estado;
    @FXML private Label lb_grados;
    @FXML private Button bo_salir;
    @FXML private RadioButton rb_masculino;
    @FXML private RadioButton rb_femenino;
    @FXML private TextField tx_codigo;
    @FXML private TextField tx_rut;
    @FXML private TextField tx_nombres;
    @FXML private TextField tx_paterno;
    @FXML private TextField tx_materno;
    @FXML private TextField tx_email;
    @FXML private TextField tx_telefono;
    @FXML private TextField tx_anexo;
    @FXML private TextField tx_oficina;
    @FXML private DatePicker dp_fecha_ini;
    @FXML private DatePicker dp_fecha_fin;
    @FXML private DatePicker dp_fecha_nac;
    @FXML private Button bo_ingresar;
    @FXML private Button bo_modificar;
    @FXML private Button bo_limpiar;
    @FXML private Button bo_mas;
    @FXML private Button bo_menos;
    @FXML private CheckBox ck_prodecano;
    @FXML private CheckBox ck_decano;
    @FXML private CheckBox ck_secretario;

    final FcsfuncionariosWeb port;

    final ObservableList<funcionario> dataFuncionario = FXCollections.observableArrayList ();
    final ObservableList<String> dataFacultad = FXCollections.observableArrayList ();
    final ObservableList<String> dataEstado = FXCollections.observableArrayList ();
    final ObservableList<String> dataEdificio = FXCollections.observableArrayList ();
    final ObservableList<String> dataCategoria = FXCollections.observableArrayList ();
    final ObservableList<String> dataInstituto = FXCollections.observableArrayList ();
    final ObservableList<String> dataTipoFuncionario = FXCollections.observableArrayList ();
    final ObservableList<String> dataTipoContrato = FXCollections.observableArrayList ();
    final ObservableList<grado> dataGrado = FXCollections.observableArrayList ();
    final ObservableList<gradosxdocente> dataGradosxdocente = FXCollections.observableArrayList ();
    final ObservableList<historiadoc> dataHistoriadoc = FXCollections.observableArrayList ();

    public Main(final DesktopTask task) throws Exception
    {
        // Se carga fxml
        loadFxml();
        task.getStage().getScene().getStylesheets().add("resources/css/NuevoEstilo.css");

      //Inicializar WS
        final FcsfuncionariosWebService service = new FcsfuncionariosWebService();
        port = service.getFcsfuncionariosWebPort();
        task.initWebServicePort(port,service,task.getCodeBase());

        //Init Grillas tb_funcionarios
        final TableColumn<funcionario, String> tc_codi_fun = new TableColumn<>("Username");
        tc_codi_fun.setCellValueFactory(new PropertyValueFactory<funcionario, String>("codi_fun"));
        tb_funcionarios.getColumns().add(tc_codi_fun);
        final TableColumn<funcionario, String> tc_codi_rut = new TableColumn<>("Rut");
        tc_codi_rut.setCellValueFactory(new PropertyValueFactory<funcionario, String>("rut_fun"));
        tb_funcionarios.getColumns().add(tc_codi_rut);
        final TableColumn<funcionario, String> tc_desc_nom = new TableColumn<>("Nombre(s)");
        tc_desc_nom.setCellValueFactory(new PropertyValueFactory<funcionario, String>("desc_nom"));
        tb_funcionarios.getColumns().add(tc_desc_nom);
        final TableColumn<funcionario, String> tc_desc_pat = new TableColumn<>("A. Paterno");
        tc_desc_pat.setCellValueFactory(new PropertyValueFactory<funcionario, String>("desc_pat"));
        tb_funcionarios.getColumns().add(tc_desc_pat);
        final TableColumn<funcionario, String> tc_tipo_fun = new TableColumn<>("Funcionario");
        tc_tipo_fun.setCellValueFactory(new PropertyValueFactory<funcionario, String>("tipo_fun"));
        tb_funcionarios.getColumns().add(tc_tipo_fun);
        final TableColumn<funcionario, String> tc_tipo_con = new TableColumn<>("Contrato");
        tc_tipo_con.setCellValueFactory(new PropertyValueFactory<funcionario, String>("tipo_con"));
        tb_funcionarios.getColumns().add(tc_tipo_con);
        tb_funcionarios.setItems(dataFuncionario);
        tc_codi_fun.prefWidthProperty().bind(tb_funcionarios.widthProperty().multiply(0.15));
        tc_codi_rut.prefWidthProperty().bind(tb_funcionarios.widthProperty().multiply(0.15));
        tc_desc_nom.prefWidthProperty().bind(tb_funcionarios.widthProperty().multiply(0.15));
        tc_desc_pat.prefWidthProperty().bind(tb_funcionarios.widthProperty().multiply(0.15));
        tc_tipo_fun.prefWidthProperty().bind(tb_funcionarios.widthProperty().multiply(0.18));
        tc_tipo_con.prefWidthProperty().bind(tb_funcionarios.widthProperty().multiply(0.18));

        //Init Grillas tb_grados
        final TableColumn<grado, String> tc_codi_grado = new TableColumn<>("Cod");
        tc_codi_grado.setCellValueFactory(new PropertyValueFactory<grado, String>("codi_grado"));
        tb_grados.getColumns().add(tc_codi_grado);
        final TableColumn<grado, String> tc_desc_grado = new TableColumn<>("Descripcion Grado");
        tc_desc_grado.setCellValueFactory(new PropertyValueFactory<grado, String>("desc_grado"));
        tb_grados.getColumns().add(tc_desc_grado);
        tb_grados.setItems(dataGrado);
        tc_codi_grado.prefWidthProperty().bind(tb_grados.widthProperty().multiply(0.15));
        tc_desc_grado.prefWidthProperty().bind(tb_grados.widthProperty().multiply(0.80));

        //Init Grillas tb_gradosxdocente
        final TableColumn<gradosxdocente, String> tc_codi_grado_1 = new TableColumn<>("Cod");
        tc_codi_grado_1.setCellValueFactory(new PropertyValueFactory<gradosxdocente, String>("codi_grado"));
        tb_gradosxdocente.getColumns().add(tc_codi_grado_1);
        final TableColumn<gradosxdocente, String> tc_desc_grado_1 = new TableColumn<>("Descripcion Grado");
        tc_desc_grado_1.setCellValueFactory(new PropertyValueFactory<gradosxdocente, String>("desc_grado"));
        tb_gradosxdocente.getColumns().add(tc_desc_grado_1);
        tb_gradosxdocente.setItems(dataGradosxdocente);
        tc_codi_grado_1.prefWidthProperty().bind(tb_gradosxdocente.widthProperty().multiply(0.15));
        tc_desc_grado_1.prefWidthProperty().bind(tb_gradosxdocente.widthProperty().multiply(0.80));

        //Init Grillas tb_historiadoc
        final TableColumn<historiadoc, String> tc_tipofunc = new TableColumn<>("Funcionario");
        tc_tipofunc.setCellValueFactory(new PropertyValueFactory<historiadoc, String>("desc_tipofunc"));
        tb_historiadoc.getColumns().add(tc_tipofunc);
        final TableColumn<historiadoc, String> tc_tipocont = new TableColumn<>("Contrato");
        tc_tipocont.setCellValueFactory(new PropertyValueFactory<historiadoc, String>("desc_tipocont"));
        tb_historiadoc.getColumns().add(tc_tipocont);
        final TableColumn<historiadoc, String> tc_fechini = new TableColumn<>("Inicio");
        tc_fechini.setCellValueFactory(new PropertyValueFactory<historiadoc, String>("fech_ini"));
        tb_historiadoc.getColumns().add(tc_fechini);
        final TableColumn<historiadoc, String> tc_fechfin = new TableColumn<>("Fin");
        tc_fechfin.setCellValueFactory(new PropertyValueFactory<historiadoc, String>("fech_fin"));
        tb_historiadoc.getColumns().add(tc_fechfin);
        tb_historiadoc.setItems(dataHistoriadoc);
        tc_tipofunc.prefWidthProperty().bind(tb_historiadoc.widthProperty().multiply(0.25));
        tc_tipocont.prefWidthProperty().bind(tb_historiadoc.widthProperty().multiply(0.25));
        tc_fechini.prefWidthProperty().bind(tb_historiadoc.widthProperty().multiply(0.25));
        tc_fechfin.prefWidthProperty().bind(tb_historiadoc.widthProperty().multiply(0.25));
        
        final ToggleGroup group = new ToggleGroup();
        rb_masculino.setToggleGroup(group);
        rb_femenino.setToggleGroup(group);
        rb_masculino.setSelected(true);

        
        ck_decano.selectedProperty().addListener((ChangeListener<Boolean>) (observable, oldValue, newValue) -> {
            if (newValue){
                ck_prodecano.setSelected(false);
                ck_secretario.setSelected(false);
            }
        });
        ck_prodecano.selectedProperty().addListener((ChangeListener<Boolean>) (observable, oldValue, newValue) -> {
            if (newValue){
                ck_decano.setSelected(false);
                ck_secretario.setSelected(false);
            }
        });
        ck_secretario.selectedProperty().addListener((ChangeListener<Boolean>) (observable, oldValue, newValue) -> {
            if (newValue){
                ck_decano.setSelected(false);
                ck_prodecano.setSelected(false);
            }
        });

        // click en Facultades, debo traer instituso
        cb_facultad.setOnAction((event) -> {
            cargaInstituto(cb_facultad.getValue().toString());
            limpiarCampos();
            cargaFuncionarios();
        });

        // click en Instituos,
        cb_instituto.setOnAction((event) -> {
            limpiarCampos();
            cargaFuncionarios();
        });


        //Click on tb_funcionarios
        tb_funcionarios.setOnMouseClicked(event -> {
            final funcionario t = dataFuncionario.get(tb_funcionarios.getSelectionModel().getSelectedIndex());
            lb_grados.setText("Grados Academicos de : "+t.getDesc_pat()+", "+t.getDesc_nom());
            cargaFuncionario(t.getCodi_fun());
            cargaGradosDocente(t.getCodi_fun());
            cargaHistoria(t.getCodi_fun());
        });

        /*//Valido rut en lostfocus
        tx_rut.focusedProperty().addListener((ChangeListener<Boolean>) (arg0, oldPropertyValue, newPropertyValue) -> {
            if (!newPropertyValue)
            {
                if (!tx_rut.getText().equals("")) {
                    if (!validaRut(tx_rut.getText().toString().trim())){
                        Dialog.showMessage(this, "Error en Rut.");
                        tx_rut.setText("");
                    }
                }
            }
        });*/

        dp_fecha_ini.setValue(LocalDate.now());
        dp_fecha_nac.setValue(LocalDate.of(2000, 1, 1));

        cargaFacultad();
        cargaInstituto(cb_facultad.getValue());
        cargaParams();
        cargaFuncionarios();

        bo_ingresar.setOnAction(evt -> ingresarFuncionario(task));
        bo_mas.setOnAction(evt -> asociarGrado());
        bo_menos.setOnAction(evt -> eliminarGradoDocente());
        bo_limpiar.setOnAction(evt -> limpiarCamposGral());
        bo_modificar.setOnAction(evt -> updateFuncionario());
        bo_salir.setOnAction(evt -> task.terminate(true));
        

    }

    public void cargaParams(){
        ParamGetResult res = null;
        try {
            res=port.paramGet();
            //---------->>>>Tipos de Funcionario
            dataTipoFuncionario.clear();
            for (final ParamGetFuncionario p : res.getFuncionarios()){
                dataTipoFuncionario.add(p.getTipofunc());
            }
            cb_tipofunc.setItems(dataTipoFuncionario);
            cb_tipofunc.getSelectionModel().select(0);  // Set valor por defecto

            //---------->>>>Tipos de Contrato
            dataTipoContrato.clear();
            for (final ParamGetContrato p : res.getContratos()){
                dataTipoContrato.add(p.getTipocont());
            }
            cb_tipocont.setItems(dataTipoContrato);
            cb_tipocont.getSelectionModel().select(0);  // Set valor por defecto

            //---------->>>>Edificios
            dataEdificio.clear();
            for (final ParamGetEdificio p : res.getEdificios()){
                dataEdificio.add(p.getEdificio());
            }
            cb_edificio.setItems(dataEdificio);
            cb_edificio.getSelectionModel().select(0);  // Set valor por defecto

            //---------->>>>Categorias
            dataCategoria.clear();
            for (final ParamGetCategoria p : res.getCategorias()){
                dataCategoria.add(p.getCategoria());
            }
            cb_categoria.setItems(dataCategoria);
            cb_categoria.getSelectionModel().select(0);  // Set valor por defecto

            //---------->>>>Grados academicos
            dataGrado.clear();
            for (final ParamGetGrado p : res.getGrados()){
                dataGrado.add(new grado( p.getIdenGradoacademico(), p.getDescGradoacademico() ));
            }
            tb_grados.setItems(dataGrado);
            tb_grados.getSelectionModel().select(0);  // Set valor por defecto
            
            //estados
            dataEstado.add("Activo");dataEstado.add("Inactivo");dataEstado.add("Retirado");
            cb_estado.setItems(dataEstado);
            cb_estado.getSelectionModel().select(0);  // Set valor por defecto

        } catch (final Exception e) {
            Dialog.showError(this, e);
        }
    }

    public void limpiarCamposGral(){
        limpiarCampos();
        cargaFuncionarios();
    }

    public void cargaHistoria(final String v_codi){
        int v_in=0;

        dataHistoriadoc.clear();
        HistoriadocGetResult res = null;
        try {
            res=port.historiadocGet(v_codi);
            for (final HistoriadocGetHistoria p : res.getHistorias()){
                String v_fechaux=p.getFechFin();
                if ( v_in == 0){
                    final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
                    final LocalDate dateIni = LocalDate.parse(p.getFechIni(), formatter);
                    final LocalDate dateFin = LocalDate.parse(p.getFechFin(), formatter);
                    dp_fecha_ini.setValue(dateIni);
                    if (p.getFechFin().equals("01-01-1900")) {
                        v_fechaux=" ";
                        dp_fecha_fin.setValue(null);
                    }else{
                        v_fechaux=p.getFechFin();
                        dp_fecha_fin.setValue(dateFin);
                    }
                    v_in = 1;
                }
                dataHistoriadoc.add( new historiadoc( p.getId(), p.getTipoFunc(), p.getTipoCont(), p.getFechIni(), v_fechaux));
            }
            tb_historiadoc.setItems(dataHistoriadoc);
        } catch (final Exception e1) {
            Dialog.showError(this, e1);
        }
    }

    public static boolean validaRut(String rut) {
        boolean validacion = false;
        try {
            rut =  rut.toUpperCase();
            rut = rut.replace(".", "");
            rut = rut.replace("-", "");
            int rutAux = Integer.parseInt(rut.substring(0, rut.length() - 1));

            final char dv = rut.charAt(rut.length() - 1);

            int m = 0, s = 1;
            for (; rutAux != 0; rutAux /= 10) {
                s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
            }
            if (dv == (char) (s != 0 ? s + 47 : 75)) {
                validacion = true;
            }

        } catch (final java.lang.NumberFormatException e) {
        } catch (final Exception e) {
        }
        return validacion;
    }

    public void eliminarGradoDocente(){
        final funcionario t = dataFuncionario.get(tb_funcionarios.getSelectionModel().getSelectedIndex());
        final gradosxdocente tt = dataGradosxdocente.get(tb_gradosxdocente.getSelectionModel().getSelectedIndex());
        DialogAction D;

        D = Dialog.showConfirm(this, "Desea eliminar grado de funcionario: "+t.getCodi_fun()+" ?");
        if ( D.toString() == "YES" ) {
            try {
                port.docenteDel(t.getCodi_fun(), Integer.parseInt(tt.getCodi_grado()));
                cargaGradosDocente(t.getCodi_fun());
            } catch (NumberFormatException | FcsfuncionariosWebException e) {
                Dialog.showError(this, e);
            }
        }
    }

    public void asociarGrado(){
        final int k = cb_facultad.getSelectionModel().getSelectedItem().indexOf(" ");
        final int v_fac = Integer.parseInt(cb_facultad.getSelectionModel().getSelectedItem().substring(0, k));
        final int l = cb_instituto.getSelectionModel().getSelectedItem().indexOf(" ");
        final int v_ins = Integer.parseInt(cb_instituto.getSelectionModel().getSelectedItem().substring(0, l));
        final funcionario t = dataFuncionario.get(tb_funcionarios.getSelectionModel().getSelectedIndex());
        final grado tt = dataGrado.get(tb_grados.getSelectionModel().getSelectedIndex());

        try {
            port.docenteSet(t.getCodi_fun(), v_fac, v_ins, Integer.parseInt(tt.getCodi_grado()));
            cargaGradosDocente(t.getCodi_fun());
        //limpiarCampos();

        } catch (NumberFormatException | FcsfuncionariosWebException e) {
            Dialog.showError(this,e);
        }
    }

    public void cargaGradosDocente(final String v_codi_fun){
        dataGradosxdocente.clear();
        GradosdocenteGetResult res = null;
        try {
            res=port.gradosdocenteGet(v_codi_fun);
            for (final GradosdocenteGetGrado p : res.getGrados()){
                dataGradosxdocente.add(new gradosxdocente( p.getIdenGradoacademico(), p.getDescGradoacademico() ));
            }
            tb_gradosxdocente.setItems(dataGradosxdocente);
            tb_gradosxdocente.getSelectionModel().select(0);  // Set valor por defecto
        } catch (final Exception e) {
            Dialog.showError(this, e);
        }
    }

    public void ingresarFuncionario(final DesktopTask T){
        String v_codi_fun, v_rut, v_desc_nom, v_desc_pat, v_desc_mat,v_genero,v_estado;
        String v_desc_email, v_desc_fono, v_fech_ini, v_anexo, v_oficina, v_decano, v_prodecano,
            v_fech_fin, v_fech_nac,v_secretario;
        Integer v_iden_fac, v_iden_ins, v_iden_tipofun, v_iden_tipocon, v_cat, v_edi;
        DialogAction D;
        if (tx_codigo.getText().trim().equals("")){
        	Dialog.showMessage(this,"Campo Username no puede ser nulo.");return;
        }
        if (tx_nombres.getText().trim().equals("")){
        	Dialog.showMessage(this,"Campo Nombres no puede ser nulo.");return;
        }
        if (tx_paterno.getText().trim().equals("")){
        	Dialog.showMessage(this,"Campo Apellido Paterno no puede ser nulo.");return;
        }

        D = Dialog.showConfirm(this, "Desea ingresar este nuevo Funcionario ?");
        if ( D.toString() == "YES" ) {
            v_codi_fun=tx_codigo.getText().trim();
            v_rut=tx_rut.getText().trim();
            v_desc_nom=tx_nombres.getText().trim();
            v_desc_pat=tx_paterno.getText().trim();
            v_desc_mat=tx_materno.getText().trim();
            v_desc_email=tx_email.getText().trim();
            v_desc_fono=tx_telefono.getText().trim();
            v_fech_ini=dp_fecha_ini.getValue().toString();
            v_fech_nac=dp_fecha_nac.getValue().toString();
            v_estado=cb_estado.getValue().trim();
            if (dp_fecha_fin.getValue() != null) {
                v_fech_fin=dp_fecha_fin.getValue().toString();
            }else{
                v_fech_fin="1900-01-01";
            }

            v_anexo=tx_anexo.getText().trim();
            v_oficina=tx_oficina.getText().trim();
            v_decano="NO";v_prodecano="NO";v_secretario="NO";
            if (ck_decano.isSelected()) {
                v_decano="SI";
            }
            if (ck_prodecano.isSelected()){
                v_prodecano="SI";
            }
            if (ck_secretario.isSelected()){
                v_secretario="SI";
            }
            if (rb_masculino.isSelected()){
            	v_genero="M";
            }else{
            	v_genero="F";
        	}
            final int i = cb_tipofunc.getSelectionModel().getSelectedItem().indexOf(" ");
            v_iden_tipofun = Integer.parseInt(cb_tipofunc.getSelectionModel().getSelectedItem().substring(0, i));
            final int j = cb_tipocont.getSelectionModel().getSelectedItem().indexOf(" ");
            v_iden_tipocon = Integer.parseInt(cb_tipocont.getSelectionModel().getSelectedItem().substring(0, j));
            final int k = cb_facultad.getSelectionModel().getSelectedItem().indexOf(" ");
            v_iden_fac = Integer.parseInt(cb_facultad.getSelectionModel().getSelectedItem().substring(0, k));
            final int l = cb_instituto.getSelectionModel().getSelectedItem().indexOf(" ");
            v_iden_ins = Integer.parseInt(cb_instituto.getSelectionModel().getSelectedItem().substring(0, l));
            final int m = cb_categoria.getSelectionModel().getSelectedItem().indexOf(" ");
            v_cat = Integer.parseInt(cb_categoria.getSelectionModel().getSelectedItem().substring(0, m));
            final int n = cb_edificio.getSelectionModel().getSelectedItem().indexOf(" ");
            v_edi = Integer.parseInt(cb_edificio.getSelectionModel().getSelectedItem().substring(0, n));

            try {
                port.funcionarioSet(v_iden_fac, v_iden_ins, v_codi_fun, v_rut, v_desc_nom, v_desc_pat, v_desc_mat
                    , v_fech_nac, v_cat, v_iden_tipofun, v_iden_tipocon, v_fech_ini, v_desc_email, v_desc_fono, v_anexo
                    , v_oficina, v_edi, v_decano, v_prodecano,v_secretario, v_fech_fin, v_genero,v_estado);
                Dialog.showMessage(this, "Funcionario ingresado correctamente.");
                cargaFuncionarios();
                limpiarCampos();
            } catch (final FcsfuncionariosWebException e) {
                Dialog.showError(this, e);
            }
        }
    }

    public void updateFuncionario(){
        String v_codi_fun, v_rut, v_desc_nom, v_desc_pat, v_desc_mat, v_secretario,v_genero,v_estado;
        String v_desc_email, v_desc_fono, v_fech_ini,v_anexo, v_oficina, v_decano, v_prodecano, v_fech_fin, v_fech_nac;
        Integer v_fac, v_ins, v_tipofun, v_tipocon, v_cat, v_edi;
        DialogAction D;

        D = Dialog.showConfirm(this, "Desea actualizar este Funcionario?");
        if ( D.toString() == "YES" ) {
            v_codi_fun=tx_codigo.getText().trim();
            v_rut=tx_rut.getText().trim();
            v_desc_nom=tx_nombres.getText().trim();
            v_desc_pat=tx_paterno.getText().trim();
            v_desc_mat=tx_materno.getText().trim();
            v_fech_nac=dp_fecha_nac.getValue().toString();
            v_estado=cb_estado.getValue().trim();

            v_desc_email=tx_email.getText().trim();
            v_desc_fono=tx_telefono.getText().trim();
            v_anexo=tx_anexo.getText().trim();
            v_oficina=tx_oficina.getText().trim();
            v_fech_ini=dp_fecha_ini.getValue().toString();
            if (dp_fecha_fin.getValue() != null) {
                v_fech_fin=dp_fecha_fin.getValue().toString();
            }else{
                v_fech_fin="1900-01-01";
            }

            v_decano="NO";v_prodecano="NO";v_secretario="NO";
            if (ck_decano.isSelected()) {
                v_decano="SI";
            }
            if (ck_prodecano.isSelected()){
                v_prodecano="SI";
            }
            if (ck_secretario.isSelected()){
                v_secretario="SI";
            }
            
            if (rb_masculino.isSelected()){
            	v_genero="M";
            }else{
            	v_genero="F";
        	}
        
            final int i = cb_tipofunc.getSelectionModel().getSelectedItem().indexOf(" ");
            v_tipofun = Integer.parseInt(cb_tipofunc.getSelectionModel().getSelectedItem().substring(0, i));
            final int j = cb_tipocont.getSelectionModel().getSelectedItem().indexOf(" ");
            v_tipocon = Integer.parseInt(cb_tipocont.getSelectionModel().getSelectedItem().substring(0, j));
            final int k = cb_facultad.getSelectionModel().getSelectedItem().indexOf(" ");
            v_fac = Integer.parseInt(cb_facultad.getSelectionModel().getSelectedItem().substring(0, k));
            final int l = cb_instituto.getSelectionModel().getSelectedItem().indexOf(" ");
            v_ins = Integer.parseInt(cb_instituto.getSelectionModel().getSelectedItem().substring(0, l));
            final int m = cb_categoria.getSelectionModel().getSelectedItem().indexOf(" ");
            v_cat = Integer.parseInt(cb_categoria.getSelectionModel().getSelectedItem().substring(0, m));
            final int n = cb_edificio.getSelectionModel().getSelectedItem().indexOf(" ");
            v_edi = Integer.parseInt(cb_edificio.getSelectionModel().getSelectedItem().substring(0, n));

            try {
                port.funcionarioUpd(v_fac, v_ins, v_codi_fun, v_rut, v_desc_nom, v_desc_pat, v_desc_mat, v_fech_nac
                    , v_cat, v_tipofun, v_tipocon, v_fech_ini, v_desc_email, v_desc_fono, v_anexo, v_oficina, v_edi
                    , v_decano, v_prodecano, v_secretario, v_fech_fin, v_genero,v_estado);
                Dialog.showMessage(this, "Funcionario actualizado correctamente.");
                cargaFuncionarios();
                limpiarCampos();
            } catch (final FcsfuncionariosWebException e) {
                Dialog.showError(this, e);
            }
        }
    }

    public void cargaFuncionario(final String v_codi){
        FuncionarioGetResult res = null;
        try {
            res = port.funcionarioGet(v_codi);
            final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            final LocalDate dateNac = LocalDate.parse(res.getFechNac(), formatter);
            tx_codigo.setText(v_codi);
            tx_rut.setText(res.getRutFun());
            tx_nombres.setText(res.getDescNombres());
            tx_paterno.setText(res.getDescPaterno());
            tx_materno.setText(res.getDescMaterno());
            dp_fecha_nac.setValue(dateNac);
            cb_categoria.setValue(res.getIdenCategoria());
            cb_tipofunc.setValue(res.getIdenTipofun());
            cb_tipocont.setValue(res.getIdenTipocon());
            tx_email.setText(res.getDescEmail());
            tx_telefono.setText(res.getDescTelefono());
            tx_anexo.setText(res.getDescAnexo());
            tx_oficina.setText(res.getDescOficina());
            cb_edificio.setValue(res.getIdenEdificio());
            cb_estado.setValue(res.getEstdFuncionario());
            ck_decano.setSelected(false);ck_prodecano.setSelected(false);ck_secretario.setSelected(false);
            if (res.getDecano().equals("SI")){
                ck_decano.setSelected(true);
            }
            if (res.getProdecano().equals("SI")){
                ck_prodecano.setSelected(true);
            }
            if (res.getSecretario().equals("SI")){
                ck_secretario.setSelected(true);
            }
            if (res.getCodiGenero().equals("M")){
            	rb_masculino.setSelected(true);
            }else{	
            	rb_femenino.setSelected(true);
            }
        } catch (final FcsfuncionariosWebException e) {
            Dialog.showError(this, e);
        }
    }

    public void limpiarCampos(){
        tx_codigo.setText("");tx_rut.setText("");tx_nombres.setText("");tx_paterno.setText("");tx_materno.setText("");
        tx_email.setText("");tx_telefono.setText("");tx_anexo.setText("");tx_oficina.setText("");
        dp_fecha_ini.setValue(LocalDate.now());
        dp_fecha_nac.setValue(LocalDate.of(2000, 1, 1));
        lb_grados.setText("Grados Academicos de : ");
        dataGradosxdocente.clear();
        dataHistoriadoc.clear();

        ck_decano.setSelected(false);ck_prodecano.setSelected(false);ck_secretario.setSelected(false);
        cb_tipofunc.getSelectionModel().select(0); // Set valor por defecto
        cb_tipocont.getSelectionModel().select(0); // Set valor por defecto
        cb_edificio.getSelectionModel().select(0); // Set valor por defecto
        cb_categoria.getSelectionModel().select(0); // Set valor por defecto
    }

    public void cargaFuncionarios(){
        dataFuncionario.clear();
        final int k = cb_facultad.getSelectionModel().getSelectedItem().indexOf(" ");
        final int v_fac = Integer.parseInt(cb_facultad.getSelectionModel().getSelectedItem().substring(0, k));

        if (!cb_instituto.getSelectionModel().isEmpty()) {
            final int l = cb_instituto.getSelectionModel().getSelectedItem().indexOf(" ");
            final int v_ins = Integer.parseInt(cb_instituto.getSelectionModel().getSelectedItem().substring(0, l));
            FuncionariosGetResult resultado = null;
            try {
                resultado = port.funcionariosGet(v_fac, v_ins);
                for ( final FuncionariosGetFunc t : resultado.getFuncs()) {
                    dataFuncionario.add(new funcionario( t.getCodiFuncionario(), t.getRutFuncionario(), t.getDescNombres(), t.getDescPaterno()
                        , t.getDescFuncionario(), t.getDescContrato()));
                }
                tb_funcionarios.setItems(dataFuncionario);
            } catch (final FcsfuncionariosWebException e) {
                Dialog.showError(this, e);
            }
        }
    }

    public void cargaFacultad(){
        dataFacultad.clear();
        FacultadesGetResult res = null;
        try {
            res=port.facultadesGet();
            for (final FacultadesGetFac p : res.getFacs()){
                dataFacultad.add(p.getFacultad());
            }
            cb_facultad.setItems(dataFacultad);
            cb_facultad.getSelectionModel().select(0);  // Set valor por defecto
        } catch (final Exception e1) {
            Dialog.showError(this, e1);
        }
    }

    public void cargaInstituto(final String v_facultad){
        dataInstituto.clear();
        final int k = v_facultad.indexOf(" ");
        final int v_iden_fac = Integer.parseInt(v_facultad.substring(0, k));

        InstitutosGetResult res = null;
        try {
            res=port.institutosGet(v_iden_fac);
            for (final InstitutosGetInstit p : res.getInstits()){
                dataInstituto.add(p.getInstituto());
            }
            cb_instituto.setItems(dataInstituto);
            cb_instituto.getSelectionModel().select(0);  // Set valor por defecto
        } catch (final Exception e) {
            Dialog.showError(this, e);
        }
    }


} // Main - FXMPane
