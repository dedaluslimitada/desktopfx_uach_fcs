package cl.dedalus.fcsfuncionarios;

import javafx.beans.property.SimpleStringProperty;

public class grado{
    private final SimpleStringProperty codi_grado;
    private final SimpleStringProperty desc_grado;

    public grado(final String codi_grado,final String desc_grado){
        this.codi_grado = new SimpleStringProperty(codi_grado);
        this.desc_grado = new SimpleStringProperty(desc_grado);
    }

    public String getCodi_grado() {
        return codi_grado.get();
    }
    public String getDesc_grado() {
        return desc_grado.get();
    }
 }
