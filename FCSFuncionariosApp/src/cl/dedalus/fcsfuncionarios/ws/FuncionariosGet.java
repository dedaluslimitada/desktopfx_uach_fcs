
package cl.dedalus.fcsfuncionarios.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para funcionariosGet complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="funcionariosGet">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idenFac" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="idenIns" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "funcionariosGet", propOrder = {
    "idenFac",
    "idenIns"
})
public class FuncionariosGet {

    protected int idenFac;
    protected int idenIns;

    /**
     * Obtiene el valor de la propiedad idenFac.
     * 
     */
    public int getIdenFac() {
        return idenFac;
    }

    /**
     * Define el valor de la propiedad idenFac.
     * 
     */
    public void setIdenFac(int value) {
        this.idenFac = value;
    }

    /**
     * Obtiene el valor de la propiedad idenIns.
     * 
     */
    public int getIdenIns() {
        return idenIns;
    }

    /**
     * Define el valor de la propiedad idenIns.
     * 
     */
    public void setIdenIns(int value) {
        this.idenIns = value;
    }

}
