
package cl.dedalus.fcsfuncionarios.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para FuncionariosGetFunc complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="FuncionariosGetFunc">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codiFuncionario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descContrato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descFuncionario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descNombres" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descPaterno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rutFuncionario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FuncionariosGetFunc", propOrder = {
    "codiFuncionario",
    "descContrato",
    "descFuncionario",
    "descNombres",
    "descPaterno",
    "rutFuncionario"
})
public class FuncionariosGetFunc {

    protected String codiFuncionario;
    protected String descContrato;
    protected String descFuncionario;
    protected String descNombres;
    protected String descPaterno;
    protected String rutFuncionario;

    /**
     * Obtiene el valor de la propiedad codiFuncionario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiFuncionario() {
        return codiFuncionario;
    }

    /**
     * Define el valor de la propiedad codiFuncionario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiFuncionario(String value) {
        this.codiFuncionario = value;
    }

    /**
     * Obtiene el valor de la propiedad descContrato.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescContrato() {
        return descContrato;
    }

    /**
     * Define el valor de la propiedad descContrato.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescContrato(String value) {
        this.descContrato = value;
    }

    /**
     * Obtiene el valor de la propiedad descFuncionario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescFuncionario() {
        return descFuncionario;
    }

    /**
     * Define el valor de la propiedad descFuncionario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescFuncionario(String value) {
        this.descFuncionario = value;
    }

    /**
     * Obtiene el valor de la propiedad descNombres.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescNombres() {
        return descNombres;
    }

    /**
     * Define el valor de la propiedad descNombres.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescNombres(String value) {
        this.descNombres = value;
    }

    /**
     * Obtiene el valor de la propiedad descPaterno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescPaterno() {
        return descPaterno;
    }

    /**
     * Define el valor de la propiedad descPaterno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescPaterno(String value) {
        this.descPaterno = value;
    }

    /**
     * Obtiene el valor de la propiedad rutFuncionario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRutFuncionario() {
        return rutFuncionario;
    }

    /**
     * Define el valor de la propiedad rutFuncionario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRutFuncionario(String value) {
        this.rutFuncionario = value;
    }

}
