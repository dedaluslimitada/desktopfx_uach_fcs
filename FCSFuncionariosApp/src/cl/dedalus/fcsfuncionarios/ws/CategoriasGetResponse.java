
package cl.dedalus.fcsfuncionarios.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para categoriasGetResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="categoriasGetResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="categoriasGetResult" type="{http://ws.fcsfuncionarios.dedalus.cl/}CategoriasGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "categoriasGetResponse", propOrder = {
    "categoriasGetResult"
})
public class CategoriasGetResponse {

    protected CategoriasGetResult categoriasGetResult;

    /**
     * Obtiene el valor de la propiedad categoriasGetResult.
     * 
     * @return
     *     possible object is
     *     {@link CategoriasGetResult }
     *     
     */
    public CategoriasGetResult getCategoriasGetResult() {
        return categoriasGetResult;
    }

    /**
     * Define el valor de la propiedad categoriasGetResult.
     * 
     * @param value
     *     allowed object is
     *     {@link CategoriasGetResult }
     *     
     */
    public void setCategoriasGetResult(CategoriasGetResult value) {
        this.categoriasGetResult = value;
    }

}
