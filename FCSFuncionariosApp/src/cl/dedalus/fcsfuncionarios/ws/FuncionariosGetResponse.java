
package cl.dedalus.fcsfuncionarios.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para funcionariosGetResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="funcionariosGetResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="funcionariosGetResult" type="{http://ws.fcsfuncionarios.dedalus.cl/}FuncionariosGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "funcionariosGetResponse", propOrder = {
    "funcionariosGetResult"
})
public class FuncionariosGetResponse {

    protected FuncionariosGetResult funcionariosGetResult;

    /**
     * Obtiene el valor de la propiedad funcionariosGetResult.
     * 
     * @return
     *     possible object is
     *     {@link FuncionariosGetResult }
     *     
     */
    public FuncionariosGetResult getFuncionariosGetResult() {
        return funcionariosGetResult;
    }

    /**
     * Define el valor de la propiedad funcionariosGetResult.
     * 
     * @param value
     *     allowed object is
     *     {@link FuncionariosGetResult }
     *     
     */
    public void setFuncionariosGetResult(FuncionariosGetResult value) {
        this.funcionariosGetResult = value;
    }

}
