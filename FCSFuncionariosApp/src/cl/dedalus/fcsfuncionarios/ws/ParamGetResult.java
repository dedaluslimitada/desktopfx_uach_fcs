
package cl.dedalus.fcsfuncionarios.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ParamGetResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ParamGetResult">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.fcsfuncionarios.dedalus.cl/}ProcedureResult">
 *       &lt;sequence>
 *         &lt;element name="categorias" type="{http://ws.fcsfuncionarios.dedalus.cl/}ParamGetCategoria" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="contratos" type="{http://ws.fcsfuncionarios.dedalus.cl/}ParamGetContrato" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="edificios" type="{http://ws.fcsfuncionarios.dedalus.cl/}ParamGetEdificio" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="funcionarios" type="{http://ws.fcsfuncionarios.dedalus.cl/}ParamGetFuncionario" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="grados" type="{http://ws.fcsfuncionarios.dedalus.cl/}ParamGetGrado" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ParamGetResult", propOrder = {
    "categorias",
    "contratos",
    "edificios",
    "funcionarios",
    "grados"
})
public class ParamGetResult
    extends ProcedureResult
{

    @XmlElement(nillable = true)
    protected List<ParamGetCategoria> categorias;
    @XmlElement(nillable = true)
    protected List<ParamGetContrato> contratos;
    @XmlElement(nillable = true)
    protected List<ParamGetEdificio> edificios;
    @XmlElement(nillable = true)
    protected List<ParamGetFuncionario> funcionarios;
    @XmlElement(nillable = true)
    protected List<ParamGetGrado> grados;

    /**
     * Gets the value of the categorias property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the categorias property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCategorias().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ParamGetCategoria }
     * 
     * 
     */
    public List<ParamGetCategoria> getCategorias() {
        if (categorias == null) {
            categorias = new ArrayList<ParamGetCategoria>();
        }
        return this.categorias;
    }

    /**
     * Gets the value of the contratos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contratos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContratos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ParamGetContrato }
     * 
     * 
     */
    public List<ParamGetContrato> getContratos() {
        if (contratos == null) {
            contratos = new ArrayList<ParamGetContrato>();
        }
        return this.contratos;
    }

    /**
     * Gets the value of the edificios property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the edificios property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEdificios().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ParamGetEdificio }
     * 
     * 
     */
    public List<ParamGetEdificio> getEdificios() {
        if (edificios == null) {
            edificios = new ArrayList<ParamGetEdificio>();
        }
        return this.edificios;
    }

    /**
     * Gets the value of the funcionarios property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the funcionarios property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFuncionarios().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ParamGetFuncionario }
     * 
     * 
     */
    public List<ParamGetFuncionario> getFuncionarios() {
        if (funcionarios == null) {
            funcionarios = new ArrayList<ParamGetFuncionario>();
        }
        return this.funcionarios;
    }

    /**
     * Gets the value of the grados property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the grados property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGrados().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ParamGetGrado }
     * 
     * 
     */
    public List<ParamGetGrado> getGrados() {
        if (grados == null) {
            grados = new ArrayList<ParamGetGrado>();
        }
        return this.grados;
    }

}
