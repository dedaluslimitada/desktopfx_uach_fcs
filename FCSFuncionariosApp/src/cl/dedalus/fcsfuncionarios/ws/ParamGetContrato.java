
package cl.dedalus.fcsfuncionarios.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ParamGetContrato complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ParamGetContrato">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="tipocont" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ParamGetContrato", propOrder = {
    "tipocont"
})
public class ParamGetContrato {

    protected String tipocont;

    /**
     * Obtiene el valor de la propiedad tipocont.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipocont() {
        return tipocont;
    }

    /**
     * Define el valor de la propiedad tipocont.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipocont(String value) {
        this.tipocont = value;
    }

}
