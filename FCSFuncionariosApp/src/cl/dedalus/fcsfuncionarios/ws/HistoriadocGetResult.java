
package cl.dedalus.fcsfuncionarios.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para HistoriadocGetResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="HistoriadocGetResult">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.fcsfuncionarios.dedalus.cl/}ProcedureResult">
 *       &lt;sequence>
 *         &lt;element name="historias" type="{http://ws.fcsfuncionarios.dedalus.cl/}HistoriadocGetHistoria" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HistoriadocGetResult", propOrder = {
    "historias"
})
public class HistoriadocGetResult
    extends ProcedureResult
{

    @XmlElement(nillable = true)
    protected List<HistoriadocGetHistoria> historias;

    /**
     * Gets the value of the historias property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the historias property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHistorias().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link HistoriadocGetHistoria }
     * 
     * 
     */
    public List<HistoriadocGetHistoria> getHistorias() {
        if (historias == null) {
            historias = new ArrayList<HistoriadocGetHistoria>();
        }
        return this.historias;
    }

}
