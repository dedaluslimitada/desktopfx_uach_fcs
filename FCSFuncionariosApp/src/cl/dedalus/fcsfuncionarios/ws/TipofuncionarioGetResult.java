
package cl.dedalus.fcsfuncionarios.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para TipofuncionarioGetResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="TipofuncionarioGetResult">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.fcsfuncionarios.dedalus.cl/}ProcedureResult">
 *       &lt;sequence>
 *         &lt;element name="funs" type="{http://ws.fcsfuncionarios.dedalus.cl/}TipofuncionarioGetFun" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TipofuncionarioGetResult", propOrder = {
    "funs"
})
public class TipofuncionarioGetResult
    extends ProcedureResult
{

    @XmlElement(nillable = true)
    protected List<TipofuncionarioGetFun> funs;

    /**
     * Gets the value of the funs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the funs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFuns().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TipofuncionarioGetFun }
     * 
     * 
     */
    public List<TipofuncionarioGetFun> getFuns() {
        if (funs == null) {
            funs = new ArrayList<TipofuncionarioGetFun>();
        }
        return this.funs;
    }

}
