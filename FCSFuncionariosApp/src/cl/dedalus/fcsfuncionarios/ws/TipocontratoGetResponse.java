
package cl.dedalus.fcsfuncionarios.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para tipocontratoGetResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipocontratoGetResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="tipocontratoGetResult" type="{http://ws.fcsfuncionarios.dedalus.cl/}TipocontratoGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipocontratoGetResponse", propOrder = {
    "tipocontratoGetResult"
})
public class TipocontratoGetResponse {

    protected TipocontratoGetResult tipocontratoGetResult;

    /**
     * Obtiene el valor de la propiedad tipocontratoGetResult.
     * 
     * @return
     *     possible object is
     *     {@link TipocontratoGetResult }
     *     
     */
    public TipocontratoGetResult getTipocontratoGetResult() {
        return tipocontratoGetResult;
    }

    /**
     * Define el valor de la propiedad tipocontratoGetResult.
     * 
     * @param value
     *     allowed object is
     *     {@link TipocontratoGetResult }
     *     
     */
    public void setTipocontratoGetResult(TipocontratoGetResult value) {
        this.tipocontratoGetResult = value;
    }

}
