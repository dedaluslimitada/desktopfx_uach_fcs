
package cl.dedalus.fcsfuncionarios.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para historiadocGetResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="historiadocGetResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="historiadocGetResult" type="{http://ws.fcsfuncionarios.dedalus.cl/}HistoriadocGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "historiadocGetResponse", propOrder = {
    "historiadocGetResult"
})
public class HistoriadocGetResponse {

    protected HistoriadocGetResult historiadocGetResult;

    /**
     * Obtiene el valor de la propiedad historiadocGetResult.
     * 
     * @return
     *     possible object is
     *     {@link HistoriadocGetResult }
     *     
     */
    public HistoriadocGetResult getHistoriadocGetResult() {
        return historiadocGetResult;
    }

    /**
     * Define el valor de la propiedad historiadocGetResult.
     * 
     * @param value
     *     allowed object is
     *     {@link HistoriadocGetResult }
     *     
     */
    public void setHistoriadocGetResult(HistoriadocGetResult value) {
        this.historiadocGetResult = value;
    }

}
