
package cl.dedalus.fcsfuncionarios.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para docenteSetResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="docenteSetResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="docenteSetResult" type="{http://ws.fcsfuncionarios.dedalus.cl/}DocenteSetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "docenteSetResponse", propOrder = {
    "docenteSetResult"
})
public class DocenteSetResponse {

    protected DocenteSetResult docenteSetResult;

    /**
     * Obtiene el valor de la propiedad docenteSetResult.
     * 
     * @return
     *     possible object is
     *     {@link DocenteSetResult }
     *     
     */
    public DocenteSetResult getDocenteSetResult() {
        return docenteSetResult;
    }

    /**
     * Define el valor de la propiedad docenteSetResult.
     * 
     * @param value
     *     allowed object is
     *     {@link DocenteSetResult }
     *     
     */
    public void setDocenteSetResult(DocenteSetResult value) {
        this.docenteSetResult = value;
    }

}
