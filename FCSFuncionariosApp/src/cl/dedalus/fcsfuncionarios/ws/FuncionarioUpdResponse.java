
package cl.dedalus.fcsfuncionarios.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para funcionarioUpdResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="funcionarioUpdResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="funcionarioUpdResult" type="{http://ws.fcsfuncionarios.dedalus.cl/}FuncionarioUpdResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "funcionarioUpdResponse", propOrder = {
    "funcionarioUpdResult"
})
public class FuncionarioUpdResponse {

    protected FuncionarioUpdResult funcionarioUpdResult;

    /**
     * Obtiene el valor de la propiedad funcionarioUpdResult.
     * 
     * @return
     *     possible object is
     *     {@link FuncionarioUpdResult }
     *     
     */
    public FuncionarioUpdResult getFuncionarioUpdResult() {
        return funcionarioUpdResult;
    }

    /**
     * Define el valor de la propiedad funcionarioUpdResult.
     * 
     * @param value
     *     allowed object is
     *     {@link FuncionarioUpdResult }
     *     
     */
    public void setFuncionarioUpdResult(FuncionarioUpdResult value) {
        this.funcionarioUpdResult = value;
    }

}
