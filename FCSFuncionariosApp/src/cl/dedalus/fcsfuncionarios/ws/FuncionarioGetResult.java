
package cl.dedalus.fcsfuncionarios.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para FuncionarioGetResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="FuncionarioGetResult">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.fcsfuncionarios.dedalus.cl/}ProcedureResult">
 *       &lt;sequence>
 *         &lt;element name="codiGenero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="decano" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descAnexo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descMaterno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descNombres" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descOficina" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descPaterno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descTelefono" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="estdFuncionario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechFin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechIni" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechNac" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idenCategoria" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idenEdificio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idenTipocon" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idenTipofun" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="prodecano" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rutFun" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="secretario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FuncionarioGetResult", propOrder = {
    "codiGenero",
    "decano",
    "descAnexo",
    "descEmail",
    "descMaterno",
    "descNombres",
    "descOficina",
    "descPaterno",
    "descTelefono",
    "estdFuncionario",
    "fechFin",
    "fechIni",
    "fechNac",
    "idenCategoria",
    "idenEdificio",
    "idenTipocon",
    "idenTipofun",
    "prodecano",
    "rutFun",
    "secretario"
})
public class FuncionarioGetResult
    extends ProcedureResult
{

    protected String codiGenero;
    protected String decano;
    protected String descAnexo;
    protected String descEmail;
    protected String descMaterno;
    protected String descNombres;
    protected String descOficina;
    protected String descPaterno;
    protected String descTelefono;
    protected String estdFuncionario;
    protected String fechFin;
    protected String fechIni;
    protected String fechNac;
    protected String idenCategoria;
    protected String idenEdificio;
    protected String idenTipocon;
    protected String idenTipofun;
    protected String prodecano;
    protected String rutFun;
    protected String secretario;

    /**
     * Obtiene el valor de la propiedad codiGenero.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiGenero() {
        return codiGenero;
    }

    /**
     * Define el valor de la propiedad codiGenero.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiGenero(String value) {
        this.codiGenero = value;
    }

    /**
     * Obtiene el valor de la propiedad decano.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDecano() {
        return decano;
    }

    /**
     * Define el valor de la propiedad decano.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDecano(String value) {
        this.decano = value;
    }

    /**
     * Obtiene el valor de la propiedad descAnexo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescAnexo() {
        return descAnexo;
    }

    /**
     * Define el valor de la propiedad descAnexo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescAnexo(String value) {
        this.descAnexo = value;
    }

    /**
     * Obtiene el valor de la propiedad descEmail.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescEmail() {
        return descEmail;
    }

    /**
     * Define el valor de la propiedad descEmail.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescEmail(String value) {
        this.descEmail = value;
    }

    /**
     * Obtiene el valor de la propiedad descMaterno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescMaterno() {
        return descMaterno;
    }

    /**
     * Define el valor de la propiedad descMaterno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescMaterno(String value) {
        this.descMaterno = value;
    }

    /**
     * Obtiene el valor de la propiedad descNombres.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescNombres() {
        return descNombres;
    }

    /**
     * Define el valor de la propiedad descNombres.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescNombres(String value) {
        this.descNombres = value;
    }

    /**
     * Obtiene el valor de la propiedad descOficina.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescOficina() {
        return descOficina;
    }

    /**
     * Define el valor de la propiedad descOficina.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescOficina(String value) {
        this.descOficina = value;
    }

    /**
     * Obtiene el valor de la propiedad descPaterno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescPaterno() {
        return descPaterno;
    }

    /**
     * Define el valor de la propiedad descPaterno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescPaterno(String value) {
        this.descPaterno = value;
    }

    /**
     * Obtiene el valor de la propiedad descTelefono.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescTelefono() {
        return descTelefono;
    }

    /**
     * Define el valor de la propiedad descTelefono.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescTelefono(String value) {
        this.descTelefono = value;
    }

    /**
     * Obtiene el valor de la propiedad estdFuncionario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstdFuncionario() {
        return estdFuncionario;
    }

    /**
     * Define el valor de la propiedad estdFuncionario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstdFuncionario(String value) {
        this.estdFuncionario = value;
    }

    /**
     * Obtiene el valor de la propiedad fechFin.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechFin() {
        return fechFin;
    }

    /**
     * Define el valor de la propiedad fechFin.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechFin(String value) {
        this.fechFin = value;
    }

    /**
     * Obtiene el valor de la propiedad fechIni.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechIni() {
        return fechIni;
    }

    /**
     * Define el valor de la propiedad fechIni.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechIni(String value) {
        this.fechIni = value;
    }

    /**
     * Obtiene el valor de la propiedad fechNac.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechNac() {
        return fechNac;
    }

    /**
     * Define el valor de la propiedad fechNac.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechNac(String value) {
        this.fechNac = value;
    }

    /**
     * Obtiene el valor de la propiedad idenCategoria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdenCategoria() {
        return idenCategoria;
    }

    /**
     * Define el valor de la propiedad idenCategoria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdenCategoria(String value) {
        this.idenCategoria = value;
    }

    /**
     * Obtiene el valor de la propiedad idenEdificio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdenEdificio() {
        return idenEdificio;
    }

    /**
     * Define el valor de la propiedad idenEdificio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdenEdificio(String value) {
        this.idenEdificio = value;
    }

    /**
     * Obtiene el valor de la propiedad idenTipocon.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdenTipocon() {
        return idenTipocon;
    }

    /**
     * Define el valor de la propiedad idenTipocon.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdenTipocon(String value) {
        this.idenTipocon = value;
    }

    /**
     * Obtiene el valor de la propiedad idenTipofun.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdenTipofun() {
        return idenTipofun;
    }

    /**
     * Define el valor de la propiedad idenTipofun.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdenTipofun(String value) {
        this.idenTipofun = value;
    }

    /**
     * Obtiene el valor de la propiedad prodecano.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProdecano() {
        return prodecano;
    }

    /**
     * Define el valor de la propiedad prodecano.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProdecano(String value) {
        this.prodecano = value;
    }

    /**
     * Obtiene el valor de la propiedad rutFun.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRutFun() {
        return rutFun;
    }

    /**
     * Define el valor de la propiedad rutFun.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRutFun(String value) {
        this.rutFun = value;
    }

    /**
     * Obtiene el valor de la propiedad secretario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecretario() {
        return secretario;
    }

    /**
     * Define el valor de la propiedad secretario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecretario(String value) {
        this.secretario = value;
    }

}
