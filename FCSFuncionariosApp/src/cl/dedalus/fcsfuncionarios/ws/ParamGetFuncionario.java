
package cl.dedalus.fcsfuncionarios.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ParamGetFuncionario complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ParamGetFuncionario">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="tipofunc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ParamGetFuncionario", propOrder = {
    "tipofunc"
})
public class ParamGetFuncionario {

    protected String tipofunc;

    /**
     * Obtiene el valor de la propiedad tipofunc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipofunc() {
        return tipofunc;
    }

    /**
     * Define el valor de la propiedad tipofunc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipofunc(String value) {
        this.tipofunc = value;
    }

}
