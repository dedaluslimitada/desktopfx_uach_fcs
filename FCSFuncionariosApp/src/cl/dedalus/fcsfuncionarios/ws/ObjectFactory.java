
package cl.dedalus.fcsfuncionarios.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cl.dedalus.fcsfuncionarios.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _FaultInfo_QNAME = new QName("http://ws.fcsfuncionarios.dedalus.cl/", "faultInfo");
    private final static QName _ParamGetResponse_QNAME = new QName("http://ws.fcsfuncionarios.dedalus.cl/", "paramGetResponse");
    private final static QName _DocenteDelResponse_QNAME = new QName("http://ws.fcsfuncionarios.dedalus.cl/", "docenteDelResponse");
    private final static QName _FuncionarioGetResponse_QNAME = new QName("http://ws.fcsfuncionarios.dedalus.cl/", "funcionarioGetResponse");
    private final static QName _FuncionariosGet_QNAME = new QName("http://ws.fcsfuncionarios.dedalus.cl/", "funcionariosGet");
    private final static QName _FuncionarioSet_QNAME = new QName("http://ws.fcsfuncionarios.dedalus.cl/", "funcionarioSet");
    private final static QName _DocenteDel_QNAME = new QName("http://ws.fcsfuncionarios.dedalus.cl/", "docenteDel");
    private final static QName _GradosdocenteGet_QNAME = new QName("http://ws.fcsfuncionarios.dedalus.cl/", "gradosdocenteGet");
    private final static QName _ParamGet_QNAME = new QName("http://ws.fcsfuncionarios.dedalus.cl/", "paramGet");
    private final static QName _DocenteSet_QNAME = new QName("http://ws.fcsfuncionarios.dedalus.cl/", "docenteSet");
    private final static QName _FuncionarioUpd_QNAME = new QName("http://ws.fcsfuncionarios.dedalus.cl/", "funcionarioUpd");
    private final static QName _FuncionarioUpdResponse_QNAME = new QName("http://ws.fcsfuncionarios.dedalus.cl/", "funcionarioUpdResponse");
    private final static QName _FacultadesGet_QNAME = new QName("http://ws.fcsfuncionarios.dedalus.cl/", "facultadesGet");
    private final static QName _DocenteSetResponse_QNAME = new QName("http://ws.fcsfuncionarios.dedalus.cl/", "docenteSetResponse");
    private final static QName _FuncionariosGetResponse_QNAME = new QName("http://ws.fcsfuncionarios.dedalus.cl/", "funcionariosGetResponse");
    private final static QName _GradosdocenteGetResponse_QNAME = new QName("http://ws.fcsfuncionarios.dedalus.cl/", "gradosdocenteGetResponse");
    private final static QName _HistoriadocGet_QNAME = new QName("http://ws.fcsfuncionarios.dedalus.cl/", "historiadocGet");
    private final static QName _FacultadesGetResponse_QNAME = new QName("http://ws.fcsfuncionarios.dedalus.cl/", "facultadesGetResponse");
    private final static QName _FuncionarioGet_QNAME = new QName("http://ws.fcsfuncionarios.dedalus.cl/", "funcionarioGet");
    private final static QName _HistoriadocGetResponse_QNAME = new QName("http://ws.fcsfuncionarios.dedalus.cl/", "historiadocGetResponse");
    private final static QName _InstitutosGetResponse_QNAME = new QName("http://ws.fcsfuncionarios.dedalus.cl/", "institutosGetResponse");
    private final static QName _InstitutosGet_QNAME = new QName("http://ws.fcsfuncionarios.dedalus.cl/", "institutosGet");
    private final static QName _FuncionarioSetResponse_QNAME = new QName("http://ws.fcsfuncionarios.dedalus.cl/", "funcionarioSetResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cl.dedalus.fcsfuncionarios.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link FacultadesGet }
     * 
     */
    public FacultadesGet createFacultadesGet() {
        return new FacultadesGet();
    }

    /**
     * Create an instance of {@link DocenteSetResponse }
     * 
     */
    public DocenteSetResponse createDocenteSetResponse() {
        return new DocenteSetResponse();
    }

    /**
     * Create an instance of {@link FuncionariosGetResponse }
     * 
     */
    public FuncionariosGetResponse createFuncionariosGetResponse() {
        return new FuncionariosGetResponse();
    }

    /**
     * Create an instance of {@link GradosdocenteGetResponse }
     * 
     */
    public GradosdocenteGetResponse createGradosdocenteGetResponse() {
        return new GradosdocenteGetResponse();
    }

    /**
     * Create an instance of {@link HistoriadocGet }
     * 
     */
    public HistoriadocGet createHistoriadocGet() {
        return new HistoriadocGet();
    }

    /**
     * Create an instance of {@link FacultadesGetResponse }
     * 
     */
    public FacultadesGetResponse createFacultadesGetResponse() {
        return new FacultadesGetResponse();
    }

    /**
     * Create an instance of {@link FuncionarioGet }
     * 
     */
    public FuncionarioGet createFuncionarioGet() {
        return new FuncionarioGet();
    }

    /**
     * Create an instance of {@link HistoriadocGetResponse }
     * 
     */
    public HistoriadocGetResponse createHistoriadocGetResponse() {
        return new HistoriadocGetResponse();
    }

    /**
     * Create an instance of {@link InstitutosGetResponse }
     * 
     */
    public InstitutosGetResponse createInstitutosGetResponse() {
        return new InstitutosGetResponse();
    }

    /**
     * Create an instance of {@link InstitutosGet }
     * 
     */
    public InstitutosGet createInstitutosGet() {
        return new InstitutosGet();
    }

    /**
     * Create an instance of {@link FuncionarioSetResponse }
     * 
     */
    public FuncionarioSetResponse createFuncionarioSetResponse() {
        return new FuncionarioSetResponse();
    }

    /**
     * Create an instance of {@link ParamGetResponse }
     * 
     */
    public ParamGetResponse createParamGetResponse() {
        return new ParamGetResponse();
    }

    /**
     * Create an instance of {@link DocenteDelResponse }
     * 
     */
    public DocenteDelResponse createDocenteDelResponse() {
        return new DocenteDelResponse();
    }

    /**
     * Create an instance of {@link FuncionarioGetResponse }
     * 
     */
    public FuncionarioGetResponse createFuncionarioGetResponse() {
        return new FuncionarioGetResponse();
    }

    /**
     * Create an instance of {@link FuncionariosGet }
     * 
     */
    public FuncionariosGet createFuncionariosGet() {
        return new FuncionariosGet();
    }

    /**
     * Create an instance of {@link FuncionarioSet }
     * 
     */
    public FuncionarioSet createFuncionarioSet() {
        return new FuncionarioSet();
    }

    /**
     * Create an instance of {@link DocenteDel }
     * 
     */
    public DocenteDel createDocenteDel() {
        return new DocenteDel();
    }

    /**
     * Create an instance of {@link GradosdocenteGet }
     * 
     */
    public GradosdocenteGet createGradosdocenteGet() {
        return new GradosdocenteGet();
    }

    /**
     * Create an instance of {@link ParamGet }
     * 
     */
    public ParamGet createParamGet() {
        return new ParamGet();
    }

    /**
     * Create an instance of {@link DocenteSet }
     * 
     */
    public DocenteSet createDocenteSet() {
        return new DocenteSet();
    }

    /**
     * Create an instance of {@link FuncionarioUpd }
     * 
     */
    public FuncionarioUpd createFuncionarioUpd() {
        return new FuncionarioUpd();
    }

    /**
     * Create an instance of {@link FuncionarioUpdResponse }
     * 
     */
    public FuncionarioUpdResponse createFuncionarioUpdResponse() {
        return new FuncionarioUpdResponse();
    }

    /**
     * Create an instance of {@link FuncionariosGetResult }
     * 
     */
    public FuncionariosGetResult createFuncionariosGetResult() {
        return new FuncionariosGetResult();
    }

    /**
     * Create an instance of {@link ParamGetFuncionario }
     * 
     */
    public ParamGetFuncionario createParamGetFuncionario() {
        return new ParamGetFuncionario();
    }

    /**
     * Create an instance of {@link GradosdocenteGetResult }
     * 
     */
    public GradosdocenteGetResult createGradosdocenteGetResult() {
        return new GradosdocenteGetResult();
    }

    /**
     * Create an instance of {@link ParamGetEdificio }
     * 
     */
    public ParamGetEdificio createParamGetEdificio() {
        return new ParamGetEdificio();
    }

    /**
     * Create an instance of {@link ParamGetGrado }
     * 
     */
    public ParamGetGrado createParamGetGrado() {
        return new ParamGetGrado();
    }

    /**
     * Create an instance of {@link FuncionarioSetResult }
     * 
     */
    public FuncionarioSetResult createFuncionarioSetResult() {
        return new FuncionarioSetResult();
    }

    /**
     * Create an instance of {@link InstitutosGetResult }
     * 
     */
    public InstitutosGetResult createInstitutosGetResult() {
        return new InstitutosGetResult();
    }

    /**
     * Create an instance of {@link DocenteDelResult }
     * 
     */
    public DocenteDelResult createDocenteDelResult() {
        return new DocenteDelResult();
    }

    /**
     * Create an instance of {@link FuncionarioGetResult }
     * 
     */
    public FuncionarioGetResult createFuncionarioGetResult() {
        return new FuncionarioGetResult();
    }

    /**
     * Create an instance of {@link DocenteSetResult }
     * 
     */
    public DocenteSetResult createDocenteSetResult() {
        return new DocenteSetResult();
    }

    /**
     * Create an instance of {@link ParamGetContrato }
     * 
     */
    public ParamGetContrato createParamGetContrato() {
        return new ParamGetContrato();
    }

    /**
     * Create an instance of {@link InstitutosGetInstit }
     * 
     */
    public InstitutosGetInstit createInstitutosGetInstit() {
        return new InstitutosGetInstit();
    }

    /**
     * Create an instance of {@link FacultadesGetFac }
     * 
     */
    public FacultadesGetFac createFacultadesGetFac() {
        return new FacultadesGetFac();
    }

    /**
     * Create an instance of {@link FuncionarioUpdResult }
     * 
     */
    public FuncionarioUpdResult createFuncionarioUpdResult() {
        return new FuncionarioUpdResult();
    }

    /**
     * Create an instance of {@link FacultadesGetResult }
     * 
     */
    public FacultadesGetResult createFacultadesGetResult() {
        return new FacultadesGetResult();
    }

    /**
     * Create an instance of {@link ParamGetResult }
     * 
     */
    public ParamGetResult createParamGetResult() {
        return new ParamGetResult();
    }

    /**
     * Create an instance of {@link FuncionariosGetFunc }
     * 
     */
    public FuncionariosGetFunc createFuncionariosGetFunc() {
        return new FuncionariosGetFunc();
    }

    /**
     * Create an instance of {@link GradosdocenteGetGrado }
     * 
     */
    public GradosdocenteGetGrado createGradosdocenteGetGrado() {
        return new GradosdocenteGetGrado();
    }

    /**
     * Create an instance of {@link ParamGetCategoria }
     * 
     */
    public ParamGetCategoria createParamGetCategoria() {
        return new ParamGetCategoria();
    }

    /**
     * Create an instance of {@link HistoriadocGetHistoria }
     * 
     */
    public HistoriadocGetHistoria createHistoriadocGetHistoria() {
        return new HistoriadocGetHistoria();
    }

    /**
     * Create an instance of {@link HistoriadocGetResult }
     * 
     */
    public HistoriadocGetResult createHistoriadocGetResult() {
        return new HistoriadocGetResult();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsfuncionarios.dedalus.cl/", name = "faultInfo")
    public JAXBElement<String> createFaultInfo(String value) {
        return new JAXBElement<String>(_FaultInfo_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ParamGetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsfuncionarios.dedalus.cl/", name = "paramGetResponse")
    public JAXBElement<ParamGetResponse> createParamGetResponse(ParamGetResponse value) {
        return new JAXBElement<ParamGetResponse>(_ParamGetResponse_QNAME, ParamGetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocenteDelResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsfuncionarios.dedalus.cl/", name = "docenteDelResponse")
    public JAXBElement<DocenteDelResponse> createDocenteDelResponse(DocenteDelResponse value) {
        return new JAXBElement<DocenteDelResponse>(_DocenteDelResponse_QNAME, DocenteDelResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FuncionarioGetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsfuncionarios.dedalus.cl/", name = "funcionarioGetResponse")
    public JAXBElement<FuncionarioGetResponse> createFuncionarioGetResponse(FuncionarioGetResponse value) {
        return new JAXBElement<FuncionarioGetResponse>(_FuncionarioGetResponse_QNAME, FuncionarioGetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FuncionariosGet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsfuncionarios.dedalus.cl/", name = "funcionariosGet")
    public JAXBElement<FuncionariosGet> createFuncionariosGet(FuncionariosGet value) {
        return new JAXBElement<FuncionariosGet>(_FuncionariosGet_QNAME, FuncionariosGet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FuncionarioSet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsfuncionarios.dedalus.cl/", name = "funcionarioSet")
    public JAXBElement<FuncionarioSet> createFuncionarioSet(FuncionarioSet value) {
        return new JAXBElement<FuncionarioSet>(_FuncionarioSet_QNAME, FuncionarioSet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocenteDel }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsfuncionarios.dedalus.cl/", name = "docenteDel")
    public JAXBElement<DocenteDel> createDocenteDel(DocenteDel value) {
        return new JAXBElement<DocenteDel>(_DocenteDel_QNAME, DocenteDel.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GradosdocenteGet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsfuncionarios.dedalus.cl/", name = "gradosdocenteGet")
    public JAXBElement<GradosdocenteGet> createGradosdocenteGet(GradosdocenteGet value) {
        return new JAXBElement<GradosdocenteGet>(_GradosdocenteGet_QNAME, GradosdocenteGet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ParamGet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsfuncionarios.dedalus.cl/", name = "paramGet")
    public JAXBElement<ParamGet> createParamGet(ParamGet value) {
        return new JAXBElement<ParamGet>(_ParamGet_QNAME, ParamGet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocenteSet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsfuncionarios.dedalus.cl/", name = "docenteSet")
    public JAXBElement<DocenteSet> createDocenteSet(DocenteSet value) {
        return new JAXBElement<DocenteSet>(_DocenteSet_QNAME, DocenteSet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FuncionarioUpd }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsfuncionarios.dedalus.cl/", name = "funcionarioUpd")
    public JAXBElement<FuncionarioUpd> createFuncionarioUpd(FuncionarioUpd value) {
        return new JAXBElement<FuncionarioUpd>(_FuncionarioUpd_QNAME, FuncionarioUpd.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FuncionarioUpdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsfuncionarios.dedalus.cl/", name = "funcionarioUpdResponse")
    public JAXBElement<FuncionarioUpdResponse> createFuncionarioUpdResponse(FuncionarioUpdResponse value) {
        return new JAXBElement<FuncionarioUpdResponse>(_FuncionarioUpdResponse_QNAME, FuncionarioUpdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FacultadesGet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsfuncionarios.dedalus.cl/", name = "facultadesGet")
    public JAXBElement<FacultadesGet> createFacultadesGet(FacultadesGet value) {
        return new JAXBElement<FacultadesGet>(_FacultadesGet_QNAME, FacultadesGet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocenteSetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsfuncionarios.dedalus.cl/", name = "docenteSetResponse")
    public JAXBElement<DocenteSetResponse> createDocenteSetResponse(DocenteSetResponse value) {
        return new JAXBElement<DocenteSetResponse>(_DocenteSetResponse_QNAME, DocenteSetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FuncionariosGetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsfuncionarios.dedalus.cl/", name = "funcionariosGetResponse")
    public JAXBElement<FuncionariosGetResponse> createFuncionariosGetResponse(FuncionariosGetResponse value) {
        return new JAXBElement<FuncionariosGetResponse>(_FuncionariosGetResponse_QNAME, FuncionariosGetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GradosdocenteGetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsfuncionarios.dedalus.cl/", name = "gradosdocenteGetResponse")
    public JAXBElement<GradosdocenteGetResponse> createGradosdocenteGetResponse(GradosdocenteGetResponse value) {
        return new JAXBElement<GradosdocenteGetResponse>(_GradosdocenteGetResponse_QNAME, GradosdocenteGetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HistoriadocGet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsfuncionarios.dedalus.cl/", name = "historiadocGet")
    public JAXBElement<HistoriadocGet> createHistoriadocGet(HistoriadocGet value) {
        return new JAXBElement<HistoriadocGet>(_HistoriadocGet_QNAME, HistoriadocGet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FacultadesGetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsfuncionarios.dedalus.cl/", name = "facultadesGetResponse")
    public JAXBElement<FacultadesGetResponse> createFacultadesGetResponse(FacultadesGetResponse value) {
        return new JAXBElement<FacultadesGetResponse>(_FacultadesGetResponse_QNAME, FacultadesGetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FuncionarioGet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsfuncionarios.dedalus.cl/", name = "funcionarioGet")
    public JAXBElement<FuncionarioGet> createFuncionarioGet(FuncionarioGet value) {
        return new JAXBElement<FuncionarioGet>(_FuncionarioGet_QNAME, FuncionarioGet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HistoriadocGetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsfuncionarios.dedalus.cl/", name = "historiadocGetResponse")
    public JAXBElement<HistoriadocGetResponse> createHistoriadocGetResponse(HistoriadocGetResponse value) {
        return new JAXBElement<HistoriadocGetResponse>(_HistoriadocGetResponse_QNAME, HistoriadocGetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InstitutosGetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsfuncionarios.dedalus.cl/", name = "institutosGetResponse")
    public JAXBElement<InstitutosGetResponse> createInstitutosGetResponse(InstitutosGetResponse value) {
        return new JAXBElement<InstitutosGetResponse>(_InstitutosGetResponse_QNAME, InstitutosGetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InstitutosGet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsfuncionarios.dedalus.cl/", name = "institutosGet")
    public JAXBElement<InstitutosGet> createInstitutosGet(InstitutosGet value) {
        return new JAXBElement<InstitutosGet>(_InstitutosGet_QNAME, InstitutosGet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FuncionarioSetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsfuncionarios.dedalus.cl/", name = "funcionarioSetResponse")
    public JAXBElement<FuncionarioSetResponse> createFuncionarioSetResponse(FuncionarioSetResponse value) {
        return new JAXBElement<FuncionarioSetResponse>(_FuncionarioSetResponse_QNAME, FuncionarioSetResponse.class, null, value);
    }

}
