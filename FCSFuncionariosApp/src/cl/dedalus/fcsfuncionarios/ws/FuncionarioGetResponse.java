
package cl.dedalus.fcsfuncionarios.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para funcionarioGetResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="funcionarioGetResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="funcionarioGetResult" type="{http://ws.fcsfuncionarios.dedalus.cl/}FuncionarioGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "funcionarioGetResponse", propOrder = {
    "funcionarioGetResult"
})
public class FuncionarioGetResponse {

    protected FuncionarioGetResult funcionarioGetResult;

    /**
     * Obtiene el valor de la propiedad funcionarioGetResult.
     * 
     * @return
     *     possible object is
     *     {@link FuncionarioGetResult }
     *     
     */
    public FuncionarioGetResult getFuncionarioGetResult() {
        return funcionarioGetResult;
    }

    /**
     * Define el valor de la propiedad funcionarioGetResult.
     * 
     * @param value
     *     allowed object is
     *     {@link FuncionarioGetResult }
     *     
     */
    public void setFuncionarioGetResult(FuncionarioGetResult value) {
        this.funcionarioGetResult = value;
    }

}
