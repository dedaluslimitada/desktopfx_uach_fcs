
package cl.dedalus.fcsfuncionarios.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para TipocontratoGetResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="TipocontratoGetResult">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.fcsfuncionarios.dedalus.cl/}ProcedureResult">
 *       &lt;sequence>
 *         &lt;element name="conts" type="{http://ws.fcsfuncionarios.dedalus.cl/}TipocontratoGetCont" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TipocontratoGetResult", propOrder = {
    "conts"
})
public class TipocontratoGetResult
    extends ProcedureResult
{

    @XmlElement(nillable = true)
    protected List<TipocontratoGetCont> conts;

    /**
     * Gets the value of the conts property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the conts property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getConts().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TipocontratoGetCont }
     * 
     * 
     */
    public List<TipocontratoGetCont> getConts() {
        if (conts == null) {
            conts = new ArrayList<TipocontratoGetCont>();
        }
        return this.conts;
    }

}
