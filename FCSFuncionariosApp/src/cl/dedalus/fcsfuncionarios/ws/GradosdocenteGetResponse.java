
package cl.dedalus.fcsfuncionarios.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para gradosdocenteGetResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="gradosdocenteGetResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="gradosdocenteGetResult" type="{http://ws.fcsfuncionarios.dedalus.cl/}GradosdocenteGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "gradosdocenteGetResponse", propOrder = {
    "gradosdocenteGetResult"
})
public class GradosdocenteGetResponse {

    protected GradosdocenteGetResult gradosdocenteGetResult;

    /**
     * Obtiene el valor de la propiedad gradosdocenteGetResult.
     * 
     * @return
     *     possible object is
     *     {@link GradosdocenteGetResult }
     *     
     */
    public GradosdocenteGetResult getGradosdocenteGetResult() {
        return gradosdocenteGetResult;
    }

    /**
     * Define el valor de la propiedad gradosdocenteGetResult.
     * 
     * @param value
     *     allowed object is
     *     {@link GradosdocenteGetResult }
     *     
     */
    public void setGradosdocenteGetResult(GradosdocenteGetResult value) {
        this.gradosdocenteGetResult = value;
    }

}
