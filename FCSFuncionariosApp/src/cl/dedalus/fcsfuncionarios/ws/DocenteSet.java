
package cl.dedalus.fcsfuncionarios.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para docenteSet complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="docenteSet">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codiDocente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idenFacultad" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="idenInstituto" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="idenGradoacademico" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "docenteSet", propOrder = {
    "codiDocente",
    "idenFacultad",
    "idenInstituto",
    "idenGradoacademico"
})
public class DocenteSet {

    protected String codiDocente;
    protected int idenFacultad;
    protected int idenInstituto;
    protected int idenGradoacademico;

    /**
     * Obtiene el valor de la propiedad codiDocente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiDocente() {
        return codiDocente;
    }

    /**
     * Define el valor de la propiedad codiDocente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiDocente(String value) {
        this.codiDocente = value;
    }

    /**
     * Obtiene el valor de la propiedad idenFacultad.
     * 
     */
    public int getIdenFacultad() {
        return idenFacultad;
    }

    /**
     * Define el valor de la propiedad idenFacultad.
     * 
     */
    public void setIdenFacultad(int value) {
        this.idenFacultad = value;
    }

    /**
     * Obtiene el valor de la propiedad idenInstituto.
     * 
     */
    public int getIdenInstituto() {
        return idenInstituto;
    }

    /**
     * Define el valor de la propiedad idenInstituto.
     * 
     */
    public void setIdenInstituto(int value) {
        this.idenInstituto = value;
    }

    /**
     * Obtiene el valor de la propiedad idenGradoacademico.
     * 
     */
    public int getIdenGradoacademico() {
        return idenGradoacademico;
    }

    /**
     * Define el valor de la propiedad idenGradoacademico.
     * 
     */
    public void setIdenGradoacademico(int value) {
        this.idenGradoacademico = value;
    }

}
