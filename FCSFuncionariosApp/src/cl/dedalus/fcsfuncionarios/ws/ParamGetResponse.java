
package cl.dedalus.fcsfuncionarios.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para paramGetResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="paramGetResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="paramGetResult" type="{http://ws.fcsfuncionarios.dedalus.cl/}ParamGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "paramGetResponse", propOrder = {
    "paramGetResult"
})
public class ParamGetResponse {

    protected ParamGetResult paramGetResult;

    /**
     * Obtiene el valor de la propiedad paramGetResult.
     * 
     * @return
     *     possible object is
     *     {@link ParamGetResult }
     *     
     */
    public ParamGetResult getParamGetResult() {
        return paramGetResult;
    }

    /**
     * Define el valor de la propiedad paramGetResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ParamGetResult }
     *     
     */
    public void setParamGetResult(ParamGetResult value) {
        this.paramGetResult = value;
    }

}
