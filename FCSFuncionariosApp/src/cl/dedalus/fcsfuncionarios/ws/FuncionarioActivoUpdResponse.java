
package cl.dedalus.fcsfuncionarios.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para funcionarioActivoUpdResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="funcionarioActivoUpdResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="funcionarioActivoUpdResult" type="{http://ws.fcsfuncionarios.dedalus.cl/}FuncionarioActivoUpdResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "funcionarioActivoUpdResponse", propOrder = {
    "funcionarioActivoUpdResult"
})
public class FuncionarioActivoUpdResponse {

    protected FuncionarioActivoUpdResult funcionarioActivoUpdResult;

    /**
     * Obtiene el valor de la propiedad funcionarioActivoUpdResult.
     * 
     * @return
     *     possible object is
     *     {@link FuncionarioActivoUpdResult }
     *     
     */
    public FuncionarioActivoUpdResult getFuncionarioActivoUpdResult() {
        return funcionarioActivoUpdResult;
    }

    /**
     * Define el valor de la propiedad funcionarioActivoUpdResult.
     * 
     * @param value
     *     allowed object is
     *     {@link FuncionarioActivoUpdResult }
     *     
     */
    public void setFuncionarioActivoUpdResult(FuncionarioActivoUpdResult value) {
        this.funcionarioActivoUpdResult = value;
    }

}
