
package cl.dedalus.fcsfuncionarios.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para funcionarioSetResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="funcionarioSetResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="funcionarioSetResult" type="{http://ws.fcsfuncionarios.dedalus.cl/}FuncionarioSetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "funcionarioSetResponse", propOrder = {
    "funcionarioSetResult"
})
public class FuncionarioSetResponse {

    protected FuncionarioSetResult funcionarioSetResult;

    /**
     * Obtiene el valor de la propiedad funcionarioSetResult.
     * 
     * @return
     *     possible object is
     *     {@link FuncionarioSetResult }
     *     
     */
    public FuncionarioSetResult getFuncionarioSetResult() {
        return funcionarioSetResult;
    }

    /**
     * Define el valor de la propiedad funcionarioSetResult.
     * 
     * @param value
     *     allowed object is
     *     {@link FuncionarioSetResult }
     *     
     */
    public void setFuncionarioSetResult(FuncionarioSetResult value) {
        this.funcionarioSetResult = value;
    }

}
