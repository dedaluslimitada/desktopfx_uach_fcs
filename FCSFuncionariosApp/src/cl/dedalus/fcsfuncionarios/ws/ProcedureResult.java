
package cl.dedalus.fcsfuncionarios.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ProcedureResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ProcedureResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProcedureResult")
@XmlSeeAlso({
    FuncionariosGetResult.class,
    GradosdocenteGetResult.class,
    FuncionarioSetResult.class,
    InstitutosGetResult.class,
    DocenteDelResult.class,
    FuncionarioGetResult.class,
    DocenteSetResult.class,
    FuncionarioUpdResult.class,
    FacultadesGetResult.class,
    ParamGetResult.class,
    HistoriadocGetResult.class
})
public abstract class ProcedureResult {


}
