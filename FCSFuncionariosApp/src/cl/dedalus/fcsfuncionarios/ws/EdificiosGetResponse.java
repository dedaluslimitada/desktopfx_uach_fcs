
package cl.dedalus.fcsfuncionarios.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para edificiosGetResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="edificiosGetResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="edificiosGetResult" type="{http://ws.fcsfuncionarios.dedalus.cl/}EdificiosGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "edificiosGetResponse", propOrder = {
    "edificiosGetResult"
})
public class EdificiosGetResponse {

    protected EdificiosGetResult edificiosGetResult;

    /**
     * Obtiene el valor de la propiedad edificiosGetResult.
     * 
     * @return
     *     possible object is
     *     {@link EdificiosGetResult }
     *     
     */
    public EdificiosGetResult getEdificiosGetResult() {
        return edificiosGetResult;
    }

    /**
     * Define el valor de la propiedad edificiosGetResult.
     * 
     * @param value
     *     allowed object is
     *     {@link EdificiosGetResult }
     *     
     */
    public void setEdificiosGetResult(EdificiosGetResult value) {
        this.edificiosGetResult = value;
    }

}
