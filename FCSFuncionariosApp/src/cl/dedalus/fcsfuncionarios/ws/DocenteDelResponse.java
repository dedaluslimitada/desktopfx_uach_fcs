
package cl.dedalus.fcsfuncionarios.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para docenteDelResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="docenteDelResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="docenteDelResult" type="{http://ws.fcsfuncionarios.dedalus.cl/}DocenteDelResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "docenteDelResponse", propOrder = {
    "docenteDelResult"
})
public class DocenteDelResponse {

    protected DocenteDelResult docenteDelResult;

    /**
     * Obtiene el valor de la propiedad docenteDelResult.
     * 
     * @return
     *     possible object is
     *     {@link DocenteDelResult }
     *     
     */
    public DocenteDelResult getDocenteDelResult() {
        return docenteDelResult;
    }

    /**
     * Define el valor de la propiedad docenteDelResult.
     * 
     * @param value
     *     allowed object is
     *     {@link DocenteDelResult }
     *     
     */
    public void setDocenteDelResult(DocenteDelResult value) {
        this.docenteDelResult = value;
    }

}
