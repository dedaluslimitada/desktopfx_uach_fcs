
package cl.dedalus.fcsfuncionarios.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ParamGetGrado complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ParamGetGrado">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="descGradoacademico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idenGradoacademico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ParamGetGrado", propOrder = {
    "descGradoacademico",
    "idenGradoacademico"
})
public class ParamGetGrado {

    protected String descGradoacademico;
    protected String idenGradoacademico;

    /**
     * Obtiene el valor de la propiedad descGradoacademico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescGradoacademico() {
        return descGradoacademico;
    }

    /**
     * Define el valor de la propiedad descGradoacademico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescGradoacademico(String value) {
        this.descGradoacademico = value;
    }

    /**
     * Obtiene el valor de la propiedad idenGradoacademico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdenGradoacademico() {
        return idenGradoacademico;
    }

    /**
     * Define el valor de la propiedad idenGradoacademico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdenGradoacademico(String value) {
        this.idenGradoacademico = value;
    }

}
