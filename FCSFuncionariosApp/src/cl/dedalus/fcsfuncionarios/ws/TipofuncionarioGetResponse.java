
package cl.dedalus.fcsfuncionarios.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para tipofuncionarioGetResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="tipofuncionarioGetResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="tipofuncionarioGetResult" type="{http://ws.fcsfuncionarios.dedalus.cl/}TipofuncionarioGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tipofuncionarioGetResponse", propOrder = {
    "tipofuncionarioGetResult"
})
public class TipofuncionarioGetResponse {

    protected TipofuncionarioGetResult tipofuncionarioGetResult;

    /**
     * Obtiene el valor de la propiedad tipofuncionarioGetResult.
     * 
     * @return
     *     possible object is
     *     {@link TipofuncionarioGetResult }
     *     
     */
    public TipofuncionarioGetResult getTipofuncionarioGetResult() {
        return tipofuncionarioGetResult;
    }

    /**
     * Define el valor de la propiedad tipofuncionarioGetResult.
     * 
     * @param value
     *     allowed object is
     *     {@link TipofuncionarioGetResult }
     *     
     */
    public void setTipofuncionarioGetResult(TipofuncionarioGetResult value) {
        this.tipofuncionarioGetResult = value;
    }

}
