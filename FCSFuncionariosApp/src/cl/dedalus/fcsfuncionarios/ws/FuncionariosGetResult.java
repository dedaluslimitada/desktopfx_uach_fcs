
package cl.dedalus.fcsfuncionarios.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para FuncionariosGetResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="FuncionariosGetResult">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.fcsfuncionarios.dedalus.cl/}ProcedureResult">
 *       &lt;sequence>
 *         &lt;element name="funcs" type="{http://ws.fcsfuncionarios.dedalus.cl/}FuncionariosGetFunc" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FuncionariosGetResult", propOrder = {
    "funcs"
})
public class FuncionariosGetResult
    extends ProcedureResult
{

    @XmlElement(nillable = true)
    protected List<FuncionariosGetFunc> funcs;

    /**
     * Gets the value of the funcs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the funcs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFuncs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FuncionariosGetFunc }
     * 
     * 
     */
    public List<FuncionariosGetFunc> getFuncs() {
        if (funcs == null) {
            funcs = new ArrayList<FuncionariosGetFunc>();
        }
        return this.funcs;
    }

}
