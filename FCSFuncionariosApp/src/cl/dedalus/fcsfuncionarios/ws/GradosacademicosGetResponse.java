
package cl.dedalus.fcsfuncionarios.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para gradosacademicosGetResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="gradosacademicosGetResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="gradosacademicosGetResult" type="{http://ws.fcsfuncionarios.dedalus.cl/}GradosacademicosGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "gradosacademicosGetResponse", propOrder = {
    "gradosacademicosGetResult"
})
public class GradosacademicosGetResponse {

    protected GradosacademicosGetResult gradosacademicosGetResult;

    /**
     * Obtiene el valor de la propiedad gradosacademicosGetResult.
     * 
     * @return
     *     possible object is
     *     {@link GradosacademicosGetResult }
     *     
     */
    public GradosacademicosGetResult getGradosacademicosGetResult() {
        return gradosacademicosGetResult;
    }

    /**
     * Define el valor de la propiedad gradosacademicosGetResult.
     * 
     * @param value
     *     allowed object is
     *     {@link GradosacademicosGetResult }
     *     
     */
    public void setGradosacademicosGetResult(GradosacademicosGetResult value) {
        this.gradosacademicosGetResult = value;
    }

}
