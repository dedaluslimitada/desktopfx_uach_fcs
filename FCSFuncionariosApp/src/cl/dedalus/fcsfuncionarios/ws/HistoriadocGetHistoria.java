
package cl.dedalus.fcsfuncionarios.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para HistoriadocGetHistoria complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="HistoriadocGetHistoria">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="fechFin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechIni" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="tipoCont" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoFunc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HistoriadocGetHistoria", propOrder = {
    "fechFin",
    "fechIni",
    "id",
    "tipoCont",
    "tipoFunc"
})
public class HistoriadocGetHistoria {

    protected String fechFin;
    protected String fechIni;
    protected long id;
    protected String tipoCont;
    protected String tipoFunc;

    /**
     * Obtiene el valor de la propiedad fechFin.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechFin() {
        return fechFin;
    }

    /**
     * Define el valor de la propiedad fechFin.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechFin(String value) {
        this.fechFin = value;
    }

    /**
     * Obtiene el valor de la propiedad fechIni.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechIni() {
        return fechIni;
    }

    /**
     * Define el valor de la propiedad fechIni.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechIni(String value) {
        this.fechIni = value;
    }

    /**
     * Obtiene el valor de la propiedad id.
     * 
     */
    public long getId() {
        return id;
    }

    /**
     * Define el valor de la propiedad id.
     * 
     */
    public void setId(long value) {
        this.id = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoCont.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoCont() {
        return tipoCont;
    }

    /**
     * Define el valor de la propiedad tipoCont.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoCont(String value) {
        this.tipoCont = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoFunc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoFunc() {
        return tipoFunc;
    }

    /**
     * Define el valor de la propiedad tipoFunc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoFunc(String value) {
        this.tipoFunc = value;
    }

}
