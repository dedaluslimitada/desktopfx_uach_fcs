
package cl.dedalus.fcsfuncionarios.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para funcionarioGet complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="funcionarioGet">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codiFun" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "funcionarioGet", propOrder = {
    "codiFun"
})
public class FuncionarioGet {

    protected String codiFun;

    /**
     * Obtiene el valor de la propiedad codiFun.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiFun() {
        return codiFun;
    }

    /**
     * Define el valor de la propiedad codiFun.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiFun(String value) {
        this.codiFun = value;
    }

}
