
package cl.dedalus.fcsfuncionarios.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para GradosacademicosGetResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="GradosacademicosGetResult">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.fcsfuncionarios.dedalus.cl/}ProcedureResult">
 *       &lt;sequence>
 *         &lt;element name="grados" type="{http://ws.fcsfuncionarios.dedalus.cl/}GradosacademicosGetGrado" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GradosacademicosGetResult", propOrder = {
    "grados"
})
public class GradosacademicosGetResult
    extends ProcedureResult
{

    @XmlElement(nillable = true)
    protected List<GradosacademicosGetGrado> grados;

    /**
     * Gets the value of the grados property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the grados property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGrados().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GradosacademicosGetGrado }
     * 
     * 
     */
    public List<GradosacademicosGetGrado> getGrados() {
        if (grados == null) {
            grados = new ArrayList<GradosacademicosGetGrado>();
        }
        return this.grados;
    }

}
