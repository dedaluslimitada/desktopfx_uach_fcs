package cl.dedalus.fcsfuncionarios;

import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;

public class historiadoc{
    private final SimpleLongProperty id;
    private final SimpleStringProperty desc_tipofunc;
    private final SimpleStringProperty desc_tipocont;
    private final SimpleStringProperty fech_ini;
    private final SimpleStringProperty fech_fin;

    public historiadoc(final Long id, final String desc_tipofunc, final String desc_tipocont, final String fech_ini, final String fech_fin ){
        this.id = new SimpleLongProperty(id);
        this.desc_tipofunc = new SimpleStringProperty(desc_tipofunc);
        this.desc_tipocont = new SimpleStringProperty(desc_tipocont);
        this.fech_ini = new SimpleStringProperty(fech_ini);
        this.fech_fin = new SimpleStringProperty(fech_fin);
    }

    public Long getId() {
        return id.get();
    }
    public String getDesc_tipofunc() {
        return desc_tipofunc.get();
    }
    public String getDesc_tipocont() {
        return desc_tipocont.get();
    }
    public String getFech_ini() {
        return fech_ini.get();
    }
    public String getFech_fin() {
        return fech_fin.get();
    }
 }
