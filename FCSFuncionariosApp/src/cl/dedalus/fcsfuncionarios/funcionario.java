package cl.dedalus.fcsfuncionarios;

import javafx.beans.property.SimpleStringProperty;

public class funcionario{
    private final SimpleStringProperty codi_fun;
    private final SimpleStringProperty rut_fun;
    private final SimpleStringProperty desc_nom;
    private final SimpleStringProperty desc_pat;
    private final SimpleStringProperty tipo_fun;
    private final SimpleStringProperty tipo_con;

    public funcionario(final String codi_fun,final String rut_fun, final String desc_nom, final String desc_pat, final String tipo_fun, final String tipo_con){
        this.codi_fun = new SimpleStringProperty(codi_fun);
        this.rut_fun = new SimpleStringProperty(rut_fun);
        this.desc_nom = new SimpleStringProperty(desc_nom);
        this.desc_pat = new SimpleStringProperty(desc_pat);
        this.tipo_fun = new SimpleStringProperty(tipo_fun);
        this.tipo_con = new SimpleStringProperty(tipo_con);

    }

    public String getCodi_fun() {
        return codi_fun.get();
    }
    public String getRut_fun() {
        return rut_fun.get();
    }
    public String getDesc_nom() {
        return desc_nom.get();
    }
    public String getDesc_pat() {
        return desc_pat.get();
    }
    public String getTipo_fun() {
        return tipo_fun.get();
    }
    public String getTipo_con() {
        return tipo_con.get();
    }


}
