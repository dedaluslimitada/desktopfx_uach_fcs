/*
 * Source: DocentesGetCaller.java - Generated by OBCOM SQL Wizard 1.110
 * Author: Aldo Ulloa Carrasco (Dedalus Limitada)
 *
 * Copyright (c) OBCOM INGENIERIA S.A. (Chile). All rights reserved.
 *
 * All rights to this product are owned by OBCOM INGENIERIA S.A. and may only be
 * used  under  the  terms of its associated license document. You may NOT copy,
 * modify, sublicense, or distribute this source file or portions of  it  unless
 * previously  authorized in writing by OBCOM INGENIERIA S.A. In any event, this
 * notice and above copyright must always be included verbatim with this file.
 */

package cl.dedalus.fcsasignaturadocente.ws;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

/**
 * Caller of procedure {@code fcsasignaturadocente$docentes_get}.
 *
 * @author Aldo Ulloa Carrasco (Dedalus Limitada)
 */
public class DocentesGetCaller extends ProcedureCaller
{
    //--------------------------------------------------------------------------
    //-- Constructor Methods ---------------------------------------------------
    //--------------------------------------------------------------------------

    /**
     * Constructs a new {@code DocentesGetCaller} instance.
     */
    public DocentesGetCaller()
    {
    }

    //--------------------------------------------------------------------------
    //-- Execute Methods -------------------------------------------------------
    //--------------------------------------------------------------------------

    /**
     * Executes procedure {@code fcsasignaturadocente$docentes_get} using a data source.
     *
     * @param  dataSource the connection data source.
     * @param  idenFac {@code _iden_fac int4}.
     * @param  idenIns {@code _iden_ins int4}.
     * @return the output parameters and result sets of the procedure.
     * @throws SQLException if an SQL error occurs.
     */
    public DocentesGetResult executeProc(DataSource dataSource, int idenFac, int idenIns)
        throws SQLException
    {
        try (final Connection conn = dataSource.getConnection()) {
            return executeProc(conn, idenFac, idenIns);
        }
    }

    /**
     * Executes procedure {@code fcsasignaturadocente$docentes_get} using a connection.
     *
     * @param  conn the database connection.
     * @param  idenFac {@code _iden_fac int4}.
     * @param  idenIns {@code _iden_ins int4}.
     * @return the output parameters and result sets of the procedure.
     * @throws SQLException if an SQL error occurs.
     */
    public DocentesGetResult executeProc(Connection conn, int idenFac, int idenIns)
        throws SQLException
    {
        final DocentesGetResult result = createProcResult();
        final String jdbcURL = getJdbcURL(conn);
        if (jdbcURL.startsWith("jdbc:oracle:")) {
            try (final CallableStatement call = prepareCall(conn, "{call fcsasignaturadocente$docentes_get(?,?,?)}")) {
                call.setInt(1, idenFac);
                call.setInt(2, idenIns);
                call.registerOutParameter(3, ORACLE_CURSOR);
                call.execute();
                result.setDocentes(readDocentes((ResultSet) call.getObject(3)));
            }
        } else if (jdbcURL.startsWith("jdbc:postgresql:")) {
            try (final CallableStatement call = prepareCall(conn, "{call fcsasignaturadocente$docentes_get(?,?,?)}")) {
                call.setInt(1, idenFac);
                call.setInt(2, idenIns);
                call.registerOutParameter(3, Types.OTHER);
                call.execute();
                result.setDocentes(readDocentes((ResultSet) call.getObject(3)));
            }
        } else {
            try (final CallableStatement call = prepareCall(conn, "{call fcsasignaturadocente$docentes_get(?,?)}")) {
                call.setInt(1, idenFac);
                call.setInt(2, idenIns);
                int updateCount = 0;
                boolean haveRset = call.execute();
                while (haveRset || updateCount != -1) {
                    if (!haveRset) {
                        updateCount = call.getUpdateCount();
                    } else if (result.getDocentes() == null) {
                        result.setDocentes(readDocentes(call.getResultSet()));
                    } else {
                        unexpectedResultSet(call.getResultSet());
                    }
                    haveRset = call.getMoreResults();
                }
            }
        }
        return result;
    }

    /**
     * Creates a new {@code DocentesGetResult} instance.
     *
     * @return a new {@code DocentesGetResult} instance.
     */
    protected DocentesGetResult createProcResult()
    {
        return new DocentesGetResult();
    }

    /**
     * Converts a result set to an array of {@code DocentesGetDocente}.
     *
     * @param  resultSet the result set to convert (can be null).
     * @return an array of {@code DocentesGetDocente} or null.
     * @throws SQLException if an SQL error occurs.
     */
    protected DocentesGetDocente[] readDocentes(final ResultSet resultSet)
        throws SQLException
    {
        if (resultSet == null)
            return null;
        try {
            // Obtain ordinal (index) numbers of result columns
            final int cCodiDocente = resultSet.findColumn("CODI_DOCENTE");
            final int cDescNombres = resultSet.findColumn("DESC_NOMBRES");
            final int cDescPaterno = resultSet.findColumn("DESC_PATERNO");
            final int cDescMaterno = resultSet.findColumn("DESC_MATERNO");

            // Convert result rows to an array of "DocentesGetDocente"
            final List<DocentesGetDocente> list = new ArrayList<>();
            while (resultSet.next()) {
                final DocentesGetDocente item = new DocentesGetDocente();
                item.setCodiDocente(resultSet.getString(cCodiDocente));
                item.setDescNombres(resultSet.getString(cDescNombres));
                item.setDescPaterno(resultSet.getString(cDescPaterno));
                item.setDescMaterno(resultSet.getString(cDescMaterno));
                if (filterDocente(item)) list.add(item);
            }
            return list.toArray(new DocentesGetDocente[list.size()]);
        } finally {
            resultSet.close();
        }
    }

    /**
     * Returns true if the supplied item should be added to the array.
     *
     * @param  item the teim that will be checked.
     * @return true if the item should be added to the array.
     */
    protected boolean filterDocente(final DocentesGetDocente item)
    {
        return true;
    }

    //--------------------------------------------------------------------------
    //-- Static Methods --------------------------------------------------------
    //--------------------------------------------------------------------------

    /**
     * Executes procedure {@code fcsasignaturadocente$docentes_get} using a data source.
     *
     * @param  dataSource the connection data source.
     * @param  idenFac {@code _iden_fac int4}.
     * @param  idenIns {@code _iden_ins int4}.
     * @return the output parameters and result sets of the procedure.
     * @throws SQLException if an SQL error occurs.
     */
    public static DocentesGetResult execute(DataSource dataSource, int idenFac, int idenIns)
        throws SQLException
    {
        return new DocentesGetCaller().executeProc(dataSource, idenFac, idenIns);
    }

    /**
     * Executes procedure {@code fcsasignaturadocente$docentes_get} using a connection.
     *
     * @param  conn the database connection.
     * @param  idenFac {@code _iden_fac int4}.
     * @param  idenIns {@code _iden_ins int4}.
     * @return the output parameters and result sets of the procedure.
     * @throws SQLException if an SQL error occurs.
     */
    public static DocentesGetResult execute(Connection conn, int idenFac, int idenIns)
        throws SQLException
    {
        return new DocentesGetCaller().executeProc(conn, idenFac, idenIns);
    }
}
