/*
 * Source: DocenteasignaturaSetResult.java - Generated by OBCOM SQL Wizard 1.110
 * Author: Aldo Ulloa Carrasco (Dedalus Limitada)
 *
 * Copyright (c) OBCOM INGENIERIA S.A. (Chile). All rights reserved.
 *
 * All rights to this product are owned by OBCOM INGENIERIA S.A. and may only be
 * used  under  the  terms of its associated license document. You may NOT copy,
 * modify, sublicense, or distribute this source file or portions of  it  unless
 * previously  authorized in writing by OBCOM INGENIERIA S.A. In any event, this
 * notice and above copyright must always be included verbatim with this file.
 */

package cl.dedalus.fcsasignaturadocente.ws;

import javax.xml.bind.annotation.XmlType;

/**
 * Results of procedure {@code fcsasignaturadocente$docenteasignatura_set}.
 *
 * @author Aldo Ulloa Carrasco (Dedalus Limitada)
 */
@XmlType(name = "DocenteasignaturaSetResult")
public class DocenteasignaturaSetResult extends ProcedureResult
{
    private static final long serialVersionUID = 1L;

    //--------------------------------------------------------------------------
    //-- Constructor Methods ---------------------------------------------------
    //--------------------------------------------------------------------------

    /**
     * Constructs a new {@code DocenteasignaturaSetResult} instance.
     */
    public DocenteasignaturaSetResult()
    {
    }
}
