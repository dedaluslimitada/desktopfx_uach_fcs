package cl.dedalus.utils.http;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class HttpRequest {
    public HttpRequest() {
        super();
    }
    public static InputStream get(String urlStr) throws Exception {
        return readFrom(urlStr, null);
    }
    public static InputStream post(String urlStr, String post) 
        throws Exception 
    {
        return readFrom(urlStr, post);
    }
    private static InputStream readFrom(String urlStr, String post) 
        throws MalformedURLException, IOException 
    {
        URLConnection conn = new URL(urlStr).openConnection();
        conn.setConnectTimeout(15000);
        conn.setReadTimeout(15000);
        conn.setDoInput(true);
        if (post != null && post.length() > 0) {
            conn.setDoOutput(true);
            DataOutputStream output = 
                    new DataOutputStream(conn.getOutputStream());
            output.writeBytes(post);
            output.flush();
            output.close();
        }
        return conn.getInputStream();
    }
}