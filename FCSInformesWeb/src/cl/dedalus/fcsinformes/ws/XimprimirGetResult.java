/*
 * Source: XimprimirGetResult.java - Generated by OBCOM SQL Wizard 1.110
 * Author: Aldo Ulloa Carrasco (Dedalus Limitada)
 *
 * Copyright (c) OBCOM INGENIERIA S.A. (Chile). All rights reserved.
 *
 * All rights to this product are owned by OBCOM INGENIERIA S.A. and may only be
 * used  under  the  terms of its associated license document. You may NOT copy,
 * modify, sublicense, or distribute this source file or portions of  it  unless
 * previously  authorized in writing by OBCOM INGENIERIA S.A. In any event, this
 * notice and above copyright must always be included verbatim with this file.
 */

package cl.dedalus.fcsinformes.ws;

import javax.xml.bind.annotation.XmlType;

/**
 * Results of procedure {@code fcsinformes$ximprimir_get}.
 *
 * <code><pre>
 * v_reporte  bytea  Output
 * </pre></code>
 *
 * @author Aldo Ulloa Carrasco (Dedalus Limitada)
 */
@XmlType(name = "XimprimirGetResult")
public class XimprimirGetResult extends ProcedureResult
{
    private static final long serialVersionUID = 1L;
    private byte[] vReporte;

    //--------------------------------------------------------------------------
    //-- Constructor Methods ---------------------------------------------------
    //--------------------------------------------------------------------------

    /**
     * Constructs a new {@code XimprimirGetResult} instance.
     */
    public XimprimirGetResult()
    {
    }

    //--------------------------------------------------------------------------
    //-- Property Methods ------------------------------------------------------
    //--------------------------------------------------------------------------

    /**
     * Returns the value of property {@code vReporte}.
     *
     * @return the current value of the property.
     */
    public byte[] getVReporte()
    {
        return vReporte;
    }

    /**
     * Changes the value of property {@code vReporte}.
     *
     * @param value the new value of the property.
     */
    public void setVReporte(final byte[] value)
    {
        this.vReporte = value;
    }
}
