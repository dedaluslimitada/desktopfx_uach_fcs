/*
 * Source: CalculaTotalCaller.java - Generated by OBCOM SQL Wizard 1.110
 * Author: Aldo Ulloa Carrasco (Dedalus Limitada)
 *
 * Copyright (c) OBCOM INGENIERIA S.A. (Chile). All rights reserved.
 *
 * All rights to this product are owned by OBCOM INGENIERIA S.A. and may only be
 * used  under  the  terms of its associated license document. You may NOT copy,
 * modify, sublicense, or distribute this source file or portions of  it  unless
 * previously  authorized in writing by OBCOM INGENIERIA S.A. In any event, this
 * notice and above copyright must always be included verbatim with this file.
 */

package cl.dedalus.fcsinformes.ws;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 * Caller of procedure {@code fcsinformes$calcula_total}.
 *
 * @author Aldo Ulloa Carrasco (Dedalus Limitada)
 */
public class CalculaTotalCaller extends ProcedureCaller
{
    //--------------------------------------------------------------------------
    //-- Constructor Methods ---------------------------------------------------
    //--------------------------------------------------------------------------

    /**
     * Constructs a new {@code CalculaTotalCaller} instance.
     */
    public CalculaTotalCaller()
    {
    }

    //--------------------------------------------------------------------------
    //-- Execute Methods -------------------------------------------------------
    //--------------------------------------------------------------------------

    /**
     * Executes procedure {@code fcsinformes$calcula_total} using a data source.
     *
     * @param  dataSource the connection data source.
     * @param  codiFacultad {@code _codi_facultad int4}.
     * @param  codiInstituto {@code _codi_instituto int4}.
     * @param  codiSemestre {@code _codi_semestre varchar}.
     * @param  codiAgno {@code _codi_agno varchar}.
     * @return the output parameters and result sets of the procedure.
     * @throws SQLException if an SQL error occurs.
     */
    public CalculaTotalResult executeProc(DataSource dataSource, int codiFacultad, int codiInstituto, String codiSemestre, String codiAgno)
        throws SQLException
    {
        try (final Connection conn = dataSource.getConnection()) {
            return executeProc(conn, codiFacultad, codiInstituto, codiSemestre, codiAgno);
        }
    }

    /**
     * Executes procedure {@code fcsinformes$calcula_total} using a connection.
     *
     * @param  conn the database connection.
     * @param  codiFacultad {@code _codi_facultad int4}.
     * @param  codiInstituto {@code _codi_instituto int4}.
     * @param  codiSemestre {@code _codi_semestre varchar}.
     * @param  codiAgno {@code _codi_agno varchar}.
     * @return the output parameters and result sets of the procedure.
     * @throws SQLException if an SQL error occurs.
     */
    public CalculaTotalResult executeProc(Connection conn, int codiFacultad, int codiInstituto, String codiSemestre, String codiAgno)
        throws SQLException
    {
        final CalculaTotalResult result = createProcResult();
        try (final CallableStatement call = prepareCall(conn, "{call fcsinformes$calcula_total(?,?,?,?)}")) {
            call.setInt(1, codiFacultad);
            call.setInt(2, codiInstituto);
            call.setString(3, codiSemestre);
            call.setString(4, codiAgno);
            call.execute();
        }
        return result;
    }

    /**
     * Creates a new {@code CalculaTotalResult} instance.
     *
     * @return a new {@code CalculaTotalResult} instance.
     */
    protected CalculaTotalResult createProcResult()
    {
        return new CalculaTotalResult();
    }

    //--------------------------------------------------------------------------
    //-- Static Methods --------------------------------------------------------
    //--------------------------------------------------------------------------

    /**
     * Executes procedure {@code fcsinformes$calcula_total} using a data source.
     *
     * @param  dataSource the connection data source.
     * @param  codiFacultad {@code _codi_facultad int4}.
     * @param  codiInstituto {@code _codi_instituto int4}.
     * @param  codiSemestre {@code _codi_semestre varchar}.
     * @param  codiAgno {@code _codi_agno varchar}.
     * @return the output parameters and result sets of the procedure.
     * @throws SQLException if an SQL error occurs.
     */
    public static CalculaTotalResult execute(DataSource dataSource, int codiFacultad, int codiInstituto, String codiSemestre, String codiAgno)
        throws SQLException
    {
        return new CalculaTotalCaller().executeProc(dataSource, codiFacultad, codiInstituto, codiSemestre, codiAgno);
    }

    /**
     * Executes procedure {@code fcsinformes$calcula_total} using a connection.
     *
     * @param  conn the database connection.
     * @param  codiFacultad {@code _codi_facultad int4}.
     * @param  codiInstituto {@code _codi_instituto int4}.
     * @param  codiSemestre {@code _codi_semestre varchar}.
     * @param  codiAgno {@code _codi_agno varchar}.
     * @return the output parameters and result sets of the procedure.
     * @throws SQLException if an SQL error occurs.
     */
    public static CalculaTotalResult execute(Connection conn, int codiFacultad, int codiInstituto, String codiSemestre, String codiAgno)
        throws SQLException
    {
        return new CalculaTotalCaller().executeProc(conn, codiFacultad, codiInstituto, codiSemestre, codiAgno);
    }
}
