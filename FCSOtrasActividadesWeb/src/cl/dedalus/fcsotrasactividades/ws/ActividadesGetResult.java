/*
 * Source: ActividadesGetResult.java - Generated by OBCOM SQL Wizard 1.108
 * Author: Aldo Ulloa Carrasco (Dedalus Limitada)
 *
 * Copyright (c) OBCOM INGENIERIA S.A. (Chile). All rights reserved.
 *
 * All rights to this product are owned by OBCOM INGENIERIA S.A. and may only be
 * used  under  the  terms of its associated license document. You may NOT copy,
 * modify, sublicense, or distribute this source file or portions of  it  unless
 * previously  authorized in writing by OBCOM INGENIERIA S.A. In any event, this
 * notice and above copyright must always be included verbatim with this file.
 */

package cl.dedalus.fcsotrasactividades.ws;

import javax.xml.bind.annotation.XmlType;

/**
 * Results of procedure {@code fcsotrasactividades$actividades_get}.
 *
 * <code><pre>
 * ACTIV  ResultSet  Output
 * </pre></code>
 *
 * @author Aldo Ulloa Carrasco (Dedalus Limitada)
 */
@XmlType(name = "ActividadesGetResult")
public class ActividadesGetResult extends ProcedureResult
{
    private static final long serialVersionUID = 1L;
    private ActividadesGetActiv[] activs;

    //--------------------------------------------------------------------------
    //-- Constructor Methods ---------------------------------------------------
    //--------------------------------------------------------------------------

    /**
     * Constructs a new {@code ActividadesGetResult} instance.
     */
    public ActividadesGetResult()
    {
    }

    //--------------------------------------------------------------------------
    //-- Property Methods ------------------------------------------------------
    //--------------------------------------------------------------------------

    /**
     * Returns the value of property {@code activs}.
     *
     * @return the current value of the property.
     */
    public ActividadesGetActiv[] getActivs()
    {
        return activs;
    }

    /**
     * Changes the value of property {@code activs}.
     *
     * @param value the new value of the property.
     */
    public void setActivs(final ActividadesGetActiv[] value)
    {
        this.activs = value;
    }
}
