package cl.dedalus.fcsbuscadoc;

import java.awt.Desktop;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;

import cl.dedalus.fcsbuscadoc.ws.DocumentoGetResult;
import cl.dedalus.fcsbuscadoc.ws.DocumentosGetDoc;
import cl.dedalus.fcsbuscadoc.ws.DocumentosGetResult;
import cl.dedalus.fcsbuscadoc.ws.FcsbuscadocWeb;
import cl.dedalus.fcsbuscadoc.ws.FcsbuscadocWebException;
import cl.dedalus.fcsbuscadoc.ws.FcsbuscadocWebService;
import cl.obcom.desktopfx.core.DesktopTask;
import cl.obcom.desktopfx.jfx.Dialog;
import cl.obcom.desktopfx.jfx.DialogAction;
import cl.obcom.desktopfx.jfx.FxmlPane;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;

public class Main extends FxmlPane
{
	@FXML TableView<doc> tb_documentos = new TableView<>();
    @FXML private Button bo_salir;
    @FXML private Button bo_buscar;
    @FXML private TextField tx_titulo;
    @FXML private TextField tx_autor;
    @FXML private TextField tx_agno;

    final FcsbuscadocWeb port;
    
    final ObservableList<doc> dataDocumento = FXCollections.observableArrayList ();

    public Main(final DesktopTask task) throws Exception
    {
        // Se carga fxml
        loadFxml();
        task.getStage().getScene().getStylesheets().add("resources/css/NuevoEstilo.css");

      
        //Inicializar WS
        final FcsbuscadocWebService service = new FcsbuscadocWebService();
        port = service.getFcsbuscadocWebPort();
        task.initWebServicePort(port,service,task.getCodeBase());

      //Init Grillas tb_documento
        final TableColumn<doc, String> cTipo = new TableColumn<>("Tipo");
        cTipo.setCellValueFactory(new PropertyValueFactory<doc, String>("tipo"));
        tb_documentos.getColumns().add(cTipo);
        final TableColumn<doc, String> cTitulo = new TableColumn<>("Titulo");
        cTitulo.setCellValueFactory(new PropertyValueFactory<doc, String>("titulo"));
        tb_documentos.getColumns().add(cTitulo);
        final TableColumn<doc, String> cAutor = new TableColumn<>("Autor");
        cAutor.setCellValueFactory(new PropertyValueFactory<doc, String>("autor"));
        tb_documentos.getColumns().add(cAutor);
        final TableColumn<doc, String> cAgno = new TableColumn<>("Año");
        cAgno.setCellValueFactory(new PropertyValueFactory<doc, String>("agno"));
        tb_documentos.getColumns().add(cAgno);
        tb_documentos.setItems(dataDocumento);
        cTipo.prefWidthProperty().bind(tb_documentos.widthProperty().multiply(0.10));
        cTitulo.prefWidthProperty().bind(tb_documentos.widthProperty().multiply(0.40));
        cAutor.prefWidthProperty().bind(tb_documentos.widthProperty().multiply(0.35));
        cAgno.prefWidthProperty().bind(tb_documentos.widthProperty().multiply(0.15));
       
        //Doble Click en tb_documentos para ver el documento
        tb_documentos.setOnMousePressed(event -> {
            if (event.isPrimaryButtonDown() && event.getClickCount() == 2) {
                verDocumento(task);
            }
        });
                
        bo_salir.setOnAction(evt -> task.terminate(true));
        bo_buscar.setOnAction(evt -> buscarDocs());
    }
    
    public void verDocumento(DesktopTask T){
    	 final doc t = dataDocumento.get(tb_documentos.getSelectionModel().getSelectedIndex());
         String v_archivo="";

         DocumentoGetResult res = null;
         byte[] P=null;

         
         try {
             res=port.documentoGet(t.getId(), t.getTipo());
             P=res.getContDocumento();
             v_archivo=res.getNombArchivo();
             
         } catch (final NumberFormatException e) {
             Dialog.showError(this, e);
         } catch (final FcsbuscadocWebException e) {
             Dialog.showError(this, e);
         }
         final String v_file = T.getPluginDirectory().toString()+"/"+v_archivo;  // en /OBCOM creo el archivo

          //Se define archivo salida
         OutputStream tt=null;
         try {
             tt = new FileOutputStream(new File(v_file));
             try {
                 tt.write(P);
             } catch (final IOException e) {
                 Dialog.showError(this, e);
             }
         } catch (final FileNotFoundException e) {
             Dialog.showError(this, e);
         }

         // Se abre en el Cliente.
         final File myFile = new File(v_file);
         openFile(myFile);
    }
    
    public void openFile(final File myfile) {
        try {
            Desktop.getDesktop().open(myfile);
        } catch (final IOException ex) {
           Dialog.showError(this, ex);
        }
    }
    
    public void buscarDocs(){
        dataDocumento.clear();
        DocumentosGetResult res = null;
        try {
        	res = port.documentosGet(tx_titulo.getText().trim(), tx_autor.getText().trim(), tx_agno.getText().trim());
        	for ( final DocumentosGetDoc t : res.getDocs()) {
        		dataDocumento.add(new doc( t.getId(), t.getTipo(), t.getTitulo(), t.getAutor(), t.getAgno() ));
        	}
        	tb_documentos.setItems(dataDocumento);
        } catch (final FcsbuscadocWebException e) {
        	Dialog.showError(this, e);
        }
    }

} // Main - FXMPane
