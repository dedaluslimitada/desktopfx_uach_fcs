
package cl.dedalus.fcsbuscadoc.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cl.dedalus.fcsbuscadoc.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DocumentoGetResponse_QNAME = new QName("http://ws.fcsbuscadoc.dedalus.cl/", "documentoGetResponse");
    private final static QName _DocumentoGet_QNAME = new QName("http://ws.fcsbuscadoc.dedalus.cl/", "documentoGet");
    private final static QName _DocumentosGet_QNAME = new QName("http://ws.fcsbuscadoc.dedalus.cl/", "documentosGet");
    private final static QName _FaultInfo_QNAME = new QName("http://ws.fcsbuscadoc.dedalus.cl/", "faultInfo");
    private final static QName _DocumentosGetResponse_QNAME = new QName("http://ws.fcsbuscadoc.dedalus.cl/", "documentosGetResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cl.dedalus.fcsbuscadoc.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DocumentosGetResponse }
     * 
     */
    public DocumentosGetResponse createDocumentosGetResponse() {
        return new DocumentosGetResponse();
    }

    /**
     * Create an instance of {@link DocumentosGet }
     * 
     */
    public DocumentosGet createDocumentosGet() {
        return new DocumentosGet();
    }

    /**
     * Create an instance of {@link DocumentoGetResponse }
     * 
     */
    public DocumentoGetResponse createDocumentoGetResponse() {
        return new DocumentoGetResponse();
    }

    /**
     * Create an instance of {@link DocumentoGet }
     * 
     */
    public DocumentoGet createDocumentoGet() {
        return new DocumentoGet();
    }

    /**
     * Create an instance of {@link DocumentosGetResult }
     * 
     */
    public DocumentosGetResult createDocumentosGetResult() {
        return new DocumentosGetResult();
    }

    /**
     * Create an instance of {@link DocumentosGetDoc }
     * 
     */
    public DocumentosGetDoc createDocumentosGetDoc() {
        return new DocumentosGetDoc();
    }

    /**
     * Create an instance of {@link DocumentoGetResult }
     * 
     */
    public DocumentoGetResult createDocumentoGetResult() {
        return new DocumentoGetResult();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocumentoGetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsbuscadoc.dedalus.cl/", name = "documentoGetResponse")
    public JAXBElement<DocumentoGetResponse> createDocumentoGetResponse(DocumentoGetResponse value) {
        return new JAXBElement<DocumentoGetResponse>(_DocumentoGetResponse_QNAME, DocumentoGetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocumentoGet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsbuscadoc.dedalus.cl/", name = "documentoGet")
    public JAXBElement<DocumentoGet> createDocumentoGet(DocumentoGet value) {
        return new JAXBElement<DocumentoGet>(_DocumentoGet_QNAME, DocumentoGet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocumentosGet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsbuscadoc.dedalus.cl/", name = "documentosGet")
    public JAXBElement<DocumentosGet> createDocumentosGet(DocumentosGet value) {
        return new JAXBElement<DocumentosGet>(_DocumentosGet_QNAME, DocumentosGet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsbuscadoc.dedalus.cl/", name = "faultInfo")
    public JAXBElement<String> createFaultInfo(String value) {
        return new JAXBElement<String>(_FaultInfo_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocumentosGetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsbuscadoc.dedalus.cl/", name = "documentosGetResponse")
    public JAXBElement<DocumentosGetResponse> createDocumentosGetResponse(DocumentosGetResponse value) {
        return new JAXBElement<DocumentosGetResponse>(_DocumentosGetResponse_QNAME, DocumentosGetResponse.class, null, value);
    }

}
