
package cl.dedalus.fcsbuscadoc.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para documentosGetResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="documentosGetResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="documentosGetResult" type="{http://ws.fcsbuscadoc.dedalus.cl/}DocumentosGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "documentosGetResponse", propOrder = {
    "documentosGetResult"
})
public class DocumentosGetResponse {

    protected DocumentosGetResult documentosGetResult;

    /**
     * Obtiene el valor de la propiedad documentosGetResult.
     * 
     * @return
     *     possible object is
     *     {@link DocumentosGetResult }
     *     
     */
    public DocumentosGetResult getDocumentosGetResult() {
        return documentosGetResult;
    }

    /**
     * Define el valor de la propiedad documentosGetResult.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentosGetResult }
     *     
     */
    public void setDocumentosGetResult(DocumentosGetResult value) {
        this.documentosGetResult = value;
    }

}
