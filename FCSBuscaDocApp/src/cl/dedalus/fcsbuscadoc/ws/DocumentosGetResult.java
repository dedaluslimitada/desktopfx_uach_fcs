
package cl.dedalus.fcsbuscadoc.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DocumentosGetResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DocumentosGetResult">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.fcsbuscadoc.dedalus.cl/}ProcedureResult">
 *       &lt;sequence>
 *         &lt;element name="docs" type="{http://ws.fcsbuscadoc.dedalus.cl/}DocumentosGetDoc" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocumentosGetResult", propOrder = {
    "docs"
})
public class DocumentosGetResult
    extends ProcedureResult
{

    @XmlElement(nillable = true)
    protected List<DocumentosGetDoc> docs;

    /**
     * Gets the value of the docs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the docs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDocs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DocumentosGetDoc }
     * 
     * 
     */
    public List<DocumentosGetDoc> getDocs() {
        if (docs == null) {
            docs = new ArrayList<DocumentosGetDoc>();
        }
        return this.docs;
    }

}
