
package cl.dedalus.fcsbuscadoc.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DocumentoGetResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DocumentoGetResult">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.fcsbuscadoc.dedalus.cl/}ProcedureResult">
 *       &lt;sequence>
 *         &lt;element name="contDocumento" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="nombArchivo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocumentoGetResult", propOrder = {
    "contDocumento",
    "nombArchivo"
})
public class DocumentoGetResult
    extends ProcedureResult
{

    protected byte[] contDocumento;
    protected String nombArchivo;

    /**
     * Obtiene el valor de la propiedad contDocumento.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getContDocumento() {
        return contDocumento;
    }

    /**
     * Define el valor de la propiedad contDocumento.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setContDocumento(byte[] value) {
        this.contDocumento = value;
    }

    /**
     * Obtiene el valor de la propiedad nombArchivo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombArchivo() {
        return nombArchivo;
    }

    /**
     * Define el valor de la propiedad nombArchivo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombArchivo(String value) {
        this.nombArchivo = value;
    }

}
