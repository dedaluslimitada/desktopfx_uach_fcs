package cl.dedalus.fcsbuscadoc;

import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;

public class doc{
    private final SimpleLongProperty id;
    private final SimpleStringProperty tipo;
    private final SimpleStringProperty titulo;
    private final SimpleStringProperty autor;
    private final SimpleStringProperty agno;

    public doc(final Long id, final String tipo,final String titulo, final String autor, final String agno){
        this.id = new SimpleLongProperty(id);
        this.tipo = new SimpleStringProperty(tipo);
        this.titulo = new SimpleStringProperty(titulo);
        this.autor = new SimpleStringProperty(autor);
        this.agno = new SimpleStringProperty(agno);
    }

    public Long getId() {
        return id.get();
    }
    public String getTipo() {
        return tipo.get();
    }
    public String getTitulo() {
        return titulo.get();
    }
    public String getAutor() {
        return autor.get();
    }
    public String getAgno() {
        return agno.get();
    }
}
