
package cl.dedalus.fcsasignaturadocente.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para AsignaturasGetAsig complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="AsignaturasGetAsig">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cantHl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantHp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantHt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantSemanas" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codiAsig" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descAsig" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AsignaturasGetAsig", propOrder = {
    "cantHl",
    "cantHp",
    "cantHt",
    "cantSemanas",
    "codiAsig",
    "descAsig"
})
public class AsignaturasGetAsig {

    protected String cantHl;
    protected String cantHp;
    protected String cantHt;
    protected String cantSemanas;
    protected String codiAsig;
    protected String descAsig;

    /**
     * Obtiene el valor de la propiedad cantHl.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantHl() {
        return cantHl;
    }

    /**
     * Define el valor de la propiedad cantHl.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantHl(String value) {
        this.cantHl = value;
    }

    /**
     * Obtiene el valor de la propiedad cantHp.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantHp() {
        return cantHp;
    }

    /**
     * Define el valor de la propiedad cantHp.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantHp(String value) {
        this.cantHp = value;
    }

    /**
     * Obtiene el valor de la propiedad cantHt.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantHt() {
        return cantHt;
    }

    /**
     * Define el valor de la propiedad cantHt.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantHt(String value) {
        this.cantHt = value;
    }

    /**
     * Obtiene el valor de la propiedad cantSemanas.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantSemanas() {
        return cantSemanas;
    }

    /**
     * Define el valor de la propiedad cantSemanas.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantSemanas(String value) {
        this.cantSemanas = value;
    }

    /**
     * Obtiene el valor de la propiedad codiAsig.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiAsig() {
        return codiAsig;
    }

    /**
     * Define el valor de la propiedad codiAsig.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiAsig(String value) {
        this.codiAsig = value;
    }

    /**
     * Obtiene el valor de la propiedad descAsig.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescAsig() {
        return descAsig;
    }

    /**
     * Define el valor de la propiedad descAsig.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescAsig(String value) {
        this.descAsig = value;
    }

}
