
package cl.dedalus.fcsasignaturadocente.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para docenteasignaturaSetResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="docenteasignaturaSetResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="docenteasignaturaSetResult" type="{http://ws.fcsasignaturadocente.dedalus.cl/}DocenteasignaturaSetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "docenteasignaturaSetResponse", propOrder = {
    "docenteasignaturaSetResult"
})
public class DocenteasignaturaSetResponse {

    protected DocenteasignaturaSetResult docenteasignaturaSetResult;

    /**
     * Obtiene el valor de la propiedad docenteasignaturaSetResult.
     * 
     * @return
     *     possible object is
     *     {@link DocenteasignaturaSetResult }
     *     
     */
    public DocenteasignaturaSetResult getDocenteasignaturaSetResult() {
        return docenteasignaturaSetResult;
    }

    /**
     * Define el valor de la propiedad docenteasignaturaSetResult.
     * 
     * @param value
     *     allowed object is
     *     {@link DocenteasignaturaSetResult }
     *     
     */
    public void setDocenteasignaturaSetResult(DocenteasignaturaSetResult value) {
        this.docenteasignaturaSetResult = value;
    }

}
