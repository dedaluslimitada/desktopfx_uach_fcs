
package cl.dedalus.fcsasignaturadocente.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para facultadesGetResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="facultadesGetResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="facultadesGetResult" type="{http://ws.fcsasignaturadocente.dedalus.cl/}FacultadesGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "facultadesGetResponse", propOrder = {
    "facultadesGetResult"
})
public class FacultadesGetResponse {

    protected FacultadesGetResult facultadesGetResult;

    /**
     * Obtiene el valor de la propiedad facultadesGetResult.
     * 
     * @return
     *     possible object is
     *     {@link FacultadesGetResult }
     *     
     */
    public FacultadesGetResult getFacultadesGetResult() {
        return facultadesGetResult;
    }

    /**
     * Define el valor de la propiedad facultadesGetResult.
     * 
     * @param value
     *     allowed object is
     *     {@link FacultadesGetResult }
     *     
     */
    public void setFacultadesGetResult(FacultadesGetResult value) {
        this.facultadesGetResult = value;
    }

}
