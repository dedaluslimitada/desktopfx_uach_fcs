
package cl.dedalus.fcsasignaturadocente.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para FacultadesGetResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="FacultadesGetResult">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.fcsasignaturadocente.dedalus.cl/}ProcedureResult">
 *       &lt;sequence>
 *         &lt;element name="facs" type="{http://ws.fcsasignaturadocente.dedalus.cl/}FacultadesGetFac" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FacultadesGetResult", propOrder = {
    "facs"
})
public class FacultadesGetResult
    extends ProcedureResult
{

    @XmlElement(nillable = true)
    protected List<FacultadesGetFac> facs;

    /**
     * Gets the value of the facs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the facs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFacs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FacultadesGetFac }
     * 
     * 
     */
    public List<FacultadesGetFac> getFacs() {
        if (facs == null) {
            facs = new ArrayList<FacultadesGetFac>();
        }
        return this.facs;
    }

}
