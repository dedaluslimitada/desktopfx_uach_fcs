
package cl.dedalus.fcsasignaturadocente.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para docenteasignaturaSet complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="docenteasignaturaSet">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codiAsig" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codiSeme" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codiAgno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codiDocente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantSemPart" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "docenteasignaturaSet", propOrder = {
    "codiAsig",
    "codiSeme",
    "codiAgno",
    "codiDocente",
    "cantSemPart"
})
public class DocenteasignaturaSet {

    protected String codiAsig;
    protected String codiSeme;
    protected String codiAgno;
    protected String codiDocente;
    protected int cantSemPart;

    /**
     * Obtiene el valor de la propiedad codiAsig.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiAsig() {
        return codiAsig;
    }

    /**
     * Define el valor de la propiedad codiAsig.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiAsig(String value) {
        this.codiAsig = value;
    }

    /**
     * Obtiene el valor de la propiedad codiSeme.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiSeme() {
        return codiSeme;
    }

    /**
     * Define el valor de la propiedad codiSeme.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiSeme(String value) {
        this.codiSeme = value;
    }

    /**
     * Obtiene el valor de la propiedad codiAgno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiAgno() {
        return codiAgno;
    }

    /**
     * Define el valor de la propiedad codiAgno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiAgno(String value) {
        this.codiAgno = value;
    }

    /**
     * Obtiene el valor de la propiedad codiDocente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiDocente() {
        return codiDocente;
    }

    /**
     * Define el valor de la propiedad codiDocente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiDocente(String value) {
        this.codiDocente = value;
    }

    /**
     * Obtiene el valor de la propiedad cantSemPart.
     * 
     */
    public int getCantSemPart() {
        return cantSemPart;
    }

    /**
     * Define el valor de la propiedad cantSemPart.
     * 
     */
    public void setCantSemPart(int value) {
        this.cantSemPart = value;
    }

}
