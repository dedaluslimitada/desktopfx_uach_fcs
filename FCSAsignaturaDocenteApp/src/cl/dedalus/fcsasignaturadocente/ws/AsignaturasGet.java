
package cl.dedalus.fcsasignaturadocente.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para asignaturasGet complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="asignaturasGet">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idenFac" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="idenIns" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="codiSeme" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codiAgno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "asignaturasGet", propOrder = {
    "idenFac",
    "idenIns",
    "codiSeme",
    "codiAgno"
})
public class AsignaturasGet {

    protected int idenFac;
    protected int idenIns;
    protected String codiSeme;
    protected String codiAgno;

    /**
     * Obtiene el valor de la propiedad idenFac.
     * 
     */
    public int getIdenFac() {
        return idenFac;
    }

    /**
     * Define el valor de la propiedad idenFac.
     * 
     */
    public void setIdenFac(int value) {
        this.idenFac = value;
    }

    /**
     * Obtiene el valor de la propiedad idenIns.
     * 
     */
    public int getIdenIns() {
        return idenIns;
    }

    /**
     * Define el valor de la propiedad idenIns.
     * 
     */
    public void setIdenIns(int value) {
        this.idenIns = value;
    }

    /**
     * Obtiene el valor de la propiedad codiSeme.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiSeme() {
        return codiSeme;
    }

    /**
     * Define el valor de la propiedad codiSeme.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiSeme(String value) {
        this.codiSeme = value;
    }

    /**
     * Obtiene el valor de la propiedad codiAgno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiAgno() {
        return codiAgno;
    }

    /**
     * Define el valor de la propiedad codiAgno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiAgno(String value) {
        this.codiAgno = value;
    }

}
