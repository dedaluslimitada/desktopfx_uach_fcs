
package cl.dedalus.fcsasignaturadocente.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para InstitutosGetResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="InstitutosGetResult">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.fcsasignaturadocente.dedalus.cl/}ProcedureResult">
 *       &lt;sequence>
 *         &lt;element name="instits" type="{http://ws.fcsasignaturadocente.dedalus.cl/}InstitutosGetInstit" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InstitutosGetResult", propOrder = {
    "instits"
})
public class InstitutosGetResult
    extends ProcedureResult
{

    @XmlElement(nillable = true)
    protected List<InstitutosGetInstit> instits;

    /**
     * Gets the value of the instits property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the instits property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInstits().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InstitutosGetInstit }
     * 
     * 
     */
    public List<InstitutosGetInstit> getInstits() {
        if (instits == null) {
            instits = new ArrayList<InstitutosGetInstit>();
        }
        return this.instits;
    }

}
