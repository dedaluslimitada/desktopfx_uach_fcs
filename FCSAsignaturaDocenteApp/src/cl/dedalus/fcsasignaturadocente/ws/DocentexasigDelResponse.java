
package cl.dedalus.fcsasignaturadocente.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para docentexasigDelResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="docentexasigDelResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="docentexasigDelResult" type="{http://ws.fcsasignaturadocente.dedalus.cl/}DocentexasigDelResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "docentexasigDelResponse", propOrder = {
    "docentexasigDelResult"
})
public class DocentexasigDelResponse {

    protected DocentexasigDelResult docentexasigDelResult;

    /**
     * Obtiene el valor de la propiedad docentexasigDelResult.
     * 
     * @return
     *     possible object is
     *     {@link DocentexasigDelResult }
     *     
     */
    public DocentexasigDelResult getDocentexasigDelResult() {
        return docentexasigDelResult;
    }

    /**
     * Define el valor de la propiedad docentexasigDelResult.
     * 
     * @param value
     *     allowed object is
     *     {@link DocentexasigDelResult }
     *     
     */
    public void setDocentexasigDelResult(DocentexasigDelResult value) {
        this.docentexasigDelResult = value;
    }

}
