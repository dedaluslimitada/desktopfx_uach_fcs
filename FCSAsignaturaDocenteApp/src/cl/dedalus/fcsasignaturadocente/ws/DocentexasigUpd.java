
package cl.dedalus.fcsasignaturadocente.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para docentexasigUpd complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="docentexasigUpd">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codiDocente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="flagResponsable" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="flagPrimerizo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantAlumnos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codiAsignatura" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codiSemestre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codiAgno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "docentexasigUpd", propOrder = {
    "codiDocente",
    "id",
    "flagResponsable",
    "flagPrimerizo",
    "cantAlumnos",
    "codiAsignatura",
    "codiSemestre",
    "codiAgno"
})
public class DocentexasigUpd {

    protected String codiDocente;
    protected int id;
    protected String flagResponsable;
    protected String flagPrimerizo;
    protected String cantAlumnos;
    protected String codiAsignatura;
    protected String codiSemestre;
    protected String codiAgno;

    /**
     * Obtiene el valor de la propiedad codiDocente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiDocente() {
        return codiDocente;
    }

    /**
     * Define el valor de la propiedad codiDocente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiDocente(String value) {
        this.codiDocente = value;
    }

    /**
     * Obtiene el valor de la propiedad id.
     * 
     */
    public int getId() {
        return id;
    }

    /**
     * Define el valor de la propiedad id.
     * 
     */
    public void setId(int value) {
        this.id = value;
    }

    /**
     * Obtiene el valor de la propiedad flagResponsable.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlagResponsable() {
        return flagResponsable;
    }

    /**
     * Define el valor de la propiedad flagResponsable.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlagResponsable(String value) {
        this.flagResponsable = value;
    }

    /**
     * Obtiene el valor de la propiedad flagPrimerizo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlagPrimerizo() {
        return flagPrimerizo;
    }

    /**
     * Define el valor de la propiedad flagPrimerizo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlagPrimerizo(String value) {
        this.flagPrimerizo = value;
    }

    /**
     * Obtiene el valor de la propiedad cantAlumnos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantAlumnos() {
        return cantAlumnos;
    }

    /**
     * Define el valor de la propiedad cantAlumnos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantAlumnos(String value) {
        this.cantAlumnos = value;
    }

    /**
     * Obtiene el valor de la propiedad codiAsignatura.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiAsignatura() {
        return codiAsignatura;
    }

    /**
     * Define el valor de la propiedad codiAsignatura.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiAsignatura(String value) {
        this.codiAsignatura = value;
    }

    /**
     * Obtiene el valor de la propiedad codiSemestre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiSemestre() {
        return codiSemestre;
    }

    /**
     * Define el valor de la propiedad codiSemestre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiSemestre(String value) {
        this.codiSemestre = value;
    }

    /**
     * Obtiene el valor de la propiedad codiAgno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiAgno() {
        return codiAgno;
    }

    /**
     * Define el valor de la propiedad codiAgno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiAgno(String value) {
        this.codiAgno = value;
    }

}
