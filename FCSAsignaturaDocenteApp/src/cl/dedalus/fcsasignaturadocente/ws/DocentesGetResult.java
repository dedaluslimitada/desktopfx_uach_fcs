
package cl.dedalus.fcsasignaturadocente.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DocentesGetResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DocentesGetResult">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.fcsasignaturadocente.dedalus.cl/}ProcedureResult">
 *       &lt;sequence>
 *         &lt;element name="docentes" type="{http://ws.fcsasignaturadocente.dedalus.cl/}DocentesGetDocente" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocentesGetResult", propOrder = {
    "docentes"
})
public class DocentesGetResult
    extends ProcedureResult
{

    @XmlElement(nillable = true)
    protected List<DocentesGetDocente> docentes;

    /**
     * Gets the value of the docentes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the docentes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDocentes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DocentesGetDocente }
     * 
     * 
     */
    public List<DocentesGetDocente> getDocentes() {
        if (docentes == null) {
            docentes = new ArrayList<DocentesGetDocente>();
        }
        return this.docentes;
    }

}
