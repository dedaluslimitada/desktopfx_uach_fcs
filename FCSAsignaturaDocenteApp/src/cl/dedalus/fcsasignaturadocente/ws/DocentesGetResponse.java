
package cl.dedalus.fcsasignaturadocente.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para docentesGetResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="docentesGetResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="docentesGetResult" type="{http://ws.fcsasignaturadocente.dedalus.cl/}DocentesGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "docentesGetResponse", propOrder = {
    "docentesGetResult"
})
public class DocentesGetResponse {

    protected DocentesGetResult docentesGetResult;

    /**
     * Obtiene el valor de la propiedad docentesGetResult.
     * 
     * @return
     *     possible object is
     *     {@link DocentesGetResult }
     *     
     */
    public DocentesGetResult getDocentesGetResult() {
        return docentesGetResult;
    }

    /**
     * Define el valor de la propiedad docentesGetResult.
     * 
     * @param value
     *     allowed object is
     *     {@link DocentesGetResult }
     *     
     */
    public void setDocentesGetResult(DocentesGetResult value) {
        this.docentesGetResult = value;
    }

}
