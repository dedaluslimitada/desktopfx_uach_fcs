
package cl.dedalus.fcsasignaturadocente.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DocentesGetDocente complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DocentesGetDocente">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codiDocente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descMaterno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descNombres" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descPaterno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocentesGetDocente", propOrder = {
    "codiDocente",
    "descMaterno",
    "descNombres",
    "descPaterno"
})
public class DocentesGetDocente {

    protected String codiDocente;
    protected String descMaterno;
    protected String descNombres;
    protected String descPaterno;

    /**
     * Obtiene el valor de la propiedad codiDocente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiDocente() {
        return codiDocente;
    }

    /**
     * Define el valor de la propiedad codiDocente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiDocente(String value) {
        this.codiDocente = value;
    }

    /**
     * Obtiene el valor de la propiedad descMaterno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescMaterno() {
        return descMaterno;
    }

    /**
     * Define el valor de la propiedad descMaterno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescMaterno(String value) {
        this.descMaterno = value;
    }

    /**
     * Obtiene el valor de la propiedad descNombres.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescNombres() {
        return descNombres;
    }

    /**
     * Define el valor de la propiedad descNombres.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescNombres(String value) {
        this.descNombres = value;
    }

    /**
     * Obtiene el valor de la propiedad descPaterno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescPaterno() {
        return descPaterno;
    }

    /**
     * Define el valor de la propiedad descPaterno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescPaterno(String value) {
        this.descPaterno = value;
    }

}
