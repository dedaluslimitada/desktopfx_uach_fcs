
package cl.dedalus.fcsasignaturadocente.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cl.dedalus.fcsasignaturadocente.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DocentesasignaturaGet_QNAME = new QName("http://ws.fcsasignaturadocente.dedalus.cl/", "docentesasignaturaGet");
    private final static QName _AsignaturasGetResponse_QNAME = new QName("http://ws.fcsasignaturadocente.dedalus.cl/", "asignaturasGetResponse");
    private final static QName _DocentesGetResponse_QNAME = new QName("http://ws.fcsasignaturadocente.dedalus.cl/", "docentesGetResponse");
    private final static QName _DocentexasigDel_QNAME = new QName("http://ws.fcsasignaturadocente.dedalus.cl/", "docentexasigDel");
    private final static QName _DocentexasigUpd_QNAME = new QName("http://ws.fcsasignaturadocente.dedalus.cl/", "docentexasigUpd");
    private final static QName _DocentexasigUpdResponse_QNAME = new QName("http://ws.fcsasignaturadocente.dedalus.cl/", "docentexasigUpdResponse");
    private final static QName _FaultInfo_QNAME = new QName("http://ws.fcsasignaturadocente.dedalus.cl/", "faultInfo");
    private final static QName _DocentesasignaturaGetResponse_QNAME = new QName("http://ws.fcsasignaturadocente.dedalus.cl/", "docentesasignaturaGetResponse");
    private final static QName _DocenteasignaturaSet_QNAME = new QName("http://ws.fcsasignaturadocente.dedalus.cl/", "docenteasignaturaSet");
    private final static QName _DocentesGet_QNAME = new QName("http://ws.fcsasignaturadocente.dedalus.cl/", "docentesGet");
    private final static QName _DocenteasignaturaSetResponse_QNAME = new QName("http://ws.fcsasignaturadocente.dedalus.cl/", "docenteasignaturaSetResponse");
    private final static QName _InstitutosGetResponse_QNAME = new QName("http://ws.fcsasignaturadocente.dedalus.cl/", "institutosGetResponse");
    private final static QName _AsignaturasGet_QNAME = new QName("http://ws.fcsasignaturadocente.dedalus.cl/", "asignaturasGet");
    private final static QName _InstitutosGet_QNAME = new QName("http://ws.fcsasignaturadocente.dedalus.cl/", "institutosGet");
    private final static QName _DocentexasigDelResponse_QNAME = new QName("http://ws.fcsasignaturadocente.dedalus.cl/", "docentexasigDelResponse");
    private final static QName _FacultadesGet_QNAME = new QName("http://ws.fcsasignaturadocente.dedalus.cl/", "facultadesGet");
    private final static QName _FacultadesGetResponse_QNAME = new QName("http://ws.fcsasignaturadocente.dedalus.cl/", "facultadesGetResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cl.dedalus.fcsasignaturadocente.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DocentexasigDelResponse }
     * 
     */
    public DocentexasigDelResponse createDocentexasigDelResponse() {
        return new DocentexasigDelResponse();
    }

    /**
     * Create an instance of {@link FacultadesGet }
     * 
     */
    public FacultadesGet createFacultadesGet() {
        return new FacultadesGet();
    }

    /**
     * Create an instance of {@link FacultadesGetResponse }
     * 
     */
    public FacultadesGetResponse createFacultadesGetResponse() {
        return new FacultadesGetResponse();
    }

    /**
     * Create an instance of {@link DocenteasignaturaSet }
     * 
     */
    public DocenteasignaturaSet createDocenteasignaturaSet() {
        return new DocenteasignaturaSet();
    }

    /**
     * Create an instance of {@link DocentesGet }
     * 
     */
    public DocentesGet createDocentesGet() {
        return new DocentesGet();
    }

    /**
     * Create an instance of {@link DocenteasignaturaSetResponse }
     * 
     */
    public DocenteasignaturaSetResponse createDocenteasignaturaSetResponse() {
        return new DocenteasignaturaSetResponse();
    }

    /**
     * Create an instance of {@link InstitutosGetResponse }
     * 
     */
    public InstitutosGetResponse createInstitutosGetResponse() {
        return new InstitutosGetResponse();
    }

    /**
     * Create an instance of {@link AsignaturasGet }
     * 
     */
    public AsignaturasGet createAsignaturasGet() {
        return new AsignaturasGet();
    }

    /**
     * Create an instance of {@link InstitutosGet }
     * 
     */
    public InstitutosGet createInstitutosGet() {
        return new InstitutosGet();
    }

    /**
     * Create an instance of {@link DocentesasignaturaGetResponse }
     * 
     */
    public DocentesasignaturaGetResponse createDocentesasignaturaGetResponse() {
        return new DocentesasignaturaGetResponse();
    }

    /**
     * Create an instance of {@link DocentesasignaturaGet }
     * 
     */
    public DocentesasignaturaGet createDocentesasignaturaGet() {
        return new DocentesasignaturaGet();
    }

    /**
     * Create an instance of {@link AsignaturasGetResponse }
     * 
     */
    public AsignaturasGetResponse createAsignaturasGetResponse() {
        return new AsignaturasGetResponse();
    }

    /**
     * Create an instance of {@link DocentesGetResponse }
     * 
     */
    public DocentesGetResponse createDocentesGetResponse() {
        return new DocentesGetResponse();
    }

    /**
     * Create an instance of {@link DocentexasigDel }
     * 
     */
    public DocentexasigDel createDocentexasigDel() {
        return new DocentexasigDel();
    }

    /**
     * Create an instance of {@link DocentexasigUpd }
     * 
     */
    public DocentexasigUpd createDocentexasigUpd() {
        return new DocentexasigUpd();
    }

    /**
     * Create an instance of {@link DocentexasigUpdResponse }
     * 
     */
    public DocentexasigUpdResponse createDocentexasigUpdResponse() {
        return new DocentexasigUpdResponse();
    }

    /**
     * Create an instance of {@link DocentesGetResult }
     * 
     */
    public DocentesGetResult createDocentesGetResult() {
        return new DocentesGetResult();
    }

    /**
     * Create an instance of {@link DocentesGetDocente }
     * 
     */
    public DocentesGetDocente createDocentesGetDocente() {
        return new DocentesGetDocente();
    }

    /**
     * Create an instance of {@link DocentesasignaturaGetDocente }
     * 
     */
    public DocentesasignaturaGetDocente createDocentesasignaturaGetDocente() {
        return new DocentesasignaturaGetDocente();
    }

    /**
     * Create an instance of {@link AsignaturasGetAsig }
     * 
     */
    public AsignaturasGetAsig createAsignaturasGetAsig() {
        return new AsignaturasGetAsig();
    }

    /**
     * Create an instance of {@link DocentexasigDelResult }
     * 
     */
    public DocentexasigDelResult createDocentexasigDelResult() {
        return new DocentexasigDelResult();
    }

    /**
     * Create an instance of {@link InstitutosGetResult }
     * 
     */
    public InstitutosGetResult createInstitutosGetResult() {
        return new InstitutosGetResult();
    }

    /**
     * Create an instance of {@link InstitutosGetInstit }
     * 
     */
    public InstitutosGetInstit createInstitutosGetInstit() {
        return new InstitutosGetInstit();
    }

    /**
     * Create an instance of {@link DocentesasignaturaGetResult }
     * 
     */
    public DocentesasignaturaGetResult createDocentesasignaturaGetResult() {
        return new DocentesasignaturaGetResult();
    }

    /**
     * Create an instance of {@link FacultadesGetFac }
     * 
     */
    public FacultadesGetFac createFacultadesGetFac() {
        return new FacultadesGetFac();
    }

    /**
     * Create an instance of {@link AsignaturasGetResult }
     * 
     */
    public AsignaturasGetResult createAsignaturasGetResult() {
        return new AsignaturasGetResult();
    }

    /**
     * Create an instance of {@link FacultadesGetResult }
     * 
     */
    public FacultadesGetResult createFacultadesGetResult() {
        return new FacultadesGetResult();
    }

    /**
     * Create an instance of {@link DocentexasigUpdResult }
     * 
     */
    public DocentexasigUpdResult createDocentexasigUpdResult() {
        return new DocentexasigUpdResult();
    }

    /**
     * Create an instance of {@link DocenteasignaturaSetResult }
     * 
     */
    public DocenteasignaturaSetResult createDocenteasignaturaSetResult() {
        return new DocenteasignaturaSetResult();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocentesasignaturaGet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsasignaturadocente.dedalus.cl/", name = "docentesasignaturaGet")
    public JAXBElement<DocentesasignaturaGet> createDocentesasignaturaGet(DocentesasignaturaGet value) {
        return new JAXBElement<DocentesasignaturaGet>(_DocentesasignaturaGet_QNAME, DocentesasignaturaGet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AsignaturasGetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsasignaturadocente.dedalus.cl/", name = "asignaturasGetResponse")
    public JAXBElement<AsignaturasGetResponse> createAsignaturasGetResponse(AsignaturasGetResponse value) {
        return new JAXBElement<AsignaturasGetResponse>(_AsignaturasGetResponse_QNAME, AsignaturasGetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocentesGetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsasignaturadocente.dedalus.cl/", name = "docentesGetResponse")
    public JAXBElement<DocentesGetResponse> createDocentesGetResponse(DocentesGetResponse value) {
        return new JAXBElement<DocentesGetResponse>(_DocentesGetResponse_QNAME, DocentesGetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocentexasigDel }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsasignaturadocente.dedalus.cl/", name = "docentexasigDel")
    public JAXBElement<DocentexasigDel> createDocentexasigDel(DocentexasigDel value) {
        return new JAXBElement<DocentexasigDel>(_DocentexasigDel_QNAME, DocentexasigDel.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocentexasigUpd }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsasignaturadocente.dedalus.cl/", name = "docentexasigUpd")
    public JAXBElement<DocentexasigUpd> createDocentexasigUpd(DocentexasigUpd value) {
        return new JAXBElement<DocentexasigUpd>(_DocentexasigUpd_QNAME, DocentexasigUpd.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocentexasigUpdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsasignaturadocente.dedalus.cl/", name = "docentexasigUpdResponse")
    public JAXBElement<DocentexasigUpdResponse> createDocentexasigUpdResponse(DocentexasigUpdResponse value) {
        return new JAXBElement<DocentexasigUpdResponse>(_DocentexasigUpdResponse_QNAME, DocentexasigUpdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsasignaturadocente.dedalus.cl/", name = "faultInfo")
    public JAXBElement<String> createFaultInfo(String value) {
        return new JAXBElement<String>(_FaultInfo_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocentesasignaturaGetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsasignaturadocente.dedalus.cl/", name = "docentesasignaturaGetResponse")
    public JAXBElement<DocentesasignaturaGetResponse> createDocentesasignaturaGetResponse(DocentesasignaturaGetResponse value) {
        return new JAXBElement<DocentesasignaturaGetResponse>(_DocentesasignaturaGetResponse_QNAME, DocentesasignaturaGetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocenteasignaturaSet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsasignaturadocente.dedalus.cl/", name = "docenteasignaturaSet")
    public JAXBElement<DocenteasignaturaSet> createDocenteasignaturaSet(DocenteasignaturaSet value) {
        return new JAXBElement<DocenteasignaturaSet>(_DocenteasignaturaSet_QNAME, DocenteasignaturaSet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocentesGet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsasignaturadocente.dedalus.cl/", name = "docentesGet")
    public JAXBElement<DocentesGet> createDocentesGet(DocentesGet value) {
        return new JAXBElement<DocentesGet>(_DocentesGet_QNAME, DocentesGet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocenteasignaturaSetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsasignaturadocente.dedalus.cl/", name = "docenteasignaturaSetResponse")
    public JAXBElement<DocenteasignaturaSetResponse> createDocenteasignaturaSetResponse(DocenteasignaturaSetResponse value) {
        return new JAXBElement<DocenteasignaturaSetResponse>(_DocenteasignaturaSetResponse_QNAME, DocenteasignaturaSetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InstitutosGetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsasignaturadocente.dedalus.cl/", name = "institutosGetResponse")
    public JAXBElement<InstitutosGetResponse> createInstitutosGetResponse(InstitutosGetResponse value) {
        return new JAXBElement<InstitutosGetResponse>(_InstitutosGetResponse_QNAME, InstitutosGetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AsignaturasGet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsasignaturadocente.dedalus.cl/", name = "asignaturasGet")
    public JAXBElement<AsignaturasGet> createAsignaturasGet(AsignaturasGet value) {
        return new JAXBElement<AsignaturasGet>(_AsignaturasGet_QNAME, AsignaturasGet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InstitutosGet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsasignaturadocente.dedalus.cl/", name = "institutosGet")
    public JAXBElement<InstitutosGet> createInstitutosGet(InstitutosGet value) {
        return new JAXBElement<InstitutosGet>(_InstitutosGet_QNAME, InstitutosGet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocentexasigDelResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsasignaturadocente.dedalus.cl/", name = "docentexasigDelResponse")
    public JAXBElement<DocentexasigDelResponse> createDocentexasigDelResponse(DocentexasigDelResponse value) {
        return new JAXBElement<DocentexasigDelResponse>(_DocentexasigDelResponse_QNAME, DocentexasigDelResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FacultadesGet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsasignaturadocente.dedalus.cl/", name = "facultadesGet")
    public JAXBElement<FacultadesGet> createFacultadesGet(FacultadesGet value) {
        return new JAXBElement<FacultadesGet>(_FacultadesGet_QNAME, FacultadesGet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FacultadesGetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsasignaturadocente.dedalus.cl/", name = "facultadesGetResponse")
    public JAXBElement<FacultadesGetResponse> createFacultadesGetResponse(FacultadesGetResponse value) {
        return new JAXBElement<FacultadesGetResponse>(_FacultadesGetResponse_QNAME, FacultadesGetResponse.class, null, value);
    }

}
