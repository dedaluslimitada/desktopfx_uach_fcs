
package cl.dedalus.fcsasignaturadocente.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DocentesasignaturaGetDocente complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DocentesasignaturaGetDocente">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codiDocente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descNombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="flagPrim" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="flagResp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocentesasignaturaGetDocente", propOrder = {
    "codiDocente",
    "descNombre",
    "flagPrim",
    "flagResp",
    "id"
})
public class DocentesasignaturaGetDocente {

    protected String codiDocente;
    protected String descNombre;
    protected String flagPrim;
    protected String flagResp;
    protected int id;

    /**
     * Obtiene el valor de la propiedad codiDocente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiDocente() {
        return codiDocente;
    }

    /**
     * Define el valor de la propiedad codiDocente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiDocente(String value) {
        this.codiDocente = value;
    }

    /**
     * Obtiene el valor de la propiedad descNombre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescNombre() {
        return descNombre;
    }

    /**
     * Define el valor de la propiedad descNombre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescNombre(String value) {
        this.descNombre = value;
    }

    /**
     * Obtiene el valor de la propiedad flagPrim.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlagPrim() {
        return flagPrim;
    }

    /**
     * Define el valor de la propiedad flagPrim.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlagPrim(String value) {
        this.flagPrim = value;
    }

    /**
     * Obtiene el valor de la propiedad flagResp.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlagResp() {
        return flagResp;
    }

    /**
     * Define el valor de la propiedad flagResp.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlagResp(String value) {
        this.flagResp = value;
    }

    /**
     * Obtiene el valor de la propiedad id.
     * 
     */
    public int getId() {
        return id;
    }

    /**
     * Define el valor de la propiedad id.
     * 
     */
    public void setId(int value) {
        this.id = value;
    }

}
