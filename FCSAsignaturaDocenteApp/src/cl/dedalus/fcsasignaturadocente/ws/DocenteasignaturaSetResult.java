
package cl.dedalus.fcsasignaturadocente.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DocenteasignaturaSetResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DocenteasignaturaSetResult">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.fcsasignaturadocente.dedalus.cl/}ProcedureResult">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocenteasignaturaSetResult")
public class DocenteasignaturaSetResult
    extends ProcedureResult
{


}
