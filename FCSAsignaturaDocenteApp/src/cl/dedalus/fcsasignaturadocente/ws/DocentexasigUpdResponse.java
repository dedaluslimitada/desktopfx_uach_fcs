
package cl.dedalus.fcsasignaturadocente.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para docentexasigUpdResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="docentexasigUpdResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="docentexasigUpdResult" type="{http://ws.fcsasignaturadocente.dedalus.cl/}DocentexasigUpdResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "docentexasigUpdResponse", propOrder = {
    "docentexasigUpdResult"
})
public class DocentexasigUpdResponse {

    protected DocentexasigUpdResult docentexasigUpdResult;

    /**
     * Obtiene el valor de la propiedad docentexasigUpdResult.
     * 
     * @return
     *     possible object is
     *     {@link DocentexasigUpdResult }
     *     
     */
    public DocentexasigUpdResult getDocentexasigUpdResult() {
        return docentexasigUpdResult;
    }

    /**
     * Define el valor de la propiedad docentexasigUpdResult.
     * 
     * @param value
     *     allowed object is
     *     {@link DocentexasigUpdResult }
     *     
     */
    public void setDocentexasigUpdResult(DocentexasigUpdResult value) {
        this.docentexasigUpdResult = value;
    }

}
