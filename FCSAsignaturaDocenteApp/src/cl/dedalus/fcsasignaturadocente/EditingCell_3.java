package cl.dedalus.fcsasignaturadocente;

import java.util.ArrayList;
import java.util.List;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;

class EditingCell_3 extends TableCell<docentexasig, String> {
        private TextField textField;

        public EditingCell_3() {

        }

        @Override
        public void startEdit() {
            super.startEdit();
           // if (textField == null) {
                createTextField();
           // }
            setGraphic(textField);
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            Platform.runLater(() -> {
                textField.selectAll();
                textField.requestFocus();
            });
        }


        @Override
        public void cancelEdit() {
            super.cancelEdit();
            setText(getItem());
            setContentDisplay(ContentDisplay.TEXT_ONLY);
        }

        @Override
        public void updateItem(final String item, final boolean empty) {
            super.updateItem(item, empty);

            if (empty) {
                setText(null);
                setGraphic(null);
            } else {

                if (isEditing()) {
                    if (textField != null) {
                        textField.setText(getString());
                    }
                    setGraphic(textField);
                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                } else {
                    setText(getString());
                    setContentDisplay(ContentDisplay.TEXT_ONLY);
                }
            }
        }

        private void createTextField() {
            textField = new TextField(getString());
            textField.setMinWidth(this.getWidth() - this.getGraphicTextGap()*2);
            textField.setOnKeyPressed(t -> {

                if (t.getCode() == KeyCode.ENTER) {
                    commitEditFinal(textField.getText());
                } else if (t.getCode() == KeyCode.ESCAPE) {
                    cancelEdit();
                } else if (t.getCode() == KeyCode.TAB) {
                    commitEditFinal(textField.getText());
                    final TableColumn nextColumn = getNextColumn(!t.isShiftDown());
                    if (nextColumn != null) {
                        getTableView().edit(getTableRow().getIndex(), nextColumn);
                    }
                }
            });
            textField.focusedProperty().addListener((ChangeListener<Boolean>) (observable, oldValue, newValue) -> {
                if (!newValue && textField != null) {
                    commitEditFinal(textField.getText());
                }
            });
        }

        private void commitEditFinal(final String aa){
             //if ((aa.matches("\\d+"))) {
             if ((aa.matches("[0-9][0-9][0-9][0-9]"))) {    // valida un numero de 4 digitos=año
                 commitEdit(aa);
             }else{
                 commitEdit("2000");
             }
        }

        private String getString() {
            return getItem() == null ? "" : getItem().toString();
        }
        /**
         *
         * @param forward true gets the column to the right, false the column to the left of the current column
         * @return
         */
        private TableColumn<docentexasig, ?> getNextColumn(final boolean forward) {
            final List<TableColumn<docentexasig, ?>> columns = new ArrayList<>();
            for (final TableColumn<docentexasig, ?> column : getTableView().getColumns()) {
                columns.addAll(getLeaves(column));

            }
            //There is no other column that supports editing.
            if (columns.size() < 2) {
                return null;
            }
            final int currentIndex = columns.indexOf(getTableColumn());
            int nextIndex = currentIndex;
            if (forward) {
                nextIndex++;
                if (nextIndex > columns.size() - 1) {
                    nextIndex = 0;
                }
            } else {
                nextIndex--;
                if (nextIndex < 0) {
                    nextIndex = columns.size() - 1;
                }
            }
            return columns.get(nextIndex);
        }

        private List<TableColumn<docentexasig, ?>> getLeaves(final TableColumn<docentexasig, ?> root) {
            final List<TableColumn<docentexasig, ?>> columns = new ArrayList<>();
            if (root.getColumns().isEmpty()) {
                //We only want the leaves that are editable.
                if (root.isEditable()) {
                    columns.add(root);
                }
                return columns;
            } else {
                for (final TableColumn<docentexasig, ?> column : root.getColumns()) {
                    columns.addAll(getLeaves(column));
                }
                return columns;
            }
        }
    } // Class EditingCell