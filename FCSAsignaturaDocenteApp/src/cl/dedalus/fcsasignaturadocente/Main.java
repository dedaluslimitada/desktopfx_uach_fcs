package cl.dedalus.fcsasignaturadocente;

import java.awt.Toolkit;
import java.util.Calendar;

import cl.dedalus.fcsasignaturadocente.ws.AsignaturasGetAsig;
import cl.dedalus.fcsasignaturadocente.ws.AsignaturasGetResult;
import cl.dedalus.fcsasignaturadocente.ws.DocentesGetDocente;
import cl.dedalus.fcsasignaturadocente.ws.DocentesGetResult;
import cl.dedalus.fcsasignaturadocente.ws.DocentesasignaturaGetDocente;
import cl.dedalus.fcsasignaturadocente.ws.DocentesasignaturaGetResult;
import cl.dedalus.fcsasignaturadocente.ws.FacultadesGetFac;
import cl.dedalus.fcsasignaturadocente.ws.FacultadesGetResult;
import cl.dedalus.fcsasignaturadocente.ws.FcsasignaturadocenteWeb;
import cl.dedalus.fcsasignaturadocente.ws.FcsasignaturadocenteWebException;
import cl.dedalus.fcsasignaturadocente.ws.FcsasignaturadocenteWebService;
import cl.dedalus.fcsasignaturadocente.ws.InstitutosGetInstit;
import cl.dedalus.fcsasignaturadocente.ws.InstitutosGetResult;
import cl.obcom.desktopfx.core.DesktopTask;
import cl.obcom.desktopfx.jfx.Dialog;
import cl.obcom.desktopfx.jfx.DialogAction;
import cl.obcom.desktopfx.jfx.FxmlPane;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;

public class Main extends FxmlPane
{
    @FXML TableView<asignatura> tb_asignaturas = new TableView<>();
    @FXML TableView<docente> tb_docentes = new TableView<>();
    @FXML TableView<docentexasig> tb_docentesxasig = new TableView<>();
    @FXML private ComboBox<String> cb_facultad;
    @FXML private ComboBox<String> cb_instituto;
    @FXML private ComboBox<String> cb_semestre;
    @FXML private ComboBox<String> cb_agno;
    @FXML private Button bo_salir;
    @FXML private Button bo_mas;
    @FXML private Button bo_menos;
    @FXML private Label lb_docentes;
    @FXML private TextField tx_filtro;

    final FcsasignaturadocenteWeb port;

    final ObservableList<String> dataFacultad = FXCollections.observableArrayList ();
    final ObservableList<String> dataInstituto = FXCollections.observableArrayList ();
    final ObservableList<String> dataSemestre = FXCollections.observableArrayList ();
    final ObservableList<String> dataAgno = FXCollections.observableArrayList ();
    final ObservableList<asignatura> dataAsignatura = FXCollections.observableArrayList ();
    final ObservableList<docente> dataDocente = FXCollections.observableArrayList ();
    final ObservableList<docente> dataDocenteF = FXCollections.observableArrayList ();
    final ObservableList<docentexasig> dataDocentexasig = FXCollections.observableArrayList ();

    public Main(final DesktopTask task) throws Exception
    {
        // Se carga fxml
        loadFxml();
        task.getStage().getScene().getStylesheets().add("resources/css/NuevoEstilo.css");

        final Callback<TableColumn<docentexasig,String>, TableCell<docentexasig,String>> cellFactory_1 =
            p -> new EditingCell_1();

        //Inicializar WS
        final FcsasignaturadocenteWebService service = new FcsasignaturadocenteWebService();
        port = service.getFcsasignaturadocenteWebPort();
        task.initWebServicePort(port,service,task.getCodeBase());

        //Init Grillas tb_asignatura
        final TableColumn<asignatura, String> tc_codi_asig = new TableColumn<>("Codigo");
        tc_codi_asig.setCellValueFactory(new PropertyValueFactory<asignatura, String>("codi_asig"));
        tb_asignaturas.getColumns().add(tc_codi_asig);
        final TableColumn<asignatura, String> tc_desc_asig = new TableColumn<>("Descripcion");
        tc_desc_asig.setCellValueFactory(new PropertyValueFactory<asignatura, String>("desc_asig"));
        tb_asignaturas.getColumns().add(tc_desc_asig);
        final TableColumn<asignatura, String> tc_ht = new TableColumn<>("H Teoricas");
        tc_ht.setCellValueFactory(new PropertyValueFactory<asignatura, String>("cant_ht"));
        tb_asignaturas.getColumns().add(tc_ht);
        final TableColumn<asignatura, String> tc_hp = new TableColumn<>("H Practicas");
        tc_hp.setCellValueFactory(new PropertyValueFactory<asignatura, String>("cant_hp"));
        tb_asignaturas.getColumns().add(tc_hp);
        final TableColumn<asignatura, String> tc_hl = new TableColumn<>("H Laboratorio");
        tc_hl.setCellValueFactory(new PropertyValueFactory<asignatura, String>("cant_hl"));
        tb_asignaturas.getColumns().add(tc_hl);
        final TableColumn<asignatura, String> tc_cant_sem = new TableColumn<>("Semanas");
        tc_cant_sem.setCellValueFactory(new PropertyValueFactory<asignatura, String>("cant_semanas"));
        tb_asignaturas.getColumns().add(tc_cant_sem);
        tb_asignaturas.setItems(dataAsignatura);
        tc_codi_asig.prefWidthProperty().bind(tb_asignaturas.widthProperty().multiply(0.15));
        tc_desc_asig.prefWidthProperty().bind(tb_asignaturas.widthProperty().multiply(0.35));
        tc_ht.prefWidthProperty().bind(tb_asignaturas.widthProperty().multiply(0.13));
        tc_hp.prefWidthProperty().bind(tb_asignaturas.widthProperty().multiply(0.13));
        tc_hl.prefWidthProperty().bind(tb_asignaturas.widthProperty().multiply(0.13));
        tc_cant_sem.prefWidthProperty().bind(tb_asignaturas.widthProperty().multiply(0.10));

        //Init Grillas tb_docentes
        final TableColumn<docente, String> tc_codi_doc = new TableColumn<>("Username");
        tc_codi_doc.setCellValueFactory(new PropertyValueFactory<docente, String>("codi_docente"));
        tb_docentes.getColumns().add(tc_codi_doc);
        final TableColumn<docente, String> tc_desc_nom = new TableColumn<>("Nombre Docente");
        tc_desc_nom.setCellValueFactory(new PropertyValueFactory<docente, String>("desc_nombres"));
        tb_docentes.getColumns().add(tc_desc_nom);
        tb_docentes.setItems(dataDocente);
        tc_codi_doc.prefWidthProperty().bind(tb_docentes.widthProperty().multiply(0.30));
        tc_desc_nom.prefWidthProperty().bind(tb_docentes.widthProperty().multiply(0.70));

        //Init Grillas tb_docentesxasig
        final TableColumn<docentexasig, String> tc_codi_doc_1 = new TableColumn<>("Username");
        tc_codi_doc_1.setCellValueFactory(new PropertyValueFactory<docentexasig, String>("codi_docente"));
        tb_docentesxasig.getColumns().add(tc_codi_doc_1);
        final TableColumn<docentexasig, String> tc_desc_nom_1 = new TableColumn<>("Nombre Docente");
        tc_desc_nom_1.setCellValueFactory(new PropertyValueFactory<docentexasig, String>("desc_nombres"));
        tb_docentesxasig.getColumns().add(tc_desc_nom_1);
        final TableColumn<docentexasig, String> tc_flag_resp_1 = new TableColumn<>("Coord");
        tc_flag_resp_1.setCellValueFactory(new PropertyValueFactory<docentexasig, String>("flag_resp"));
        tc_flag_resp_1.setCellFactory(cellFactory_1);
        tc_flag_resp_1.setOnEditCommit(
            t -> {
                updateDocenteAsignatura(t.getRowValue().getCodi_docente(), t.getRowValue().getId(),t.getNewValue(), t.getRowValue().getFlag_prim());
                t.getRowValue().setFlag_resp(t.getNewValue());
            });
        tb_docentesxasig.getColumns().add(tc_flag_resp_1);

        final TableColumn<docentexasig, String> tc_flag_prim_1 = new TableColumn<>("Prim");
        tc_flag_prim_1.setCellValueFactory(new PropertyValueFactory<docentexasig, String>("flag_prim"));
        tc_flag_prim_1.setCellFactory(cellFactory_1);
        tc_flag_prim_1.setOnEditCommit(
            t -> {
                updateDocenteAsignatura(t.getRowValue().getCodi_docente(),t.getRowValue().getId(),t.getRowValue().getFlag_resp(), t.getNewValue());
                t.getRowValue().setFlag_prim(t.getNewValue());
            });
        tb_docentesxasig.getColumns().add(tc_flag_prim_1);

        tb_docentesxasig.setItems(dataDocentexasig);
        tb_docentesxasig.setEditable(true);
        tc_codi_doc_1.prefWidthProperty().bind(tb_docentesxasig.widthProperty().multiply(0.25));
        tc_desc_nom_1.prefWidthProperty().bind(tb_docentesxasig.widthProperty().multiply(0.45));
        tc_flag_resp_1.prefWidthProperty().bind(tb_docentesxasig.widthProperty().multiply(0.15));
        tc_flag_prim_1.prefWidthProperty().bind(tb_docentesxasig.widthProperty().multiply(0.15));

        // click en Facultades, debo traer instituso
        cb_facultad.setOnAction((event) -> {
            cargaInstituto(cb_facultad.getValue().toString());
            cargaAsignaturas();
            cargaDocentes();
        });

        // click en Instituos,
        cb_instituto.setOnAction((event) -> {
            cargaAsignaturas();
            cargaDocentes();
        });

        // click en semestre
        cb_semestre.setOnAction((event) -> {
            cargaAsignaturas();
            cargaDocentes();
        });

        // click en Año
        cb_agno.setOnAction((event) -> {
            cargaAsignaturas();
            cargaDocentes();
        });

        //Click on tb_asignaturas
        tb_asignaturas.setOnMouseClicked(event -> {
            final asignatura t = dataAsignatura.get(tb_asignaturas.getSelectionModel().getSelectedIndex());
            lb_docentes.setText("Docentes por Asignatura : "+ t.getCodi_asig()+" "+t.getDesc_asig().trim());           
            cargaDocentesAsignatura(t.getCodi_asig(), cb_semestre.getValue().toString(), cb_agno.getValue().toString());

        });
        
        // escucha los cambios en tx_filtro para articulos
        tx_filtro.textProperty().addListener((ChangeListener<String>) (observable, oldValue, newValue) -> {
            dataDocenteF.clear();
            for (final docente p : dataDocente) {
                if (matchesFilter(p)) {  dataDocenteF.add(p);  }
            }
           // tb_articulo.setItems(dataArticuloF);   // copio a la tabla, datos filtrados
            if (dataDocenteF.size()!=0) {
                tb_docentes.setItems(dataDocenteF);   // copio a la tabla, datos filtrados
            } else {
                Toolkit.getDefaultToolkit().beep();
                tx_filtro.setText(oldValue);
            }
         });

        cargaFacultad();
        cargaInstituto(cb_facultad.getValue());
        cargaDocentes();
        cargaSemestreAgno();
        cargaAsignaturas();

        bo_mas.setOnAction(evt -> ingresarDocente());
        bo_menos.setOnAction(evt -> eliminarDocente());
        bo_salir.setOnAction(evt -> task.terminate(true));
    }
    
    boolean matchesFilter(final docente t) {
        final String filterString = tx_filtro.getText();
        if (filterString == null || filterString.isEmpty()) {
            // No filter --> Add all.
            return true;
        }
        final String lowerCaseFilterString = filterString.toLowerCase();
        
        if (t.getCodi_docente().toLowerCase().indexOf(lowerCaseFilterString) != -1) {
            return true;
        } else if (t.getDesc_nombres().toString().toLowerCase().indexOf(lowerCaseFilterString) != -1) {
            return true;
        } 
        return false; // Does not match
    }

    public void cargaSemestreAgno(){
        int v_year = Calendar.getInstance().get(Calendar.YEAR);
        int v_mes = Calendar.getInstance().get(Calendar.MONTH);
        
        // Solo el semestre actual y año actual
        dataSemestre.clear();
        dataAgno.clear();
        
        dataSemestre.add("1");
        dataSemestre.add("2");
        cb_semestre.setItems(dataSemestre);
        v_mes=v_mes+1;
        if ((v_mes>=1) && (v_mes<=6)){  cb_semestre.getSelectionModel().selectFirst();; } 
        else {	  cb_semestre.getSelectionModel().selectLast(); }	

        dataAgno.add(Integer.toString(v_year-1));
        dataAgno.add(Integer.toString(v_year));
        cb_agno.setItems(dataAgno);
        cb_agno.getSelectionModel().selectLast();

    }

    public void eliminarDocente(){
        if (tb_docentesxasig.getSelectionModel().getSelectedIndex()<0) { return; }
        if (tb_asignaturas.getSelectionModel().getSelectedIndex()<0) { return; }

        final docentexasig t = dataDocentexasig.get(tb_docentesxasig.getSelectionModel().getSelectedIndex());
        final asignatura tt = dataAsignatura.get(tb_asignaturas.getSelectionModel().getSelectedIndex());
        DialogAction D;

        D = Dialog.showConfirm(this, "Desea eliminar este Docente: "+t.getCodi_docente()+" ?");
        if ( D.toString() == "YES" ) {
            try {
                port.docentexasigDel(t.getId());
                cargaDocentesAsignatura(tt.getCodi_asig(), cb_semestre.getValue().toString(), cb_agno.getValue().toString());
            } catch (final FcsasignaturadocenteWebException e) {
                Dialog.showError(this, e);
            }

        }
    }

    public void updateDocenteAsignatura(final String v_docente, final Integer v_id,  final String v_res, final String v_prim){
        final asignatura t = dataAsignatura.get(tb_asignaturas.getSelectionModel().getSelectedIndex());
        try {
        	//xxxxxxxxxx
            port.docentexasigUpd(v_docente, v_id, v_res, v_prim, "1", t.getCodi_asig().trim()
                , cb_semestre.getValue().toString().trim(),cb_agno.getValue().toString().trim() );
        } catch (final NumberFormatException e) {
            Dialog.showError(this, e);
        } catch (final FcsasignaturadocenteWebException e) {
            Dialog.showError(this,e);
        }
    }

    public void ingresarDocente(){
        Integer v_cant_sem; final String v_seme, v_agno;
        DialogAction D;

        if (tb_asignaturas.getSelectionModel().getSelectedIndex()<0) { 
        	Dialog.showMessage(this, "Debe seleccionar una asignatura.");
        	return;
        }
        if (tb_docentes.getSelectionModel().getSelectedIndex()<0) { 
        	Dialog.showMessage(this, "Debe seleccionar un docente.");
        	return;
        }
        
        D = Dialog.showConfirm(this, "Desea asociar docente ?");
        if ( D.toString() == "YES" ) {
        	try {
        		final asignatura t = dataAsignatura.get(tb_asignaturas.getSelectionModel().getSelectedIndex());
            	final docente tt = dataDocenteF.get(tb_docentes.getSelectionModel().getSelectedIndex());
            
            	//xxxxxxxxxxxxxxxxxxxxxxx
            	v_seme=cb_semestre.getValue().toString();
            	v_agno=cb_agno.getValue().toString();
                port.docenteasignaturaSet(t.getCodi_asig(), v_seme, v_agno ,tt.getCodi_docente(),Integer.parseInt(t.getCant_semanas()));
                cargaDocentesAsignatura(t.getCodi_asig(), cb_semestre.getValue().toString(), cb_agno.getValue().toString());
            } catch (final FcsasignaturadocenteWebException e) {
                Dialog.showError(this,e);
            }
        }
    }

    public void cargaDocentesAsignatura(final String v_codi_asig, final String v_sem, final String v_agno){
        dataDocentexasig.clear();
        DocentesasignaturaGetResult res = null;
        try {

            res=port.docentesasignaturaGet(v_codi_asig, v_sem, v_agno);
            for (final DocentesasignaturaGetDocente p : res.getDocentes()){
                dataDocentexasig.add(new docentexasig( p.getId(), p.getCodiDocente(), p.getDescNombre(), p.getFlagResp(), p.getFlagPrim() ));
            }
            tb_docentesxasig.setItems(dataDocentexasig);
            tb_docentesxasig.getSelectionModel().select(0);  // Set valor por defecto
        } catch (final Exception e1) {
            Dialog.showError(this, e1);
        }
    }

    public void cargaDocentes(){
        final int i = cb_facultad.getSelectionModel().getSelectedItem().indexOf(" ");
        final int v_iden_fac = Integer.parseInt(cb_facultad.getSelectionModel().getSelectedItem().substring(0, i));

        if (!cb_instituto.getSelectionModel().isEmpty()) {
            final int j = cb_instituto.getSelectionModel().getSelectedItem().indexOf(" ");
            final int v_iden_ins = Integer.parseInt(cb_instituto.getSelectionModel().getSelectedItem().substring(0, j));

            dataDocente.clear();
            dataDocenteF.clear();
            DocentesGetResult res = null;
            try {
                res=port.docentesGet(v_iden_fac, v_iden_ins);
                for (final DocentesGetDocente p : res.getDocentes()){
                    dataDocente.add(new docente( p.getCodiDocente(), p.getDescNombres()+" "+p.getDescPaterno()+" "+p.getDescMaterno() ));
                }
 
                tb_docentes.setItems(dataDocente);
                dataDocenteF.addAll(dataDocente);
            } catch (final Exception e1) {
                Dialog.showError(this, e1);
            }
        }
    }

    public void cargaAsignaturas(){
        dataAsignatura.clear();
        final int k = cb_facultad.getSelectionModel().getSelectedItem().indexOf(" ");
        final int v_fac = Integer.parseInt(cb_facultad.getSelectionModel().getSelectedItem().substring(0, k));

        if (!cb_instituto.getSelectionModel().isEmpty() & !cb_semestre.getSelectionModel().isEmpty()
                & !cb_agno.getSelectionModel().isEmpty()) {
            final int l = cb_instituto.getSelectionModel().getSelectedItem().indexOf(" ");
            final int v_ins = Integer.parseInt(cb_instituto.getSelectionModel().getSelectedItem().substring(0, l));
            final String v_seme= cb_semestre.getValue().toString();
            final String v_agno=cb_agno.getValue().toString();

            AsignaturasGetResult resultado = null;
            try {
                resultado = port.asignaturasGet(v_fac, v_ins, v_seme, v_agno);
                for ( final AsignaturasGetAsig t : resultado.getAsigs()) {
                    dataAsignatura.add(new asignatura( t.getCodiAsig(), t.getDescAsig(), t.getCantHt(), t.getCantHp()
                               ,t.getCantHl(), t.getCantSemanas()));
                }
                tb_asignaturas.setItems(dataAsignatura);
            } catch (final FcsasignaturadocenteWebException e) {
                Dialog.showError(this, e);
            }
        }
    }

    public void cargaFacultad(){
        dataFacultad.clear();
        FacultadesGetResult res = null;
        try {
            res=port.facultadesGet();
            for (final FacultadesGetFac p : res.getFacs()){
                dataFacultad.add(p.getFacultad());
            }
            cb_facultad.setItems(dataFacultad);
            cb_facultad.getSelectionModel().select(0);  // Set valor por defecto
        } catch (final Exception e1) {
            Dialog.showError(this, e1);
        }
    }

    public void cargaInstituto(final String v_facultad){
        dataInstituto.clear();
        final int k = v_facultad.indexOf(" ");
        final int v_iden_fac = Integer.parseInt(v_facultad.substring(0, k));

        InstitutosGetResult res = null;
        try {
            res=port.institutosGet(v_iden_fac);
            for (final InstitutosGetInstit p : res.getInstits()){
                dataInstituto.add(p.getInstituto());
            }
            cb_instituto.setItems(dataInstituto);
            cb_instituto.getSelectionModel().select(0);  // Set valor por defecto
        } catch (final Exception e) {
            Dialog.showError(this, e);
        }
    }


} // Main - FXMPane
