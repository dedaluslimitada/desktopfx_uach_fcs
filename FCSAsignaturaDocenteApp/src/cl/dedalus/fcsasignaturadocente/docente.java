package cl.dedalus.fcsasignaturadocente;

import javafx.beans.property.SimpleStringProperty;

public class docente{
    private final SimpleStringProperty codi_docente;
    private final SimpleStringProperty desc_nombres;

    public docente(final String codi_docente,final String desc_nombres){
        this.codi_docente = new SimpleStringProperty(codi_docente);
        this.desc_nombres = new SimpleStringProperty(desc_nombres);
    }

    public String getCodi_docente() {
        return codi_docente.get();
    }
    public String getDesc_nombres() {
        return desc_nombres.get();
    }
 }
