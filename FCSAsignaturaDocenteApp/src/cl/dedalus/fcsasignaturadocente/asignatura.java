package cl.dedalus.fcsasignaturadocente;

import javafx.beans.property.SimpleStringProperty;

public class asignatura{
    private final SimpleStringProperty codi_asig;
    private final SimpleStringProperty desc_asig;
    private final SimpleStringProperty cant_ht;
    private final SimpleStringProperty cant_hp;
    private final SimpleStringProperty cant_hl;
    private final SimpleStringProperty cant_semanas;

    public asignatura(final String codi_asig,final String desc_asig, final String cant_ht, final String cant_hp, final String cant_hl
                      , final String cant_semanas){
        this.codi_asig = new SimpleStringProperty(codi_asig);
        this.desc_asig = new SimpleStringProperty(desc_asig);
        this.cant_ht = new SimpleStringProperty(cant_ht);
        this.cant_hp = new SimpleStringProperty(cant_hp);
        this.cant_hl = new SimpleStringProperty(cant_hl);
        this.cant_semanas = new SimpleStringProperty(cant_semanas);
    }

    public String getCodi_asig() {
        return codi_asig.get();
    }
    public String getDesc_asig() {
        return desc_asig.get();
    }
    public String getCant_ht() {
        return cant_ht.get();
    }
    public String getCant_hp() {
        return cant_hp.get();
    }
    public String getCant_hl() {
        return cant_hl.get();
    }
    public String getCant_semanas() {
        return cant_semanas.get();
    }
}
