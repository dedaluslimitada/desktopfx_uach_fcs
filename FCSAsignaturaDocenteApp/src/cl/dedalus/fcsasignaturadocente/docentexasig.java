package cl.dedalus.fcsasignaturadocente;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class docentexasig{
    private final SimpleIntegerProperty id;
    private final SimpleStringProperty codi_docente;
    private final SimpleStringProperty desc_nombres;
    private final SimpleStringProperty flag_resp;
    private final SimpleStringProperty flag_prim;

    public docentexasig(final Integer id, final String codi_docente,final String desc_nombres,final String flag_resp
                            , final String flag_prim){
        this.id = new SimpleIntegerProperty(id);
        this.codi_docente = new SimpleStringProperty(codi_docente);
        this.desc_nombres = new SimpleStringProperty(desc_nombres);
        this.flag_resp = new SimpleStringProperty(flag_resp);

        this.flag_prim = new SimpleStringProperty(flag_prim);
    }
    public Integer getId() {
        return id.get();
    }

    public String getCodi_docente() {
        return codi_docente.get();
    }
    public String getDesc_nombres() {
        return desc_nombres.get();
    }

    public String getFlag_resp() {
        return flag_resp.get();
    }
    public void setFlag_resp(final String iflag_r) {
        flag_resp.set(iflag_r);
    }
    public String getFlag_prim() {
        return flag_prim.get();
    }
    public void setFlag_prim(final String iflag_p) {
        flag_prim.set(iflag_p);
    }

}
