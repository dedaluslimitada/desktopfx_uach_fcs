/*
 * Copyright (c) DEDALUS Limitada. All rights reserved.
 *
 * All rights to this product are owned by DEDALUS Limitada and may only be used
 * under the terms of its associated license document. You may NOT copy, modify,
 * sublicense,  or  distribute  this  source  file  or  portions  of  it  unless
 * previously authorized in writing by  DEDALUS  Limitada. In  any  event,  this
 * notice and above copyright must always be included verbatim with this file.
 */

package cl.obcom.desktopfx.ext;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * DEDALUS DesktopFX Extension.
 *
 * @author Eduardo Ostertag
 */
public final class Extension
{
    //--------------------------------------------------------------------------
    //-- Constructor Methods ---------------------------------------------------
    //--------------------------------------------------------------------------

    /**
     * Constructs a new {@code Extension} instance.
     */
    public Extension()
    {
    }

    //--------------------------------------------------------------------------
    //-- Extension Methods -----------------------------------------------------
    //--------------------------------------------------------------------------

    /**
     * Returns the DesktopFX extension parameters.
     *
     * @return the DesktopFX extension parameters.
     */
    public Map<String,Object> getParameters()
    {
        // Create configuration parameters
        final Map<String,Object> params = new HashMap<>();

        // Don't modify the following "desktopfx.license.*" parameters
        params.put("desktopfx.license.serial", new byte[] {-123,-86,-49,81,95,-54,83,-79,104,12,-104,-109,109,-113,97,-86,51,124,98,121});
        params.put("desktopfx.license.title", "DEDALUS");
        params.put("desktopfx.license.company", "DEDALUS Limitada");
        params.put("desktopfx.license.message", "Facultad de Ciencias UACH");
        params.put("desktopfx.license.expires", Long.valueOf(1483239599999L));

        // Backend configuration parameters for a JEE server
        params.put("desktopfx.https.port", Integer.valueOf(8181));
        params.put("desktopfx.page.extension", ".jsp");
        params.put("desktopfx.service.prefix", "");
        params.put("desktopfx.service.suffix", "");
        params.put("desktopfx.shared.codebase", Boolean.FALSE);

        // Enable the desktop offline database functionality
        params.put("desktopfx.offline.enable", Boolean.FALSE);

        // Return unmodifiable config parameters
        return Collections.unmodifiableMap(params);
    }
}
