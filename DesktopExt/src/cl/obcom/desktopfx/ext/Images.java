/*
 * Copyright (c) DEDALUS Limitada. All rights reserved.
 *
 * All rights to this product are owned by DEDALUS Limitada and may only be used
 * under the terms of its associated license document. You may NOT copy, modify,
 * sublicense,  or  distribute  this  source  file  or  portions  of  it  unless
 * previously authorized in writing by  DEDALUS  Limitada. In  any  event,  this
 * notice and above copyright must always be included verbatim with this file.
 */

package cl.obcom.desktopfx.ext;

import java.net.URL;
import javafx.scene.image.Image;

/**
 * Provides images used by the Desktop.
 *
 * @author Eduardo Ostertag
 */
public final class Images
{
    //--------------------------------------------------------------------------
    //-- Constructor Methods ---------------------------------------------------
    //--------------------------------------------------------------------------

    /**
     * Constructs a new {@code Images} instance.
     *
     * @param  codebase the Desktop codebase.
     * @throws NullPointerException if {@code codebase} is {@code null}.
     */
    public Images(final URL codebase)
    {
        if (codebase == null)
            throw new NullPointerException("codebase is null");
    }

    //--------------------------------------------------------------------------
    //-- Images Methods --------------------------------------------------------
    //--------------------------------------------------------------------------

    /**
     * Returns the image indexed by supplied name or {@code null} if not found.
     *
     * @param  name the name of the required image (without extension).
     * @return the image indexed by {@code name} or {@code null} if not found.
     * @throws NullPointerException if {@code name} is {@code null}.
     */
    public Image getImage(final String name)
    {
        // Check supplied arguments
        if (name == null || name.isEmpty())
            throw new NullPointerException("image name is null or empty");
        final String baseName = "images/" + name;

        // Try to load an image with ".png" extension
        final String pngURL = getImageURL(baseName + ".png");
        if (pngURL != null) return new Image(pngURL);

        // Try to load an image with ".jpg" extension
        final String jpgURL = getImageURL(baseName + ".jpg");
        if (jpgURL != null) return new Image(jpgURL);

        // Not found
        return null;
    }

    //--------------------------------------------------------------------------
    //-- Utility Methods -------------------------------------------------------
    //--------------------------------------------------------------------------

    /**
     * Returns the URL of the image with name or {@code null} if not found.
     *
     * @param  name the name of the required image.
     * @return the URL of the required image or {@code null} if not found.
     */
    private static String getImageURL(final String name)
    {
        final URL url = Images.class.getResource(name);
        return (url != null) ? url.toExternalForm() : null;
    }
}
