
package cl.dedalus.fcsaprobasignaturas.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ProcedureResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ProcedureResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProcedureResult")
@XmlSeeAlso({
    AsignaturaUpdResult.class,
    AprobarUpdResult.class,
    AsignaturaSetResult.class,
    AprobasignaturasGetResult.class,
    InstitutosGetResult.class,
    AsignaturasGetResult.class,
    FacultadesGetResult.class,
    Asignatura2DelResult.class,
    AprobasignaturaPutResult.class
})
public abstract class ProcedureResult {


}
