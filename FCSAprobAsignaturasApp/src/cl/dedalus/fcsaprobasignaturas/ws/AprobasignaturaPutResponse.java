
package cl.dedalus.fcsaprobasignaturas.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para aprobasignaturaPutResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="aprobasignaturaPutResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="aprobasignaturaPutResult" type="{http://ws.fcsaprobasignaturas.dedalus.cl/}AprobasignaturaPutResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "aprobasignaturaPutResponse", propOrder = {
    "aprobasignaturaPutResult"
})
public class AprobasignaturaPutResponse {

    protected AprobasignaturaPutResult aprobasignaturaPutResult;

    /**
     * Obtiene el valor de la propiedad aprobasignaturaPutResult.
     * 
     * @return
     *     possible object is
     *     {@link AprobasignaturaPutResult }
     *     
     */
    public AprobasignaturaPutResult getAprobasignaturaPutResult() {
        return aprobasignaturaPutResult;
    }

    /**
     * Define el valor de la propiedad aprobasignaturaPutResult.
     * 
     * @param value
     *     allowed object is
     *     {@link AprobasignaturaPutResult }
     *     
     */
    public void setAprobasignaturaPutResult(AprobasignaturaPutResult value) {
        this.aprobasignaturaPutResult = value;
    }

}
