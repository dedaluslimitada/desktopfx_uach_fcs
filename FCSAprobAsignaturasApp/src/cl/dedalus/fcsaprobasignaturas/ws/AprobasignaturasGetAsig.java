
package cl.dedalus.fcsaprobasignaturas.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para AprobasignaturasGetAsig complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="AprobasignaturasGetAsig">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cantHlab" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantHpra" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantHteo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codiAsig" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descAsig" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="flagAprobada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AprobasignaturasGetAsig", propOrder = {
    "cantHlab",
    "cantHpra",
    "cantHteo",
    "codiAsig",
    "descAsig",
    "flagAprobada"
})
public class AprobasignaturasGetAsig {

    protected String cantHlab;
    protected String cantHpra;
    protected String cantHteo;
    protected String codiAsig;
    protected String descAsig;
    protected String flagAprobada;

    /**
     * Obtiene el valor de la propiedad cantHlab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantHlab() {
        return cantHlab;
    }

    /**
     * Define el valor de la propiedad cantHlab.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantHlab(String value) {
        this.cantHlab = value;
    }

    /**
     * Obtiene el valor de la propiedad cantHpra.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantHpra() {
        return cantHpra;
    }

    /**
     * Define el valor de la propiedad cantHpra.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantHpra(String value) {
        this.cantHpra = value;
    }

    /**
     * Obtiene el valor de la propiedad cantHteo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantHteo() {
        return cantHteo;
    }

    /**
     * Define el valor de la propiedad cantHteo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantHteo(String value) {
        this.cantHteo = value;
    }

    /**
     * Obtiene el valor de la propiedad codiAsig.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiAsig() {
        return codiAsig;
    }

    /**
     * Define el valor de la propiedad codiAsig.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiAsig(String value) {
        this.codiAsig = value;
    }

    /**
     * Obtiene el valor de la propiedad descAsig.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescAsig() {
        return descAsig;
    }

    /**
     * Define el valor de la propiedad descAsig.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescAsig(String value) {
        this.descAsig = value;
    }

    /**
     * Obtiene el valor de la propiedad flagAprobada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlagAprobada() {
        return flagAprobada;
    }

    /**
     * Define el valor de la propiedad flagAprobada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlagAprobada(String value) {
        this.flagAprobada = value;
    }

}
