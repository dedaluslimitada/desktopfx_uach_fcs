
package cl.dedalus.fcsaprobasignaturas.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para aprobasignaturaPut complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="aprobasignaturaPut">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codiAsig" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codiSeme" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codiAgno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codiFac" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="codiIns" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="cantHt" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="cantHp" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="cantHl" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "aprobasignaturaPut", propOrder = {
    "codiAsig",
    "codiSeme",
    "codiAgno",
    "codiFac",
    "codiIns",
    "cantHt",
    "cantHp",
    "cantHl"
})
public class AprobasignaturaPut {

    protected String codiAsig;
    protected String codiSeme;
    protected String codiAgno;
    protected int codiFac;
    protected int codiIns;
    protected int cantHt;
    protected int cantHp;
    protected int cantHl;

    /**
     * Obtiene el valor de la propiedad codiAsig.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiAsig() {
        return codiAsig;
    }

    /**
     * Define el valor de la propiedad codiAsig.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiAsig(String value) {
        this.codiAsig = value;
    }

    /**
     * Obtiene el valor de la propiedad codiSeme.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiSeme() {
        return codiSeme;
    }

    /**
     * Define el valor de la propiedad codiSeme.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiSeme(String value) {
        this.codiSeme = value;
    }

    /**
     * Obtiene el valor de la propiedad codiAgno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiAgno() {
        return codiAgno;
    }

    /**
     * Define el valor de la propiedad codiAgno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiAgno(String value) {
        this.codiAgno = value;
    }

    /**
     * Obtiene el valor de la propiedad codiFac.
     * 
     */
    public int getCodiFac() {
        return codiFac;
    }

    /**
     * Define el valor de la propiedad codiFac.
     * 
     */
    public void setCodiFac(int value) {
        this.codiFac = value;
    }

    /**
     * Obtiene el valor de la propiedad codiIns.
     * 
     */
    public int getCodiIns() {
        return codiIns;
    }

    /**
     * Define el valor de la propiedad codiIns.
     * 
     */
    public void setCodiIns(int value) {
        this.codiIns = value;
    }

    /**
     * Obtiene el valor de la propiedad cantHt.
     * 
     */
    public int getCantHt() {
        return cantHt;
    }

    /**
     * Define el valor de la propiedad cantHt.
     * 
     */
    public void setCantHt(int value) {
        this.cantHt = value;
    }

    /**
     * Obtiene el valor de la propiedad cantHp.
     * 
     */
    public int getCantHp() {
        return cantHp;
    }

    /**
     * Define el valor de la propiedad cantHp.
     * 
     */
    public void setCantHp(int value) {
        this.cantHp = value;
    }

    /**
     * Obtiene el valor de la propiedad cantHl.
     * 
     */
    public int getCantHl() {
        return cantHl;
    }

    /**
     * Define el valor de la propiedad cantHl.
     * 
     */
    public void setCantHl(int value) {
        this.cantHl = value;
    }

}
