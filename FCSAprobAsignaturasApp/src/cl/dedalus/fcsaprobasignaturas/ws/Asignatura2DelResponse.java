
package cl.dedalus.fcsaprobasignaturas.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para asignatura2DelResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="asignatura2DelResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="asignatura2DelResult" type="{http://ws.fcsaprobasignaturas.dedalus.cl/}Asignatura2DelResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "asignatura2DelResponse", propOrder = {
    "asignatura2DelResult"
})
public class Asignatura2DelResponse {

    protected Asignatura2DelResult asignatura2DelResult;

    /**
     * Obtiene el valor de la propiedad asignatura2DelResult.
     * 
     * @return
     *     possible object is
     *     {@link Asignatura2DelResult }
     *     
     */
    public Asignatura2DelResult getAsignatura2DelResult() {
        return asignatura2DelResult;
    }

    /**
     * Define el valor de la propiedad asignatura2DelResult.
     * 
     * @param value
     *     allowed object is
     *     {@link Asignatura2DelResult }
     *     
     */
    public void setAsignatura2DelResult(Asignatura2DelResult value) {
        this.asignatura2DelResult = value;
    }

}
