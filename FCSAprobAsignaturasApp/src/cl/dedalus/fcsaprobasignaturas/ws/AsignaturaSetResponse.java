
package cl.dedalus.fcsaprobasignaturas.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para asignaturaSetResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="asignaturaSetResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="asignaturaSetResult" type="{http://ws.fcsaprobasignaturas.dedalus.cl/}AsignaturaSetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "asignaturaSetResponse", propOrder = {
    "asignaturaSetResult"
})
public class AsignaturaSetResponse {

    protected AsignaturaSetResult asignaturaSetResult;

    /**
     * Obtiene el valor de la propiedad asignaturaSetResult.
     * 
     * @return
     *     possible object is
     *     {@link AsignaturaSetResult }
     *     
     */
    public AsignaturaSetResult getAsignaturaSetResult() {
        return asignaturaSetResult;
    }

    /**
     * Define el valor de la propiedad asignaturaSetResult.
     * 
     * @param value
     *     allowed object is
     *     {@link AsignaturaSetResult }
     *     
     */
    public void setAsignaturaSetResult(AsignaturaSetResult value) {
        this.asignaturaSetResult = value;
    }

}
