
package cl.dedalus.fcsaprobasignaturas.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para aprobasignaturasGetResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="aprobasignaturasGetResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="aprobasignaturasGetResult" type="{http://ws.fcsaprobasignaturas.dedalus.cl/}AprobasignaturasGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "aprobasignaturasGetResponse", propOrder = {
    "aprobasignaturasGetResult"
})
public class AprobasignaturasGetResponse {

    protected AprobasignaturasGetResult aprobasignaturasGetResult;

    /**
     * Obtiene el valor de la propiedad aprobasignaturasGetResult.
     * 
     * @return
     *     possible object is
     *     {@link AprobasignaturasGetResult }
     *     
     */
    public AprobasignaturasGetResult getAprobasignaturasGetResult() {
        return aprobasignaturasGetResult;
    }

    /**
     * Define el valor de la propiedad aprobasignaturasGetResult.
     * 
     * @param value
     *     allowed object is
     *     {@link AprobasignaturasGetResult }
     *     
     */
    public void setAprobasignaturasGetResult(AprobasignaturasGetResult value) {
        this.aprobasignaturasGetResult = value;
    }

}
