
package cl.dedalus.fcsaprobasignaturas.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para institutosGetResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="institutosGetResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="institutosGetResult" type="{http://ws.fcsaprobasignaturas.dedalus.cl/}InstitutosGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "institutosGetResponse", propOrder = {
    "institutosGetResult"
})
public class InstitutosGetResponse {

    protected InstitutosGetResult institutosGetResult;

    /**
     * Obtiene el valor de la propiedad institutosGetResult.
     * 
     * @return
     *     possible object is
     *     {@link InstitutosGetResult }
     *     
     */
    public InstitutosGetResult getInstitutosGetResult() {
        return institutosGetResult;
    }

    /**
     * Define el valor de la propiedad institutosGetResult.
     * 
     * @param value
     *     allowed object is
     *     {@link InstitutosGetResult }
     *     
     */
    public void setInstitutosGetResult(InstitutosGetResult value) {
        this.institutosGetResult = value;
    }

}
