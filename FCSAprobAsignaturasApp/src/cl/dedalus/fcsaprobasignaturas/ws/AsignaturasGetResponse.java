
package cl.dedalus.fcsaprobasignaturas.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para asignaturasGetResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="asignaturasGetResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="asignaturasGetResult" type="{http://ws.fcsaprobasignaturas.dedalus.cl/}AsignaturasGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "asignaturasGetResponse", propOrder = {
    "asignaturasGetResult"
})
public class AsignaturasGetResponse {

    protected AsignaturasGetResult asignaturasGetResult;

    /**
     * Obtiene el valor de la propiedad asignaturasGetResult.
     * 
     * @return
     *     possible object is
     *     {@link AsignaturasGetResult }
     *     
     */
    public AsignaturasGetResult getAsignaturasGetResult() {
        return asignaturasGetResult;
    }

    /**
     * Define el valor de la propiedad asignaturasGetResult.
     * 
     * @param value
     *     allowed object is
     *     {@link AsignaturasGetResult }
     *     
     */
    public void setAsignaturasGetResult(AsignaturasGetResult value) {
        this.asignaturasGetResult = value;
    }

}
