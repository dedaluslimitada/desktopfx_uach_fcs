
package cl.dedalus.fcsaprobasignaturas.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para asignaturaSet complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="asignaturaSet">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codiAsig" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descAsig" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantHt" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="cantHp" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="cantHl" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="idenFac" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="idenIns" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "asignaturaSet", propOrder = {
    "codiAsig",
    "descAsig",
    "cantHt",
    "cantHp",
    "cantHl",
    "idenFac",
    "idenIns"
})
public class AsignaturaSet {

    protected String codiAsig;
    protected String descAsig;
    protected int cantHt;
    protected int cantHp;
    protected int cantHl;
    protected int idenFac;
    protected int idenIns;

    /**
     * Obtiene el valor de la propiedad codiAsig.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiAsig() {
        return codiAsig;
    }

    /**
     * Define el valor de la propiedad codiAsig.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiAsig(String value) {
        this.codiAsig = value;
    }

    /**
     * Obtiene el valor de la propiedad descAsig.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescAsig() {
        return descAsig;
    }

    /**
     * Define el valor de la propiedad descAsig.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescAsig(String value) {
        this.descAsig = value;
    }

    /**
     * Obtiene el valor de la propiedad cantHt.
     * 
     */
    public int getCantHt() {
        return cantHt;
    }

    /**
     * Define el valor de la propiedad cantHt.
     * 
     */
    public void setCantHt(int value) {
        this.cantHt = value;
    }

    /**
     * Obtiene el valor de la propiedad cantHp.
     * 
     */
    public int getCantHp() {
        return cantHp;
    }

    /**
     * Define el valor de la propiedad cantHp.
     * 
     */
    public void setCantHp(int value) {
        this.cantHp = value;
    }

    /**
     * Obtiene el valor de la propiedad cantHl.
     * 
     */
    public int getCantHl() {
        return cantHl;
    }

    /**
     * Define el valor de la propiedad cantHl.
     * 
     */
    public void setCantHl(int value) {
        this.cantHl = value;
    }

    /**
     * Obtiene el valor de la propiedad idenFac.
     * 
     */
    public int getIdenFac() {
        return idenFac;
    }

    /**
     * Define el valor de la propiedad idenFac.
     * 
     */
    public void setIdenFac(int value) {
        this.idenFac = value;
    }

    /**
     * Obtiene el valor de la propiedad idenIns.
     * 
     */
    public int getIdenIns() {
        return idenIns;
    }

    /**
     * Define el valor de la propiedad idenIns.
     * 
     */
    public void setIdenIns(int value) {
        this.idenIns = value;
    }

}
