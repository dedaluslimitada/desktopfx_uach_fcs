
package cl.dedalus.fcsaprobasignaturas.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para AsignaturasGetAsig complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="AsignaturasGetAsig">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cantHlab" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="cantHpra" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="cantHteo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="codiAsig" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descAsig" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AsignaturasGetAsig", propOrder = {
    "cantHlab",
    "cantHpra",
    "cantHteo",
    "codiAsig",
    "descAsig"
})
public class AsignaturasGetAsig {

    protected int cantHlab;
    protected int cantHpra;
    protected int cantHteo;
    protected String codiAsig;
    protected String descAsig;

    /**
     * Obtiene el valor de la propiedad cantHlab.
     * 
     */
    public int getCantHlab() {
        return cantHlab;
    }

    /**
     * Define el valor de la propiedad cantHlab.
     * 
     */
    public void setCantHlab(int value) {
        this.cantHlab = value;
    }

    /**
     * Obtiene el valor de la propiedad cantHpra.
     * 
     */
    public int getCantHpra() {
        return cantHpra;
    }

    /**
     * Define el valor de la propiedad cantHpra.
     * 
     */
    public void setCantHpra(int value) {
        this.cantHpra = value;
    }

    /**
     * Obtiene el valor de la propiedad cantHteo.
     * 
     */
    public int getCantHteo() {
        return cantHteo;
    }

    /**
     * Define el valor de la propiedad cantHteo.
     * 
     */
    public void setCantHteo(int value) {
        this.cantHteo = value;
    }

    /**
     * Obtiene el valor de la propiedad codiAsig.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiAsig() {
        return codiAsig;
    }

    /**
     * Define el valor de la propiedad codiAsig.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiAsig(String value) {
        this.codiAsig = value;
    }

    /**
     * Obtiene el valor de la propiedad descAsig.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescAsig() {
        return descAsig;
    }

    /**
     * Define el valor de la propiedad descAsig.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescAsig(String value) {
        this.descAsig = value;
    }

}
