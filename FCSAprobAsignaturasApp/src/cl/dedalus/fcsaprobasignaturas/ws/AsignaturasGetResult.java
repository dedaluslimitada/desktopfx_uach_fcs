
package cl.dedalus.fcsaprobasignaturas.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para AsignaturasGetResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="AsignaturasGetResult">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.fcsaprobasignaturas.dedalus.cl/}ProcedureResult">
 *       &lt;sequence>
 *         &lt;element name="asigs" type="{http://ws.fcsaprobasignaturas.dedalus.cl/}AsignaturasGetAsig" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AsignaturasGetResult", propOrder = {
    "asigs"
})
public class AsignaturasGetResult
    extends ProcedureResult
{

    @XmlElement(nillable = true)
    protected List<AsignaturasGetAsig> asigs;

    /**
     * Gets the value of the asigs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the asigs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAsigs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AsignaturasGetAsig }
     * 
     * 
     */
    public List<AsignaturasGetAsig> getAsigs() {
        if (asigs == null) {
            asigs = new ArrayList<AsignaturasGetAsig>();
        }
        return this.asigs;
    }

}
