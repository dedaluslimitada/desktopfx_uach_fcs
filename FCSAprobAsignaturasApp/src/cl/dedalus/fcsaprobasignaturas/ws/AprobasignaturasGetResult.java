
package cl.dedalus.fcsaprobasignaturas.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para AprobasignaturasGetResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="AprobasignaturasGetResult">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.fcsaprobasignaturas.dedalus.cl/}ProcedureResult">
 *       &lt;sequence>
 *         &lt;element name="asigs" type="{http://ws.fcsaprobasignaturas.dedalus.cl/}AprobasignaturasGetAsig" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AprobasignaturasGetResult", propOrder = {
    "asigs"
})
public class AprobasignaturasGetResult
    extends ProcedureResult
{

    @XmlElement(nillable = true)
    protected List<AprobasignaturasGetAsig> asigs;

    /**
     * Gets the value of the asigs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the asigs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAsigs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AprobasignaturasGetAsig }
     * 
     * 
     */
    public List<AprobasignaturasGetAsig> getAsigs() {
        if (asigs == null) {
            asigs = new ArrayList<AprobasignaturasGetAsig>();
        }
        return this.asigs;
    }

}
