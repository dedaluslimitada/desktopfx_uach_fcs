
package cl.dedalus.fcsaprobasignaturas.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para aprobarUpd complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="aprobarUpd">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codiSeme" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codiAgno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codiFac" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="codiIns" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "aprobarUpd", propOrder = {
    "codiSeme",
    "codiAgno",
    "codiFac",
    "codiIns"
})
public class AprobarUpd {

    protected String codiSeme;
    protected String codiAgno;
    protected int codiFac;
    protected int codiIns;

    /**
     * Obtiene el valor de la propiedad codiSeme.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiSeme() {
        return codiSeme;
    }

    /**
     * Define el valor de la propiedad codiSeme.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiSeme(String value) {
        this.codiSeme = value;
    }

    /**
     * Obtiene el valor de la propiedad codiAgno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiAgno() {
        return codiAgno;
    }

    /**
     * Define el valor de la propiedad codiAgno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiAgno(String value) {
        this.codiAgno = value;
    }

    /**
     * Obtiene el valor de la propiedad codiFac.
     * 
     */
    public int getCodiFac() {
        return codiFac;
    }

    /**
     * Define el valor de la propiedad codiFac.
     * 
     */
    public void setCodiFac(int value) {
        this.codiFac = value;
    }

    /**
     * Obtiene el valor de la propiedad codiIns.
     * 
     */
    public int getCodiIns() {
        return codiIns;
    }

    /**
     * Define el valor de la propiedad codiIns.
     * 
     */
    public void setCodiIns(int value) {
        this.codiIns = value;
    }

}
