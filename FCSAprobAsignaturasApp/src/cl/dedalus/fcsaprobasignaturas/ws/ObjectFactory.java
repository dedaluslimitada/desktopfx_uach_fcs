
package cl.dedalus.fcsaprobasignaturas.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cl.dedalus.fcsaprobasignaturas.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AprobarUpd_QNAME = new QName("http://ws.fcsaprobasignaturas.dedalus.cl/", "aprobarUpd");
    private final static QName _FacultadesGetResponse_QNAME = new QName("http://ws.fcsaprobasignaturas.dedalus.cl/", "facultadesGetResponse");
    private final static QName _AsignaturaUpd_QNAME = new QName("http://ws.fcsaprobasignaturas.dedalus.cl/", "asignaturaUpd");
    private final static QName _FacultadesGet_QNAME = new QName("http://ws.fcsaprobasignaturas.dedalus.cl/", "facultadesGet");
    private final static QName _AsignaturasGet_QNAME = new QName("http://ws.fcsaprobasignaturas.dedalus.cl/", "asignaturasGet");
    private final static QName _InstitutosGet_QNAME = new QName("http://ws.fcsaprobasignaturas.dedalus.cl/", "institutosGet");
    private final static QName _AprobasignaturaPut_QNAME = new QName("http://ws.fcsaprobasignaturas.dedalus.cl/", "aprobasignaturaPut");
    private final static QName _AprobasignaturaPutResponse_QNAME = new QName("http://ws.fcsaprobasignaturas.dedalus.cl/", "aprobasignaturaPutResponse");
    private final static QName _Asignatura2DelResponse_QNAME = new QName("http://ws.fcsaprobasignaturas.dedalus.cl/", "asignatura2DelResponse");
    private final static QName _InstitutosGetResponse_QNAME = new QName("http://ws.fcsaprobasignaturas.dedalus.cl/", "institutosGetResponse");
    private final static QName _AprobasignaturasGetResponse_QNAME = new QName("http://ws.fcsaprobasignaturas.dedalus.cl/", "aprobasignaturasGetResponse");
    private final static QName _AprobarUpdResponse_QNAME = new QName("http://ws.fcsaprobasignaturas.dedalus.cl/", "aprobarUpdResponse");
    private final static QName _Asignatura2Del_QNAME = new QName("http://ws.fcsaprobasignaturas.dedalus.cl/", "asignatura2Del");
    private final static QName _AsignaturaSetResponse_QNAME = new QName("http://ws.fcsaprobasignaturas.dedalus.cl/", "asignaturaSetResponse");
    private final static QName _FaultInfo_QNAME = new QName("http://ws.fcsaprobasignaturas.dedalus.cl/", "faultInfo");
    private final static QName _AprobasignaturasGet_QNAME = new QName("http://ws.fcsaprobasignaturas.dedalus.cl/", "aprobasignaturasGet");
    private final static QName _AsignaturaSet_QNAME = new QName("http://ws.fcsaprobasignaturas.dedalus.cl/", "asignaturaSet");
    private final static QName _AsignaturaUpdResponse_QNAME = new QName("http://ws.fcsaprobasignaturas.dedalus.cl/", "asignaturaUpdResponse");
    private final static QName _AsignaturasGetResponse_QNAME = new QName("http://ws.fcsaprobasignaturas.dedalus.cl/", "asignaturasGetResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cl.dedalus.fcsaprobasignaturas.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link FacultadesGet }
     * 
     */
    public FacultadesGet createFacultadesGet() {
        return new FacultadesGet();
    }

    /**
     * Create an instance of {@link AsignaturaUpd }
     * 
     */
    public AsignaturaUpd createAsignaturaUpd() {
        return new AsignaturaUpd();
    }

    /**
     * Create an instance of {@link AprobarUpd }
     * 
     */
    public AprobarUpd createAprobarUpd() {
        return new AprobarUpd();
    }

    /**
     * Create an instance of {@link FacultadesGetResponse }
     * 
     */
    public FacultadesGetResponse createFacultadesGetResponse() {
        return new FacultadesGetResponse();
    }

    /**
     * Create an instance of {@link AprobasignaturasGetResponse }
     * 
     */
    public AprobasignaturasGetResponse createAprobasignaturasGetResponse() {
        return new AprobasignaturasGetResponse();
    }

    /**
     * Create an instance of {@link AprobasignaturaPut }
     * 
     */
    public AprobasignaturaPut createAprobasignaturaPut() {
        return new AprobasignaturaPut();
    }

    /**
     * Create an instance of {@link AprobasignaturaPutResponse }
     * 
     */
    public AprobasignaturaPutResponse createAprobasignaturaPutResponse() {
        return new AprobasignaturaPutResponse();
    }

    /**
     * Create an instance of {@link Asignatura2DelResponse }
     * 
     */
    public Asignatura2DelResponse createAsignatura2DelResponse() {
        return new Asignatura2DelResponse();
    }

    /**
     * Create an instance of {@link InstitutosGetResponse }
     * 
     */
    public InstitutosGetResponse createInstitutosGetResponse() {
        return new InstitutosGetResponse();
    }

    /**
     * Create an instance of {@link AsignaturasGet }
     * 
     */
    public AsignaturasGet createAsignaturasGet() {
        return new AsignaturasGet();
    }

    /**
     * Create an instance of {@link InstitutosGet }
     * 
     */
    public InstitutosGet createInstitutosGet() {
        return new InstitutosGet();
    }

    /**
     * Create an instance of {@link AprobasignaturasGet }
     * 
     */
    public AprobasignaturasGet createAprobasignaturasGet() {
        return new AprobasignaturasGet();
    }

    /**
     * Create an instance of {@link AsignaturaSetResponse }
     * 
     */
    public AsignaturaSetResponse createAsignaturaSetResponse() {
        return new AsignaturaSetResponse();
    }

    /**
     * Create an instance of {@link AprobarUpdResponse }
     * 
     */
    public AprobarUpdResponse createAprobarUpdResponse() {
        return new AprobarUpdResponse();
    }

    /**
     * Create an instance of {@link Asignatura2Del }
     * 
     */
    public Asignatura2Del createAsignatura2Del() {
        return new Asignatura2Del();
    }

    /**
     * Create an instance of {@link AsignaturaUpdResponse }
     * 
     */
    public AsignaturaUpdResponse createAsignaturaUpdResponse() {
        return new AsignaturaUpdResponse();
    }

    /**
     * Create an instance of {@link AsignaturasGetResponse }
     * 
     */
    public AsignaturasGetResponse createAsignaturasGetResponse() {
        return new AsignaturasGetResponse();
    }

    /**
     * Create an instance of {@link AsignaturaSet }
     * 
     */
    public AsignaturaSet createAsignaturaSet() {
        return new AsignaturaSet();
    }

    /**
     * Create an instance of {@link AsignaturaUpdResult }
     * 
     */
    public AsignaturaUpdResult createAsignaturaUpdResult() {
        return new AsignaturaUpdResult();
    }

    /**
     * Create an instance of {@link AprobarUpdResult }
     * 
     */
    public AprobarUpdResult createAprobarUpdResult() {
        return new AprobarUpdResult();
    }

    /**
     * Create an instance of {@link AsignaturaSetResult }
     * 
     */
    public AsignaturaSetResult createAsignaturaSetResult() {
        return new AsignaturaSetResult();
    }

    /**
     * Create an instance of {@link AsignaturasGetAsig }
     * 
     */
    public AsignaturasGetAsig createAsignaturasGetAsig() {
        return new AsignaturasGetAsig();
    }

    /**
     * Create an instance of {@link AprobasignaturasGetResult }
     * 
     */
    public AprobasignaturasGetResult createAprobasignaturasGetResult() {
        return new AprobasignaturasGetResult();
    }

    /**
     * Create an instance of {@link InstitutosGetResult }
     * 
     */
    public InstitutosGetResult createInstitutosGetResult() {
        return new InstitutosGetResult();
    }

    /**
     * Create an instance of {@link InstitutosGetInstit }
     * 
     */
    public InstitutosGetInstit createInstitutosGetInstit() {
        return new InstitutosGetInstit();
    }

    /**
     * Create an instance of {@link AprobasignaturasGetAsig }
     * 
     */
    public AprobasignaturasGetAsig createAprobasignaturasGetAsig() {
        return new AprobasignaturasGetAsig();
    }

    /**
     * Create an instance of {@link FacultadesGetFac }
     * 
     */
    public FacultadesGetFac createFacultadesGetFac() {
        return new FacultadesGetFac();
    }

    /**
     * Create an instance of {@link AsignaturasGetResult }
     * 
     */
    public AsignaturasGetResult createAsignaturasGetResult() {
        return new AsignaturasGetResult();
    }

    /**
     * Create an instance of {@link FacultadesGetResult }
     * 
     */
    public FacultadesGetResult createFacultadesGetResult() {
        return new FacultadesGetResult();
    }

    /**
     * Create an instance of {@link Asignatura2DelResult }
     * 
     */
    public Asignatura2DelResult createAsignatura2DelResult() {
        return new Asignatura2DelResult();
    }

    /**
     * Create an instance of {@link AprobasignaturaPutResult }
     * 
     */
    public AprobasignaturaPutResult createAprobasignaturaPutResult() {
        return new AprobasignaturaPutResult();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AprobarUpd }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsaprobasignaturas.dedalus.cl/", name = "aprobarUpd")
    public JAXBElement<AprobarUpd> createAprobarUpd(AprobarUpd value) {
        return new JAXBElement<AprobarUpd>(_AprobarUpd_QNAME, AprobarUpd.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FacultadesGetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsaprobasignaturas.dedalus.cl/", name = "facultadesGetResponse")
    public JAXBElement<FacultadesGetResponse> createFacultadesGetResponse(FacultadesGetResponse value) {
        return new JAXBElement<FacultadesGetResponse>(_FacultadesGetResponse_QNAME, FacultadesGetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AsignaturaUpd }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsaprobasignaturas.dedalus.cl/", name = "asignaturaUpd")
    public JAXBElement<AsignaturaUpd> createAsignaturaUpd(AsignaturaUpd value) {
        return new JAXBElement<AsignaturaUpd>(_AsignaturaUpd_QNAME, AsignaturaUpd.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FacultadesGet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsaprobasignaturas.dedalus.cl/", name = "facultadesGet")
    public JAXBElement<FacultadesGet> createFacultadesGet(FacultadesGet value) {
        return new JAXBElement<FacultadesGet>(_FacultadesGet_QNAME, FacultadesGet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AsignaturasGet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsaprobasignaturas.dedalus.cl/", name = "asignaturasGet")
    public JAXBElement<AsignaturasGet> createAsignaturasGet(AsignaturasGet value) {
        return new JAXBElement<AsignaturasGet>(_AsignaturasGet_QNAME, AsignaturasGet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InstitutosGet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsaprobasignaturas.dedalus.cl/", name = "institutosGet")
    public JAXBElement<InstitutosGet> createInstitutosGet(InstitutosGet value) {
        return new JAXBElement<InstitutosGet>(_InstitutosGet_QNAME, InstitutosGet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AprobasignaturaPut }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsaprobasignaturas.dedalus.cl/", name = "aprobasignaturaPut")
    public JAXBElement<AprobasignaturaPut> createAprobasignaturaPut(AprobasignaturaPut value) {
        return new JAXBElement<AprobasignaturaPut>(_AprobasignaturaPut_QNAME, AprobasignaturaPut.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AprobasignaturaPutResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsaprobasignaturas.dedalus.cl/", name = "aprobasignaturaPutResponse")
    public JAXBElement<AprobasignaturaPutResponse> createAprobasignaturaPutResponse(AprobasignaturaPutResponse value) {
        return new JAXBElement<AprobasignaturaPutResponse>(_AprobasignaturaPutResponse_QNAME, AprobasignaturaPutResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Asignatura2DelResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsaprobasignaturas.dedalus.cl/", name = "asignatura2DelResponse")
    public JAXBElement<Asignatura2DelResponse> createAsignatura2DelResponse(Asignatura2DelResponse value) {
        return new JAXBElement<Asignatura2DelResponse>(_Asignatura2DelResponse_QNAME, Asignatura2DelResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InstitutosGetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsaprobasignaturas.dedalus.cl/", name = "institutosGetResponse")
    public JAXBElement<InstitutosGetResponse> createInstitutosGetResponse(InstitutosGetResponse value) {
        return new JAXBElement<InstitutosGetResponse>(_InstitutosGetResponse_QNAME, InstitutosGetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AprobasignaturasGetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsaprobasignaturas.dedalus.cl/", name = "aprobasignaturasGetResponse")
    public JAXBElement<AprobasignaturasGetResponse> createAprobasignaturasGetResponse(AprobasignaturasGetResponse value) {
        return new JAXBElement<AprobasignaturasGetResponse>(_AprobasignaturasGetResponse_QNAME, AprobasignaturasGetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AprobarUpdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsaprobasignaturas.dedalus.cl/", name = "aprobarUpdResponse")
    public JAXBElement<AprobarUpdResponse> createAprobarUpdResponse(AprobarUpdResponse value) {
        return new JAXBElement<AprobarUpdResponse>(_AprobarUpdResponse_QNAME, AprobarUpdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Asignatura2Del }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsaprobasignaturas.dedalus.cl/", name = "asignatura2Del")
    public JAXBElement<Asignatura2Del> createAsignatura2Del(Asignatura2Del value) {
        return new JAXBElement<Asignatura2Del>(_Asignatura2Del_QNAME, Asignatura2Del.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AsignaturaSetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsaprobasignaturas.dedalus.cl/", name = "asignaturaSetResponse")
    public JAXBElement<AsignaturaSetResponse> createAsignaturaSetResponse(AsignaturaSetResponse value) {
        return new JAXBElement<AsignaturaSetResponse>(_AsignaturaSetResponse_QNAME, AsignaturaSetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsaprobasignaturas.dedalus.cl/", name = "faultInfo")
    public JAXBElement<String> createFaultInfo(String value) {
        return new JAXBElement<String>(_FaultInfo_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AprobasignaturasGet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsaprobasignaturas.dedalus.cl/", name = "aprobasignaturasGet")
    public JAXBElement<AprobasignaturasGet> createAprobasignaturasGet(AprobasignaturasGet value) {
        return new JAXBElement<AprobasignaturasGet>(_AprobasignaturasGet_QNAME, AprobasignaturasGet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AsignaturaSet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsaprobasignaturas.dedalus.cl/", name = "asignaturaSet")
    public JAXBElement<AsignaturaSet> createAsignaturaSet(AsignaturaSet value) {
        return new JAXBElement<AsignaturaSet>(_AsignaturaSet_QNAME, AsignaturaSet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AsignaturaUpdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsaprobasignaturas.dedalus.cl/", name = "asignaturaUpdResponse")
    public JAXBElement<AsignaturaUpdResponse> createAsignaturaUpdResponse(AsignaturaUpdResponse value) {
        return new JAXBElement<AsignaturaUpdResponse>(_AsignaturaUpdResponse_QNAME, AsignaturaUpdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AsignaturasGetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsaprobasignaturas.dedalus.cl/", name = "asignaturasGetResponse")
    public JAXBElement<AsignaturasGetResponse> createAsignaturasGetResponse(AsignaturasGetResponse value) {
        return new JAXBElement<AsignaturasGetResponse>(_AsignaturasGetResponse_QNAME, AsignaturasGetResponse.class, null, value);
    }

}
