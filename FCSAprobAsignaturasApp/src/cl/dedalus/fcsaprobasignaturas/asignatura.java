package cl.dedalus.fcsaprobasignaturas;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class asignatura{
    private final SimpleBooleanProperty codi_sel;
    private final SimpleStringProperty codi_asig;
    private final SimpleStringProperty desc_asig;
    private final SimpleIntegerProperty cant_ht;
    private final SimpleIntegerProperty cant_hp;
    private final SimpleIntegerProperty cant_hl;

    public asignatura(final Boolean codi_sel, final String codi_asig,final String desc_asig, final int cant_ht, final int cant_hp
                      , final int cant_hl ){
        this.codi_sel = new SimpleBooleanProperty(codi_sel);
        this.codi_asig = new SimpleStringProperty(codi_asig);
        this.desc_asig = new SimpleStringProperty(desc_asig);
        this.cant_ht = new SimpleIntegerProperty(cant_ht);
        this.cant_hp = new SimpleIntegerProperty(cant_hp);
        this.cant_hl = new SimpleIntegerProperty(cant_hl);
    }

    public Boolean getCodi_sel() {
        return codi_sel.get();
    }
    public SimpleBooleanProperty codi_selProperty() {return codi_sel;}

    public String getCodi_asig() {
        return codi_asig.get();
    }
    public String getDesc_asig() {
        return desc_asig.get();
    }
    public Integer getCant_ht() {
        return cant_ht.get();
    }
    public Integer getCant_hp() {
        return cant_hp.get();
    }
    public Integer getCant_hl() {
        return cant_hl.get();
    }

}
