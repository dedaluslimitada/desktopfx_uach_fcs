package cl.dedalus.fcsaprobasignaturas;

import java.awt.Toolkit;
import java.util.Calendar;

import cl.dedalus.fcsaprobasignaturas.ws.AprobasignaturasGetAsig;
import cl.dedalus.fcsaprobasignaturas.ws.AprobasignaturasGetResult;
import cl.dedalus.fcsaprobasignaturas.ws.AsignaturasGetAsig;
import cl.dedalus.fcsaprobasignaturas.ws.AsignaturasGetResult;
import cl.dedalus.fcsaprobasignaturas.ws.FacultadesGetFac;
import cl.dedalus.fcsaprobasignaturas.ws.FacultadesGetResult;
import cl.dedalus.fcsaprobasignaturas.ws.FcsaprobasignaturasWeb;
import cl.dedalus.fcsaprobasignaturas.ws.FcsaprobasignaturasWebException;
import cl.dedalus.fcsaprobasignaturas.ws.FcsaprobasignaturasWebService;
import cl.dedalus.fcsaprobasignaturas.ws.InstitutosGetInstit;
import cl.dedalus.fcsaprobasignaturas.ws.InstitutosGetResult;
import cl.obcom.desktopfx.core.DesktopTask;
import cl.obcom.desktopfx.jfx.Dialog;
import cl.obcom.desktopfx.jfx.DialogAction;
import cl.obcom.desktopfx.jfx.FxmlPane;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;

public class Main extends FxmlPane
{
    private static final boolean CHECKBOX_CODI_SEL_DEFAULT_VALUE = false;
	@FXML TableView<asignatura> tb_asignatura = new TableView<>();
    @FXML TableView<asignatura2> tb_asignatura2 = new TableView<>();
    @FXML private ComboBox<String> cb_facultad;
    @FXML private ComboBox<String> cb_instituto;
    @FXML private ComboBox<String> cb_semestre;
    @FXML private ComboBox<String> cb_agno;
    @FXML private Button bo_salir;
    @FXML private Button bo_ingresar;
    @FXML private Button bo_agregar_semestre;
    @FXML private Button bo_eliminar;
    @FXML private Button bo_aprobar;
    @FXML private Button bo_modificar;
    @FXML private TextField tx_codigo;
    @FXML private TextField tx_filtro;
    @FXML private TextField tx_descripcion;
    @FXML private TextField tx_hrs_teoricas;
    @FXML private TextField tx_hrs_practicas;
    @FXML private TextField tx_hrs_laboratorio;

    final FcsaprobasignaturasWeb port;

    final ObservableList<String> dataSemestre = FXCollections.observableArrayList ();
    final ObservableList<String> dataAgno = FXCollections.observableArrayList ();
    final ObservableList<String> dataFacultad = FXCollections.observableArrayList ();
    final ObservableList<String> dataInstituto = FXCollections.observableArrayList ();
    final ObservableList<asignatura> dataAsignatura = FXCollections.observableArrayList ();
    final ObservableList<asignatura> dataAsignaturaF = FXCollections.observableArrayList ();
    final ObservableList<asignatura2> dataAsignatura2 = FXCollections.observableArrayList ();


    public Main(final DesktopTask task) throws Exception
    {
        // Se carga fxml
        loadFxml();
        task.getStage().getScene().getStylesheets().add("resources/css/NuevoEstilo.css");

        //Inicializar WS
        final FcsaprobasignaturasWebService service = new FcsaprobasignaturasWebService();
        port = service.getFcsaprobasignaturasWebPort();
        task.initWebServicePort(port,service,task.getCodeBase());

        //Init Grillas tb_asignatura1
        initGrillaTbAsignatura1();

        //Init Grillas tb_asignatura2
        initGrillaTbAsignatura2();


        // click en Facultades, debo traer institutos
        cb_facultad.setOnAction((event) -> {
            cargaInstituto(cb_facultad.getValue().toString());
            limpiarCampos();
            cargaAsignaturas();
            cargaAsignaturas2();
        });

        // click en Institutos,
        cb_instituto.setOnAction((event) -> {
            limpiarCampos();
            cargaAsignaturas();
            cargaAsignaturas2();
        });


        // click en semestre
        cb_semestre.setOnAction((event) -> {
            //limpiarCampos();
            cargaAsignaturas2();
        });

        // click en Año
        cb_agno.setOnAction((event) -> {
            //limpiarCampos();
            cargaAsignaturas2();
        });

        //Click on tb_asignaturas
        tb_asignatura.setOnMouseClicked(event -> {
        	if (tb_asignatura.getSelectionModel().getSelectedIndex() >= 0) {
        		final asignatura t = dataAsignatura.get(tb_asignatura.getSelectionModel().getSelectedIndex());
                tx_codigo.setText(t.getCodi_asig());tx_descripcion.setText(t.getDesc_asig());
                tx_hrs_teoricas.setText(t.getCant_ht().toString());tx_hrs_practicas.setText(t.getCant_hp().toString());
                tx_hrs_laboratorio.setText(t.getCant_hl().toString());
        	}
            
        });
        
        // escucha los cambios en tx_filtro para articulos
        tx_filtro.textProperty().addListener((ChangeListener<String>) (observable, oldValue, newValue) -> {
            dataAsignaturaF.clear();
            for (final asignatura p : dataAsignatura) {
                if (matchesFilterAsignatura(p)) {  dataAsignaturaF.add(p);  }
            }
           // tb_articulo.setItems(dataArticuloF);   // copio a la tabla, datos filtrados
            if (dataAsignaturaF.size()!=0) {
                tb_asignatura.setItems(dataAsignaturaF);   // copio a la tabla, datos filtrados
            } else {
                Toolkit.getDefaultToolkit().beep();
                tx_filtro.setText(oldValue);
            }
         });

        cargaFacultad();
        cargaInstituto(cb_facultad.getValue());
        cargaSemestreAgno();
        cargaAsignaturas();
        cargaAsignaturas2();

        bo_agregar_semestre.setOnAction(evt -> seleccionarAsignaturas());
        bo_eliminar.setOnAction(evt -> eliminarAsignatura());
        bo_ingresar.setOnAction(evt -> ingresarAsignatura());
        bo_aprobar.setOnAction(evt -> aprobarAsignaturas());
        bo_modificar.setOnAction(evt -> updateAsignatura());
        bo_salir.setOnAction(evt -> task.terminate(true));
    }
    
    boolean matchesFilterAsignatura(final asignatura t) {
        final String filterString = tx_filtro.getText();
        if (filterString == null || filterString.isEmpty()) {
            // No filter --> Add all.
            return true;
        }
        final String lowerCaseFilterString = filterString.toLowerCase();
        
        if (t.getCodi_asig().toLowerCase().indexOf(lowerCaseFilterString) != -1) {
            return true;
        } else if (t.getDesc_asig().toString().toLowerCase().indexOf(lowerCaseFilterString) != -1) {
            return true;
        } 
        return false; // Does not match
    }

	private void initGrillaTbAsignatura2() {
		final TableColumn<asignatura2, String> cCodi2 = new TableColumn<>("Codigo");
        cCodi2.setCellValueFactory(new PropertyValueFactory<asignatura2, String>("codi_asig"));
        tb_asignatura2.getColumns().add(cCodi2);
        final TableColumn<asignatura2, String> cDesc2 = new TableColumn<>("Descripcion");
        cDesc2.setCellValueFactory(new PropertyValueFactory<asignatura2, String>("desc_asig"));
        tb_asignatura2.getColumns().add(cDesc2);
        final TableColumn<asignatura2, String> cHteo2 = new TableColumn<>("H Teo");
        cHteo2.setCellValueFactory(new PropertyValueFactory<asignatura2, String>("cant_ht"));
        tb_asignatura2.getColumns().add(cHteo2);
        final TableColumn<asignatura2, String> cHpra2 = new TableColumn<>("H Pra");
        cHpra2.setCellValueFactory(new PropertyValueFactory<asignatura2, String>("cant_hp"));
        tb_asignatura2.getColumns().add(cHpra2);
        final TableColumn<asignatura2, String> cHlab2 = new TableColumn<>("H Lab");
        cHlab2.setCellValueFactory(new PropertyValueFactory<asignatura2, String>("cant_hl"));
        tb_asignatura2.getColumns().add(cHlab2);
        final TableColumn<asignatura2, String> cFlag = new TableColumn<>("Aprob");
        cFlag.setCellValueFactory(new PropertyValueFactory<asignatura2, String>("flag_aprobada"));
        tb_asignatura2.getColumns().add(cFlag);
        tb_asignatura2.setItems(dataAsignatura2);
        cCodi2.prefWidthProperty().bind(tb_asignatura2.widthProperty().multiply(0.15));
        cDesc2.prefWidthProperty().bind(tb_asignatura2.widthProperty().multiply(0.50));
        cHteo2.prefWidthProperty().bind(tb_asignatura2.widthProperty().multiply(0.08));
        cHpra2.prefWidthProperty().bind(tb_asignatura2.widthProperty().multiply(0.08));
        cHlab2.prefWidthProperty().bind(tb_asignatura2.widthProperty().multiply(0.08));
        cFlag.prefWidthProperty().bind(tb_asignatura2.widthProperty().multiply(0.08));
	}

	private void initGrillaTbAsignatura1() {
		final TableColumn<asignatura, Boolean> cSel = new TableColumn<>("Sel");
        cSel.setCellValueFactory(new PropertyValueFactory<asignatura, Boolean>("codi_sel"));
        cSel.setCellFactory(CheckBoxTableCell.forTableColumn(cSel));
        cSel.setEditable(true);
        tb_asignatura.getColumns().add(cSel);

        final TableColumn<asignatura, String> cCodi = new TableColumn<>("Codigo");
        cCodi.setCellValueFactory(new PropertyValueFactory<asignatura, String>("codi_asig"));
        tb_asignatura.getColumns().add(cCodi);
        final TableColumn<asignatura, String> cDesc = new TableColumn<>("Descripcion");
        cDesc.setCellValueFactory(new PropertyValueFactory<asignatura, String>("desc_asig"));
        tb_asignatura.getColumns().add(cDesc);
        final TableColumn<asignatura, String> cHteo = new TableColumn<>("H Teo");
        cHteo.setCellValueFactory(new PropertyValueFactory<asignatura, String>("cant_ht"));
        tb_asignatura.getColumns().add(cHteo);
        final TableColumn<asignatura, String> cHpra = new TableColumn<>("H Pra");
        cHpra.setCellValueFactory(new PropertyValueFactory<asignatura, String>("cant_hp"));
        tb_asignatura.getColumns().add(cHpra);
        final TableColumn<asignatura, String> cHlab = new TableColumn<>("H Lab");
        cHlab.setCellValueFactory(new PropertyValueFactory<asignatura, String>("cant_hl"));
        tb_asignatura.getColumns().add(cHlab);
        tb_asignatura.setItems(dataAsignatura);
        tb_asignatura.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        cSel.prefWidthProperty().bind(tb_asignatura.widthProperty().multiply(0.08));
        cCodi.prefWidthProperty().bind(tb_asignatura.widthProperty().multiply(0.15));
        cDesc.prefWidthProperty().bind(tb_asignatura.widthProperty().multiply(0.50));
        cHteo.prefWidthProperty().bind(tb_asignatura.widthProperty().multiply(0.08));
        cHpra.prefWidthProperty().bind(tb_asignatura.widthProperty().multiply(0.08));
        cHlab.prefWidthProperty().bind(tb_asignatura.widthProperty().multiply(0.08));
	}

    public void cargaAsignaturas2(){
        String v_seme, v_agno;
        dataAsignatura2.clear();
        final int k = cb_facultad.getSelectionModel().getSelectedItem().indexOf(" ");
        final int v_fac = Integer.parseInt(cb_facultad.getSelectionModel().getSelectedItem().substring(0, k));

        if (!cb_instituto.getSelectionModel().isEmpty() & !cb_semestre.getSelectionModel().isEmpty()
        & !cb_agno.getSelectionModel().isEmpty()) {
            final int l = cb_instituto.getSelectionModel().getSelectedItem().indexOf(" ");
            final int v_ins = Integer.parseInt(cb_instituto.getSelectionModel().getSelectedItem().substring(0, l));
            v_seme=cb_semestre.getValue().toString();
            v_agno=cb_agno.getValue().toString();
            AprobasignaturasGetResult resultado = null;
            try {
                resultado = port.aprobasignaturasGet(v_fac, v_ins, v_seme, v_agno);
                for ( final AprobasignaturasGetAsig t : resultado.getAsigs()) {
                    dataAsignatura2.add(new asignatura2( t.getCodiAsig(), t.getDescAsig(), t.getCantHteo(), t.getCantHpra()
                        ,t.getCantHlab(), t.getFlagAprobada() ));
               }
            } catch (final FcsaprobasignaturasWebException e) {
                Dialog.showError(this, e);
            }
        }
        tb_asignatura2.setItems(dataAsignatura2);
    }
    
    public void eliminarAsignatura(){
   	 final String v_seme, v_agno;
     final Integer v_fac, v_ins;
     DialogAction D;
     
     final asignatura2 t = dataAsignatura2.get(tb_asignatura2.getSelectionModel().getSelectedIndex());
     v_seme=cb_semestre.getValue().toString().trim();
     v_agno=cb_agno.getValue().toString().trim();
     D = Dialog.showConfirm(this, "Desea eliminar esta asignatura:"+t.getCodi_asig()+" del Semestre:"+v_seme+" Año:"+v_agno);
     if ( D.toString() == "YES" ) {
         try {
        	 port.asignatura2Del(t.getCodi_asig().trim(),v_seme, v_agno);
             Dialog.showMessage(this, "Asignatura eliminada correctamente.");
             cargaAsignaturas2();
         } catch (final FcsaprobasignaturasWebException e) {
        	 Dialog.showError(this,e);
         }
     }
     
    }
    
    public void aprobarAsignaturas(){
    	 final String v_seme, v_agno;
         final Integer v_fac, v_ins;
         DialogAction D;
         
         v_seme=cb_semestre.getValue().toString();
         v_agno=cb_agno.getValue().toString();
         D = Dialog.showConfirm(this, "Desea aprobar esta(s) Asignatura(s) del semestre:"+v_seme+" del Año:"+v_agno);
         if ( D.toString() == "YES" ) {
             final int i = cb_facultad.getSelectionModel().getSelectedItem().indexOf(" ");
             v_fac = Integer.parseInt(cb_facultad.getSelectionModel().getSelectedItem().substring(0, i));
             final int j = cb_instituto.getSelectionModel().getSelectedItem().indexOf(" ");
             v_ins = Integer.parseInt(cb_instituto.getSelectionModel().getSelectedItem().substring(0, j));

             try {
            	 port.aprobarUpd(v_seme, v_agno, v_fac, v_ins);
                 Dialog.showMessage(this, "Asignatura(s) aprobada(s) correctamente.");
                 cargaAsignaturas2();
             } catch (final FcsaprobasignaturasWebException e) {
            	 Dialog.showError(this,e);
             }
         }
    }

    public void seleccionarAsignaturas(){
        final String v_seme, v_agno;
        final Integer v_fac, v_ins;
        DialogAction D;
        
        v_seme=cb_semestre.getValue().toString();
        v_agno=cb_agno.getValue().toString();
        D = Dialog.showConfirm(this, "Desea asignar esta(s) Asignatura(s) al semestre:"+v_seme+" del Año:"+v_agno);
        if ( D.toString() == "YES" ) {
            final int i = cb_facultad.getSelectionModel().getSelectedItem().indexOf(" ");
            v_fac = Integer.parseInt(cb_facultad.getSelectionModel().getSelectedItem().substring(0, i));
            final int j = cb_instituto.getSelectionModel().getSelectedItem().indexOf(" ");
            v_ins = Integer.parseInt(cb_instituto.getSelectionModel().getSelectedItem().substring(0, j));
            for (final asignatura t : dataAsignaturaF) {
                if (t.getCodi_sel()){   // Asignatura seleccionada
                    try {
                        port.aprobasignaturaPut(t.getCodi_asig(), v_seme, v_agno, v_fac, v_ins, t.getCant_ht(), t.getCant_hp(), t.getCant_hl());
                        //limpiarCampos();
                    } catch (final FcsaprobasignaturasWebException e) {
                        Dialog.showError(this,e);
                    }
                }
            }
            Dialog.showMessage(this, "Asignatura(s) asignadas(s) correctamente.");
            cargaAsignaturas2();
        }
    }

    public void ingresarAsignatura(){
        String v_codi_asig, v_desc;
        Integer v_fac, v_ins, v_hrs_teo, v_hrs_pra, v_hrs_lab;

        DialogAction D;

        D = Dialog.showConfirm(this, "Desea ingresar este Asignatura ?");
        if ( D.toString() == "YES" ) {
            v_codi_asig=tx_codigo.getText().trim();
            v_desc=tx_descripcion.getText().trim();
            v_hrs_teo=Integer.parseInt(tx_hrs_teoricas.getText().trim());
            v_hrs_pra=Integer.parseInt(tx_hrs_practicas.getText().trim());
            v_hrs_lab=Integer.parseInt(tx_hrs_laboratorio.getText().trim());

            final int i = cb_facultad.getSelectionModel().getSelectedItem().indexOf(" ");
            v_fac = Integer.parseInt(cb_facultad.getSelectionModel().getSelectedItem().substring(0, i));
            final int j = cb_instituto.getSelectionModel().getSelectedItem().indexOf(" ");
            v_ins = Integer.parseInt(cb_instituto.getSelectionModel().getSelectedItem().substring(0, j));

            try {
                port.asignaturaSet(v_codi_asig, v_desc, v_hrs_teo, v_hrs_pra, v_hrs_lab, v_fac, v_ins);
                Dialog.showMessage(this, "Asignatura ingresada correctamente.");
                cargaAsignaturas();
                limpiarCampos();
            } catch (final FcsaprobasignaturasWebException e) {
                Dialog.showError(this,e);
            }
        }
    }

    public void updateAsignatura(){
        String v_codi_asig, v_desc;
        Integer v_hrs_teo, v_hrs_pra, v_hrs_lab;

        DialogAction D;

        D = Dialog.showConfirm(this, "Desea actualizar datos de Asignatura ?");
        if ( D.toString() == "YES" ) {
            v_codi_asig=tx_codigo.getText().trim();
            v_desc=tx_descripcion.getText().trim();
            v_hrs_teo=Integer.parseInt(tx_hrs_teoricas.getText().trim());
            v_hrs_pra=Integer.parseInt(tx_hrs_practicas.getText().trim());
            v_hrs_lab=Integer.parseInt(tx_hrs_laboratorio.getText().trim());

            try {
                port.asignaturaUpd(v_codi_asig,v_desc, v_hrs_teo, v_hrs_pra,v_hrs_lab);
                Dialog.showMessage(this, "Asignatura actualizada correctamente.");
                cargaAsignaturas();
                limpiarCampos();
            } catch (final FcsaprobasignaturasWebException e) {
                Dialog.showError(this,e);
            }
        }
    }

    public void limpiarCampos(){
        tx_codigo.setText("");
        tx_descripcion.setText("");
        tx_hrs_teoricas.setText("0");
        tx_hrs_practicas.setText("0");
        tx_hrs_laboratorio.setText("0");
       // cb_facultad.getSelectionModel().select(0);  // Set valor por defecto
       // cb_instituto.getSelectionModel().select(0);  // Set valor por defecto
    }

    public void cargaSemestreAgno(){
        int v_year = Calendar.getInstance().get(Calendar.YEAR);
        int v_mes = Calendar.getInstance().get(Calendar.MONTH);
        
        // Solo el semestre actual y año actual
        dataSemestre.clear();
        dataAgno.clear();
        
        dataSemestre.add("1");
        dataSemestre.add("2");
        cb_semestre.setItems(dataSemestre);
        v_mes=v_mes+1;
        if ((v_mes>=1) && (v_mes<=6)){  cb_semestre.getSelectionModel().selectFirst();; } 
        else {	  cb_semestre.getSelectionModel().selectLast(); }	

        dataAgno.add(Integer.toString(v_year-1));
        dataAgno.add(Integer.toString(v_year));
        cb_agno.setItems(dataAgno);
        cb_agno.getSelectionModel().selectLast();
    }

    public void cargaAsignaturas(){
        dataAsignatura.clear();
        final int k = cb_facultad.getSelectionModel().getSelectedItem().indexOf(" ");
        final int v_fac = Integer.parseInt(cb_facultad.getSelectionModel().getSelectedItem().substring(0, k));

        if (!cb_instituto.getSelectionModel().isEmpty()) {
            final int l = cb_instituto.getSelectionModel().getSelectedItem().indexOf(" ");
            final int v_ins = Integer.parseInt(cb_instituto.getSelectionModel().getSelectedItem().substring(0, l));

            AsignaturasGetResult resultado = null;
            try {
                resultado = port.asignaturasGet(v_fac, v_ins);
                for ( final AsignaturasGetAsig t : resultado.getAsigs()) {
                    dataAsignatura.add(new asignatura( CHECKBOX_CODI_SEL_DEFAULT_VALUE, t.getCodiAsig(), t.getDescAsig()
                    		, t.getCantHteo(), t.getCantHpra(), t.getCantHlab() ));
               }
                dataAsignaturaF.addAll(dataAsignatura);
            } catch (final FcsaprobasignaturasWebException e) {
                Dialog.showError(this, e);
            }
        }
        tb_asignatura.setItems(dataAsignatura);
    }

    public void cargaFacultad(){
        dataFacultad.clear();
        FacultadesGetResult res = null;
        try {
            res=port.facultadesGet();
            for (final FacultadesGetFac p : res.getFacs()){
                dataFacultad.add(p.getFacultad());
            }
            cb_facultad.setItems(dataFacultad);
            cb_facultad.getSelectionModel().select(0);  // Set valor por defecto
        } catch (final Exception e1) {
            Dialog.showError(this, e1);
        }
    }

    public void cargaInstituto(final String v_facultad){
        dataInstituto.clear();
        final int k = v_facultad.indexOf(" ");
        final int v_iden_fac = Integer.parseInt(v_facultad.substring(0, k));

        InstitutosGetResult res = null;
        try {
            res=port.institutosGet(v_iden_fac);
            for (final InstitutosGetInstit p : res.getInstits()){
                dataInstituto.add(p.getInstituto());
            }
            cb_instituto.setItems(dataInstituto);
            cb_instituto.getSelectionModel().select(0);  // Set valor por defecto
        } catch (final Exception e) {
            Dialog.showError(this, e);
        }
    }



} // Main - FXMPane
