/*
 * Source: HistoriadocGetHistoria.java - Generated by OBCOM SQL Wizard 1.112
 * Author: Aldo Ulloa Carrasco (Dedalus Limitada)
 *
 * Copyright (c) OBCOM INGENIERIA S.A. (Chile). All rights reserved.
 *
 * All rights to this product are owned by OBCOM INGENIERIA S.A. and may only be
 * used  under  the  terms of its associated license document. You may NOT copy,
 * modify, sublicense, or distribute this source file or portions of  it  unless
 * previously  authorized in writing by OBCOM INGENIERIA S.A. In any event, this
 * notice and above copyright must always be included verbatim with this file.
 */

package cl.dedalus.fcsfuncionarios.ws;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlType;

/**
 * Row of result set {@code HISTORIA} of procedure {@code fcsfuncionarios$historiadoc_get}.
 *
 * <code><pre>
 * #ResultSet HISTORIA HISTORIAS
 *   #Column  ID         BIGINT
 *   #Column  TIPO_FUNC  VARCHAR
 *   #Column  TIPO_CONT  VARCHAR
 *   #Column  FECH_INI   VARCHAR
 *   #Column  FECH_FIN   VARCHAR
 * #EndResultSet
 * </pre></code>
 *
 * @author Aldo Ulloa Carrasco (Dedalus Limitada)
 */
@XmlType(name = "HistoriadocGetHistoria")
public class HistoriadocGetHistoria implements Serializable
{
    private static final long serialVersionUID = 1L;
    private long id;
    private String tipoFunc;
    private String tipoCont;
    private String fechIni;
    private String fechFin;

    //--------------------------------------------------------------------------
    //-- Constructor Methods ---------------------------------------------------
    //--------------------------------------------------------------------------

    /**
     * Constructs a new {@code HistoriadocGetHistoria} instance.
     */
    public HistoriadocGetHistoria()
    {
    }

    //--------------------------------------------------------------------------
    //-- Property Methods ------------------------------------------------------
    //--------------------------------------------------------------------------

    /**
     * Returns the value of property {@code id}.
     *
     * @return the current value of the property.
     */
    public long getId()
    {
        return id;
    }

    /**
     * Changes the value of property {@code id}.
     *
     * @param value the new value of the property.
     */
    public void setId(final long value)
    {
        this.id = value;
    }

    /**
     * Returns the value of property {@code tipoFunc}.
     *
     * @return the current value of the property.
     */
    public String getTipoFunc()
    {
        return tipoFunc;
    }

    /**
     * Changes the value of property {@code tipoFunc}.
     *
     * @param value the new value of the property.
     */
    public void setTipoFunc(final String value)
    {
        this.tipoFunc = value;
    }

    /**
     * Returns the value of property {@code tipoCont}.
     *
     * @return the current value of the property.
     */
    public String getTipoCont()
    {
        return tipoCont;
    }

    /**
     * Changes the value of property {@code tipoCont}.
     *
     * @param value the new value of the property.
     */
    public void setTipoCont(final String value)
    {
        this.tipoCont = value;
    }

    /**
     * Returns the value of property {@code fechIni}.
     *
     * @return the current value of the property.
     */
    public String getFechIni()
    {
        return fechIni;
    }

    /**
     * Changes the value of property {@code fechIni}.
     *
     * @param value the new value of the property.
     */
    public void setFechIni(final String value)
    {
        this.fechIni = value;
    }

    /**
     * Returns the value of property {@code fechFin}.
     *
     * @return the current value of the property.
     */
    public String getFechFin()
    {
        return fechFin;
    }

    /**
     * Changes the value of property {@code fechFin}.
     *
     * @param value the new value of the property.
     */
    public void setFechFin(final String value)
    {
        this.fechFin = value;
    }
}
