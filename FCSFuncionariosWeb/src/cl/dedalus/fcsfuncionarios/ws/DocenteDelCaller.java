/*
 * Source: DocenteDelCaller.java - Generated by OBCOM SQL Wizard 1.112
 * Author: Aldo Ulloa Carrasco (Dedalus Limitada)
 *
 * Copyright (c) OBCOM INGENIERIA S.A. (Chile). All rights reserved.
 *
 * All rights to this product are owned by OBCOM INGENIERIA S.A. and may only be
 * used  under  the  terms of its associated license document. You may NOT copy,
 * modify, sublicense, or distribute this source file or portions of  it  unless
 * previously  authorized in writing by OBCOM INGENIERIA S.A. In any event, this
 * notice and above copyright must always be included verbatim with this file.
 */

package cl.dedalus.fcsfuncionarios.ws;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import javax.sql.DataSource;

/**
 * Caller of procedure {@code fcsfuncionarios$docente_del}.
 *
 * @author Aldo Ulloa Carrasco (Dedalus Limitada)
 */
public class DocenteDelCaller extends ProcedureCaller
{
    //--------------------------------------------------------------------------
    //-- Constructor Methods ---------------------------------------------------
    //--------------------------------------------------------------------------

    /**
     * Constructs a new {@code DocenteDelCaller} instance.
     */
    public DocenteDelCaller()
    {
    }

    //--------------------------------------------------------------------------
    //-- Execute Methods -------------------------------------------------------
    //--------------------------------------------------------------------------

    /**
     * Executes procedure {@code fcsfuncionarios$docente_del} using a data source.
     *
     * @param  dataSource the connection data source.
     * @param  codiDocente {@code _codi_docente varchar}.
     * @param  idenGrado {@code _iden_grado int4}.
     * @return the output parameters and result sets of the procedure.
     * @throws SQLException if an SQL error occurs.
     */
    public DocenteDelResult executeProc(DataSource dataSource, String codiDocente, int idenGrado)
        throws SQLException
    {
        try (final Connection conn = dataSource.getConnection()) {
            return executeProc(conn, codiDocente, idenGrado);
        }
    }

    /**
     * Executes procedure {@code fcsfuncionarios$docente_del} using a connection.
     *
     * @param  conn the database connection.
     * @param  codiDocente {@code _codi_docente varchar}.
     * @param  idenGrado {@code _iden_grado int4}.
     * @return the output parameters and result sets of the procedure.
     * @throws SQLException if an SQL error occurs.
     */
    public DocenteDelResult executeProc(Connection conn, String codiDocente, int idenGrado)
        throws SQLException
    {
        final DocenteDelResult result = createProcResult();
        try (final CallableStatement call = prepareCall(conn, "{call fcsfuncionarios$docente_del(?,?)}")) {
            call.setString(1, codiDocente);
            call.setInt(2, idenGrado);
            call.execute();
        }
        return result;
    }

    /**
     * Creates a new {@code DocenteDelResult} instance.
     *
     * @return a new {@code DocenteDelResult} instance.
     */
    protected DocenteDelResult createProcResult()
    {
        return new DocenteDelResult();
    }

    //--------------------------------------------------------------------------
    //-- Static Methods --------------------------------------------------------
    //--------------------------------------------------------------------------

    /**
     * Executes procedure {@code fcsfuncionarios$docente_del} using a data source.
     *
     * @param  dataSource the connection data source.
     * @param  codiDocente {@code _codi_docente varchar}.
     * @param  idenGrado {@code _iden_grado int4}.
     * @return the output parameters and result sets of the procedure.
     * @throws SQLException if an SQL error occurs.
     */
    public static DocenteDelResult execute(DataSource dataSource, String codiDocente, int idenGrado)
        throws SQLException
    {
        return new DocenteDelCaller().executeProc(dataSource, codiDocente, idenGrado);
    }

    /**
     * Executes procedure {@code fcsfuncionarios$docente_del} using a connection.
     *
     * @param  conn the database connection.
     * @param  codiDocente {@code _codi_docente varchar}.
     * @param  idenGrado {@code _iden_grado int4}.
     * @return the output parameters and result sets of the procedure.
     * @throws SQLException if an SQL error occurs.
     */
    public static DocenteDelResult execute(Connection conn, String codiDocente, int idenGrado)
        throws SQLException
    {
        return new DocenteDelCaller().executeProc(conn, codiDocente, idenGrado);
    }
}
