/*
 * Source: HistoriadocGetCaller.java - Generated by OBCOM SQL Wizard 1.112
 * Author: Aldo Ulloa Carrasco (Dedalus Limitada)
 *
 * Copyright (c) OBCOM INGENIERIA S.A. (Chile). All rights reserved.
 *
 * All rights to this product are owned by OBCOM INGENIERIA S.A. and may only be
 * used  under  the  terms of its associated license document. You may NOT copy,
 * modify, sublicense, or distribute this source file or portions of  it  unless
 * previously  authorized in writing by OBCOM INGENIERIA S.A. In any event, this
 * notice and above copyright must always be included verbatim with this file.
 */

package cl.dedalus.fcsfuncionarios.ws;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

/**
 * Caller of procedure {@code fcsfuncionarios$historiadoc_get}.
 *
 * @author Aldo Ulloa Carrasco (Dedalus Limitada)
 */
public class HistoriadocGetCaller extends ProcedureCaller
{
    //--------------------------------------------------------------------------
    //-- Constructor Methods ---------------------------------------------------
    //--------------------------------------------------------------------------

    /**
     * Constructs a new {@code HistoriadocGetCaller} instance.
     */
    public HistoriadocGetCaller()
    {
    }

    //--------------------------------------------------------------------------
    //-- Execute Methods -------------------------------------------------------
    //--------------------------------------------------------------------------

    /**
     * Executes procedure {@code fcsfuncionarios$historiadoc_get} using a data source.
     *
     * @param  dataSource the connection data source.
     * @param  codiFun {@code _codi_fun varchar}.
     * @return the output parameters and result sets of the procedure.
     * @throws SQLException if an SQL error occurs.
     */
    public HistoriadocGetResult executeProc(DataSource dataSource, String codiFun)
        throws SQLException
    {
        try (final Connection conn = dataSource.getConnection()) {
            return executeProc(conn, codiFun);
        }
    }

    /**
     * Executes procedure {@code fcsfuncionarios$historiadoc_get} using a connection.
     *
     * @param  conn the database connection.
     * @param  codiFun {@code _codi_fun varchar}.
     * @return the output parameters and result sets of the procedure.
     * @throws SQLException if an SQL error occurs.
     */
    public HistoriadocGetResult executeProc(Connection conn, String codiFun)
        throws SQLException
    {
        final HistoriadocGetResult result = createProcResult();
        final String jdbcURL = getJdbcURL(conn);
        if (jdbcURL.startsWith("jdbc:oracle:")) {
            try (final CallableStatement call = prepareCall(conn, "{call fcsfuncionarios$historiadoc_get(?,?)}")) {
                call.setString(1, codiFun);
                call.registerOutParameter(2, ORACLE_CURSOR);
                call.execute();
                result.setHistorias(readHistorias((ResultSet) call.getObject(2)));
            }
        } else if (jdbcURL.startsWith("jdbc:postgresql:")) {
            try (final CallableStatement call = prepareCall(conn, "{call fcsfuncionarios$historiadoc_get(?,?)}")) {
                call.setString(1, codiFun);
                call.registerOutParameter(2, Types.OTHER);
                call.execute();
                result.setHistorias(readHistorias((ResultSet) call.getObject(2)));
            }
        } else {
            try (final CallableStatement call = prepareCall(conn, "{call fcsfuncionarios$historiadoc_get(?)}")) {
                call.setString(1, codiFun);
                int updateCount = 0;
                boolean haveRset = call.execute();
                while (haveRset || updateCount != -1) {
                    if (!haveRset) {
                        updateCount = call.getUpdateCount();
                    } else if (result.getHistorias() == null) {
                        result.setHistorias(readHistorias(call.getResultSet()));
                    } else {
                        unexpectedResultSet(call.getResultSet());
                    }
                    haveRset = call.getMoreResults();
                }
            }
        }
        return result;
    }

    /**
     * Creates a new {@code HistoriadocGetResult} instance.
     *
     * @return a new {@code HistoriadocGetResult} instance.
     */
    protected HistoriadocGetResult createProcResult()
    {
        return new HistoriadocGetResult();
    }

    /**
     * Converts a result set to an array of {@code HistoriadocGetHistoria}.
     *
     * @param  resultSet the result set to convert (can be null).
     * @return an array of {@code HistoriadocGetHistoria} or null.
     * @throws SQLException if an SQL error occurs.
     */
    protected HistoriadocGetHistoria[] readHistorias(final ResultSet resultSet)
        throws SQLException
    {
        if (resultSet == null)
            return null;
        try {
            // Obtain ordinal (index) numbers of result columns
            final int cId = resultSet.findColumn("ID");
            final int cTipoFunc = resultSet.findColumn("TIPO_FUNC");
            final int cTipoCont = resultSet.findColumn("TIPO_CONT");
            final int cFechIni = resultSet.findColumn("FECH_INI");
            final int cFechFin = resultSet.findColumn("FECH_FIN");

            // Convert result rows to an array of "HistoriadocGetHistoria"
            final List<HistoriadocGetHistoria> list = new ArrayList<>();
            while (resultSet.next()) {
                final HistoriadocGetHistoria item = new HistoriadocGetHistoria();
                item.setId(resultSet.getLong(cId));
                item.setTipoFunc(resultSet.getString(cTipoFunc));
                item.setTipoCont(resultSet.getString(cTipoCont));
                item.setFechIni(resultSet.getString(cFechIni));
                item.setFechFin(resultSet.getString(cFechFin));
                if (filterHistoria(item)) list.add(item);
            }
            return list.toArray(new HistoriadocGetHistoria[list.size()]);
        } finally {
            resultSet.close();
        }
    }

    /**
     * Determines if the supplied item should be added to the result array.
     *
     * @param  item the {@code HistoriadocGetHistoria} to be check.
     * @return {@code true} if {@code item} should be added to the array.
     */
    protected boolean filterHistoria(final HistoriadocGetHistoria item)
    {
        return true;
    }

    //--------------------------------------------------------------------------
    //-- Static Methods --------------------------------------------------------
    //--------------------------------------------------------------------------

    /**
     * Executes procedure {@code fcsfuncionarios$historiadoc_get} using a data source.
     *
     * @param  dataSource the connection data source.
     * @param  codiFun {@code _codi_fun varchar}.
     * @return the output parameters and result sets of the procedure.
     * @throws SQLException if an SQL error occurs.
     */
    public static HistoriadocGetResult execute(DataSource dataSource, String codiFun)
        throws SQLException
    {
        return new HistoriadocGetCaller().executeProc(dataSource, codiFun);
    }

    /**
     * Executes procedure {@code fcsfuncionarios$historiadoc_get} using a connection.
     *
     * @param  conn the database connection.
     * @param  codiFun {@code _codi_fun varchar}.
     * @return the output parameters and result sets of the procedure.
     * @throws SQLException if an SQL error occurs.
     */
    public static HistoriadocGetResult execute(Connection conn, String codiFun)
        throws SQLException
    {
        return new HistoriadocGetCaller().executeProc(conn, codiFun);
    }
}
