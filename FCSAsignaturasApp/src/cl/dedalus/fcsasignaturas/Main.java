package cl.dedalus.fcsasignaturas;

import cl.dedalus.fcsasignaturas.ws.DocenteGetAsig;
import cl.dedalus.fcsasignaturas.ws.DocenteGetResult;
import cl.dedalus.fcsasignaturas.ws.FcsasignaturasWeb;
import cl.dedalus.fcsasignaturas.ws.FcsasignaturasWebException;
import cl.dedalus.fcsasignaturas.ws.FcsasignaturasWebService;
import cl.obcom.desktopfx.core.DesktopTask;
import cl.obcom.desktopfx.jfx.Dialog;
import cl.obcom.desktopfx.jfx.FxmlPane;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;

public class Main extends FxmlPane
{
    @FXML TableView<asignatura> tb_asignaturas = new TableView<>();
    @FXML private Button bo_salir;
    @FXML private TextField tx_codigo;
    @FXML private TextField tx_instituto;
    @FXML private TextField tx_facultad;
    @FXML private TextField tx_nombre;
    @FXML private TextField tx_rut;
    @FXML private TextField tx_email;

    final FcsasignaturasWeb port;

    final ObservableList<asignatura> dataAsignatura = FXCollections.observableArrayList ();
    
    public Main(final DesktopTask task) throws Exception
    {
        // Se carga fxml
        loadFxml();
        task.getStage().getScene().getStylesheets().add("resources/css/NuevoEstilo.css");

        final Callback<TableColumn<asignatura,String>, TableCell<asignatura,String>> cellFactory_1 =
            p -> new EditingCell_1();

        //Inicializar WS
        final FcsasignaturasWebService service = new FcsasignaturasWebService();
        port = service.getFcsasignaturasWebPort();
        task.initWebServicePort(port,service,task.getCodeBase());

        //Init Grillas tb_asignatura
        final TableColumn<asignatura, String> tc_codi_asig = new TableColumn<>("Codigo");
        tc_codi_asig.setCellValueFactory(new PropertyValueFactory<asignatura, String>("codi_asig"));
        tb_asignaturas.getColumns().add(tc_codi_asig);
        final TableColumn<asignatura, String> tc_desc_asig = new TableColumn<>("Descripcion");
        tc_desc_asig.setCellValueFactory(new PropertyValueFactory<asignatura, String>("desc_asig"));
        tb_asignaturas.getColumns().add(tc_desc_asig);
        final TableColumn<asignatura, String> tc_codi_seme = new TableColumn<>("Semestre");
        tc_codi_seme.setCellValueFactory(new PropertyValueFactory<asignatura, String>("codi_seme"));
        tb_asignaturas.getColumns().add(tc_codi_seme);
        final TableColumn<asignatura, String> tc_codi_agno = new TableColumn<>("Año");
        tc_codi_agno.setCellValueFactory(new PropertyValueFactory<asignatura, String>("codi_agno"));
        tb_asignaturas.getColumns().add(tc_codi_agno);
        final TableColumn<asignatura, String> tc_semanas = new TableColumn<>("Semanas");
        tc_semanas.setCellValueFactory(new PropertyValueFactory<asignatura, String>("cant_semanas"));
        tb_asignaturas.getColumns().add(tc_semanas);
        final TableColumn<asignatura, String> tc_alumnos= new TableColumn<>("Alumnos");
        tc_alumnos.setCellValueFactory(new PropertyValueFactory<asignatura, String>("cant_alumnos"));       
        tc_alumnos.setCellFactory(cellFactory_1);
        tc_alumnos.setOnEditCommit(
            t -> {
                t.getRowValue().setCant_alumnos(t.getNewValue());
                // Update de alumnos 
                updateAlumnosGrupos(t.getRowValue().getId(), t.getRowValue().getCodi_asig(), t.getRowValue().getCodi_seme()
                		, t.getRowValue().getCodi_agno(), t.getNewValue()
                		, t.getRowValue().getCant_gruposT(), t.getRowValue().getCant_gruposL() ); 
 
            });
        tb_asignaturas.getColumns().add(tc_alumnos);
        
        final TableColumn<asignatura, String> tc_cant_teo = new TableColumn<>("Grp Teo");
        tc_cant_teo.setCellValueFactory(new PropertyValueFactory<asignatura, String>("cant_gruposT"));
        tc_cant_teo.setCellFactory(cellFactory_1);
        tc_cant_teo.setOnEditCommit(
            t -> {
                t.getRowValue().setCant_gruposT(t.getNewValue());
                // Update de grupos Teoricos 
                updateAlumnosGrupos(t.getRowValue().getId(), t.getRowValue().getCodi_asig(), t.getRowValue().getCodi_seme()
                		, t.getRowValue().getCodi_agno(), t.getRowValue().getCant_alumnos()
                		, t.getNewValue(), t.getRowValue().getCant_gruposL() ); 
            });
        tb_asignaturas.getColumns().add(tc_cant_teo);
        
        final TableColumn<asignatura, String> tc_cant_lab = new TableColumn<>("Grp Lab");
        tc_cant_lab.setCellValueFactory(new PropertyValueFactory<asignatura, String>("cant_gruposL"));
        tc_cant_lab.setCellFactory(cellFactory_1);
        tc_cant_lab.setOnEditCommit(
            t -> {
                t.getRowValue().setCant_gruposL(t.getNewValue());
                // Update de grupos laboratorio, 
                updateAlumnosGrupos(t.getRowValue().getId(), t.getRowValue().getCodi_asig(), t.getRowValue().getCodi_seme()
                		, t.getRowValue().getCodi_agno(), t.getRowValue().getCant_alumnos()
                		, t.getRowValue().getCant_gruposT(), t.getNewValue()); 
            });
        tb_asignaturas.getColumns().add(tc_cant_lab);
        
        tb_asignaturas.setItems(dataAsignatura);
        tc_codi_asig.prefWidthProperty().bind(tb_asignaturas.widthProperty().multiply(0.15));
        tc_desc_asig.prefWidthProperty().bind(tb_asignaturas.widthProperty().multiply(0.25));
        tc_codi_seme.prefWidthProperty().bind(tb_asignaturas.widthProperty().multiply(0.10));
        tc_codi_agno.prefWidthProperty().bind(tb_asignaturas.widthProperty().multiply(0.10));
        tc_semanas.prefWidthProperty().bind(tb_asignaturas.widthProperty().multiply(0.10));
        tc_alumnos.prefWidthProperty().bind(tb_asignaturas.widthProperty().multiply(0.10));
        tc_cant_teo.prefWidthProperty().bind(tb_asignaturas.widthProperty().multiply(0.10));
        tc_cant_lab.prefWidthProperty().bind(tb_asignaturas.widthProperty().multiply(0.10));

        cargaAsignaturas(task.getUser().getCode().trim());

        bo_salir.setOnAction(evt -> task.terminate(true));
    }
    
    public void updateAlumnosGrupos(Long v_id, String v_asig, String v_seme, String v_agno, String v_alum
    			, String v_grp_teo, String v_grp_lab){
    	try {
			port.asignaturaUpd(v_id, v_asig, v_seme, v_agno, v_alum, v_grp_teo, v_grp_lab);
		} catch (FcsasignaturasWebException e) {
			Dialog.showError(this, e);
		}
    }

    public void cargaAsignaturas(String v_user){
        dataAsignatura.clear();
        DocenteGetResult res = null;
        try {
        	res = port.docenteGet(v_user);
        	tx_codigo.setText(v_user);
        	tx_facultad.setText(res.getDescFac());
        	tx_instituto.setText(res.getDescIns());
        	tx_nombre.setText(res.getDescNom());
        	tx_rut.setText(res.getDescRut());
        	tx_email.setText(res.getDescEma());
        	for ( final DocenteGetAsig t : res.getAsigs()) {
        		dataAsignatura.add(new asignatura( t.getId(), t.getCodiAsignatura(), t.getDescAsignatura(), t.getCodiSeme()
        				,t.getCodiAgno(), t.getCantSem(), t.getCantAlumnos(),t.getCantGrpTeo(), t.getCantGrpLab() ));
        	}
        	tb_asignaturas.setItems(dataAsignatura);
        } catch (final FcsasignaturasWebException e) {
        	Dialog.showError(this, e);
        }
    }

} // Main - FXMPane
