
package cl.dedalus.fcsasignaturas.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DocenteGetAsig complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DocenteGetAsig">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cantAlumnos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantGrpLab" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantGrpTeo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantSem" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codiAgno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codiAsignatura" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codiSeme" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descAsignatura" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocenteGetAsig", propOrder = {
    "cantAlumnos",
    "cantGrpLab",
    "cantGrpTeo",
    "cantSem",
    "codiAgno",
    "codiAsignatura",
    "codiSeme",
    "descAsignatura",
    "id"
})
public class DocenteGetAsig {

    protected String cantAlumnos;
    protected String cantGrpLab;
    protected String cantGrpTeo;
    protected String cantSem;
    protected String codiAgno;
    protected String codiAsignatura;
    protected String codiSeme;
    protected String descAsignatura;
    protected long id;

    /**
     * Obtiene el valor de la propiedad cantAlumnos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantAlumnos() {
        return cantAlumnos;
    }

    /**
     * Define el valor de la propiedad cantAlumnos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantAlumnos(String value) {
        this.cantAlumnos = value;
    }

    /**
     * Obtiene el valor de la propiedad cantGrpLab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantGrpLab() {
        return cantGrpLab;
    }

    /**
     * Define el valor de la propiedad cantGrpLab.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantGrpLab(String value) {
        this.cantGrpLab = value;
    }

    /**
     * Obtiene el valor de la propiedad cantGrpTeo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantGrpTeo() {
        return cantGrpTeo;
    }

    /**
     * Define el valor de la propiedad cantGrpTeo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantGrpTeo(String value) {
        this.cantGrpTeo = value;
    }

    /**
     * Obtiene el valor de la propiedad cantSem.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantSem() {
        return cantSem;
    }

    /**
     * Define el valor de la propiedad cantSem.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantSem(String value) {
        this.cantSem = value;
    }

    /**
     * Obtiene el valor de la propiedad codiAgno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiAgno() {
        return codiAgno;
    }

    /**
     * Define el valor de la propiedad codiAgno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiAgno(String value) {
        this.codiAgno = value;
    }

    /**
     * Obtiene el valor de la propiedad codiAsignatura.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiAsignatura() {
        return codiAsignatura;
    }

    /**
     * Define el valor de la propiedad codiAsignatura.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiAsignatura(String value) {
        this.codiAsignatura = value;
    }

    /**
     * Obtiene el valor de la propiedad codiSeme.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiSeme() {
        return codiSeme;
    }

    /**
     * Define el valor de la propiedad codiSeme.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiSeme(String value) {
        this.codiSeme = value;
    }

    /**
     * Obtiene el valor de la propiedad descAsignatura.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescAsignatura() {
        return descAsignatura;
    }

    /**
     * Define el valor de la propiedad descAsignatura.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescAsignatura(String value) {
        this.descAsignatura = value;
    }

    /**
     * Obtiene el valor de la propiedad id.
     * 
     */
    public long getId() {
        return id;
    }

    /**
     * Define el valor de la propiedad id.
     * 
     */
    public void setId(long value) {
        this.id = value;
    }

}
