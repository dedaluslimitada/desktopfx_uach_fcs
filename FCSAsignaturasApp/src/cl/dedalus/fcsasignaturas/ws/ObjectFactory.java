
package cl.dedalus.fcsasignaturas.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cl.dedalus.fcsasignaturas.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AsignaturaUpdResponse_QNAME = new QName("http://ws.fcsasignaturas.dedalus.cl/", "asignaturaUpdResponse");
    private final static QName _DocenteGet_QNAME = new QName("http://ws.fcsasignaturas.dedalus.cl/", "docenteGet");
    private final static QName _DocenteGetResponse_QNAME = new QName("http://ws.fcsasignaturas.dedalus.cl/", "docenteGetResponse");
    private final static QName _FaultInfo_QNAME = new QName("http://ws.fcsasignaturas.dedalus.cl/", "faultInfo");
    private final static QName _AsignaturaUpd_QNAME = new QName("http://ws.fcsasignaturas.dedalus.cl/", "asignaturaUpd");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cl.dedalus.fcsasignaturas.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AsignaturaUpd }
     * 
     */
    public AsignaturaUpd createAsignaturaUpd() {
        return new AsignaturaUpd();
    }

    /**
     * Create an instance of {@link DocenteGet }
     * 
     */
    public DocenteGet createDocenteGet() {
        return new DocenteGet();
    }

    /**
     * Create an instance of {@link AsignaturaUpdResponse }
     * 
     */
    public AsignaturaUpdResponse createAsignaturaUpdResponse() {
        return new AsignaturaUpdResponse();
    }

    /**
     * Create an instance of {@link DocenteGetResponse }
     * 
     */
    public DocenteGetResponse createDocenteGetResponse() {
        return new DocenteGetResponse();
    }

    /**
     * Create an instance of {@link DocenteGetAsig }
     * 
     */
    public DocenteGetAsig createDocenteGetAsig() {
        return new DocenteGetAsig();
    }

    /**
     * Create an instance of {@link AsignaturaUpdResult }
     * 
     */
    public AsignaturaUpdResult createAsignaturaUpdResult() {
        return new AsignaturaUpdResult();
    }

    /**
     * Create an instance of {@link DocenteGetResult }
     * 
     */
    public DocenteGetResult createDocenteGetResult() {
        return new DocenteGetResult();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AsignaturaUpdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsasignaturas.dedalus.cl/", name = "asignaturaUpdResponse")
    public JAXBElement<AsignaturaUpdResponse> createAsignaturaUpdResponse(AsignaturaUpdResponse value) {
        return new JAXBElement<AsignaturaUpdResponse>(_AsignaturaUpdResponse_QNAME, AsignaturaUpdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocenteGet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsasignaturas.dedalus.cl/", name = "docenteGet")
    public JAXBElement<DocenteGet> createDocenteGet(DocenteGet value) {
        return new JAXBElement<DocenteGet>(_DocenteGet_QNAME, DocenteGet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocenteGetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsasignaturas.dedalus.cl/", name = "docenteGetResponse")
    public JAXBElement<DocenteGetResponse> createDocenteGetResponse(DocenteGetResponse value) {
        return new JAXBElement<DocenteGetResponse>(_DocenteGetResponse_QNAME, DocenteGetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsasignaturas.dedalus.cl/", name = "faultInfo")
    public JAXBElement<String> createFaultInfo(String value) {
        return new JAXBElement<String>(_FaultInfo_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AsignaturaUpd }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsasignaturas.dedalus.cl/", name = "asignaturaUpd")
    public JAXBElement<AsignaturaUpd> createAsignaturaUpd(AsignaturaUpd value) {
        return new JAXBElement<AsignaturaUpd>(_AsignaturaUpd_QNAME, AsignaturaUpd.class, null, value);
    }

}
