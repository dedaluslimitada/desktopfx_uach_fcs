
package cl.dedalus.fcsasignaturas.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para asignaturaUpdResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="asignaturaUpdResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="asignaturaUpdResult" type="{http://ws.fcsasignaturas.dedalus.cl/}AsignaturaUpdResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "asignaturaUpdResponse", propOrder = {
    "asignaturaUpdResult"
})
public class AsignaturaUpdResponse {

    protected AsignaturaUpdResult asignaturaUpdResult;

    /**
     * Obtiene el valor de la propiedad asignaturaUpdResult.
     * 
     * @return
     *     possible object is
     *     {@link AsignaturaUpdResult }
     *     
     */
    public AsignaturaUpdResult getAsignaturaUpdResult() {
        return asignaturaUpdResult;
    }

    /**
     * Define el valor de la propiedad asignaturaUpdResult.
     * 
     * @param value
     *     allowed object is
     *     {@link AsignaturaUpdResult }
     *     
     */
    public void setAsignaturaUpdResult(AsignaturaUpdResult value) {
        this.asignaturaUpdResult = value;
    }

}
