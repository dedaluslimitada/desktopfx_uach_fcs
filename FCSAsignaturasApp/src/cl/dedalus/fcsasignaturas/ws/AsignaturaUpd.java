
package cl.dedalus.fcsasignaturas.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para asignaturaUpd complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="asignaturaUpd">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="codiAsig" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codiSeme" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codiAgno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantAlumnos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantGrpTeo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantGrpPra" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "asignaturaUpd", propOrder = {
    "id",
    "codiAsig",
    "codiSeme",
    "codiAgno",
    "cantAlumnos",
    "cantGrpTeo",
    "cantGrpPra"
})
public class AsignaturaUpd {

    protected long id;
    protected String codiAsig;
    protected String codiSeme;
    protected String codiAgno;
    protected String cantAlumnos;
    protected String cantGrpTeo;
    protected String cantGrpPra;

    /**
     * Obtiene el valor de la propiedad id.
     * 
     */
    public long getId() {
        return id;
    }

    /**
     * Define el valor de la propiedad id.
     * 
     */
    public void setId(long value) {
        this.id = value;
    }

    /**
     * Obtiene el valor de la propiedad codiAsig.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiAsig() {
        return codiAsig;
    }

    /**
     * Define el valor de la propiedad codiAsig.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiAsig(String value) {
        this.codiAsig = value;
    }

    /**
     * Obtiene el valor de la propiedad codiSeme.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiSeme() {
        return codiSeme;
    }

    /**
     * Define el valor de la propiedad codiSeme.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiSeme(String value) {
        this.codiSeme = value;
    }

    /**
     * Obtiene el valor de la propiedad codiAgno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiAgno() {
        return codiAgno;
    }

    /**
     * Define el valor de la propiedad codiAgno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiAgno(String value) {
        this.codiAgno = value;
    }

    /**
     * Obtiene el valor de la propiedad cantAlumnos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantAlumnos() {
        return cantAlumnos;
    }

    /**
     * Define el valor de la propiedad cantAlumnos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantAlumnos(String value) {
        this.cantAlumnos = value;
    }

    /**
     * Obtiene el valor de la propiedad cantGrpTeo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantGrpTeo() {
        return cantGrpTeo;
    }

    /**
     * Define el valor de la propiedad cantGrpTeo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantGrpTeo(String value) {
        this.cantGrpTeo = value;
    }

    /**
     * Obtiene el valor de la propiedad cantGrpPra.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantGrpPra() {
        return cantGrpPra;
    }

    /**
     * Define el valor de la propiedad cantGrpPra.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantGrpPra(String value) {
        this.cantGrpPra = value;
    }

}
