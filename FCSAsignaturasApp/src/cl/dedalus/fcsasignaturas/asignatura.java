package cl.dedalus.fcsasignaturas;

import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;

public class asignatura{
	private final SimpleLongProperty id;
    private final SimpleStringProperty codi_asig;
    private final SimpleStringProperty desc_asig;
    private final SimpleStringProperty codi_seme;
    private final SimpleStringProperty codi_agno;
    private final SimpleStringProperty cant_semanas;
    private final SimpleStringProperty cant_alumnos;
    private final SimpleStringProperty cant_gruposT;
    private final SimpleStringProperty cant_gruposL;

    public asignatura(final Long id, final String codi_asig,final String desc_asig, final String codi_seme, final String codi_agno
    			, final String cant_semanas , final String cant_alumnos, final String cant_gruposT, final String cant_gruposL)
    {
    	this.id = new SimpleLongProperty(id);
        this.codi_asig = new SimpleStringProperty(codi_asig);
        this.desc_asig = new SimpleStringProperty(desc_asig);
        this.codi_seme = new SimpleStringProperty(codi_seme);
        this.codi_agno = new SimpleStringProperty(codi_agno);
        this.cant_semanas = new SimpleStringProperty(cant_semanas);
        this.cant_alumnos = new SimpleStringProperty(cant_alumnos);
        this.cant_gruposT = new SimpleStringProperty(cant_gruposT);
        this.cant_gruposL = new SimpleStringProperty(cant_gruposL);
    }

    public Long getId() {
        return id.get();
    }
    public String getCodi_asig() {
        return codi_asig.get();
    }
    public String getCodi_seme() {
        return codi_seme.get();
    }
    public String getCodi_agno() {
        return codi_agno.get();
    }
    public String getCant_semanas() {
        return cant_semanas.get();
    }
    public String getDesc_asig() {
        return desc_asig.get();
    }
//------------------------------------------------------
    public String getCant_alumnos() {
        return cant_alumnos.get();
    }
    public void setCant_alumnos(final String ialumnos) {
        cant_alumnos.set(ialumnos);
    }
//------------------------------------------------------    
    public String getCant_gruposT() {
        return cant_gruposT.get();
    }
    public void setCant_gruposT(final String iteo) {
        cant_gruposT.set(iteo);
    }
//------------------------------------------------------    
    public String getCant_gruposL() {
        return cant_gruposL.get();
    }
    public void setCant_gruposL(final String ilab) {
        cant_gruposL.set(ilab);
    }

}
