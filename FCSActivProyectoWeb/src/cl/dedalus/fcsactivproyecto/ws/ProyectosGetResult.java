/*
 * Source: ProyectosGetResult.java - Generated by OBCOM SQL Wizard 1.108
 * Author: Aldo Ulloa Carrasco (Dedalus Limitada)
 *
 * Copyright (c) OBCOM INGENIERIA S.A. (Chile). All rights reserved.
 *
 * All rights to this product are owned by OBCOM INGENIERIA S.A. and may only be
 * used  under  the  terms of its associated license document. You may NOT copy,
 * modify, sublicense, or distribute this source file or portions of  it  unless
 * previously  authorized in writing by OBCOM INGENIERIA S.A. In any event, this
 * notice and above copyright must always be included verbatim with this file.
 */

package cl.dedalus.fcsactivproyecto.ws;

import javax.xml.bind.annotation.XmlType;

/**
 * Results of procedure {@code fcsactivproyecto$proyectos_get}.
 *
 * <code><pre>
 * PRY  ResultSet  Output
 * </pre></code>
 *
 * @author Aldo Ulloa Carrasco (Dedalus Limitada)
 */
@XmlType(name = "ProyectosGetResult")
public class ProyectosGetResult extends ProcedureResult
{
    private static final long serialVersionUID = 1L;
    private ProyectosGetPry[] prys;

    //--------------------------------------------------------------------------
    //-- Constructor Methods ---------------------------------------------------
    //--------------------------------------------------------------------------

    /**
     * Constructs a new {@code ProyectosGetResult} instance.
     */
    public ProyectosGetResult()
    {
    }

    //--------------------------------------------------------------------------
    //-- Property Methods ------------------------------------------------------
    //--------------------------------------------------------------------------

    /**
     * Returns the value of property {@code prys}.
     *
     * @return the current value of the property.
     */
    public ProyectosGetPry[] getPrys()
    {
        return prys;
    }

    /**
     * Changes the value of property {@code prys}.
     *
     * @param value the new value of the property.
     */
    public void setPrys(final ProyectosGetPry[] value)
    {
        this.prys = value;
    }
}
