/*
 * Source: RevistasGetResult.java - Generated by OBCOM SQL Wizard 1.108
 * Author: Aldo Ulloa Carrasco (Dedalus Limitada)
 *
 * Copyright (c) OBCOM INGENIERIA S.A. (Chile). All rights reserved.
 *
 * All rights to this product are owned by OBCOM INGENIERIA S.A. and may only be
 * used  under  the  terms of its associated license document. You may NOT copy,
 * modify, sublicense, or distribute this source file or portions of  it  unless
 * previously  authorized in writing by OBCOM INGENIERIA S.A. In any event, this
 * notice and above copyright must always be included verbatim with this file.
 */

package cl.dedalus.fcsactivproyecto.ws;

import javax.xml.bind.annotation.XmlType;

/**
 * Results of procedure {@code fcsactivproyecto$revistas_get}.
 *
 * <code><pre>
 * REVISTA  ResultSet  Output
 * </pre></code>
 *
 * @author Aldo Ulloa Carrasco (Dedalus Limitada)
 */
@XmlType(name = "RevistasGetResult")
public class RevistasGetResult extends ProcedureResult
{
    private static final long serialVersionUID = 1L;
    private RevistasGetRevista[] revistas;

    //--------------------------------------------------------------------------
    //-- Constructor Methods ---------------------------------------------------
    //--------------------------------------------------------------------------

    /**
     * Constructs a new {@code RevistasGetResult} instance.
     */
    public RevistasGetResult()
    {
    }

    //--------------------------------------------------------------------------
    //-- Property Methods ------------------------------------------------------
    //--------------------------------------------------------------------------

    /**
     * Returns the value of property {@code revistas}.
     *
     * @return the current value of the property.
     */
    public RevistasGetRevista[] getRevistas()
    {
        return revistas;
    }

    /**
     * Changes the value of property {@code revistas}.
     *
     * @param value the new value of the property.
     */
    public void setRevistas(final RevistasGetRevista[] value)
    {
        this.revistas = value;
    }
}
