/*
 * Source: ProyectosGetCaller.java - Generated by OBCOM SQL Wizard 1.108
 * Author: Aldo Ulloa Carrasco (Dedalus Limitada)
 *
 * Copyright (c) OBCOM INGENIERIA S.A. (Chile). All rights reserved.
 *
 * All rights to this product are owned by OBCOM INGENIERIA S.A. and may only be
 * used  under  the  terms of its associated license document. You may NOT copy,
 * modify, sublicense, or distribute this source file or portions of  it  unless
 * previously  authorized in writing by OBCOM INGENIERIA S.A. In any event, this
 * notice and above copyright must always be included verbatim with this file.
 */

package cl.dedalus.fcsactivproyecto.ws;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

/**
 * Caller of procedure {@code fcsactivproyecto$proyectos_get}.
 *
 * @author Aldo Ulloa Carrasco (Dedalus Limitada)
 */
public class ProyectosGetCaller extends ProcedureCaller
{
    //--------------------------------------------------------------------------
    //-- Constructor Methods ---------------------------------------------------
    //--------------------------------------------------------------------------

    /**
     * Constructs a new {@code ProyectosGetCaller} instance.
     */
    public ProyectosGetCaller()
    {
    }

    //--------------------------------------------------------------------------
    //-- Execute Methods -------------------------------------------------------
    //--------------------------------------------------------------------------

    /**
     * Executes procedure {@code fcsactivproyecto$proyectos_get} using a data source.
     *
     * @param  dataSource the connection data source.
     * @param  codiDocente {@code _codi_docente varchar}.
     * @return the output parameters and result sets of the procedure.
     * @throws SQLException if an SQL error occurs.
     */
    public ProyectosGetResult executeProc(DataSource dataSource, String codiDocente)
        throws SQLException
    {
        try (final Connection conn = dataSource.getConnection()) {
            return executeProc(conn, codiDocente);
        }
    }

    /**
     * Executes procedure {@code fcsactivproyecto$proyectos_get} using a connection.
     *
     * @param  conn the database connection.
     * @param  codiDocente {@code _codi_docente varchar}.
     * @return the output parameters and result sets of the procedure.
     * @throws SQLException if an SQL error occurs.
     */
    public ProyectosGetResult executeProc(Connection conn, String codiDocente)
        throws SQLException
    {
        final ProyectosGetResult result = createProcResult();
        final String jdbcURL = getJdbcURL(conn);
        if (jdbcURL.startsWith("jdbc:oracle:")) {
            try (final CallableStatement call = prepareCall(conn, "{call fcsactivproyecto$proyectos_get(?,?)}")) {
                call.setString(1, codiDocente);
                call.registerOutParameter(2, ORACLE_CURSOR);
                call.execute();
                result.setPrys(readPrys((ResultSet) call.getObject(2)));
            }
        } else if (jdbcURL.startsWith("jdbc:postgresql:")) {
            try (final CallableStatement call = prepareCall(conn, "{call fcsactivproyecto$proyectos_get(?,?)}")) {
                call.setString(1, codiDocente);
                call.registerOutParameter(2, Types.OTHER);
                call.execute();
                result.setPrys(readPrys((ResultSet) call.getObject(2)));
            }
        } else {
            try (final CallableStatement call = prepareCall(conn, "{call fcsactivproyecto$proyectos_get(?)}")) {
                call.setString(1, codiDocente);
                int updateCount = 0;
                boolean haveRset = call.execute();
                while (haveRset || updateCount != -1) {
                    if (!haveRset) {
                        updateCount = call.getUpdateCount();
                    } else if (result.getPrys() == null) {
                        result.setPrys(readPrys(call.getResultSet()));
                    } else {
                        unexpectedResultSet(call.getResultSet());
                    }
                    haveRset = call.getMoreResults();
                }
            }
        }
        return result;
    }

    /**
     * Creates a new {@code ProyectosGetResult} instance.
     *
     * @return a new {@code ProyectosGetResult} instance.
     */
    protected ProyectosGetResult createProcResult()
    {
        return new ProyectosGetResult();
    }

    /**
     * Converts a result set to an array of {@code ProyectosGetPry}.
     *
     * @param  resultSet the result set to convert (can be null).
     * @return an array of {@code ProyectosGetPry} or null.
     * @throws SQLException if an SQL error occurs.
     */
    protected ProyectosGetPry[] readPrys(final ResultSet resultSet)
        throws SQLException
    {
        if (resultSet == null)
            return null;
        try {
            // Obtain ordinal (index) numbers of result columns
            final int cId = resultSet.findColumn("ID");
            final int cCodiProyecto = resultSet.findColumn("CODI_PROYECTO");
            final int cDescTitulo = resultSet.findColumn("DESC_TITULO");
            final int cFechIniF = resultSet.findColumn("FECH_INI_F");
            final int cFechFinF = resultSet.findColumn("FECH_FIN_F");
            final int cFechIniE = resultSet.findColumn("FECH_INI_E");
            final int cFechFinE = resultSet.findColumn("FECH_FIN_E");
            final int cDescEstado = resultSet.findColumn("DESC_ESTADO");

            // Convert result rows to an array of "ProyectosGetPry"
            final List<ProyectosGetPry> list = new ArrayList<>();
            while (resultSet.next()) {
                final ProyectosGetPry item = new ProyectosGetPry();
                item.setId(resultSet.getLong(cId));
                item.setCodiProyecto(resultSet.getString(cCodiProyecto));
                item.setDescTitulo(resultSet.getString(cDescTitulo));
                item.setFechIniF(resultSet.getString(cFechIniF));
                item.setFechFinF(resultSet.getString(cFechFinF));
                item.setFechIniE(resultSet.getString(cFechIniE));
                item.setFechFinE(resultSet.getString(cFechFinE));
                item.setDescEstado(resultSet.getString(cDescEstado));
                if (filterPry(item)) list.add(item);
            }
            return list.toArray(new ProyectosGetPry[list.size()]);
        } finally {
            resultSet.close();
        }
    }

    /**
     * Returns true if the supplied item should be added to the array.
     *
     * @param  item the teim that will be checked.
     * @return true if the item should be added to the array.
     */
    protected boolean filterPry(final ProyectosGetPry item)
    {
        return true;
    }

    //--------------------------------------------------------------------------
    //-- Static Methods --------------------------------------------------------
    //--------------------------------------------------------------------------

    /**
     * Executes procedure {@code fcsactivproyecto$proyectos_get} using a data source.
     *
     * @param  dataSource the connection data source.
     * @param  codiDocente {@code _codi_docente varchar}.
     * @return the output parameters and result sets of the procedure.
     * @throws SQLException if an SQL error occurs.
     */
    public static ProyectosGetResult execute(DataSource dataSource, String codiDocente)
        throws SQLException
    {
        return new ProyectosGetCaller().executeProc(dataSource, codiDocente);
    }

    /**
     * Executes procedure {@code fcsactivproyecto$proyectos_get} using a connection.
     *
     * @param  conn the database connection.
     * @param  codiDocente {@code _codi_docente varchar}.
     * @return the output parameters and result sets of the procedure.
     * @throws SQLException if an SQL error occurs.
     */
    public static ProyectosGetResult execute(Connection conn, String codiDocente)
        throws SQLException
    {
        return new ProyectosGetCaller().executeProc(conn, codiDocente);
    }
}
