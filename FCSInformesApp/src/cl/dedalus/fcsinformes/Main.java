package cl.dedalus.fcsinformes;

import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;
import cl.dedalus.fcsinformes.ws.FacultadesGetFac;
import cl.dedalus.fcsinformes.ws.FacultadesGetResult;
import cl.dedalus.fcsinformes.ws.FcsinformesWeb;
import cl.dedalus.fcsinformes.ws.FcsinformesWebException;
import cl.dedalus.fcsinformes.ws.FcsinformesWebService;
import cl.dedalus.fcsinformes.ws.InstitutosGetInstit;
import cl.dedalus.fcsinformes.ws.InstitutosGetResult;
import cl.dedalus.fcsinformes.ws.XimprimirGetResult;
import cl.obcom.desktopfx.core.DesktopTask;
import cl.obcom.desktopfx.jfx.Dialog;
import cl.obcom.desktopfx.jfx.DialogAction;
import cl.obcom.desktopfx.jfx.FxmlPane;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;

public class Main extends FxmlPane
{

    @FXML private ComboBox<String> cb_facultad;
    @FXML private ComboBox<String> cb_instituto;
    @FXML private ComboBox<String> cb_semestre;
    @FXML private ComboBox<String> cb_agno;  
    @FXML private Button bo_salir;
    @FXML private Button bo_imprimir;
    @FXML private RadioButton rb_instituto;
    @FXML private RadioButton rb_proyectos;
    

    final FcsinformesWeb port;

    final ObservableList<String> dataFacultad = FXCollections.observableArrayList ();
    final ObservableList<String> dataInstituto = FXCollections.observableArrayList ();
    final ObservableList<String> dataSemestre = FXCollections.observableArrayList ();
    final ObservableList<String> dataAgno = FXCollections.observableArrayList ();
   

    public Main(final DesktopTask task) throws Exception
    {
        // Se carga fxml
        loadFxml();
        task.getStage().getScene().getStylesheets().add("resources/css/NuevoEstilo.css");

        //Inicializar WS
        final FcsinformesWebService service = new FcsinformesWebService();
        port = service.getFcsinformesWebPort();
        task.initWebServicePort(port,service,task.getCodeBase());

        // radio button para los informes	
        final ToggleGroup groupRB = new ToggleGroup();
        rb_instituto.setToggleGroup(groupRB);
        rb_proyectos.setToggleGroup(groupRB);
        rb_instituto.setSelected(true);
        
        // click en Facultades, debo traer instituso
        cb_facultad.setOnAction((event) -> {
            cargaInstituto(cb_facultad.getValue().toString());
        });
        
        cargaFacultad();
        cargaInstituto(cb_facultad.getValue());
        cargaSemestreAgno();

        bo_imprimir.setOnAction(evt -> imprimirInforme(task));
        bo_salir.setOnAction(evt -> task.terminate(true));
    }
    
    public String obtenerString(DesktopTask T1){
    	String v_cmd=null;
    	String v_server=T1.getCodeBase().toString().substring(7);          
        v_server=v_server.substring(0, v_server.indexOf(":"));
    	
        if (rb_instituto.isSelected()){
        	String v_semestre=cb_semestre.getValue().toString();
        	String v_agno=cb_agno.getValue().toString();
            final int i = cb_facultad.getSelectionModel().getSelectedItem().indexOf(" ");
            String v_fac = cb_facultad.getSelectionModel().getSelectedItem().substring(0, i);
            final int j = cb_instituto.getSelectionModel().getSelectedItem().indexOf(" ");
            String v_ins = cb_instituto.getSelectionModel().getSelectedItem().substring(0, j);
        	
        	v_cmd = "http://"+v_server+":8088/jasperserver/rest_v2/reports/reports/Fciencias/";
 	   		v_cmd = v_cmd + "rep_instituto.pdf?j_username=jasperadmin&j_password=jasperadmin&";
 	   		v_cmd = v_cmd + "P_facultad="+v_fac+"&P_instituto="+v_ins+"&P_semestre="+v_semestre+"&P_agnoasignatura="+v_agno;
        }
        if (rb_proyectos.isSelected()){
        	String v_semestre=cb_semestre.getValue().toString();
        	String v_agno=cb_agno.getValue().toString();
            final int i = cb_facultad.getSelectionModel().getSelectedItem().indexOf(" ");
            String v_fac = cb_facultad.getSelectionModel().getSelectedItem().substring(0, i);
            final int j = cb_instituto.getSelectionModel().getSelectedItem().indexOf(" ");
            String v_ins = cb_instituto.getSelectionModel().getSelectedItem().substring(0, j);
        	
        	v_cmd = "http://"+v_server+":8088/jasperserver/rest_v2/reports/reports/Fciencias/";
 	   		v_cmd = v_cmd + "rep_instituto2.pdf?j_username=jasperadmin&j_password=jasperadmin&";
 	   		v_cmd = v_cmd + "P_facultad="+v_fac+"&P_instituto="+v_ins+"&P_semestre="+v_semestre+"&P_agnoasignatura="+v_agno;        	
        }
    	return v_cmd;
    }
    
    public String obtenerNombreArchivo(DesktopTask T1){
    	String v_file=null;
    	
    	if (rb_instituto.isSelected()){
     	   	v_file = T1.getPluginDirectory().toString()+"/rep_instituto_temp.pdf";
        }
    	if (rb_proyectos.isSelected()){
     	   	v_file = T1.getPluginDirectory().toString()+"/rep_instituto2_temp.pdf";
        }
    	return v_file;
    }
    
    public void calculaHorasFormEjecProyecto(){
    	final int i = cb_facultad.getSelectionModel().getSelectedItem().indexOf(" ");
    	Integer v_fac = Integer.parseInt(cb_facultad.getSelectionModel().getSelectedItem().substring(0, i));
    	final int j = cb_instituto.getSelectionModel().getSelectedItem().indexOf(" ");
    	Integer v_ins = Integer.parseInt(cb_instituto.getSelectionModel().getSelectedItem().substring(0, j));


    	try {
    		port.calculaTotal(v_fac, v_ins, cb_semestre.getValue().toString(), cb_agno.getValue().toString());
    	} catch (FcsinformesWebException e) {
    		Dialog.showError(this, e);
    	}
    }
    
    public void imprimirInforme(DesktopTask T){
  	   final DialogAction D=Dialog.showConfirm(this, "Esta seguro de generar el Informe ?");
       String v_cmd = null, v_file = null;                   
       
       if (D.toString() == "YES"){
    	   calculaHorasFormEjecProyecto();
    	   v_cmd = obtenerString(T);
    	   v_file = obtenerNombreArchivo(T);
    	   
           XimprimirGetResult r1 = null;
           byte[] P;
           try {
               //  Leo el .PDF (P) desde el procedimiento xinventarioget
        	   
               r1 = port.ximprimirGet(v_cmd);
               P = r1.getVReporte();
               //String ST= new String(P);
               //System.out.println("XXX->:"+P);
               //System.out.println("ST->:"+ST);
               //System.out.println("Largo->:"+P.length);

               // se define el archivo de salida para el .PDF
               OutputStream tt=null;
               try {
                   tt = new FileOutputStream(new File(v_file));
               } catch (final FileNotFoundException e) {
                   Dialog.showError(this, e);
               }

               // Escribo el contenido en el archivo .PDF
               try {
                   tt.write(P);
               } catch (final IOException e) {
                   Dialog.showError(this, e);
               }

               // Se abre el .PDF en el Cliente.
               final File myFile = new File(v_file);
               try {
                   Desktop.getDesktop().open(myFile);
               } catch (final IOException e) {
                   Dialog.showError(this, e);
               }

           } catch (final FcsinformesWebException e) {
               Dialog.showError(this, e);
           }
       }   // if=YES	
    }

    public void cargaSemestreAgno(){
        int v_year = Calendar.getInstance().get(Calendar.YEAR);
        int v_mes = Calendar.getInstance().get(Calendar.MONTH);
        
        // Solo el semestre actual y año actual
        dataSemestre.clear();
        dataAgno.clear();
        
        dataSemestre.add("1");
        dataSemestre.add("2");
        cb_semestre.setItems(dataSemestre);
        v_mes=v_mes+1;
        if ((v_mes>=1) && (v_mes<=6)){  cb_semestre.getSelectionModel().selectFirst();; } 
        else {	  cb_semestre.getSelectionModel().selectLast(); }	

        dataAgno.add(Integer.toString(v_year-1));
        dataAgno.add(Integer.toString(v_year));
        cb_agno.setItems(dataAgno);
        cb_agno.getSelectionModel().selectLast();
    }


    public void cargaFacultad(){
        dataFacultad.clear();
        FacultadesGetResult res = null;
        try {
            res=port.facultadesGet();
            for (final FacultadesGetFac p : res.getFacs()){
                dataFacultad.add(p.getFacultad());
            }
            cb_facultad.setItems(dataFacultad);
            cb_facultad.getSelectionModel().select(0);  // Set valor por defecto
        } catch (final Exception e1) {
            Dialog.showError(this, e1);
        }
    }

    public void cargaInstituto(final String v_facultad){
        dataInstituto.clear();
        final int k = v_facultad.indexOf(" ");
        final int v_iden_fac = Integer.parseInt(v_facultad.substring(0, k));

        InstitutosGetResult res = null;
        try {
            res=port.institutosGet(v_iden_fac);
            for (final InstitutosGetInstit p : res.getInstits()){
                dataInstituto.add(p.getInstituto());
            }
            cb_instituto.setItems(dataInstituto);
            cb_instituto.getSelectionModel().select(0);  // Set valor por defecto
        } catch (final Exception e) {
            Dialog.showError(this, e);
        }
    }


} // Main - FXMPane
