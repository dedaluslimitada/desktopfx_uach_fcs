
package cl.dedalus.fcsinformes.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ximprimirGetResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ximprimirGetResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ximprimirGetResult" type="{http://ws.fcsinformes.dedalus.cl/}XimprimirGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ximprimirGetResponse", propOrder = {
    "ximprimirGetResult"
})
public class XimprimirGetResponse {

    protected XimprimirGetResult ximprimirGetResult;

    /**
     * Obtiene el valor de la propiedad ximprimirGetResult.
     * 
     * @return
     *     possible object is
     *     {@link XimprimirGetResult }
     *     
     */
    public XimprimirGetResult getXimprimirGetResult() {
        return ximprimirGetResult;
    }

    /**
     * Define el valor de la propiedad ximprimirGetResult.
     * 
     * @param value
     *     allowed object is
     *     {@link XimprimirGetResult }
     *     
     */
    public void setXimprimirGetResult(XimprimirGetResult value) {
        this.ximprimirGetResult = value;
    }

}
