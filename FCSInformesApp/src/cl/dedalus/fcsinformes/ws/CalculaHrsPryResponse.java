
package cl.dedalus.fcsinformes.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para calculaHrsPryResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="calculaHrsPryResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="calculaHrsPryResult" type="{http://ws.fcsinformes.dedalus.cl/}CalculaHrsPryResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "calculaHrsPryResponse", propOrder = {
    "calculaHrsPryResult"
})
public class CalculaHrsPryResponse {

    protected CalculaHrsPryResult calculaHrsPryResult;

    /**
     * Obtiene el valor de la propiedad calculaHrsPryResult.
     * 
     * @return
     *     possible object is
     *     {@link CalculaHrsPryResult }
     *     
     */
    public CalculaHrsPryResult getCalculaHrsPryResult() {
        return calculaHrsPryResult;
    }

    /**
     * Define el valor de la propiedad calculaHrsPryResult.
     * 
     * @param value
     *     allowed object is
     *     {@link CalculaHrsPryResult }
     *     
     */
    public void setCalculaHrsPryResult(CalculaHrsPryResult value) {
        this.calculaHrsPryResult = value;
    }

}
