
package cl.dedalus.fcsinformes.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para calculaTotal complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="calculaTotal">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codiFacultad" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="codiInstituto" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="codiSemestre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codiAgno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "calculaTotal", propOrder = {
    "codiFacultad",
    "codiInstituto",
    "codiSemestre",
    "codiAgno"
})
public class CalculaTotal {

    protected int codiFacultad;
    protected int codiInstituto;
    protected String codiSemestre;
    protected String codiAgno;

    /**
     * Obtiene el valor de la propiedad codiFacultad.
     * 
     */
    public int getCodiFacultad() {
        return codiFacultad;
    }

    /**
     * Define el valor de la propiedad codiFacultad.
     * 
     */
    public void setCodiFacultad(int value) {
        this.codiFacultad = value;
    }

    /**
     * Obtiene el valor de la propiedad codiInstituto.
     * 
     */
    public int getCodiInstituto() {
        return codiInstituto;
    }

    /**
     * Define el valor de la propiedad codiInstituto.
     * 
     */
    public void setCodiInstituto(int value) {
        this.codiInstituto = value;
    }

    /**
     * Obtiene el valor de la propiedad codiSemestre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiSemestre() {
        return codiSemestre;
    }

    /**
     * Define el valor de la propiedad codiSemestre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiSemestre(String value) {
        this.codiSemestre = value;
    }

    /**
     * Obtiene el valor de la propiedad codiAgno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiAgno() {
        return codiAgno;
    }

    /**
     * Define el valor de la propiedad codiAgno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiAgno(String value) {
        this.codiAgno = value;
    }

}
