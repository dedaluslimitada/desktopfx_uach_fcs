
package cl.dedalus.fcsinformes.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para XimprimirGetResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="XimprimirGetResult">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.fcsinformes.dedalus.cl/}ProcedureResult">
 *       &lt;sequence>
 *         &lt;element name="VReporte" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "XimprimirGetResult", propOrder = {
    "vReporte"
})
public class XimprimirGetResult
    extends ProcedureResult
{

    @XmlElement(name = "VReporte")
    protected byte[] vReporte;

    /**
     * Obtiene el valor de la propiedad vReporte.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getVReporte() {
        return vReporte;
    }

    /**
     * Define el valor de la propiedad vReporte.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setVReporte(byte[] value) {
        this.vReporte = value;
    }

}
