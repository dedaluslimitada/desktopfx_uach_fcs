
package cl.dedalus.fcsinformes.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para calculaTotalResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="calculaTotalResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="calculaTotalResult" type="{http://ws.fcsinformes.dedalus.cl/}CalculaTotalResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "calculaTotalResponse", propOrder = {
    "calculaTotalResult"
})
public class CalculaTotalResponse {

    protected CalculaTotalResult calculaTotalResult;

    /**
     * Obtiene el valor de la propiedad calculaTotalResult.
     * 
     * @return
     *     possible object is
     *     {@link CalculaTotalResult }
     *     
     */
    public CalculaTotalResult getCalculaTotalResult() {
        return calculaTotalResult;
    }

    /**
     * Define el valor de la propiedad calculaTotalResult.
     * 
     * @param value
     *     allowed object is
     *     {@link CalculaTotalResult }
     *     
     */
    public void setCalculaTotalResult(CalculaTotalResult value) {
        this.calculaTotalResult = value;
    }

}
