
package cl.dedalus.fcsinformes.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cl.dedalus.fcsinformes.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CalculaHrsPry_QNAME = new QName("http://ws.fcsinformes.dedalus.cl/", "calculaHrsPry");
    private final static QName _InstitutosGetResponse_QNAME = new QName("http://ws.fcsinformes.dedalus.cl/", "institutosGetResponse");
    private final static QName _CalculaTotal_QNAME = new QName("http://ws.fcsinformes.dedalus.cl/", "calculaTotal");
    private final static QName _InstitutosGet_QNAME = new QName("http://ws.fcsinformes.dedalus.cl/", "institutosGet");
    private final static QName _CalculaTotalResponse_QNAME = new QName("http://ws.fcsinformes.dedalus.cl/", "calculaTotalResponse");
    private final static QName _XimprimirGetResponse_QNAME = new QName("http://ws.fcsinformes.dedalus.cl/", "ximprimirGetResponse");
    private final static QName _FaultInfo_QNAME = new QName("http://ws.fcsinformes.dedalus.cl/", "faultInfo");
    private final static QName _FacultadesGet_QNAME = new QName("http://ws.fcsinformes.dedalus.cl/", "facultadesGet");
    private final static QName _XimprimirGet_QNAME = new QName("http://ws.fcsinformes.dedalus.cl/", "ximprimirGet");
    private final static QName _CalculaHrsPryResponse_QNAME = new QName("http://ws.fcsinformes.dedalus.cl/", "calculaHrsPryResponse");
    private final static QName _FacultadesGetResponse_QNAME = new QName("http://ws.fcsinformes.dedalus.cl/", "facultadesGetResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cl.dedalus.fcsinformes.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link FacultadesGet }
     * 
     */
    public FacultadesGet createFacultadesGet() {
        return new FacultadesGet();
    }

    /**
     * Create an instance of {@link XimprimirGet }
     * 
     */
    public XimprimirGet createXimprimirGet() {
        return new XimprimirGet();
    }

    /**
     * Create an instance of {@link CalculaHrsPryResponse }
     * 
     */
    public CalculaHrsPryResponse createCalculaHrsPryResponse() {
        return new CalculaHrsPryResponse();
    }

    /**
     * Create an instance of {@link FacultadesGetResponse }
     * 
     */
    public FacultadesGetResponse createFacultadesGetResponse() {
        return new FacultadesGetResponse();
    }

    /**
     * Create an instance of {@link CalculaHrsPry }
     * 
     */
    public CalculaHrsPry createCalculaHrsPry() {
        return new CalculaHrsPry();
    }

    /**
     * Create an instance of {@link InstitutosGetResponse }
     * 
     */
    public InstitutosGetResponse createInstitutosGetResponse() {
        return new InstitutosGetResponse();
    }

    /**
     * Create an instance of {@link CalculaTotal }
     * 
     */
    public CalculaTotal createCalculaTotal() {
        return new CalculaTotal();
    }

    /**
     * Create an instance of {@link InstitutosGet }
     * 
     */
    public InstitutosGet createInstitutosGet() {
        return new InstitutosGet();
    }

    /**
     * Create an instance of {@link CalculaTotalResponse }
     * 
     */
    public CalculaTotalResponse createCalculaTotalResponse() {
        return new CalculaTotalResponse();
    }

    /**
     * Create an instance of {@link XimprimirGetResponse }
     * 
     */
    public XimprimirGetResponse createXimprimirGetResponse() {
        return new XimprimirGetResponse();
    }

    /**
     * Create an instance of {@link CalculaTotalResult }
     * 
     */
    public CalculaTotalResult createCalculaTotalResult() {
        return new CalculaTotalResult();
    }

    /**
     * Create an instance of {@link XimprimirGetResult }
     * 
     */
    public XimprimirGetResult createXimprimirGetResult() {
        return new XimprimirGetResult();
    }

    /**
     * Create an instance of {@link FacultadesGetFac }
     * 
     */
    public FacultadesGetFac createFacultadesGetFac() {
        return new FacultadesGetFac();
    }

    /**
     * Create an instance of {@link CalculaHrsPryResult }
     * 
     */
    public CalculaHrsPryResult createCalculaHrsPryResult() {
        return new CalculaHrsPryResult();
    }

    /**
     * Create an instance of {@link FacultadesGetResult }
     * 
     */
    public FacultadesGetResult createFacultadesGetResult() {
        return new FacultadesGetResult();
    }

    /**
     * Create an instance of {@link InstitutosGetResult }
     * 
     */
    public InstitutosGetResult createInstitutosGetResult() {
        return new InstitutosGetResult();
    }

    /**
     * Create an instance of {@link InstitutosGetInstit }
     * 
     */
    public InstitutosGetInstit createInstitutosGetInstit() {
        return new InstitutosGetInstit();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CalculaHrsPry }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsinformes.dedalus.cl/", name = "calculaHrsPry")
    public JAXBElement<CalculaHrsPry> createCalculaHrsPry(CalculaHrsPry value) {
        return new JAXBElement<CalculaHrsPry>(_CalculaHrsPry_QNAME, CalculaHrsPry.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InstitutosGetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsinformes.dedalus.cl/", name = "institutosGetResponse")
    public JAXBElement<InstitutosGetResponse> createInstitutosGetResponse(InstitutosGetResponse value) {
        return new JAXBElement<InstitutosGetResponse>(_InstitutosGetResponse_QNAME, InstitutosGetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CalculaTotal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsinformes.dedalus.cl/", name = "calculaTotal")
    public JAXBElement<CalculaTotal> createCalculaTotal(CalculaTotal value) {
        return new JAXBElement<CalculaTotal>(_CalculaTotal_QNAME, CalculaTotal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InstitutosGet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsinformes.dedalus.cl/", name = "institutosGet")
    public JAXBElement<InstitutosGet> createInstitutosGet(InstitutosGet value) {
        return new JAXBElement<InstitutosGet>(_InstitutosGet_QNAME, InstitutosGet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CalculaTotalResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsinformes.dedalus.cl/", name = "calculaTotalResponse")
    public JAXBElement<CalculaTotalResponse> createCalculaTotalResponse(CalculaTotalResponse value) {
        return new JAXBElement<CalculaTotalResponse>(_CalculaTotalResponse_QNAME, CalculaTotalResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XimprimirGetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsinformes.dedalus.cl/", name = "ximprimirGetResponse")
    public JAXBElement<XimprimirGetResponse> createXimprimirGetResponse(XimprimirGetResponse value) {
        return new JAXBElement<XimprimirGetResponse>(_XimprimirGetResponse_QNAME, XimprimirGetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsinformes.dedalus.cl/", name = "faultInfo")
    public JAXBElement<String> createFaultInfo(String value) {
        return new JAXBElement<String>(_FaultInfo_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FacultadesGet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsinformes.dedalus.cl/", name = "facultadesGet")
    public JAXBElement<FacultadesGet> createFacultadesGet(FacultadesGet value) {
        return new JAXBElement<FacultadesGet>(_FacultadesGet_QNAME, FacultadesGet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XimprimirGet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsinformes.dedalus.cl/", name = "ximprimirGet")
    public JAXBElement<XimprimirGet> createXimprimirGet(XimprimirGet value) {
        return new JAXBElement<XimprimirGet>(_XimprimirGet_QNAME, XimprimirGet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CalculaHrsPryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsinformes.dedalus.cl/", name = "calculaHrsPryResponse")
    public JAXBElement<CalculaHrsPryResponse> createCalculaHrsPryResponse(CalculaHrsPryResponse value) {
        return new JAXBElement<CalculaHrsPryResponse>(_CalculaHrsPryResponse_QNAME, CalculaHrsPryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FacultadesGetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsinformes.dedalus.cl/", name = "facultadesGetResponse")
    public JAXBElement<FacultadesGetResponse> createFacultadesGetResponse(FacultadesGetResponse value) {
        return new JAXBElement<FacultadesGetResponse>(_FacultadesGetResponse_QNAME, FacultadesGetResponse.class, null, value);
    }

}
