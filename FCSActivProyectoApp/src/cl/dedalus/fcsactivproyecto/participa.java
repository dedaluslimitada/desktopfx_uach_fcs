package cl.dedalus.fcsactivproyecto;

import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;

public class participa{
    private final SimpleLongProperty id;
    private final SimpleStringProperty codi_part;
    private final SimpleStringProperty nomb_part;
    private final SimpleStringProperty orig_part;

    public participa(final Long id, final String codi_part, final String nomb_part, final String orig_part){
        this.id = new SimpleLongProperty(id);
        this.codi_part = new SimpleStringProperty(codi_part);
        this.nomb_part = new SimpleStringProperty(nomb_part);
        this.orig_part = new SimpleStringProperty(orig_part);
    }

    public Long getId() {
        return id.get();
    }
    public String getCodi_part() {
        return codi_part.get();
    }
    public String getNomb_part() {
        return nomb_part.get();
    }
    public String getOrig_part() {
        return orig_part.get();
    }

}
