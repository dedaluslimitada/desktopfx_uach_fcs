/*
 * Copyright (c) DEDALUS Limitada. All rights reserved.
 *
 * All rights to this product are owned by DEDALUS Limitada and may only be used
 * under the terms of its associated license document. You may NOT copy, modify,
 * sublicense,  or  distribute  this  source  file  or  portions  of  it  unless
 * previously authorized in writing by  DEDALUS  Limitada. In  any  event,  this
 * notice and above copyright must always be included verbatim with this file.
 */

package cl.dedalus.fcsactivproyecto;

/**
 * @author Aulloa
 */
public class ANewCongresoData {
    //variables visibility set to private
    private String  tituloC;
    private String  ciudadC;
    private String  desdeC;
    private String  hastaC;
    private String  descC;


    //constructor takes String and Integer as parameters
    public ANewCongresoData(){
        this.tituloC="";
        this.ciudadC="";
        this.desdeC="";
        this.hastaC="";
        this.descC="";
    }

    //public method that gets/set
    public String getTituloC(){
        return tituloC;
    }
    public void setTituloC(final String ititulo){
        this.tituloC=ititulo;
    }

    //public method that gets/set
    public String getCiudadC(){
        return ciudadC;
    }
    public void setCiudadC(final String iciudad){
        this.ciudadC=iciudad;
    }

    //public method that gets/set
    public String getDesdeC(){
        return desdeC;
    }
    public void setDesdeC(final String idesde){
        this.desdeC=idesde;
    }

    //public method that gets/set
    public String getHastaC(){
        return hastaC;
    }
    public void setHastaC(final String ihasta){
        this.hastaC=ihasta;
    }

    //public method that gets/set
    public String getDescC(){
        return descC;
    }
    public void setDescC(final String idesc){
        this.descC=idesc;
    }

}
