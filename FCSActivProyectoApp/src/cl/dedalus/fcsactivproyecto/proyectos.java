package cl.dedalus.fcsactivproyecto;

import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;

public class proyectos{
    private final SimpleLongProperty id;
    private final SimpleStringProperty codigo;
    private final SimpleStringProperty titulo;
    private final SimpleStringProperty fech_ini_f;
    private final SimpleStringProperty fech_fin_f;
    private final SimpleStringProperty fech_ini_e;
    private final SimpleStringProperty fech_fin_e;
    private final SimpleStringProperty estado;


    public proyectos(final Long id, final String codigo, final String titulo,final String fech_ini_f,final String fech_fin_f
                     ,final String fech_ini_e, final String fech_fin_e, final String estado){
        this.id = new SimpleLongProperty(id);
        this.codigo = new SimpleStringProperty(codigo);
        this.titulo = new SimpleStringProperty(titulo);
        this.fech_ini_f = new SimpleStringProperty(fech_ini_f);
        this.fech_fin_f = new SimpleStringProperty(fech_fin_f);
        this.fech_ini_e = new SimpleStringProperty(fech_ini_e);
        this.fech_fin_e = new SimpleStringProperty(fech_fin_e);
        this.estado = new SimpleStringProperty(estado);
    }

    public Long getId() {
        return id.get();
    }
    public String getCodigo() {
        return codigo.get();
    }
    public String getTitulo() {
        return titulo.get();
    }
    public String getFech_ini_f() {
        return fech_ini_f.get();
    }
    public String getFech_fin_f() {
        return fech_fin_f.get();
    }
    public String getFech_ini_e() {
        return fech_ini_e.get();
    }
    public String getFech_fin_e() {
        return fech_fin_e.get();
    }
    public String getEstado() {
        return estado.get();
    }
}
