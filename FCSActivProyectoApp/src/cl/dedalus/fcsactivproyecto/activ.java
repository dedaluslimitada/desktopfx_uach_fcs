package cl.dedalus.fcsactivproyecto;

import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;

public class activ{
    private final   SimpleLongProperty id;
    private final   SimpleStringProperty iden_activ;
    private final   SimpleStringProperty fech_activ;
    private final   SimpleStringProperty desc_activ;
    private final   SimpleStringProperty usua_activ;

    public activ(final Long id, final String iden_activ, final String fech_activ, final String desc_activ, final String usua_activ){
        this.id = new SimpleLongProperty(id);
        this.iden_activ = new SimpleStringProperty(iden_activ);
        this.fech_activ = new SimpleStringProperty(fech_activ);
        this.desc_activ = new SimpleStringProperty(desc_activ);
        this.usua_activ = new SimpleStringProperty(usua_activ);
    }

    public Long getId() {
        return id.get();
    }
    public String getIden_activ() {
        return iden_activ.get();
    }
    public String getFech_activ() {
        return fech_activ.get();
    }

    public String getDesc_activ() {
        return desc_activ.get();
    }
    public void setDesc_activ(final String idesc) {
        desc_activ.set(idesc);
    }

    public String getUsua_activ() {
        return usua_activ.get();
    }
}
