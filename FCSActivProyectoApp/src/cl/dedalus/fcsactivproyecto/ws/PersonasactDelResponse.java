
package cl.dedalus.fcsactivproyecto.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para personasactDelResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="personasactDelResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="personasactDelResult" type="{http://ws.fcsactivproyecto.dedalus.cl/}PersonasactDelResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "personasactDelResponse", propOrder = {
    "personasactDelResult"
})
public class PersonasactDelResponse {

    protected PersonasactDelResult personasactDelResult;

    /**
     * Obtiene el valor de la propiedad personasactDelResult.
     * 
     * @return
     *     possible object is
     *     {@link PersonasactDelResult }
     *     
     */
    public PersonasactDelResult getPersonasactDelResult() {
        return personasactDelResult;
    }

    /**
     * Define el valor de la propiedad personasactDelResult.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonasactDelResult }
     *     
     */
    public void setPersonasactDelResult(PersonasactDelResult value) {
        this.personasactDelResult = value;
    }

}
