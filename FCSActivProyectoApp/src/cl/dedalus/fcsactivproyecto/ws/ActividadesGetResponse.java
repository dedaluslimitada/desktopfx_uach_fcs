
package cl.dedalus.fcsactivproyecto.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para actividadesGetResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="actividadesGetResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="actividadesGetResult" type="{http://ws.fcsactivproyecto.dedalus.cl/}ActividadesGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "actividadesGetResponse", propOrder = {
    "actividadesGetResult"
})
public class ActividadesGetResponse {

    protected ActividadesGetResult actividadesGetResult;

    /**
     * Obtiene el valor de la propiedad actividadesGetResult.
     * 
     * @return
     *     possible object is
     *     {@link ActividadesGetResult }
     *     
     */
    public ActividadesGetResult getActividadesGetResult() {
        return actividadesGetResult;
    }

    /**
     * Define el valor de la propiedad actividadesGetResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ActividadesGetResult }
     *     
     */
    public void setActividadesGetResult(ActividadesGetResult value) {
        this.actividadesGetResult = value;
    }

}
