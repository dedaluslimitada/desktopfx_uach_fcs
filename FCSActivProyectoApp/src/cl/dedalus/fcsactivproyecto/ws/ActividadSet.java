
package cl.dedalus.fcsactivproyecto.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para actividadSet complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="actividadSet">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idProyecto" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="idenActiv" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechActiv" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descActiv" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="funcActiv" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "actividadSet", propOrder = {
    "idProyecto",
    "idenActiv",
    "fechActiv",
    "descActiv",
    "funcActiv"
})
public class ActividadSet {

    protected long idProyecto;
    protected String idenActiv;
    protected String fechActiv;
    protected String descActiv;
    protected String funcActiv;

    /**
     * Obtiene el valor de la propiedad idProyecto.
     * 
     */
    public long getIdProyecto() {
        return idProyecto;
    }

    /**
     * Define el valor de la propiedad idProyecto.
     * 
     */
    public void setIdProyecto(long value) {
        this.idProyecto = value;
    }

    /**
     * Obtiene el valor de la propiedad idenActiv.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdenActiv() {
        return idenActiv;
    }

    /**
     * Define el valor de la propiedad idenActiv.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdenActiv(String value) {
        this.idenActiv = value;
    }

    /**
     * Obtiene el valor de la propiedad fechActiv.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechActiv() {
        return fechActiv;
    }

    /**
     * Define el valor de la propiedad fechActiv.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechActiv(String value) {
        this.fechActiv = value;
    }

    /**
     * Obtiene el valor de la propiedad descActiv.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescActiv() {
        return descActiv;
    }

    /**
     * Define el valor de la propiedad descActiv.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescActiv(String value) {
        this.descActiv = value;
    }

    /**
     * Obtiene el valor de la propiedad funcActiv.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFuncActiv() {
        return funcActiv;
    }

    /**
     * Define el valor de la propiedad funcActiv.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFuncActiv(String value) {
        this.funcActiv = value;
    }

}
