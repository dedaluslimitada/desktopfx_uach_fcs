
package cl.dedalus.fcsactivproyecto.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para actividadcongresoSet complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="actividadcongresoSet">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idProyecto" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="idenActiv" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechActiv" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descActiv" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codiFunc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descTitulo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descCiudad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechDesde" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechHasta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "actividadcongresoSet", propOrder = {
    "idProyecto",
    "idenActiv",
    "fechActiv",
    "descActiv",
    "codiFunc",
    "descTitulo",
    "descCiudad",
    "fechDesde",
    "fechHasta"
})
public class ActividadcongresoSet {

    protected long idProyecto;
    protected String idenActiv;
    protected String fechActiv;
    protected String descActiv;
    protected String codiFunc;
    protected String descTitulo;
    protected String descCiudad;
    protected String fechDesde;
    protected String fechHasta;

    /**
     * Obtiene el valor de la propiedad idProyecto.
     * 
     */
    public long getIdProyecto() {
        return idProyecto;
    }

    /**
     * Define el valor de la propiedad idProyecto.
     * 
     */
    public void setIdProyecto(long value) {
        this.idProyecto = value;
    }

    /**
     * Obtiene el valor de la propiedad idenActiv.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdenActiv() {
        return idenActiv;
    }

    /**
     * Define el valor de la propiedad idenActiv.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdenActiv(String value) {
        this.idenActiv = value;
    }

    /**
     * Obtiene el valor de la propiedad fechActiv.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechActiv() {
        return fechActiv;
    }

    /**
     * Define el valor de la propiedad fechActiv.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechActiv(String value) {
        this.fechActiv = value;
    }

    /**
     * Obtiene el valor de la propiedad descActiv.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescActiv() {
        return descActiv;
    }

    /**
     * Define el valor de la propiedad descActiv.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescActiv(String value) {
        this.descActiv = value;
    }

    /**
     * Obtiene el valor de la propiedad codiFunc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiFunc() {
        return codiFunc;
    }

    /**
     * Define el valor de la propiedad codiFunc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiFunc(String value) {
        this.codiFunc = value;
    }

    /**
     * Obtiene el valor de la propiedad descTitulo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescTitulo() {
        return descTitulo;
    }

    /**
     * Define el valor de la propiedad descTitulo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescTitulo(String value) {
        this.descTitulo = value;
    }

    /**
     * Obtiene el valor de la propiedad descCiudad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescCiudad() {
        return descCiudad;
    }

    /**
     * Define el valor de la propiedad descCiudad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescCiudad(String value) {
        this.descCiudad = value;
    }

    /**
     * Obtiene el valor de la propiedad fechDesde.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechDesde() {
        return fechDesde;
    }

    /**
     * Define el valor de la propiedad fechDesde.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechDesde(String value) {
        this.fechDesde = value;
    }

    /**
     * Obtiene el valor de la propiedad fechHasta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechHasta() {
        return fechHasta;
    }

    /**
     * Define el valor de la propiedad fechHasta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechHasta(String value) {
        this.fechHasta = value;
    }

}
