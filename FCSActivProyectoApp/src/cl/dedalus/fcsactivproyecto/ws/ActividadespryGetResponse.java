
package cl.dedalus.fcsactivproyecto.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para actividadespryGetResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="actividadespryGetResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="actividadespryGetResult" type="{http://ws.fcsactivproyecto.dedalus.cl/}ActividadespryGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "actividadespryGetResponse", propOrder = {
    "actividadespryGetResult"
})
public class ActividadespryGetResponse {

    protected ActividadespryGetResult actividadespryGetResult;

    /**
     * Obtiene el valor de la propiedad actividadespryGetResult.
     * 
     * @return
     *     possible object is
     *     {@link ActividadespryGetResult }
     *     
     */
    public ActividadespryGetResult getActividadespryGetResult() {
        return actividadespryGetResult;
    }

    /**
     * Define el valor de la propiedad actividadespryGetResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ActividadespryGetResult }
     *     
     */
    public void setActividadespryGetResult(ActividadespryGetResult value) {
        this.actividadespryGetResult = value;
    }

}
