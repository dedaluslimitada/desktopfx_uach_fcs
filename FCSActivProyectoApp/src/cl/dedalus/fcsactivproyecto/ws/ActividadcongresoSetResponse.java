
package cl.dedalus.fcsactivproyecto.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para actividadcongresoSetResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="actividadcongresoSetResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="actividadcongresoSetResult" type="{http://ws.fcsactivproyecto.dedalus.cl/}ActividadcongresoSetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "actividadcongresoSetResponse", propOrder = {
    "actividadcongresoSetResult"
})
public class ActividadcongresoSetResponse {

    protected ActividadcongresoSetResult actividadcongresoSetResult;

    /**
     * Obtiene el valor de la propiedad actividadcongresoSetResult.
     * 
     * @return
     *     possible object is
     *     {@link ActividadcongresoSetResult }
     *     
     */
    public ActividadcongresoSetResult getActividadcongresoSetResult() {
        return actividadcongresoSetResult;
    }

    /**
     * Define el valor de la propiedad actividadcongresoSetResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ActividadcongresoSetResult }
     *     
     */
    public void setActividadcongresoSetResult(ActividadcongresoSetResult value) {
        this.actividadcongresoSetResult = value;
    }

}
