
package cl.dedalus.fcsactivproyecto.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para personasactGetResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="personasactGetResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="personasactGetResult" type="{http://ws.fcsactivproyecto.dedalus.cl/}PersonasactGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "personasactGetResponse", propOrder = {
    "personasactGetResult"
})
public class PersonasactGetResponse {

    protected PersonasactGetResult personasactGetResult;

    /**
     * Obtiene el valor de la propiedad personasactGetResult.
     * 
     * @return
     *     possible object is
     *     {@link PersonasactGetResult }
     *     
     */
    public PersonasactGetResult getPersonasactGetResult() {
        return personasactGetResult;
    }

    /**
     * Define el valor de la propiedad personasactGetResult.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonasactGetResult }
     *     
     */
    public void setPersonasactGetResult(PersonasactGetResult value) {
        this.personasactGetResult = value;
    }

}
