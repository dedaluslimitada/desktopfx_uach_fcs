
package cl.dedalus.fcsactivproyecto.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ActividadespryGetActiv complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ActividadespryGetActiv">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codiFuncionario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descActividad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechActividad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="idenActividad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ActividadespryGetActiv", propOrder = {
    "codiFuncionario",
    "descActividad",
    "fechActividad",
    "id",
    "idenActividad"
})
public class ActividadespryGetActiv {

    protected String codiFuncionario;
    protected String descActividad;
    protected String fechActividad;
    protected long id;
    protected String idenActividad;

    /**
     * Obtiene el valor de la propiedad codiFuncionario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiFuncionario() {
        return codiFuncionario;
    }

    /**
     * Define el valor de la propiedad codiFuncionario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiFuncionario(String value) {
        this.codiFuncionario = value;
    }

    /**
     * Obtiene el valor de la propiedad descActividad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescActividad() {
        return descActividad;
    }

    /**
     * Define el valor de la propiedad descActividad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescActividad(String value) {
        this.descActividad = value;
    }

    /**
     * Obtiene el valor de la propiedad fechActividad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechActividad() {
        return fechActividad;
    }

    /**
     * Define el valor de la propiedad fechActividad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechActividad(String value) {
        this.fechActividad = value;
    }

    /**
     * Obtiene el valor de la propiedad id.
     * 
     */
    public long getId() {
        return id;
    }

    /**
     * Define el valor de la propiedad id.
     * 
     */
    public void setId(long value) {
        this.id = value;
    }

    /**
     * Obtiene el valor de la propiedad idenActividad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdenActividad() {
        return idenActividad;
    }

    /**
     * Define el valor de la propiedad idenActividad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdenActividad(String value) {
        this.idenActividad = value;
    }

}
