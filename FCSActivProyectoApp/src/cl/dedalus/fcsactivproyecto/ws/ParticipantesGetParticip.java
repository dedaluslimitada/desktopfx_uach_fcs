
package cl.dedalus.fcsactivproyecto.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ParticipantesGetParticip complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ParticipantesGetParticip">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codiParticipa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="nombParticipa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="origParticipa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ParticipantesGetParticip", propOrder = {
    "codiParticipa",
    "id",
    "nombParticipa",
    "origParticipa"
})
public class ParticipantesGetParticip {

    protected String codiParticipa;
    protected long id;
    protected String nombParticipa;
    protected String origParticipa;

    /**
     * Obtiene el valor de la propiedad codiParticipa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiParticipa() {
        return codiParticipa;
    }

    /**
     * Define el valor de la propiedad codiParticipa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiParticipa(String value) {
        this.codiParticipa = value;
    }

    /**
     * Obtiene el valor de la propiedad id.
     * 
     */
    public long getId() {
        return id;
    }

    /**
     * Define el valor de la propiedad id.
     * 
     */
    public void setId(long value) {
        this.id = value;
    }

    /**
     * Obtiene el valor de la propiedad nombParticipa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombParticipa() {
        return nombParticipa;
    }

    /**
     * Define el valor de la propiedad nombParticipa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombParticipa(String value) {
        this.nombParticipa = value;
    }

    /**
     * Obtiene el valor de la propiedad origParticipa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrigParticipa() {
        return origParticipa;
    }

    /**
     * Define el valor de la propiedad origParticipa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrigParticipa(String value) {
        this.origParticipa = value;
    }

}
