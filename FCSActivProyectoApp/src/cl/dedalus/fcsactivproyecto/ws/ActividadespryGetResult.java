
package cl.dedalus.fcsactivproyecto.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ActividadespryGetResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ActividadespryGetResult">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.fcsactivproyecto.dedalus.cl/}ProcedureResult">
 *       &lt;sequence>
 *         &lt;element name="activs" type="{http://ws.fcsactivproyecto.dedalus.cl/}ActividadespryGetActiv" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ActividadespryGetResult", propOrder = {
    "activs"
})
public class ActividadespryGetResult
    extends ProcedureResult
{

    @XmlElement(nillable = true)
    protected List<ActividadespryGetActiv> activs;

    /**
     * Gets the value of the activs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the activs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getActivs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ActividadespryGetActiv }
     * 
     * 
     */
    public List<ActividadespryGetActiv> getActivs() {
        if (activs == null) {
            activs = new ArrayList<ActividadespryGetActiv>();
        }
        return this.activs;
    }

}
