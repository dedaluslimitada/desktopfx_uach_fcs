
package cl.dedalus.fcsactivproyecto.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para documentoGet complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="documentoGet">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idenActividad" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "documentoGet", propOrder = {
    "idenActividad"
})
public class DocumentoGet {

    protected long idenActividad;

    /**
     * Obtiene el valor de la propiedad idenActividad.
     * 
     */
    public long getIdenActividad() {
        return idenActividad;
    }

    /**
     * Define el valor de la propiedad idenActividad.
     * 
     */
    public void setIdenActividad(long value) {
        this.idenActividad = value;
    }

}
