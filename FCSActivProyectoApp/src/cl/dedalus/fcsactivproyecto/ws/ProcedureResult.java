
package cl.dedalus.fcsactivproyecto.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ProcedureResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ProcedureResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProcedureResult")
@XmlSeeAlso({
    ActividadcongresoSetResult.class,
    DocumentoGetResult.class,
    PersonasactSetResult.class,
    ActividadespryGetResult.class,
    CongresoGetResult.class,
    ParticipantesGetResult.class,
    ActividadSetResult.class,
    PersonasactGetResult.class,
    ProyectosGetResult.class,
    RevistasGetResult.class,
    PersonasactDelResult.class,
    ActividadesGetResult.class,
    DocactividadSetResult.class
})
public abstract class ProcedureResult {


}
