
package cl.dedalus.fcsactivproyecto.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para CongresoGetResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="CongresoGetResult">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.fcsactivproyecto.dedalus.cl/}ProcedureResult">
 *       &lt;sequence>
 *         &lt;element name="descCiudad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descTitulo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechDesde" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechHasta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CongresoGetResult", propOrder = {
    "descCiudad",
    "descTitulo",
    "fechDesde",
    "fechHasta"
})
public class CongresoGetResult
    extends ProcedureResult
{

    protected String descCiudad;
    protected String descTitulo;
    protected String fechDesde;
    protected String fechHasta;

    /**
     * Obtiene el valor de la propiedad descCiudad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescCiudad() {
        return descCiudad;
    }

    /**
     * Define el valor de la propiedad descCiudad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescCiudad(String value) {
        this.descCiudad = value;
    }

    /**
     * Obtiene el valor de la propiedad descTitulo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescTitulo() {
        return descTitulo;
    }

    /**
     * Define el valor de la propiedad descTitulo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescTitulo(String value) {
        this.descTitulo = value;
    }

    /**
     * Obtiene el valor de la propiedad fechDesde.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechDesde() {
        return fechDesde;
    }

    /**
     * Define el valor de la propiedad fechDesde.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechDesde(String value) {
        this.fechDesde = value;
    }

    /**
     * Obtiene el valor de la propiedad fechHasta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechHasta() {
        return fechHasta;
    }

    /**
     * Define el valor de la propiedad fechHasta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechHasta(String value) {
        this.fechHasta = value;
    }

}
