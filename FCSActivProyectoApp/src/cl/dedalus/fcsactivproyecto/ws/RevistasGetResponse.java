
package cl.dedalus.fcsactivproyecto.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para revistasGetResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="revistasGetResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="revistasGetResult" type="{http://ws.fcsactivproyecto.dedalus.cl/}RevistasGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "revistasGetResponse", propOrder = {
    "revistasGetResult"
})
public class RevistasGetResponse {

    protected RevistasGetResult revistasGetResult;

    /**
     * Obtiene el valor de la propiedad revistasGetResult.
     * 
     * @return
     *     possible object is
     *     {@link RevistasGetResult }
     *     
     */
    public RevistasGetResult getRevistasGetResult() {
        return revistasGetResult;
    }

    /**
     * Define el valor de la propiedad revistasGetResult.
     * 
     * @param value
     *     allowed object is
     *     {@link RevistasGetResult }
     *     
     */
    public void setRevistasGetResult(RevistasGetResult value) {
        this.revistasGetResult = value;
    }

}
