
package cl.dedalus.fcsactivproyecto.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cl.dedalus.fcsactivproyecto.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CongresoGetResponse_QNAME = new QName("http://ws.fcsactivproyecto.dedalus.cl/", "congresoGetResponse");
    private final static QName _ProyectosGet_QNAME = new QName("http://ws.fcsactivproyecto.dedalus.cl/", "proyectosGet");
    private final static QName _PersonasactSet_QNAME = new QName("http://ws.fcsactivproyecto.dedalus.cl/", "personasactSet");
    private final static QName _ActividadSetResponse_QNAME = new QName("http://ws.fcsactivproyecto.dedalus.cl/", "actividadSetResponse");
    private final static QName _ParticipantesGetResponse_QNAME = new QName("http://ws.fcsactivproyecto.dedalus.cl/", "participantesGetResponse");
    private final static QName _FaultInfo_QNAME = new QName("http://ws.fcsactivproyecto.dedalus.cl/", "faultInfo");
    private final static QName _ActividadespryGetResponse_QNAME = new QName("http://ws.fcsactivproyecto.dedalus.cl/", "actividadespryGetResponse");
    private final static QName _DocactividadSet_QNAME = new QName("http://ws.fcsactivproyecto.dedalus.cl/", "docactividadSet");
    private final static QName _PersonasactGet_QNAME = new QName("http://ws.fcsactivproyecto.dedalus.cl/", "personasactGet");
    private final static QName _ActividadSet_QNAME = new QName("http://ws.fcsactivproyecto.dedalus.cl/", "actividadSet");
    private final static QName _PersonasactDel_QNAME = new QName("http://ws.fcsactivproyecto.dedalus.cl/", "personasactDel");
    private final static QName _ParticipantesGet_QNAME = new QName("http://ws.fcsactivproyecto.dedalus.cl/", "participantesGet");
    private final static QName _DocumentoGetResponse_QNAME = new QName("http://ws.fcsactivproyecto.dedalus.cl/", "documentoGetResponse");
    private final static QName _ActividadesGet_QNAME = new QName("http://ws.fcsactivproyecto.dedalus.cl/", "actividadesGet");
    private final static QName _ActividadesGetResponse_QNAME = new QName("http://ws.fcsactivproyecto.dedalus.cl/", "actividadesGetResponse");
    private final static QName _ActividadcongresoSet_QNAME = new QName("http://ws.fcsactivproyecto.dedalus.cl/", "actividadcongresoSet");
    private final static QName _PersonasactDelResponse_QNAME = new QName("http://ws.fcsactivproyecto.dedalus.cl/", "personasactDelResponse");
    private final static QName _DocactividadSetResponse_QNAME = new QName("http://ws.fcsactivproyecto.dedalus.cl/", "docactividadSetResponse");
    private final static QName _PersonasactSetResponse_QNAME = new QName("http://ws.fcsactivproyecto.dedalus.cl/", "personasactSetResponse");
    private final static QName _CongresoGet_QNAME = new QName("http://ws.fcsactivproyecto.dedalus.cl/", "congresoGet");
    private final static QName _RevistasGetResponse_QNAME = new QName("http://ws.fcsactivproyecto.dedalus.cl/", "revistasGetResponse");
    private final static QName _PersonasactGetResponse_QNAME = new QName("http://ws.fcsactivproyecto.dedalus.cl/", "personasactGetResponse");
    private final static QName _ActividadespryGet_QNAME = new QName("http://ws.fcsactivproyecto.dedalus.cl/", "actividadespryGet");
    private final static QName _ProyectosGetResponse_QNAME = new QName("http://ws.fcsactivproyecto.dedalus.cl/", "proyectosGetResponse");
    private final static QName _RevistasGet_QNAME = new QName("http://ws.fcsactivproyecto.dedalus.cl/", "revistasGet");
    private final static QName _DocumentoGet_QNAME = new QName("http://ws.fcsactivproyecto.dedalus.cl/", "documentoGet");
    private final static QName _ActividadcongresoSetResponse_QNAME = new QName("http://ws.fcsactivproyecto.dedalus.cl/", "actividadcongresoSetResponse");
    private final static QName _DocactividadSetContDocumento_QNAME = new QName("http://ws.fcsactivproyecto.dedalus.cl/", "contDocumento");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cl.dedalus.fcsactivproyecto.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CongresoGetResponse }
     * 
     */
    public CongresoGetResponse createCongresoGetResponse() {
        return new CongresoGetResponse();
    }

    /**
     * Create an instance of {@link PersonasactSet }
     * 
     */
    public PersonasactSet createPersonasactSet() {
        return new PersonasactSet();
    }

    /**
     * Create an instance of {@link ProyectosGet }
     * 
     */
    public ProyectosGet createProyectosGet() {
        return new ProyectosGet();
    }

    /**
     * Create an instance of {@link ActividadSetResponse }
     * 
     */
    public ActividadSetResponse createActividadSetResponse() {
        return new ActividadSetResponse();
    }

    /**
     * Create an instance of {@link ParticipantesGetResponse }
     * 
     */
    public ParticipantesGetResponse createParticipantesGetResponse() {
        return new ParticipantesGetResponse();
    }

    /**
     * Create an instance of {@link PersonasactGet }
     * 
     */
    public PersonasactGet createPersonasactGet() {
        return new PersonasactGet();
    }

    /**
     * Create an instance of {@link DocactividadSet }
     * 
     */
    public DocactividadSet createDocactividadSet() {
        return new DocactividadSet();
    }

    /**
     * Create an instance of {@link ActividadespryGetResponse }
     * 
     */
    public ActividadespryGetResponse createActividadespryGetResponse() {
        return new ActividadespryGetResponse();
    }

    /**
     * Create an instance of {@link ActividadSet }
     * 
     */
    public ActividadSet createActividadSet() {
        return new ActividadSet();
    }

    /**
     * Create an instance of {@link DocumentoGetResponse }
     * 
     */
    public DocumentoGetResponse createDocumentoGetResponse() {
        return new DocumentoGetResponse();
    }

    /**
     * Create an instance of {@link ParticipantesGet }
     * 
     */
    public ParticipantesGet createParticipantesGet() {
        return new ParticipantesGet();
    }

    /**
     * Create an instance of {@link PersonasactDel }
     * 
     */
    public PersonasactDel createPersonasactDel() {
        return new PersonasactDel();
    }

    /**
     * Create an instance of {@link ActividadesGetResponse }
     * 
     */
    public ActividadesGetResponse createActividadesGetResponse() {
        return new ActividadesGetResponse();
    }

    /**
     * Create an instance of {@link ActividadesGet }
     * 
     */
    public ActividadesGet createActividadesGet() {
        return new ActividadesGet();
    }

    /**
     * Create an instance of {@link DocactividadSetResponse }
     * 
     */
    public DocactividadSetResponse createDocactividadSetResponse() {
        return new DocactividadSetResponse();
    }

    /**
     * Create an instance of {@link PersonasactDelResponse }
     * 
     */
    public PersonasactDelResponse createPersonasactDelResponse() {
        return new PersonasactDelResponse();
    }

    /**
     * Create an instance of {@link ActividadcongresoSet }
     * 
     */
    public ActividadcongresoSet createActividadcongresoSet() {
        return new ActividadcongresoSet();
    }

    /**
     * Create an instance of {@link PersonasactSetResponse }
     * 
     */
    public PersonasactSetResponse createPersonasactSetResponse() {
        return new PersonasactSetResponse();
    }

    /**
     * Create an instance of {@link CongresoGet }
     * 
     */
    public CongresoGet createCongresoGet() {
        return new CongresoGet();
    }

    /**
     * Create an instance of {@link PersonasactGetResponse }
     * 
     */
    public PersonasactGetResponse createPersonasactGetResponse() {
        return new PersonasactGetResponse();
    }

    /**
     * Create an instance of {@link RevistasGetResponse }
     * 
     */
    public RevistasGetResponse createRevistasGetResponse() {
        return new RevistasGetResponse();
    }

    /**
     * Create an instance of {@link ActividadespryGet }
     * 
     */
    public ActividadespryGet createActividadespryGet() {
        return new ActividadespryGet();
    }

    /**
     * Create an instance of {@link ActividadcongresoSetResponse }
     * 
     */
    public ActividadcongresoSetResponse createActividadcongresoSetResponse() {
        return new ActividadcongresoSetResponse();
    }

    /**
     * Create an instance of {@link DocumentoGet }
     * 
     */
    public DocumentoGet createDocumentoGet() {
        return new DocumentoGet();
    }

    /**
     * Create an instance of {@link ProyectosGetResponse }
     * 
     */
    public ProyectosGetResponse createProyectosGetResponse() {
        return new ProyectosGetResponse();
    }

    /**
     * Create an instance of {@link RevistasGet }
     * 
     */
    public RevistasGet createRevistasGet() {
        return new RevistasGet();
    }

    /**
     * Create an instance of {@link ActividadcongresoSetResult }
     * 
     */
    public ActividadcongresoSetResult createActividadcongresoSetResult() {
        return new ActividadcongresoSetResult();
    }

    /**
     * Create an instance of {@link DocumentoGetResult }
     * 
     */
    public DocumentoGetResult createDocumentoGetResult() {
        return new DocumentoGetResult();
    }

    /**
     * Create an instance of {@link PersonasactSetResult }
     * 
     */
    public PersonasactSetResult createPersonasactSetResult() {
        return new PersonasactSetResult();
    }

    /**
     * Create an instance of {@link RevistasGetRevista }
     * 
     */
    public RevistasGetRevista createRevistasGetRevista() {
        return new RevistasGetRevista();
    }

    /**
     * Create an instance of {@link ActividadespryGetResult }
     * 
     */
    public ActividadespryGetResult createActividadespryGetResult() {
        return new ActividadespryGetResult();
    }

    /**
     * Create an instance of {@link CongresoGetResult }
     * 
     */
    public CongresoGetResult createCongresoGetResult() {
        return new CongresoGetResult();
    }

    /**
     * Create an instance of {@link ParticipantesGetResult }
     * 
     */
    public ParticipantesGetResult createParticipantesGetResult() {
        return new ParticipantesGetResult();
    }

    /**
     * Create an instance of {@link ActividadSetResult }
     * 
     */
    public ActividadSetResult createActividadSetResult() {
        return new ActividadSetResult();
    }

    /**
     * Create an instance of {@link ParticipantesGetParticip }
     * 
     */
    public ParticipantesGetParticip createParticipantesGetParticip() {
        return new ParticipantesGetParticip();
    }

    /**
     * Create an instance of {@link PersonasactGetResult }
     * 
     */
    public PersonasactGetResult createPersonasactGetResult() {
        return new PersonasactGetResult();
    }

    /**
     * Create an instance of {@link ProyectosGetResult }
     * 
     */
    public ProyectosGetResult createProyectosGetResult() {
        return new ProyectosGetResult();
    }

    /**
     * Create an instance of {@link RevistasGetResult }
     * 
     */
    public RevistasGetResult createRevistasGetResult() {
        return new RevistasGetResult();
    }

    /**
     * Create an instance of {@link PersonasactDelResult }
     * 
     */
    public PersonasactDelResult createPersonasactDelResult() {
        return new PersonasactDelResult();
    }

    /**
     * Create an instance of {@link ActividadespryGetActiv }
     * 
     */
    public ActividadespryGetActiv createActividadespryGetActiv() {
        return new ActividadespryGetActiv();
    }

    /**
     * Create an instance of {@link ActividadesGetActiv }
     * 
     */
    public ActividadesGetActiv createActividadesGetActiv() {
        return new ActividadesGetActiv();
    }

    /**
     * Create an instance of {@link ActividadesGetResult }
     * 
     */
    public ActividadesGetResult createActividadesGetResult() {
        return new ActividadesGetResult();
    }

    /**
     * Create an instance of {@link PersonasactGetPersona }
     * 
     */
    public PersonasactGetPersona createPersonasactGetPersona() {
        return new PersonasactGetPersona();
    }

    /**
     * Create an instance of {@link ProyectosGetPry }
     * 
     */
    public ProyectosGetPry createProyectosGetPry() {
        return new ProyectosGetPry();
    }

    /**
     * Create an instance of {@link DocactividadSetResult }
     * 
     */
    public DocactividadSetResult createDocactividadSetResult() {
        return new DocactividadSetResult();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CongresoGetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsactivproyecto.dedalus.cl/", name = "congresoGetResponse")
    public JAXBElement<CongresoGetResponse> createCongresoGetResponse(CongresoGetResponse value) {
        return new JAXBElement<CongresoGetResponse>(_CongresoGetResponse_QNAME, CongresoGetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProyectosGet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsactivproyecto.dedalus.cl/", name = "proyectosGet")
    public JAXBElement<ProyectosGet> createProyectosGet(ProyectosGet value) {
        return new JAXBElement<ProyectosGet>(_ProyectosGet_QNAME, ProyectosGet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PersonasactSet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsactivproyecto.dedalus.cl/", name = "personasactSet")
    public JAXBElement<PersonasactSet> createPersonasactSet(PersonasactSet value) {
        return new JAXBElement<PersonasactSet>(_PersonasactSet_QNAME, PersonasactSet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActividadSetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsactivproyecto.dedalus.cl/", name = "actividadSetResponse")
    public JAXBElement<ActividadSetResponse> createActividadSetResponse(ActividadSetResponse value) {
        return new JAXBElement<ActividadSetResponse>(_ActividadSetResponse_QNAME, ActividadSetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ParticipantesGetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsactivproyecto.dedalus.cl/", name = "participantesGetResponse")
    public JAXBElement<ParticipantesGetResponse> createParticipantesGetResponse(ParticipantesGetResponse value) {
        return new JAXBElement<ParticipantesGetResponse>(_ParticipantesGetResponse_QNAME, ParticipantesGetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsactivproyecto.dedalus.cl/", name = "faultInfo")
    public JAXBElement<String> createFaultInfo(String value) {
        return new JAXBElement<String>(_FaultInfo_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActividadespryGetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsactivproyecto.dedalus.cl/", name = "actividadespryGetResponse")
    public JAXBElement<ActividadespryGetResponse> createActividadespryGetResponse(ActividadespryGetResponse value) {
        return new JAXBElement<ActividadespryGetResponse>(_ActividadespryGetResponse_QNAME, ActividadespryGetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocactividadSet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsactivproyecto.dedalus.cl/", name = "docactividadSet")
    public JAXBElement<DocactividadSet> createDocactividadSet(DocactividadSet value) {
        return new JAXBElement<DocactividadSet>(_DocactividadSet_QNAME, DocactividadSet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PersonasactGet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsactivproyecto.dedalus.cl/", name = "personasactGet")
    public JAXBElement<PersonasactGet> createPersonasactGet(PersonasactGet value) {
        return new JAXBElement<PersonasactGet>(_PersonasactGet_QNAME, PersonasactGet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActividadSet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsactivproyecto.dedalus.cl/", name = "actividadSet")
    public JAXBElement<ActividadSet> createActividadSet(ActividadSet value) {
        return new JAXBElement<ActividadSet>(_ActividadSet_QNAME, ActividadSet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PersonasactDel }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsactivproyecto.dedalus.cl/", name = "personasactDel")
    public JAXBElement<PersonasactDel> createPersonasactDel(PersonasactDel value) {
        return new JAXBElement<PersonasactDel>(_PersonasactDel_QNAME, PersonasactDel.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ParticipantesGet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsactivproyecto.dedalus.cl/", name = "participantesGet")
    public JAXBElement<ParticipantesGet> createParticipantesGet(ParticipantesGet value) {
        return new JAXBElement<ParticipantesGet>(_ParticipantesGet_QNAME, ParticipantesGet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocumentoGetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsactivproyecto.dedalus.cl/", name = "documentoGetResponse")
    public JAXBElement<DocumentoGetResponse> createDocumentoGetResponse(DocumentoGetResponse value) {
        return new JAXBElement<DocumentoGetResponse>(_DocumentoGetResponse_QNAME, DocumentoGetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActividadesGet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsactivproyecto.dedalus.cl/", name = "actividadesGet")
    public JAXBElement<ActividadesGet> createActividadesGet(ActividadesGet value) {
        return new JAXBElement<ActividadesGet>(_ActividadesGet_QNAME, ActividadesGet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActividadesGetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsactivproyecto.dedalus.cl/", name = "actividadesGetResponse")
    public JAXBElement<ActividadesGetResponse> createActividadesGetResponse(ActividadesGetResponse value) {
        return new JAXBElement<ActividadesGetResponse>(_ActividadesGetResponse_QNAME, ActividadesGetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActividadcongresoSet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsactivproyecto.dedalus.cl/", name = "actividadcongresoSet")
    public JAXBElement<ActividadcongresoSet> createActividadcongresoSet(ActividadcongresoSet value) {
        return new JAXBElement<ActividadcongresoSet>(_ActividadcongresoSet_QNAME, ActividadcongresoSet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PersonasactDelResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsactivproyecto.dedalus.cl/", name = "personasactDelResponse")
    public JAXBElement<PersonasactDelResponse> createPersonasactDelResponse(PersonasactDelResponse value) {
        return new JAXBElement<PersonasactDelResponse>(_PersonasactDelResponse_QNAME, PersonasactDelResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocactividadSetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsactivproyecto.dedalus.cl/", name = "docactividadSetResponse")
    public JAXBElement<DocactividadSetResponse> createDocactividadSetResponse(DocactividadSetResponse value) {
        return new JAXBElement<DocactividadSetResponse>(_DocactividadSetResponse_QNAME, DocactividadSetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PersonasactSetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsactivproyecto.dedalus.cl/", name = "personasactSetResponse")
    public JAXBElement<PersonasactSetResponse> createPersonasactSetResponse(PersonasactSetResponse value) {
        return new JAXBElement<PersonasactSetResponse>(_PersonasactSetResponse_QNAME, PersonasactSetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CongresoGet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsactivproyecto.dedalus.cl/", name = "congresoGet")
    public JAXBElement<CongresoGet> createCongresoGet(CongresoGet value) {
        return new JAXBElement<CongresoGet>(_CongresoGet_QNAME, CongresoGet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RevistasGetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsactivproyecto.dedalus.cl/", name = "revistasGetResponse")
    public JAXBElement<RevistasGetResponse> createRevistasGetResponse(RevistasGetResponse value) {
        return new JAXBElement<RevistasGetResponse>(_RevistasGetResponse_QNAME, RevistasGetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PersonasactGetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsactivproyecto.dedalus.cl/", name = "personasactGetResponse")
    public JAXBElement<PersonasactGetResponse> createPersonasactGetResponse(PersonasactGetResponse value) {
        return new JAXBElement<PersonasactGetResponse>(_PersonasactGetResponse_QNAME, PersonasactGetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActividadespryGet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsactivproyecto.dedalus.cl/", name = "actividadespryGet")
    public JAXBElement<ActividadespryGet> createActividadespryGet(ActividadespryGet value) {
        return new JAXBElement<ActividadespryGet>(_ActividadespryGet_QNAME, ActividadespryGet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProyectosGetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsactivproyecto.dedalus.cl/", name = "proyectosGetResponse")
    public JAXBElement<ProyectosGetResponse> createProyectosGetResponse(ProyectosGetResponse value) {
        return new JAXBElement<ProyectosGetResponse>(_ProyectosGetResponse_QNAME, ProyectosGetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RevistasGet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsactivproyecto.dedalus.cl/", name = "revistasGet")
    public JAXBElement<RevistasGet> createRevistasGet(RevistasGet value) {
        return new JAXBElement<RevistasGet>(_RevistasGet_QNAME, RevistasGet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocumentoGet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsactivproyecto.dedalus.cl/", name = "documentoGet")
    public JAXBElement<DocumentoGet> createDocumentoGet(DocumentoGet value) {
        return new JAXBElement<DocumentoGet>(_DocumentoGet_QNAME, DocumentoGet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActividadcongresoSetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsactivproyecto.dedalus.cl/", name = "actividadcongresoSetResponse")
    public JAXBElement<ActividadcongresoSetResponse> createActividadcongresoSetResponse(ActividadcongresoSetResponse value) {
        return new JAXBElement<ActividadcongresoSetResponse>(_ActividadcongresoSetResponse_QNAME, ActividadcongresoSetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsactivproyecto.dedalus.cl/", name = "contDocumento", scope = DocactividadSet.class)
    public JAXBElement<byte[]> createDocactividadSetContDocumento(byte[] value) {
        return new JAXBElement<byte[]>(_DocactividadSetContDocumento_QNAME, byte[].class, DocactividadSet.class, ((byte[]) value));
    }

}
