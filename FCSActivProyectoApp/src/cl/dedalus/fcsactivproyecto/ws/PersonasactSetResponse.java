
package cl.dedalus.fcsactivproyecto.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para personasactSetResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="personasactSetResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="personasactSetResult" type="{http://ws.fcsactivproyecto.dedalus.cl/}PersonasactSetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "personasactSetResponse", propOrder = {
    "personasactSetResult"
})
public class PersonasactSetResponse {

    protected PersonasactSetResult personasactSetResult;

    /**
     * Obtiene el valor de la propiedad personasactSetResult.
     * 
     * @return
     *     possible object is
     *     {@link PersonasactSetResult }
     *     
     */
    public PersonasactSetResult getPersonasactSetResult() {
        return personasactSetResult;
    }

    /**
     * Define el valor de la propiedad personasactSetResult.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonasactSetResult }
     *     
     */
    public void setPersonasactSetResult(PersonasactSetResult value) {
        this.personasactSetResult = value;
    }

}
