
package cl.dedalus.fcsactivproyecto.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para congresoGetResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="congresoGetResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="congresoGetResult" type="{http://ws.fcsactivproyecto.dedalus.cl/}CongresoGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "congresoGetResponse", propOrder = {
    "congresoGetResult"
})
public class CongresoGetResponse {

    protected CongresoGetResult congresoGetResult;

    /**
     * Obtiene el valor de la propiedad congresoGetResult.
     * 
     * @return
     *     possible object is
     *     {@link CongresoGetResult }
     *     
     */
    public CongresoGetResult getCongresoGetResult() {
        return congresoGetResult;
    }

    /**
     * Define el valor de la propiedad congresoGetResult.
     * 
     * @param value
     *     allowed object is
     *     {@link CongresoGetResult }
     *     
     */
    public void setCongresoGetResult(CongresoGetResult value) {
        this.congresoGetResult = value;
    }

}
