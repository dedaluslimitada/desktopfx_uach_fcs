
package cl.dedalus.fcsactivproyecto.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para congresoGet complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="congresoGet">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idActiv" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "congresoGet", propOrder = {
    "idActiv"
})
public class CongresoGet {

    protected long idActiv;

    /**
     * Obtiene el valor de la propiedad idActiv.
     * 
     */
    public long getIdActiv() {
        return idActiv;
    }

    /**
     * Define el valor de la propiedad idActiv.
     * 
     */
    public void setIdActiv(long value) {
        this.idActiv = value;
    }

}
