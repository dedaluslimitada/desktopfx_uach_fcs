
package cl.dedalus.fcsactivproyecto.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ParticipantesGetResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ParticipantesGetResult">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.fcsactivproyecto.dedalus.cl/}ProcedureResult">
 *       &lt;sequence>
 *         &lt;element name="particips" type="{http://ws.fcsactivproyecto.dedalus.cl/}ParticipantesGetParticip" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ParticipantesGetResult", propOrder = {
    "particips"
})
public class ParticipantesGetResult
    extends ProcedureResult
{

    @XmlElement(nillable = true)
    protected List<ParticipantesGetParticip> particips;

    /**
     * Gets the value of the particips property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the particips property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getParticips().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ParticipantesGetParticip }
     * 
     * 
     */
    public List<ParticipantesGetParticip> getParticips() {
        if (particips == null) {
            particips = new ArrayList<ParticipantesGetParticip>();
        }
        return this.particips;
    }

}
