
package cl.dedalus.fcsactivproyecto.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para RevistasGetRevista complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="RevistasGetRevista">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="descRevista" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RevistasGetRevista", propOrder = {
    "descRevista"
})
public class RevistasGetRevista {

    protected String descRevista;

    /**
     * Obtiene el valor de la propiedad descRevista.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescRevista() {
        return descRevista;
    }

    /**
     * Define el valor de la propiedad descRevista.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescRevista(String value) {
        this.descRevista = value;
    }

}
