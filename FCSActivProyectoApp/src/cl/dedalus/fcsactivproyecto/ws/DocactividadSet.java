
package cl.dedalus.fcsactivproyecto.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para docactividadSet complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="docactividadSet">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idProyecto" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="idenActiv" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idenActivComp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechActiv" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descActiv" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codiFunc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descTitulo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descAutor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nombDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="contDocumento" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="idenRevista" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "docactividadSet", propOrder = {
    "idProyecto",
    "idenActiv",
    "idenActivComp",
    "fechActiv",
    "descActiv",
    "codiFunc",
    "descTitulo",
    "descAutor",
    "fechDocumento",
    "nombDocumento",
    "contDocumento",
    "idenRevista"
})
public class DocactividadSet {

    protected long idProyecto;
    protected String idenActiv;
    protected String idenActivComp;
    protected String fechActiv;
    protected String descActiv;
    protected String codiFunc;
    protected String descTitulo;
    protected String descAutor;
    protected String fechDocumento;
    protected String nombDocumento;
    @XmlElementRef(name = "contDocumento", namespace = "http://ws.fcsactivproyecto.dedalus.cl/", type = JAXBElement.class, required = false)
    protected JAXBElement<byte[]> contDocumento;
    protected String idenRevista;

    /**
     * Obtiene el valor de la propiedad idProyecto.
     * 
     */
    public long getIdProyecto() {
        return idProyecto;
    }

    /**
     * Define el valor de la propiedad idProyecto.
     * 
     */
    public void setIdProyecto(long value) {
        this.idProyecto = value;
    }

    /**
     * Obtiene el valor de la propiedad idenActiv.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdenActiv() {
        return idenActiv;
    }

    /**
     * Define el valor de la propiedad idenActiv.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdenActiv(String value) {
        this.idenActiv = value;
    }

    /**
     * Obtiene el valor de la propiedad idenActivComp.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdenActivComp() {
        return idenActivComp;
    }

    /**
     * Define el valor de la propiedad idenActivComp.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdenActivComp(String value) {
        this.idenActivComp = value;
    }

    /**
     * Obtiene el valor de la propiedad fechActiv.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechActiv() {
        return fechActiv;
    }

    /**
     * Define el valor de la propiedad fechActiv.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechActiv(String value) {
        this.fechActiv = value;
    }

    /**
     * Obtiene el valor de la propiedad descActiv.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescActiv() {
        return descActiv;
    }

    /**
     * Define el valor de la propiedad descActiv.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescActiv(String value) {
        this.descActiv = value;
    }

    /**
     * Obtiene el valor de la propiedad codiFunc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiFunc() {
        return codiFunc;
    }

    /**
     * Define el valor de la propiedad codiFunc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiFunc(String value) {
        this.codiFunc = value;
    }

    /**
     * Obtiene el valor de la propiedad descTitulo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescTitulo() {
        return descTitulo;
    }

    /**
     * Define el valor de la propiedad descTitulo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescTitulo(String value) {
        this.descTitulo = value;
    }

    /**
     * Obtiene el valor de la propiedad descAutor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescAutor() {
        return descAutor;
    }

    /**
     * Define el valor de la propiedad descAutor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescAutor(String value) {
        this.descAutor = value;
    }

    /**
     * Obtiene el valor de la propiedad fechDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechDocumento() {
        return fechDocumento;
    }

    /**
     * Define el valor de la propiedad fechDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechDocumento(String value) {
        this.fechDocumento = value;
    }

    /**
     * Obtiene el valor de la propiedad nombDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombDocumento() {
        return nombDocumento;
    }

    /**
     * Define el valor de la propiedad nombDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombDocumento(String value) {
        this.nombDocumento = value;
    }

    /**
     * Obtiene el valor de la propiedad contDocumento.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     *     
     */
    public JAXBElement<byte[]> getContDocumento() {
        return contDocumento;
    }

    /**
     * Define el valor de la propiedad contDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     *     
     */
    public void setContDocumento(JAXBElement<byte[]> value) {
        this.contDocumento = value;
    }

    /**
     * Obtiene el valor de la propiedad idenRevista.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdenRevista() {
        return idenRevista;
    }

    /**
     * Define el valor de la propiedad idenRevista.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdenRevista(String value) {
        this.idenRevista = value;
    }

}
