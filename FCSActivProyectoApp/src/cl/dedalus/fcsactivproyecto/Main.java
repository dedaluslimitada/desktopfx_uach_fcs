package cl.dedalus.fcsactivproyecto;

import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.time.LocalDate;
import cl.dedalus.fcsactivproyecto.ws.ActividadesGetActiv;
import cl.dedalus.fcsactivproyecto.ws.ActividadesGetResult;
import cl.dedalus.fcsactivproyecto.ws.ActividadespryGetActiv;
import cl.dedalus.fcsactivproyecto.ws.ActividadespryGetResult;
import cl.dedalus.fcsactivproyecto.ws.CongresoGetResult;
import cl.dedalus.fcsactivproyecto.ws.DocumentoGetResult;
import cl.dedalus.fcsactivproyecto.ws.FcsactivproyectoWeb;
import cl.dedalus.fcsactivproyecto.ws.FcsactivproyectoWebException;
import cl.dedalus.fcsactivproyecto.ws.FcsactivproyectoWebService;
import cl.dedalus.fcsactivproyecto.ws.ParticipantesGetParticip;
import cl.dedalus.fcsactivproyecto.ws.ParticipantesGetResult;
import cl.dedalus.fcsactivproyecto.ws.PersonasactGetPersona;
import cl.dedalus.fcsactivproyecto.ws.PersonasactGetResult;
import cl.dedalus.fcsactivproyecto.ws.ProyectosGetPry;
import cl.dedalus.fcsactivproyecto.ws.ProyectosGetResult;
import cl.obcom.desktopfx.core.DesktopTask;
import cl.obcom.desktopfx.jfx.Dialog;
import cl.obcom.desktopfx.jfx.DialogAction;
import cl.obcom.desktopfx.jfx.FxmlPane;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class Main extends FxmlPane
{
    @FXML private Button bo_salir;
    @FXML private Button bo_mas;
    @FXML private Button bo_menos;
    @FXML private Button bo_ingresar;
    @FXML private Button bo_modificar;
    @FXML private Button bo_cierre;
    @FXML private ComboBox<String> cb_actividad;
    @FXML TableView<proyectos> tb_proyectos = new TableView<>();
    @FXML TableView<participa> tb_participa = new TableView<>();
    @FXML private DatePicker dp_fecha_activ;
    @FXML TreeTableView<activ> tb3_actividadespry = new TreeTableView<>();
    @FXML private Button bo_ins_part;
    @FXML private Label lb_prys;
    @FXML private Label lb_activ;

    final FcsactivproyectoWeb port;

    final ObservableList<proyectos> dataProyecto = FXCollections.observableArrayList ();
    final ObservableList<participa> dataParticipa = FXCollections.observableArrayList ();
    final ObservableList<String> dataActividad = FXCollections.observableArrayList ();
    final TreeItem<activ> root =
        new TreeItem<>(new activ(-1L,"Iden","Fech","Desc", "Usu"));

    public Main(final DesktopTask task) throws Exception
    {
        // Se carga fxml
        loadFxml();
        task.getStage().getScene().getStylesheets().add("resources/css/NuevoEstilo.css");

        //Inicializar WS
        final FcsactivproyectoWebService service = new FcsactivproyectoWebService();
        port = service.getFcsactivproyectoWebPort();
        task.initWebServicePort(port,service,task.getCodeBase());

        //Init Grilla tb_proyectos
        final TableColumn<proyectos, String> tc_codigo = new TableColumn<>("Codigo");
        tc_codigo.setCellValueFactory(new PropertyValueFactory<proyectos, String>("codigo"));
        tb_proyectos.getColumns().add(tc_codigo);
        final TableColumn<proyectos, String> tc_titulo = new TableColumn<>("Titulo");
        tc_titulo.setCellValueFactory(new PropertyValueFactory<proyectos, String>("titulo"));
        tb_proyectos.getColumns().add(tc_titulo);
        final TableColumn<proyectos, String> tc_ini_f = new TableColumn<>("F.ini F");
        tc_ini_f.setCellValueFactory(new PropertyValueFactory<proyectos, String>("fech_ini_f"));
        tb_proyectos.getColumns().add(tc_ini_f);
        final TableColumn<proyectos, String> tc_fin_f = new TableColumn<>("F.fin F");
        tc_fin_f.setCellValueFactory(new PropertyValueFactory<proyectos, String>("fech_fin_f"));
        tb_proyectos.getColumns().add(tc_fin_f);
        final TableColumn<proyectos, String> tc_ini_e = new TableColumn<>("F.ini E");
        tc_ini_e.setCellValueFactory(new PropertyValueFactory<proyectos, String>("fech_ini_e"));
        tb_proyectos.getColumns().add(tc_ini_e);
        final TableColumn<proyectos, String> tc_fin_e = new TableColumn<>("F.fin E");
        tc_fin_e.setCellValueFactory(new PropertyValueFactory<proyectos, String>("fech_fin_e"));
        tb_proyectos.getColumns().add(tc_fin_e);
        final TableColumn<proyectos, String> tc_estado = new TableColumn<>("Estado");
        tc_estado.setCellValueFactory(new PropertyValueFactory<proyectos, String>("estado"));
        tb_proyectos.getColumns().add(tc_estado);
        tb_proyectos.setItems(dataProyecto);
        tc_codigo.prefWidthProperty().bind(tb_proyectos.widthProperty().multiply(0.15));
        tc_titulo.prefWidthProperty().bind(tb_proyectos.widthProperty().multiply(0.30));
        tc_ini_f.prefWidthProperty().bind(tb_proyectos.widthProperty().multiply(0.10));
        tc_fin_f.prefWidthProperty().bind(tb_proyectos.widthProperty().multiply(0.10));
        tc_ini_e.prefWidthProperty().bind(tb_proyectos.widthProperty().multiply(0.10));
        tc_fin_e.prefWidthProperty().bind(tb_proyectos.widthProperty().multiply(0.10));
        tc_estado.prefWidthProperty().bind(tb_proyectos.widthProperty().multiply(0.15));

        //Init Grilla tb_participa
        final TableColumn<participa, String> cCodigo = new TableColumn<>("Username");
        cCodigo.setCellValueFactory(new PropertyValueFactory<participa, String>("codi_part"));
        tb_participa.getColumns().add(cCodigo);
        final TableColumn<participa, String> cNombre = new TableColumn<>("Nombre");
        cNombre.setCellValueFactory(new PropertyValueFactory<participa, String>("nomb_part"));
        tb_participa.getColumns().add(cNombre);
        final TableColumn<participa, String> cOrigen = new TableColumn<>("Orig");
        cOrigen.setCellValueFactory(new PropertyValueFactory<participa, String>("orig_part"));
        tb_participa.getColumns().add(cOrigen);
        tb_participa.setItems(dataParticipa);
        cCodigo.prefWidthProperty().bind(tb_participa.widthProperty().multiply(0.25));
        cNombre.prefWidthProperty().bind(tb_participa.widthProperty().multiply(0.55));
        cOrigen.prefWidthProperty().bind(tb_participa.widthProperty().multiply(0.20));

        //3TableView
        final TreeTableColumn<activ, String> cIden = new TreeTableColumn<>("Actividad");
        cIden.setCellValueFactory(
            (final TreeTableColumn.CellDataFeatures<activ, String> param) ->
            new ReadOnlyStringWrapper(param.getValue().getValue().getIden_activ())
        );
        tb3_actividadespry.getColumns().add(cIden);

        final TreeTableColumn<activ, String> cFech = new TreeTableColumn<>("F.Activ");
        cFech.setCellValueFactory(
            (final TreeTableColumn.CellDataFeatures<activ, String> param) ->
            new ReadOnlyStringWrapper(param.getValue().getValue().getFech_activ())
        );
        tb3_actividadespry.getColumns().add(cFech);

        final TreeTableColumn<activ, String> cDesc = new TreeTableColumn<>("Descripcion");
        cDesc.setCellValueFactory(
            (final TreeTableColumn.CellDataFeatures<activ, String> param) ->
            new ReadOnlyStringWrapper(param.getValue().getValue().getDesc_activ())
        );
        tb3_actividadespry.getColumns().add(cDesc);

        final TreeTableColumn<activ, String> cUsua = new TreeTableColumn<>("Usuario R");
        cUsua.setCellValueFactory(
            (final TreeTableColumn.CellDataFeatures<activ, String> param) ->
            new ReadOnlyStringWrapper(param.getValue().getValue().getUsua_activ())
        );
        tb3_actividadespry.getColumns().add(cUsua);

        cIden.prefWidthProperty().bind(tb3_actividadespry.widthProperty().multiply(0.30));
        cFech.prefWidthProperty().bind(tb3_actividadespry.widthProperty().multiply(0.15));
        cDesc.prefWidthProperty().bind(tb3_actividadespry.widthProperty().multiply(0.40));
        cUsua.prefWidthProperty().bind(tb3_actividadespry.widthProperty().multiply(0.15));
        tb3_actividadespry.setRoot(root);
        tb3_actividadespry.setShowRoot(false);
        tb3_actividadespry.setId("LetraTree");

        //Double click en tb3_activpry
        tb3_actividadespry.setOnMousePressed(event -> {
            if (event.isPrimaryButtonDown() && event.getClickCount() == 2) {
                String v_activ;
                final TreeItem<activ> a = root.getChildren().get(tb3_actividadespry.getSelectionModel().getSelectedIndex());
                v_activ=a.getValue().getIden_activ().substring(0,2);
                if (v_activ.equals("AC")){
                    verCongreso(a.getValue().getId());
                }
                if ((v_activ.equals("AR")) || (v_activ.equals("IN")) || (v_activ.equals("EP")) || (v_activ.equals("PA"))) {
                    verDocumento(a.getValue().getId(), task);
                }
            }
        });

        //Click on tb_proyectos
        tb_proyectos.setOnMouseClicked(event -> {
            final proyectos t = dataProyecto.get(tb_proyectos.getSelectionModel().getSelectedIndex());
            cargaParticipantesPry(t.getId());
            cargaActividadesPry(t.getId());
            lb_activ.setText("Actividades :"+t.getTitulo());
        });

        dp_fecha_activ.setValue(LocalDate.now());
        lb_prys.setText("Proyectos:"+task.getUser().getCode());
        cargaProyectos(task.getUser().getCode().trim());
        cargaActividades();

        bo_menos.setOnAction(evt -> deletePersona());
        bo_mas.setOnAction(evt -> ingresarPersona());
        bo_ingresar.setOnAction(evt -> ingresarActividad(task));
        bo_salir.setOnAction(evt -> task.terminate(true));

    }

    public void verCongreso(final Long v_id_activ){
        String v_msg;
        CongresoGetResult res = null;

        try {
            res=port.congresoGet(v_id_activ);
            v_msg="Titulo:"+res.getDescTitulo();
            v_msg=v_msg+"\nCiudad:"+res.getDescCiudad();
            v_msg=v_msg+"\nDesde:"+res.getFechDesde();
            v_msg=v_msg+"\nHasta:"+res.getFechHasta();
            Dialog.showMessage(this, v_msg,"Datos Congreso");
        } catch (final FcsactivproyectoWebException e) {
            Dialog.showError(this, e);
        }
    }

    public void verDocumento(final Long v_id_activ, final DesktopTask T){
        String v_archivo="";
        byte[] P=null;

        DocumentoGetResult res = null;
        try {
            res=port.documentoGet(v_id_activ);
            P=res.getContDocumento();
            v_archivo=res.getNombDocumento();
        } catch (final NumberFormatException e) {
            Dialog.showError(this, e); return;
        } catch (final FcsactivproyectoWebException e) {
            Dialog.showError(this, e); return;
        }
        final String v_file = T.getPluginDirectory().toString()+"/"+v_archivo;  // en /OBCOM creo el archivo

        OutputStream tt = null;
        try {
            tt = new FileOutputStream(new File(v_file));
            // Escribo el contenido en el archivo
            try {
               tt.write(P);
           } catch (final IOException e) {
               Dialog.showError(this,e);
           }
        } catch (final FileNotFoundException e) {
            Dialog.showError(this, e);
        }

         // Se abre en el Cliente.
        final File myFile = new File(v_file);
        openFile(myFile);
    }

    public void openFile(final File myfile) {
        try {
            Desktop.getDesktop().open(myfile);
        } catch (final IOException ex) {
           Dialog.showError(this, ex);
        }
    }

    public void deletePersona(){
        final Long v_id_activ;
        DialogAction D;
        final TreeItem<activ> item = tb3_actividadespry.getSelectionModel().getSelectedItem();
        v_id_activ = item.getValue().getId();
        // Si hace click sobre una actividad, entonces sale , x q no es un usuario
        if (!item.getValue().getDesc_activ().equals("")) { return; }

        D = Dialog.showConfirm(this, "Desea eliminar esta persona de la Act.?");
        if ( D.toString() == "YES" ) {
            final proyectos t = dataProyecto.get(tb_proyectos.getSelectionModel().getSelectedIndex());

            try {
                port.personasactDel(v_id_activ);
                cargaActividadesPry(t.getId());
            } catch (final FcsactivproyectoWebException e) {
                Dialog.showError(this,e);
            }
        }
    }

    public void ingresarPersona(){
        String v_id_activ;
        DialogAction D;

        if (tb_proyectos.getSelectionModel().getSelectedIndex()<0) { return; }
        if (tb_participa.getSelectionModel().getSelectedIndex()<0) { return; }

        D = Dialog.showConfirm(this, "Desea asociar esta Persona a una Act.?");
        if ( D.toString() == "YES" ) {
            final proyectos t = dataProyecto.get(tb_proyectos.getSelectionModel().getSelectedIndex());
            final participa p = dataParticipa.get(tb_participa.getSelectionModel().getSelectedIndex());
            final TreeItem<activ> a = root.getChildren().get(tb3_actividadespry.getSelectionModel().getSelectedIndex());
            v_id_activ = a.getValue().getIden_activ().substring(0, 2);

            try {
                port.personasactSet(t.getId(), a.getValue().getId() ,v_id_activ, p.getCodi_part());
                cargaActividadesPry(t.getId());
            } catch (final FcsactivproyectoWebException e) {
                Dialog.showError(this,e);
            }
        }
    }

    public void cargaActividadesPry(final Long v_codi_proyecto){
        root.getChildren().clear();
        ActividadespryGetResult r = null;
        try {
            r=port.actividadespryGet(v_codi_proyecto);
            for (final ActividadespryGetActiv p : r.getActivs()){
                final TreeItem<activ> root1 =
                    new TreeItem<>(new activ(p.getId(),p.getIdenActividad(),p.getFechActividad(),p.getDescActividad(),p.getCodiFuncionario()));
                // root1.setExpanded(true);
                //busco personas de la misma actividad
                PersonasactGetResult r1 = null;
                try {
                    r1=port.personasactGet(v_codi_proyecto, p.getId());
                    for (final PersonasactGetPersona pp : r1.getPersonas()){
                        root1.getChildren().add(new TreeItem<>( new activ(pp.getId(),pp.getDescPersona(),"","","")));
                    }
                    root.getChildren().add(root1);
                } catch (final FcsactivproyectoWebException e) {
                    Dialog.showError(this,e);
                }
            }
        } catch (final Exception e1) {
            Dialog.showError(this, e1);
        }
    }

    public void ingresarActividad(final DesktopTask T){
        String v_fecha_activ,v_activ,v_activ_comp;
        DialogAction D;

        if (tb_proyectos.getSelectionModel().getSelectedIndex()<0) { return; }
        if (tb_participa.getSelectionModel().getSelectedIndex()<0) { return; }

        D = Dialog.showConfirm(this, "Desea ingresar este Actividad?");
        if ( D.toString() == "YES" ) {
            final proyectos t = dataProyecto.get(tb_proyectos.getSelectionModel().getSelectedIndex());
            final participa p = dataParticipa.get(tb_participa.getSelectionModel().getSelectedIndex());

            v_fecha_activ=dp_fecha_activ.getValue().toString();
            final int k = cb_actividad.getSelectionModel().getSelectedItem().indexOf("-");
            v_activ = cb_actividad.getSelectionModel().getSelectedItem().substring(0, k);
            v_activ_comp=cb_actividad.getValue().toString().trim();

            if ((v_activ.equals("AR")) || (v_activ.equals("IN")) || (v_activ.equals("EP")) || (v_activ.equals("PA"))) {
                byte[] fileBytes = null;
                final ANewFileData personData=new ANewFileData();
                if (ANewFileDialog.show(port, T, personData))  // Si presiona "aceptar", retorna true
                    try {
                        fileBytes=buscaArchivo(personData.getFileP());  // busco el archivo y traigo los bytes
                        port.docactividadSet(t.getId(), v_activ, v_activ_comp, v_fecha_activ, personData.getDescP(), p.getCodi_part()
                            , personData.getTituloP(), personData.getAutorP(), personData.getAgnoP(), personData.getNomfileP()
                            , fileBytes, personData.getRevistaP());
                        cargaActividadesPry(t.getId());
                    } catch (final FcsactivproyectoWebException e) {
                        Dialog.showError(this, e);
                    }
            }
            if (v_activ.equals("AC")){                  // Asistencia a Congreso
                final ANewCongresoData congresoData=new ANewCongresoData();
                if (ANewCongresoDialog.show(T, congresoData)){
                    try {
                        port.actividadcongresoSet(t.getId(), v_activ, v_fecha_activ, congresoData.getDescC(), p.getCodi_part()
                            , congresoData.getTituloC(), congresoData.getCiudadC()
                            , congresoData.getDesdeC(), congresoData.getHastaC());
                        cargaActividadesPry(t.getId());
                    } catch (final FcsactivproyectoWebException e) {
                        Dialog.showError(this, e);
                    }

                }
            }
            if (v_activ.equals("OT")){
                final String v_desc=Dialog.showInput(this, "Descripcion:","Ingrese una Descripcion");
                if (v_desc != null){
                    try {
                        port.actividadSet(t.getId(), v_activ, v_fecha_activ, v_desc.trim(), p.getCodi_part());
                        cargaActividadesPry(t.getId());
                    } catch (final FcsactivproyectoWebException e) {
                        Dialog.showError(this,e);
                    }
                }
            } // Otras actividades

        } // YES para Ingresar
    }

    public byte[] buscaArchivo(final File file){
        byte[] ArchivoBytes = null;

        if (file != null) {
            //System.out.println("Absolute Path:"+file.getAbsolutePath());
            //System.out.println("Name:"+file.getName());
            //System.out.println("Path"+file.getPath());
            //final byte[] ArchivoBytes = new byte[(int) file.length()];
            ArchivoBytes = new byte[(int) file.length()];
            try {
                final FileInputStream fileInputStream = new FileInputStream(file);
                try {
                    fileInputStream.read(ArchivoBytes);
                    fileInputStream.close();
                } catch (final IOException e) {
                    Dialog.showError(this, e);
                }
            } catch (final FileNotFoundException e) {
                Dialog.showError(this, e);
            }
            //-----El archivo esta en la variable ArchivoBytes,
        }
        return ArchivoBytes;
    }

    public void cargaActividades(){
        dataActividad.clear();
        ActividadesGetResult r = null;
        try {
            r=port.actividadesGet();
            for (final ActividadesGetActiv p : r.getActivs()){
                dataActividad.add(p.getDescActividad());
            }
            cb_actividad.setItems(dataActividad);
            cb_actividad.getSelectionModel().select(0);  // Set valor por defecto
        } catch (final Exception e1) {
            Dialog.showError(this, e1);
        }
    }

    public void cargaParticipantesPry(final Long id_proyecto){
        dataParticipa.clear();
        ParticipantesGetResult resultado = null;
        try {
            resultado = port.participantesGet(id_proyecto);
            for ( final ParticipantesGetParticip t : resultado.getParticips()) {
                dataParticipa.add(new participa( t.getId(), t.getCodiParticipa(), t.getNombParticipa(), t.getOrigParticipa() ));
            }
            tb_participa.setItems(dataParticipa);
        } catch (final FcsactivproyectoWebException e) {
            Dialog.showError(this, e);
        }
    }

    public void cargaProyectos(final String v_codi_docente){
        dataProyecto.clear();
        ProyectosGetResult resultado = null;
        try {
            resultado = port.proyectosGet(v_codi_docente);
            for ( final ProyectosGetPry t : resultado.getPrys()) {
                String v_ini_e,v_fin_e;
                if (t.getFechIniE().equals("01-01-1900")) { v_ini_e=""; } else {v_ini_e=t.getFechIniE().toString(); }
                if (t.getFechFinE().equals("01-01-1900")) { v_fin_e=""; } else {v_fin_e=t.getFechFinE().toString(); }
                dataProyecto.add(new proyectos(t.getId(), t.getCodiProyecto(), t.getDescTitulo(),t.getFechIniF()
                    ,t.getFechFinF(),v_ini_e,v_fin_e, t.getDescEstado() ));
            }
            tb_proyectos.setItems(dataProyecto);

        } catch (final FcsactivproyectoWebException e) {
            Dialog.showError(this, e);
        }
    }


} // Main - FXMPane
