/*
 * Copyright (c) OBCOM INGENIERIA S.A. (Chile). All rights reserved.
 *
 * All rights to this product are owned by OBCOM INGENIERIA S.A. and may only be
 * used  under  the  terms of its associated license document. You may NOT copy,
 * modify, sublicense, or distribute this source file or portions of  it  unless
 * previously  authorized in writing by OBCOM INGENIERIA S.A. In any event, this
 * notice and above copyright must always be included verbatim with this file.
 */

package cl.dedalus.fcsactivproyecto;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import cl.obcom.desktopfx.core.DesktopTask;
import cl.obcom.desktopfx.jfx.Dialog;
import javafx.event.ActionEvent;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import javafx.stage.WindowEvent;

/**
 * Implements the NetSecurity Search Audit Dialog.
 *
 * @author Eduardo Ostertag
 */
final class ANewCongresoDialog extends Stage {
    private final TextField tituloText;
    private final TextField ciudadText;
    private final TextField desdeText;
    private final TextField hastaText;
    private final TextField descText;
    private final ANewCongresoData congresoData;

    private boolean cancel;

    //--------------------------------------------------------------------------
    //-- Constructor Methods ---------------------------------------------------
    //--------------------------------------------------------------------------

    /**
     * Creates the new {@code AuditDialog} instance.
     *
     * @param  task the desktop task instance.
     * @param  fromDate the initial from date.
     * @param  toDate the initial to date.
     * @throws NullPointerException if an argument is {@code null}.
     */
    private ANewCongresoDialog(final DesktopTask task, final ANewCongresoData data)
    {
        // Check supplied arguments
        if (task == null)
            throw new NullPointerException("task is null");
        if (data == null)
            throw new NullPointerException("data is null");

        // Save supplied arguments
        this.cancel = true;
        this.congresoData = data;

        // Initialize dialog properties
        setResizable(false);
        initOwner(task.getStage());
        initStyle(StageStyle.UTILITY);
        setOnShown(e -> onDialogShown(e));
        setTitle("Ingresar Datos del Congreso.");
        initModality(Modality.APPLICATION_MODAL);


        // Create and initialize root rootVBox
        final VBox rootVBox = new VBox(10);
        {
            // Initialize rootVBox properties
            rootVBox.setPadding(new Insets(10, 10, 10, 10));

            // Create and initialize centerGrid
            final GridPane centerGrid = new GridPane();
            {
                // Initialize centerGrid properties
                centerGrid.setHgap(5);
                centerGrid.setVgap(5);

                // Create and initialize Titulo Congreso--------------------------------------
                final Label tituloLabel = new Label("Titulo");
                {
                    GridPane.setHalignment(tituloLabel, HPos.RIGHT);
                }
                centerGrid.add(tituloLabel, 0, 0);
                // Textfield participante code
                tituloText = new TextField("Titulo");
                {
                    tituloText.setPrefColumnCount(20);
                    GridPane.setHgrow(tituloText, Priority.ALWAYS);
                }
                centerGrid.add(tituloText, 1, 0);

                // Create and initialize campo Ciudad Congreso --------------------------------------
                final Label ciudadLabel = new Label("Ciudad");
                {
                    GridPane.setHalignment(ciudadLabel, HPos.RIGHT);
                }
                centerGrid.add(ciudadLabel, 0, 1);
                ciudadText = new TextField("Ciudad");
                {
                    ciudadText.setPrefColumnCount(20);
                    GridPane.setHgrow(ciudadText, Priority.ALWAYS);
                }
                centerGrid.add(ciudadText, 1, 1);

                // Create and initialize Desde --------------------------------------
                final Label desdeLabel = new Label("Desde");
                {
                    GridPane.setHalignment(desdeLabel, HPos.RIGHT);
                }
                centerGrid.add(desdeLabel, 0, 2);
               // Create and initialize apellidosText
                desdeText = new TextField("dd-mm-yyyy");
                {
                    desdeText.setPrefColumnCount(20);
                    GridPane.setHgrow(desdeText, Priority.ALWAYS);
                }
                centerGrid.add(desdeText, 1, 2);

                // Create and initialize Hasta ----------------------------------------
                final Label hastaLabel = new Label("Hasta");
                {
                    GridPane.setHalignment(hastaLabel, HPos.RIGHT);
                }
                centerGrid.add(hastaLabel, 0, 3);
                hastaText = new TextField("dd-mm-yyyy");
                {
                    hastaText.setPrefColumnCount(20);
                    GridPane.setHgrow(hastaText, Priority.ALWAYS);
                }
                centerGrid.add(hastaText, 1, 3);

             // Create and initialize Descripcion ----------------------------------------
                final Label descLabel = new Label("Descripcion");
                {
                    GridPane.setHalignment(descLabel, HPos.RIGHT);
                }
                centerGrid.add(descLabel, 0, 4);
                descText = new TextField("Descripcion");
                {
                    descText.setPrefColumnCount(20);
                    GridPane.setHgrow(descText, Priority.ALWAYS);
                }
                centerGrid.add(descText, 1, 4);

            }
            rootVBox.getChildren().add(centerGrid);

            // Create and initialize buttonsHBox
            final HBox buttonsHBox = new HBox(5);
            {
                // Initialize buttonsHBox properties
                buttonsHBox.setAlignment(Pos.CENTER_RIGHT);

                // Create and initialize acceptButton
                final Button acceptButton = new Button(("Aceptar"));
                {
                    acceptButton.setOnAction(e -> onAcceptButtonClick(e));
                    acceptButton.setDefaultButton(true);
                    acceptButton.setMinWidth(70);
                }
                buttonsHBox.getChildren().add(acceptButton);

                // Create and initialize cancelButton
                final Button cancelButton = new Button(("Cancelar"));
                {
                    cancelButton.setOnAction(e -> onCancelButtonClick(e));
                    cancelButton.setCancelButton(true);
                    cancelButton.setMinWidth(70);
                }
                buttonsHBox.getChildren().add(cancelButton);
            }
            rootVBox.getChildren().add(buttonsHBox);
        }

        // Create and initialize scene
        setScene(new Scene(rootVBox));
        sizeToScene();
    }

    //--------------------------------------------------------------------------
    //-- Handler Methods -------------------------------------------------------
    //--------------------------------------------------------------------------

    /**
     * Called just after the Window is shown: center to owner.
     *
     * @param event describes the window event.
     */
    private void onDialogShown(final WindowEvent event)
    {
        final Window owner = getOwner();
        setX(owner.getX() + ((owner.getWidth() - getWidth()) / 2));
        setY(owner.getY() + ((owner.getHeight() - getHeight()) / 2));
    }

    /**
     * Called when the "Accept" button is clicked.
     *
     * @param event describes the action event.
     */
    private void onAcceptButtonClick(final ActionEvent event)
    {
        try {
            // Obtain data
            if (descText.getText().toString().equals("Descripcion")) { return; }

            if (!isFechaValida(desdeText.getText().toString().trim())) {
                Dialog.showMessage(this, "Desde: Fecha invalida." );
                desdeText.setText("dd-mm-yyyy");
                desdeText.requestFocus();
                return;
            }
            if (!isFechaValida(hastaText.getText().toString().trim())) {
                Dialog.showMessage(this, "Hasta: Fecha invalida." );
                hastaText.setText("dd-mm-yyyy");
                hastaText.requestFocus();
                return;
            }

            this.congresoData.setTituloC(tituloText.getText().toString().trim());
            this.congresoData.setCiudadC(ciudadText.getText().toString().trim());
            this.congresoData.setDesdeC(desdeText.getText().toString().trim());
            this.congresoData.setHastaC(hastaText.getText().toString().trim());
            this.congresoData.setDescC(descText.getText().toString().trim());
            // File ya esta en la variable del objecto

            // Accept and clode dialog
            cancel = false;
            hide();
        } catch (final Throwable thrown) {
            Dialog.showError(this, thrown);
        }
    }

    public static boolean isFechaValida(final String v_fecha) {
        try {
            final SimpleDateFormat formatoFecha = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
            formatoFecha.setLenient(false);
            formatoFecha.parse(v_fecha);
        } catch (final ParseException e) {
            return false;
        }
        return true;
    }

    /**
     * Called when the "Cancel" button is clicked.
     *
     * @param event describes the action event.
     */
    private void onCancelButtonClick(final ActionEvent event)
    {
        cancel = true;
        hide();
    }

    //--------------------------------------------------------------------------
    //-- AuditDialog Methods ---------------------------------------------------
    //--------------------------------------------------------------------------
    /**
     * Returns {@code true} if the dialog was accepted.
     *
     * @return {@code true} if the dialog was accepted.
     */
    private boolean getAccepted()
    {
        return !cancel;
    }

    //--------------------------------------------------------------------------
    //-- Static Methods --------------------------------------------------------
    //--------------------------------------------------------------------------

    /**
     * Displays the dialog and returns the accepted message.
     *
     * @param  task the desktop task instance.
     * @param  auditData the initial values of the filters.
     * @return {@code true} if the dialog was accepted.
     * @throws NullPointerException if an argument is {@code null}.
     */
    static boolean show(final DesktopTask task, final ANewCongresoData adata)
    {
        final ANewCongresoDialog dialog = new ANewCongresoDialog(task,adata);
        dialog.showAndWait();
        return dialog.getAccepted();
    }
}
