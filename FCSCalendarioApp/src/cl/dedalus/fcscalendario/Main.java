package cl.dedalus.fcscalendario;

import java.awt.Toolkit;
import java.util.Calendar;
import cl.obcom.desktopfx.core.DesktopTask;
import cl.obcom.desktopfx.jfx.Dialog;
import cl.obcom.desktopfx.jfx.DialogAction;
import cl.obcom.desktopfx.jfx.FxmlPane;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;

public class Main extends FxmlPane
{
 
    @FXML private ComboBox<String> cb_facultad;
    @FXML private ComboBox<String> cb_instituto;
    @FXML private ComboBox<String> cb_semestre;
    @FXML private ComboBox<String> cb_agno;
    @FXML private Button bo_salir;
    @FXML private Button bo_mas;
    @FXML private Button bo_menos;
    @FXML private Button bo_modificar;
    @FXML private Label lb_docentes;
    @FXML private TextField tx_codigo;
    @FXML private TextField tx_filtro;
    @FXML private TextField tx_descripcion;
    @FXML private TextField tx_semanas;
    @FXML private TextField tx_sal_terreno;
    @FXML private TextField tx_alumnos;
    @FXML private TextField tx_grupos_teo;
    @FXML private TextField tx_grupos_pra;

    //final FcsasignaturasWeb port;

    final ObservableList<String> dataFacultad = FXCollections.observableArrayList ();
    final ObservableList<String> dataInstituto = FXCollections.observableArrayList ();
    final ObservableList<String> dataSemestre = FXCollections.observableArrayList ();
    final ObservableList<String> dataAgno = FXCollections.observableArrayList ();
   
    public Main(final DesktopTask task) throws Exception
    {
        // Se carga fxml
        loadFxml();
        task.getStage().getScene().getStylesheets().add("resources/css/NuevoEstilo.css");

      
        //Inicializar WS
       // final FcsasignaturasWebService service = new FcsasignaturasWebService();
       // port = service.getFcsasignaturasWebPort();
       // task.initWebServicePort(port,service,task.getCodeBase());

       
        bo_salir.setOnAction(evt -> task.terminate(true));
    }

} // Main - FXMPane
