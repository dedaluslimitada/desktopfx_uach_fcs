package cl.dedalus.fcsotrasactividades;

import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;

public class actividad{
    private final SimpleLongProperty id;
    private final SimpleStringProperty iden_actividad;
    private final SimpleStringProperty fech_actividad;
    private final SimpleStringProperty fech_termino;
    private final SimpleStringProperty desc_actividad;
    private final SimpleStringProperty cant_semana;
    private final SimpleStringProperty cant_horas_semana;
    private final SimpleStringProperty estd_actividad;

    public actividad(final Long id, final String iden_actividad, final String fech_actividad, final String fech_termino, final String desc_actividad
                     ,final String cant_semana, final String cant_horas_semana, final String estd_actividad){
        this.id = new SimpleLongProperty(id);
        this.iden_actividad = new SimpleStringProperty(iden_actividad);
        this.fech_actividad = new SimpleStringProperty(fech_actividad);
        this.fech_termino = new SimpleStringProperty(fech_termino);
        this.desc_actividad = new SimpleStringProperty(desc_actividad);
        this.cant_semana = new SimpleStringProperty(cant_semana);
        this.cant_horas_semana = new SimpleStringProperty(cant_horas_semana);
        this.estd_actividad = new SimpleStringProperty(estd_actividad);
    }

    public Long getId() {
        return id.get();
    }
    public String getIden_actividad() {
        return iden_actividad.get();
    }
    public String getFech_actividad() {
        return fech_actividad.get();
    }
    public String getFech_termino() {
        return fech_termino.get();
    }
    public String getDesc_actividad() {
        return desc_actividad.get();
    }
    public String getCant_semana() {
        return cant_semana.get();
    }
    public String getCant_horas_semana() {
        return cant_horas_semana.get();
    }
    public String getEstd_actividad() {
        return estd_actividad.get();
    }
}
