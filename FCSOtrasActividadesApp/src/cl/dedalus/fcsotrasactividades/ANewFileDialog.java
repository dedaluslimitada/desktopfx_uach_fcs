/*
 * Copyright (c) OBCOM INGENIERIA S.A. (Chile). All rights reserved.
 *
 * All rights to this product are owned by OBCOM INGENIERIA S.A. and may only be
 * used  under  the  terms of its associated license document. You may NOT copy,
 * modify, sublicense, or distribute this source file or portions of  it  unless
 * previously  authorized in writing by OBCOM INGENIERIA S.A. In any event, this
 * notice and above copyright must always be included verbatim with this file.
 */

package cl.dedalus.fcsotrasactividades;

import java.io.File;
import cl.obcom.desktopfx.core.DesktopTask;
import cl.obcom.desktopfx.jfx.Dialog;
import javafx.event.ActionEvent;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import javafx.stage.WindowEvent;

/**
 * Implements the NetSecurity Search Audit Dialog.
 *
 * @author Eduardo Ostertag
 */
final class ANewFileDialog extends Stage {
    private final TextField tituloText;
    private final TextField autorText;
    private final TextField agnoText;
    private final TextField fileText;
    private final TextField descText;
    private final ANewFileData personData;

    private boolean cancel;

    //--------------------------------------------------------------------------
    //-- Constructor Methods ---------------------------------------------------
    //--------------------------------------------------------------------------

    /**
     * Creates the new {@code AuditDialog} instance.
     *
     * @param  task the desktop task instance.
     * @param  fromDate the initial from date.
     * @param  toDate the initial to date.
     * @throws NullPointerException if an argument is {@code null}.
     */
    private ANewFileDialog(final DesktopTask task, final ANewFileData data)
    {
        // Check supplied arguments
        if (task == null)
            throw new NullPointerException("task is null");
        if (data == null)
            throw new NullPointerException("data is null");

        // Save supplied arguments
        this.cancel = true;
        this.personData = data;


        // Initialize dialog properties
        setResizable(false);
        initOwner(task.getStage());
        initStyle(StageStyle.UTILITY);
        setOnShown(e -> onDialogShown(e));
        setTitle("Ingresar Datos del Documento.");
        initModality(Modality.APPLICATION_MODAL);


        // Create and initialize root rootVBox
        final VBox rootVBox = new VBox(10);
        {
            // Initialize rootVBox properties
            rootVBox.setPadding(new Insets(10, 10, 10, 10));

            // Create and initialize centerGrid
            final GridPane centerGrid = new GridPane();
            {
                // Initialize centerGrid properties
                centerGrid.setHgap(5);
                centerGrid.setVgap(5);

                // Create and initialize Titulo --------------------------------------
                final Label tituloLabel = new Label("Titulo");
                {
                    GridPane.setHalignment(tituloLabel, HPos.RIGHT);
                }
                centerGrid.add(tituloLabel, 0, 0);
                // Textfield participante code
                tituloText = new TextField("Titulo");
                {
                    tituloText.setPrefColumnCount(20);
                    GridPane.setHgrow(tituloText, Priority.ALWAYS);
                }
                centerGrid.add(tituloText, 1, 0);

                // Create and initialize campo Autor --------------------------------------
                final Label autorLabel = new Label("Autor");
                {
                    GridPane.setHalignment(autorLabel, HPos.RIGHT);
                }
                centerGrid.add(autorLabel, 0, 1);
                autorText = new TextField("Autor");
                {
                    autorText.setPrefColumnCount(20);
                    GridPane.setHgrow(autorText, Priority.ALWAYS);
                }
                centerGrid.add(autorText, 1, 1);

                // Create and initialize Año --------------------------------------
                final Label agnoLabel = new Label("Año");
                {
                    GridPane.setHalignment(agnoLabel, HPos.RIGHT);
                }
                centerGrid.add(agnoLabel, 0, 2);
               // Create and initialize apellidosText
                agnoText = new TextField("2000");
                {
                    agnoText.setPrefColumnCount(20);
                    GridPane.setHgrow(agnoText, Priority.ALWAYS);
                }
                centerGrid.add(agnoText, 1, 2);

                // Create and initialize profesionLabel ----------------------------------------
                final Label fileLabel = new Label("File");
                {
                    GridPane.setHalignment(fileLabel, HPos.RIGHT);
                }
                centerGrid.add(fileLabel, 0, 3);
                fileText = new TextField("File");
                {
                    fileText.setEditable(false);
                    fileText.setPrefColumnCount(20);
                    GridPane.setHgrow(fileText, Priority.ALWAYS);
                }
                centerGrid.add(fileText, 1, 3);

             // Create and initialize Descripcion ----------------------------------------
                final Label descLabel = new Label("Descripcion");
                {
                    GridPane.setHalignment(descLabel, HPos.RIGHT);
                }
                centerGrid.add(descLabel, 0, 4);
                descText = new TextField("Descripcion");
                {
                    descText.setPrefColumnCount(20);
                    GridPane.setHgrow(descText, Priority.ALWAYS);
                }
                centerGrid.add(descText, 1, 4);

            }
            rootVBox.getChildren().add(centerGrid);

            // Create and initialize buttonsHBox
            final HBox buttonsHBox = new HBox(5);
            {
                // Initialize buttonsHBox properties
                buttonsHBox.setAlignment(Pos.CENTER_RIGHT);

                // Create and initialize acceptButton
                final Button acceptButton = new Button(("Aceptar"));
                {
                    acceptButton.setOnAction(e -> onAcceptButtonClick(e));
                    acceptButton.setDefaultButton(true);
                    acceptButton.setMinWidth(70);
                }
                buttonsHBox.getChildren().add(acceptButton);

                // Create and initialize fileButton
                final Button fileButton = new Button(("File"));
                {
                    fileButton.setOnAction(e -> onFileButtonClick(e,task));
                    fileButton.setDefaultButton(true);
                    fileButton.setMinWidth(70);
                }
                buttonsHBox.getChildren().add(fileButton);

                // Create and initialize cancelButton
                final Button cancelButton = new Button(("Cancelar"));
                {
                    cancelButton.setOnAction(e -> onCancelButtonClick(e));
                    cancelButton.setCancelButton(true);
                    cancelButton.setMinWidth(70);
                }
                buttonsHBox.getChildren().add(cancelButton);
            }
            rootVBox.getChildren().add(buttonsHBox);
        }

        // Create and initialize scene
        setScene(new Scene(rootVBox));
        sizeToScene();
    }

    //--------------------------------------------------------------------------
    //-- Handler Methods -------------------------------------------------------
    //--------------------------------------------------------------------------

    /**
     * Called just after the Window is shown: center to owner.
     *
     * @param event describes the window event.
     */
    private void onDialogShown(final WindowEvent event)
    {
        final Window owner = getOwner();
        setX(owner.getX() + ((owner.getWidth() - getWidth()) / 2));
        setY(owner.getY() + ((owner.getHeight() - getHeight()) / 2));
    }

    /**
     * Called when the "Accept" button is clicked.
     *
     * @param event describes the action event.
     */
    private void onFileButtonClick(final ActionEvent event, final DesktopTask T)
    {
        try {

            final FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Open Resource File");
            final File file = fileChooser.showOpenDialog(T.getPrimaryStage());

            if (file != null) {
                // Obtain data
                fileText.setText(file.getName());
                this.personData.setFileP(file);

            }else{
                fileText.setText("File");
            }

            // Accept and clode dialog
            // cancel = false;
            // hide();
        } catch (final Throwable thrown) {
            Dialog.showError(this, thrown);
        }
    }

    /**
     * Called when the "Accept" button is clicked.
     *
     * @param event describes the action event.
     */
    private void onAcceptButtonClick(final ActionEvent event)
    {
        try {
            // Obtain data
            //if (fileText.getText().toString().equals("File")) { return; }
        	//this.personData.setFileP(tituloText.getText().trim());
            this.personData.setTituloP(tituloText.getText().trim());
            this.personData.setAutorP(autorText.getText().toString().trim());
            this.personData.setAgnoP(agnoText.getText().toString().trim());
            this.personData.setNomfileP(fileText.getText().trim());
            this.personData.setDescP(descText.getText().trim());
            // File ya esta en la variable del objecto

            // Accept and clode dialog
            cancel = false;
            hide();
        } catch (final Throwable thrown) {
            Dialog.showError(this, thrown);
        }
    }

    /**
     * Called when the "Cancel" button is clicked.
     *
     * @param event describes the action event.
     */
    private void onCancelButtonClick(final ActionEvent event)
    {
        cancel = true;
        hide();
    }

    //--------------------------------------------------------------------------
    //-- AuditDialog Methods ---------------------------------------------------
    //--------------------------------------------------------------------------
    /**
     * Returns {@code true} if the dialog was accepted.
     *
     * @return {@code true} if the dialog was accepted.
     */
    private boolean getAccepted()
    {
        return !cancel;
    }

    //--------------------------------------------------------------------------
    //-- Static Methods --------------------------------------------------------
    //--------------------------------------------------------------------------

    /**
     * Displays the dialog and returns the accepted message.
     *
     * @param  task the desktop task instance.
     * @param  auditData the initial values of the filters.
     * @return {@code true} if the dialog was accepted.
     * @throws NullPointerException if an argument is {@code null}.
     */
    static boolean show(final DesktopTask task, final ANewFileData adata)
    {
        final ANewFileDialog dialog = new ANewFileDialog(task,adata);
        dialog.showAndWait();
        return dialog.getAccepted();
    }
}
