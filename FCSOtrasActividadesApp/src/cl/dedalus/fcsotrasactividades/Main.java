package cl.dedalus.fcsotrasactividades;


import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import cl.dedalus.fcsotrasactividades.ws.ActividadesGetActiv;
import cl.dedalus.fcsotrasactividades.ws.ActividadesGetResult;
import cl.dedalus.fcsotrasactividades.ws.DocenteGetAsig;
import cl.dedalus.fcsotrasactividades.ws.DocenteGetResult;
import cl.dedalus.fcsotrasactividades.ws.DocumentoGetResult;
import cl.dedalus.fcsotrasactividades.ws.FcsotrasactividadesWeb;
import cl.dedalus.fcsotrasactividades.ws.FcsotrasactividadesWebException;
import cl.dedalus.fcsotrasactividades.ws.FcsotrasactividadesWebService;
import cl.dedalus.fcsotrasactividades.ws.OtrasactivGetActiv;
import cl.dedalus.fcsotrasactividades.ws.OtrasactivGetResult;
import cl.obcom.desktopfx.core.DesktopTask;
import cl.obcom.desktopfx.jfx.Dialog;
import cl.obcom.desktopfx.jfx.DialogAction;
import cl.obcom.desktopfx.jfx.FxmlPane;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

public class Main extends FxmlPane
{
    @FXML TableView<actividad> tb_actividad = new TableView<>();
    @FXML private TextField tx_codigo;
    @FXML private TextField tx_facultad;
    @FXML private TextField tx_instituto;
    @FXML private TextField tx_nombre;
    @FXML private TextField tx_rut;
    @FXML private TextField tx_email;
    @FXML private Button bo_salir;
    @FXML private Button bo_ingresar;
    @FXML private Button bo_terminar;
    @FXML private ComboBox<String> cb_actividad;
    @FXML private DatePicker dp_fecha_activ;
    @FXML private DatePicker dp_fecha_termino;

    final FcsotrasactividadesWeb port;

    final ObservableList<String> dataActividades = FXCollections.observableArrayList ();
    final ObservableList<actividad> dataActividad = FXCollections.observableArrayList ();


    public Main(final DesktopTask task) throws Exception
    {
        // Se carga fxml
        loadFxml();
        task.getStage().getScene().getStylesheets().add("resources/css/NuevoEstilo.css");

        //Inicializar WS
        final FcsotrasactividadesWebService service = new FcsotrasactividadesWebService();
        port = service.getFcsotrasactividadesWebPort();
        task.initWebServicePort(port,service,task.getCodeBase());

        //Init Grillas tb_actividades
        final TableColumn<actividad, String> cActiv = new TableColumn<>("Actividad");
        cActiv.setCellValueFactory(new PropertyValueFactory<actividad, String>("iden_actividad"));
        tb_actividad.getColumns().add(cActiv);
        final TableColumn<actividad, String> cFecha = new TableColumn<>("Fecha");
        cFecha.setCellValueFactory(new PropertyValueFactory<actividad, String>("fech_actividad"));
        tb_actividad.getColumns().add(cFecha);
        final TableColumn<actividad, String> cFecht = new TableColumn<>("Fecha T");
        cFecht.setCellValueFactory(new PropertyValueFactory<actividad, String>("fech_termino"));
        tb_actividad.getColumns().add(cFecht);
        final TableColumn<actividad, String> cDesc = new TableColumn<>("Descripcion");
        cDesc.setCellValueFactory(new PropertyValueFactory<actividad, String>("desc_actividad"));
        tb_actividad.getColumns().add(cDesc);
        //final TableColumn<actividad, String> cSema = new TableColumn<>("Sem");
        //cSema.setCellValueFactory(new PropertyValueFactory<actividad, String>("cant_semana"));
        //tb_actividad.getColumns().add(cSema);
        final TableColumn<actividad, String> cHora = new TableColumn<>("H Sem");
        cHora.setCellValueFactory(new PropertyValueFactory<actividad, String>("cant_horas_semana"));
        tb_actividad.getColumns().add(cHora);
        final TableColumn<actividad, String> cEstd = new TableColumn<>("estado");
        cEstd.setCellValueFactory(new PropertyValueFactory<actividad, String>("estd_actividad"));
        tb_actividad.getColumns().add(cEstd);
        tb_actividad.setItems(dataActividad);
        cActiv.prefWidthProperty().bind(tb_actividad.widthProperty().multiply(0.25));
        cFecha.prefWidthProperty().bind(tb_actividad.widthProperty().multiply(0.12));
        cFecht.prefWidthProperty().bind(tb_actividad.widthProperty().multiply(0.12));
        cDesc.prefWidthProperty().bind(tb_actividad.widthProperty().multiply(0.30));
        //cSema.prefWidthProperty().bind(tb_actividad.widthProperty().multiply(0.07));
        cHora.prefWidthProperty().bind(tb_actividad.widthProperty().multiply(0.07));
        cEstd.prefWidthProperty().bind(tb_actividad.widthProperty().multiply(0.10));

        //Click on tb_asignaturas
        tb_actividad.setOnMouseClicked(event -> {
            final actividad t = dataActividad.get(tb_actividad.getSelectionModel().getSelectedIndex());
            final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
            final LocalDate dateActiv = LocalDate.parse(t.getFech_actividad(), formatter);
            dp_fecha_activ.setValue(dateActiv);
        });

        //Doble click para ver el documento
        tb_actividad.setOnMousePressed(event -> {
            if (event.isPrimaryButtonDown() && event.getClickCount() == 2) {
                String v_activ;
                final actividad t = dataActividad.get(tb_actividad.getSelectionModel().getSelectedIndex());
                v_activ=t.getIden_actividad().substring(0,2);
                if (v_activ.equals("EP")){
                    verDocumento(task,t.getId());
                }
            }
        });

        dp_fecha_activ.setValue(LocalDate.now());
        dp_fecha_termino.setValue(null);
        cargaDatosDocente(task.getUser().getCode().trim());
        cargaActividades();

        bo_salir.setOnAction(evt -> task.terminate(true));
        bo_ingresar.setOnAction(evt -> ingresarActividad(task));
        bo_terminar.setOnAction(evt -> terminarActividad(task.getUser().getCode().toString()));
    }

    public void terminarActividad(String v_user){
         DialogAction D;
         if (tb_actividad.getSelectionModel().getSelectedIndex() < 0) { return; }
         final actividad t = dataActividad.get(tb_actividad.getSelectionModel().getSelectedIndex());

         D = Dialog.showConfirm(this, "Desea terminar esta Actividad?");
         if ( D.toString() == "YES" ) {
        	 try {
				port.terminoactividadUpd(t.getId());
				cargaOtrasActividades(v_user);
			} catch (FcsotrasactividadesWebException e) {
				Dialog.showError(this, e);
			}
         }  	
    }
    
    public void verDocumento(final DesktopTask T, final Long v_id){
        String v_archivo="";

        DocumentoGetResult res = null;
        byte[] P=null;

        try {
            res=port.documentoGet(v_id);
            P=res.getContDocumento();
            v_archivo=res.getNombDocumento();
            if (v_archivo.equals("File")){
            	Dialog.showMessage(this, "No existe un documento asociado.");
            	return;
            }
        } catch (final NumberFormatException e) {
            Dialog.showError(this, e);
        } catch (final FcsotrasactividadesWebException e) {
            Dialog.showError(this, e);
        }
        final String v_file = T.getPluginDirectory().toString()+"/"+v_archivo;  // en /OBCOM creo el archivo

        OutputStream tt = null;
        try {
            tt = new FileOutputStream(new File(v_file));
            try {
             // Escribo el contenido en el archivo
                tt.write(P);
            } catch (final IOException e) {
                Dialog.showError(this,e);
            }
        } catch (final FileNotFoundException e) {
            Dialog.showError(this, e);
        }

         // Se abre en el Cliente.
        final File myFile = new File(v_file);
        openFile(myFile);
    }

    public void openFile(final File myfile) {
        try {
            Desktop.getDesktop().open(myfile);
        } catch (final IOException ex) {
           Dialog.showError(this, ex);
        }
    }

    public void ingresarActividad(final DesktopTask T){
        String v_activ, v_user,v_fecha_activ,v_fecha_termino;
        DialogAction D;

        D = Dialog.showConfirm(this, "Desea ingresar este Actividad?");
        if ( D.toString() == "YES" ) {
            v_user=T.getUser().getCode();
            v_fecha_activ=dp_fecha_activ.getValue().toString();
            
            if (dp_fecha_termino.getValue() == null) { v_fecha_termino="1900-01-01"; }
            else { v_fecha_termino=dp_fecha_termino.getValue().toString(); }
            
            final int k = cb_actividad.getSelectionModel().getSelectedItem().indexOf("-");
            v_activ = cb_actividad.getSelectionModel().getSelectedItem().substring(0, k);

            if (v_activ.equals("EP")){			// Subir Documento
                byte[] fileBytes = null;
                final ANewFileData personData=new ANewFileData();
                if (ANewFileDialog.show(T, personData))  // Si presiona "aceptar", retorna true
                    try {
                        fileBytes=buscaArchivo(personData.getFileP());
                        port.docactividadSet(v_user, v_activ, v_fecha_activ, v_fecha_termino, personData.getDescP(), personData.getTituloP()
                            , personData.getAutorP(), personData.getAgnoP(), personData.getNomfileP(), fileBytes);
                        cargaOtrasActividades(v_user);
                    } catch (final FcsotrasactividadesWebException e) {
                        Dialog.showError(this,e);
                    }

            }else {
                final ANewActivData activData=new ANewActivData();
                if (ANewActivDialog.show(T, activData)){
                    try {
                        port.actividadSet(v_user, v_activ, v_fecha_activ, v_fecha_termino, activData.getDescripcionC()
                            , activData.getHorasC(), activData.getSemanasC());
                        cargaOtrasActividades(v_user);
                    } catch (final FcsotrasactividadesWebException e) {
                        Dialog.showError(this, e);
                    }
                }
            }
        }
    }

    public byte[] buscaArchivo(final File file){
        byte[] ArchivoBytes = null;

        if (file != null) {
            //System.out.println("Absolute Path:"+file.getAbsolutePath());
            //System.out.println("Name:"+file.getName());
            //System.out.println("Path"+file.getPath());
            //final byte[] ArchivoBytes = new byte[(int) file.length()];
            ArchivoBytes = new byte[(int) file.length()];
            try {
                final FileInputStream fileInputStream = new FileInputStream(file);
                try {
                    fileInputStream.read(ArchivoBytes);
                    fileInputStream.close();
                } catch (final IOException e) {
                    Dialog.showError(this, e);
                }
            } catch (final FileNotFoundException e) {
                Dialog.showError(this, e);
            }
            //-----El archivo esta en la variable ArchivoBytes,

        }
        return ArchivoBytes;
    }

    public void cargaActividades(){
        dataActividades.clear();
        ActividadesGetResult r = null;
        try {
            r=port.actividadesGet();
            for (final ActividadesGetActiv p : r.getActivs()){
                dataActividades.add(p.getDescActividad());
            }
            cb_actividad.setItems(dataActividades);
            cb_actividad.getSelectionModel().select(0);  // Set valor por defecto
        } catch (final Exception e1) {
            Dialog.showError(this, e1);
        }
    }

    public void cargaOtrasActividades(final String v_user){
        dataActividad.clear();
        OtrasactivGetResult res;
        try {
            res=port.otrasactivGet(v_user);

            for (final OtrasactivGetActiv p : res.getActivs()){
                dataActividad.add(new actividad( p.getId(), p.getIdenActividad(), p.getFechActividad(),p.getFechTermino(), p.getDescActividad()
                    , p.getCantSemana(), p.getCantHoras(), p.getEstdActividad() ));
            }
            tb_actividad.setItems(dataActividad);
        } catch (final FcsotrasactividadesWebException e) {
            Dialog.showError(this, e);
        }

    }

    public void cargaDatosDocente(final String v_user){
        dataActividad.clear();
        DocenteGetResult res = null;
        try {
            res=port.docenteGet(v_user);
            tx_codigo.setText(v_user);
            tx_facultad.setText(res.getDescFac());
            tx_instituto.setText(res.getDescIns());
            tx_nombre.setText(res.getDescNom());
            tx_rut.setText(res.getDescRut());
            tx_email.setText(res.getDescEma());
            for (final DocenteGetAsig p : res.getAsigs()){
                dataActividad.add(new actividad( p.getId(), p.getIdenActividad(), p.getFechActividad(), p.getFechTermino(),p.getDescActividad()
                    , p.getCantSemana(), p.getCantHoras(), p.getEstdActividad() ));
            }
            tb_actividad.setItems(dataActividad);
        } catch (final Exception e1) {
            Dialog.showError(this, e1);
        }
    }

} // Main - FXMPane
