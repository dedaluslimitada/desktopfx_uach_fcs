/*
 * Copyright (c) OBCOM INGENIERIA S.A. (Chile). All rights reserved.
 *
 * All rights to this product are owned by OBCOM INGENIERIA S.A. and may only be
 * used  under  the  terms of its associated license document. You may NOT copy,
 * modify, sublicense, or distribute this source file or portions of  it  unless
 * previously  authorized in writing by OBCOM INGENIERIA S.A. In any event, this
 * notice and above copyright must always be included verbatim with this file.
 */

package cl.dedalus.fcsotrasactividades;

import cl.obcom.desktopfx.core.DesktopTask;
import cl.obcom.desktopfx.jfx.Dialog;
import javafx.event.ActionEvent;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import javafx.stage.WindowEvent;

/**
 * Implements the NetSecurity Search Audit Dialog.
 *
 * @author Eduardo Ostertag
 */
final class ANewActivDialog extends Stage {
    private final TextField descripcionText;
    private final TextField horasText;
    private final ANewActivData activData;

    private boolean cancel;

    //--------------------------------------------------------------------------
    //-- Constructor Methods ---------------------------------------------------
    //--------------------------------------------------------------------------

    /**
     * Creates the new {@code AuditDialog} instance.
     *
     * @param  task the desktop task instance.
     * @param  fromDate the initial from date.
     * @param  toDate the initial to date.
     * @throws NullPointerException if an argument is {@code null}.
     */
    private ANewActivDialog(final DesktopTask task, final ANewActivData data)
    {
        // Check supplied arguments
        if (task == null)
            throw new NullPointerException("task is null");
        if (data == null)
            throw new NullPointerException("data is null");

        // Save supplied arguments
        this.cancel = true;
        this.activData = data;

        // Initialize dialog properties
        setResizable(false);
        initOwner(task.getStage());
        initStyle(StageStyle.UTILITY);
        setOnShown(e -> onDialogShown(e));
        setTitle("Ingresar Datos de la Actividad.");
        initModality(Modality.APPLICATION_MODAL);

        // Create and initialize root rootVBox
        final VBox rootVBox = new VBox(10);
        {
            // Initialize rootVBox properties
            rootVBox.setPadding(new Insets(10, 10, 10, 10));

            // Create and initialize centerGrid
            final GridPane centerGrid = new GridPane();
            {
                // Initialize centerGrid properties
                centerGrid.setHgap(5);
                centerGrid.setVgap(5);

                // Create and initialize Descripcion actividad--------------------------------------
                final Label descLabel = new Label("Descripcion");
                {
                    GridPane.setHalignment(descLabel, HPos.RIGHT);
                }
                centerGrid.add(descLabel, 0, 0);
                // Textfield participante code
                descripcionText = new TextField("Descripcion");
                {
                    descripcionText.setPrefColumnCount(20);
                    GridPane.setHgrow(descripcionText, Priority.ALWAYS);
                }
                centerGrid.add(descripcionText, 1, 0);

                // Create and initialize Horas a la semana pra la activ --------------------------------------
                final Label horasLabel = new Label("Horas por Semana");
                {
                    GridPane.setHalignment(horasLabel, HPos.RIGHT);
                }
                centerGrid.add(horasLabel, 0, 1);
               // Create and initialize apellidosText
                horasText = new TextField("0");
                {
                    horasText.setPrefColumnCount(20);
                    GridPane.setHgrow(horasText, Priority.ALWAYS);
                }
                centerGrid.add(horasText, 1, 1);

            }
            rootVBox.getChildren().add(centerGrid);

            // Create and initialize buttonsHBox
            final HBox buttonsHBox = new HBox(5);
            {
                // Initialize buttonsHBox properties
                buttonsHBox.setAlignment(Pos.CENTER_RIGHT);

                // Create and initialize acceptButton
                final Button acceptButton = new Button(("Aceptar"));
                {
                    acceptButton.setOnAction(e -> onAcceptButtonClick(e));
                    acceptButton.setDefaultButton(true);
                    acceptButton.setMinWidth(70);
                }
                buttonsHBox.getChildren().add(acceptButton);

                // Create and initialize cancelButton
                final Button cancelButton = new Button(("Cancelar"));
                {
                    cancelButton.setOnAction(e -> onCancelButtonClick(e));
                    cancelButton.setCancelButton(true);
                    cancelButton.setMinWidth(70);
                }
                buttonsHBox.getChildren().add(cancelButton);
            }
            rootVBox.getChildren().add(buttonsHBox);
        }

        // Create and initialize scene
        setScene(new Scene(rootVBox));
        sizeToScene();
    }

    //--------------------------------------------------------------------------
    //-- Handler Methods -------------------------------------------------------
    //--------------------------------------------------------------------------

    /**
     * Called just after the Window is shown: center to owner.
     *
     * @param event describes the window event.
     */
    private void onDialogShown(final WindowEvent event)
    {
        final Window owner = getOwner();
        setX(owner.getX() + ((owner.getWidth() - getWidth()) / 2));
        setY(owner.getY() + ((owner.getHeight() - getHeight()) / 2));
    }

    /**
     * Called when the "Accept" button is clicked.
     *
     * @param event describes the action event.
     */
    private void onAcceptButtonClick(final ActionEvent event)
    {
        try {
            // Obtain data
            if (descripcionText.getText().toString().equals("Descripcion")) { return; }

            if (!isNumeric(horasText.getText().toString().trim())){
                Dialog.showMessage(this, "Campo horas debe ser numerico." );
                horasText.setText("0");
                horasText.requestFocus();
                return;
            }

            this.activData.setDescripcionC(descripcionText.getText().toString().trim());
            this.activData.setSemanasC("0");
            this.activData.setHorasC(horasText.getText().toString().trim());

            // Accept and clode dialog
            cancel = false;
            hide();
        } catch (final Throwable thrown) {
            Dialog.showError(this, thrown);
        }
    }

    private static boolean isNumeric(final String cadena){
        try {
            Integer.parseInt(cadena);
            return true;
        } catch (final NumberFormatException nfe){
            return false;
        }
    }

    /**
     * Called when the "Cancel" button is clicked.
     *
     * @param event describes the action event.
     */
    private void onCancelButtonClick(final ActionEvent event)
    {
        cancel = true;
        hide();
    }

    //--------------------------------------------------------------------------
    //-- AuditDialog Methods ---------------------------------------------------
    //--------------------------------------------------------------------------
    /**
     * Returns {@code true} if the dialog was accepted.
     *
     * @return {@code true} if the dialog was accepted.
     */
    private boolean getAccepted()
    {
        return !cancel;
    }

    //--------------------------------------------------------------------------
    //-- Static Methods --------------------------------------------------------
    //--------------------------------------------------------------------------

    /**
     * Displays the dialog and returns the accepted message.
     *
     * @param  task the desktop task instance.
     * @param  auditData the initial values of the filters.
     * @return {@code true} if the dialog was accepted.
     * @throws NullPointerException if an argument is {@code null}.
     */
    static boolean show(final DesktopTask task, final ANewActivData adata)
    {
        final ANewActivDialog dialog = new ANewActivDialog(task,adata);
        dialog.showAndWait();
        return dialog.getAccepted();
    }
}
