/*
 * Copyright (c) DEDALUS Limitada. All rights reserved.
 *
 * All rights to this product are owned by DEDALUS Limitada and may only be used
 * under the terms of its associated license document. You may NOT copy, modify,
 * sublicense,  or  distribute  this  source  file  or  portions  of  it  unless
 * previously authorized in writing by  DEDALUS  Limitada. In  any  event,  this
 * notice and above copyright must always be included verbatim with this file.
 */

package cl.dedalus.fcsotrasactividades;

import java.io.File;

/**
 * @author Aulloa
 */
public class ANewFileData {
    //variables visibility set to private
    private String  tituloP;
    private String  autorP;
    private String  agnoP;
    private String  nomfileP;
    private File    fileP;
    private String  descP;

    //constructor takes String and Integer as parameters
    public ANewFileData(){
        this.tituloP="";
        this.autorP="";
        this.agnoP="";
        this.nomfileP="";
        this.fileP=null;
        this.descP="";
    }

    //public method that gets/set
    public String getTituloP(){
        return tituloP;
    }
    public void setTituloP(final String ititulo){
        this.tituloP=ititulo;
    }

    //public method that gets/set
    public String getAutorP(){
        return autorP;
    }
    public void setAutorP(final String iautor){
        this.autorP=iautor;
    }

    //public method that gets/set
    public String getAgnoP(){
        return agnoP;
    }
    public void setAgnoP(final String iagno){
        this.agnoP=iagno;
    }

    //public method that gets/set
    public String getNomfileP(){
        return nomfileP;
    }
    public void setNomfileP(final String ifile){
        this.nomfileP=ifile;
    }

    //public method that gets/set
    public File getFileP(){
        return fileP;
    }
    public void setFileP(final File ifile1){
        this.fileP=ifile1;
    }

    //public method that gets/set
    public String getDescP(){
        return descP;
    }
    public void setDescP(final String idesc){
        this.descP=idesc;
    }
}
