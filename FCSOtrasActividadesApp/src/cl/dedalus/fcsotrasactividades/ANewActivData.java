/*
 * Copyright (c) DEDALUS Limitada. All rights reserved.
 *
 * All rights to this product are owned by DEDALUS Limitada and may only be used
 * under the terms of its associated license document. You may NOT copy, modify,
 * sublicense,  or  distribute  this  source  file  or  portions  of  it  unless
 * previously authorized in writing by  DEDALUS  Limitada. In  any  event,  this
 * notice and above copyright must always be included verbatim with this file.
 */

package cl.dedalus.fcsotrasactividades;

/**
 * @author Aulloa
 */
public class ANewActivData {
    //variables visibility set to private
    private String  descripcionC;
    private String  semanasC;
    private String  horasC;


    //constructor takes String and Integer as parameters
    public ANewActivData(){
        this.descripcionC="";
        this.semanasC="";
        this.horasC="";
    }

    //public method that gets/set
    public String getDescripcionC(){
        return descripcionC;
    }
    public void setDescripcionC(final String idesc){
        this.descripcionC=idesc;
    }

    //public method that gets/set
    public String getSemanasC(){
        return semanasC;
    }
    public void setSemanasC(final String isemanas){
        this.semanasC=isemanas;
    }

    //public method that gets/set
    public String getHorasC(){
        return horasC;
    }
    public void setHorasC(final String ihoras){
        this.horasC=ihoras;
    }
}
