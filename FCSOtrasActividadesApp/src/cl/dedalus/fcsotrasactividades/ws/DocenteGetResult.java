
package cl.dedalus.fcsotrasactividades.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DocenteGetResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DocenteGetResult">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.fcsotrasactividades.dedalus.cl/}ProcedureResult">
 *       &lt;sequence>
 *         &lt;element name="asigs" type="{http://ws.fcsotrasactividades.dedalus.cl/}DocenteGetAsig" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="descEma" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descFac" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descIns" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descNom" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descRut" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocenteGetResult", propOrder = {
    "asigs",
    "descEma",
    "descFac",
    "descIns",
    "descNom",
    "descRut"
})
public class DocenteGetResult
    extends ProcedureResult
{

    @XmlElement(nillable = true)
    protected List<DocenteGetAsig> asigs;
    protected String descEma;
    protected String descFac;
    protected String descIns;
    protected String descNom;
    protected String descRut;

    /**
     * Gets the value of the asigs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the asigs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAsigs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DocenteGetAsig }
     * 
     * 
     */
    public List<DocenteGetAsig> getAsigs() {
        if (asigs == null) {
            asigs = new ArrayList<DocenteGetAsig>();
        }
        return this.asigs;
    }

    /**
     * Obtiene el valor de la propiedad descEma.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescEma() {
        return descEma;
    }

    /**
     * Define el valor de la propiedad descEma.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescEma(String value) {
        this.descEma = value;
    }

    /**
     * Obtiene el valor de la propiedad descFac.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescFac() {
        return descFac;
    }

    /**
     * Define el valor de la propiedad descFac.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescFac(String value) {
        this.descFac = value;
    }

    /**
     * Obtiene el valor de la propiedad descIns.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescIns() {
        return descIns;
    }

    /**
     * Define el valor de la propiedad descIns.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescIns(String value) {
        this.descIns = value;
    }

    /**
     * Obtiene el valor de la propiedad descNom.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescNom() {
        return descNom;
    }

    /**
     * Define el valor de la propiedad descNom.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescNom(String value) {
        this.descNom = value;
    }

    /**
     * Obtiene el valor de la propiedad descRut.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescRut() {
        return descRut;
    }

    /**
     * Define el valor de la propiedad descRut.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescRut(String value) {
        this.descRut = value;
    }

}
