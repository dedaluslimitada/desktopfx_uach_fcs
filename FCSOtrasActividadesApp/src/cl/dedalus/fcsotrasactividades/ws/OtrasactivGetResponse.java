
package cl.dedalus.fcsotrasactividades.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para otrasactivGetResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="otrasactivGetResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="otrasactivGetResult" type="{http://ws.fcsotrasactividades.dedalus.cl/}OtrasactivGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "otrasactivGetResponse", propOrder = {
    "otrasactivGetResult"
})
public class OtrasactivGetResponse {

    protected OtrasactivGetResult otrasactivGetResult;

    /**
     * Obtiene el valor de la propiedad otrasactivGetResult.
     * 
     * @return
     *     possible object is
     *     {@link OtrasactivGetResult }
     *     
     */
    public OtrasactivGetResult getOtrasactivGetResult() {
        return otrasactivGetResult;
    }

    /**
     * Define el valor de la propiedad otrasactivGetResult.
     * 
     * @param value
     *     allowed object is
     *     {@link OtrasactivGetResult }
     *     
     */
    public void setOtrasactivGetResult(OtrasactivGetResult value) {
        this.otrasactivGetResult = value;
    }

}
