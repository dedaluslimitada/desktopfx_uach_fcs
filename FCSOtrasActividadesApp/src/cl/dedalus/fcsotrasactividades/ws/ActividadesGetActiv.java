
package cl.dedalus.fcsotrasactividades.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ActividadesGetActiv complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ActividadesGetActiv">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="descActividad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ActividadesGetActiv", propOrder = {
    "descActividad"
})
public class ActividadesGetActiv {

    protected String descActividad;

    /**
     * Obtiene el valor de la propiedad descActividad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescActividad() {
        return descActividad;
    }

    /**
     * Define el valor de la propiedad descActividad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescActividad(String value) {
        this.descActividad = value;
    }

}
