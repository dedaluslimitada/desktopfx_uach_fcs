
package cl.dedalus.fcsotrasactividades.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para actividadSet complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="actividadSet">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codiDocente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idenActiv" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechActiv" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechTermino" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descActiv" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantHoras" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantSemanas" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "actividadSet", propOrder = {
    "codiDocente",
    "idenActiv",
    "fechActiv",
    "fechTermino",
    "descActiv",
    "cantHoras",
    "cantSemanas"
})
public class ActividadSet {

    protected String codiDocente;
    protected String idenActiv;
    protected String fechActiv;
    protected String fechTermino;
    protected String descActiv;
    protected String cantHoras;
    protected String cantSemanas;

    /**
     * Obtiene el valor de la propiedad codiDocente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiDocente() {
        return codiDocente;
    }

    /**
     * Define el valor de la propiedad codiDocente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiDocente(String value) {
        this.codiDocente = value;
    }

    /**
     * Obtiene el valor de la propiedad idenActiv.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdenActiv() {
        return idenActiv;
    }

    /**
     * Define el valor de la propiedad idenActiv.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdenActiv(String value) {
        this.idenActiv = value;
    }

    /**
     * Obtiene el valor de la propiedad fechActiv.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechActiv() {
        return fechActiv;
    }

    /**
     * Define el valor de la propiedad fechActiv.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechActiv(String value) {
        this.fechActiv = value;
    }

    /**
     * Obtiene el valor de la propiedad fechTermino.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechTermino() {
        return fechTermino;
    }

    /**
     * Define el valor de la propiedad fechTermino.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechTermino(String value) {
        this.fechTermino = value;
    }

    /**
     * Obtiene el valor de la propiedad descActiv.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescActiv() {
        return descActiv;
    }

    /**
     * Define el valor de la propiedad descActiv.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescActiv(String value) {
        this.descActiv = value;
    }

    /**
     * Obtiene el valor de la propiedad cantHoras.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantHoras() {
        return cantHoras;
    }

    /**
     * Define el valor de la propiedad cantHoras.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantHoras(String value) {
        this.cantHoras = value;
    }

    /**
     * Obtiene el valor de la propiedad cantSemanas.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantSemanas() {
        return cantSemanas;
    }

    /**
     * Define el valor de la propiedad cantSemanas.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantSemanas(String value) {
        this.cantSemanas = value;
    }

}
