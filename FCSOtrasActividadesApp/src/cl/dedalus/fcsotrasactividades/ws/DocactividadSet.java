
package cl.dedalus.fcsotrasactividades.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para docactividadSet complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="docactividadSet">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codiDocente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idenActiv" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechActiv" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechTermino" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descActiv" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descTitulo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descAutor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nombDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="contDocumento" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "docactividadSet", propOrder = {
    "codiDocente",
    "idenActiv",
    "fechActiv",
    "fechTermino",
    "descActiv",
    "descTitulo",
    "descAutor",
    "fechDocumento",
    "nombDocumento",
    "contDocumento"
})
public class DocactividadSet {

    protected String codiDocente;
    protected String idenActiv;
    protected String fechActiv;
    protected String fechTermino;
    protected String descActiv;
    protected String descTitulo;
    protected String descAutor;
    protected String fechDocumento;
    protected String nombDocumento;
    @XmlElementRef(name = "contDocumento", namespace = "http://ws.fcsotrasactividades.dedalus.cl/", type = JAXBElement.class, required = false)
    protected JAXBElement<byte[]> contDocumento;

    /**
     * Obtiene el valor de la propiedad codiDocente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiDocente() {
        return codiDocente;
    }

    /**
     * Define el valor de la propiedad codiDocente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiDocente(String value) {
        this.codiDocente = value;
    }

    /**
     * Obtiene el valor de la propiedad idenActiv.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdenActiv() {
        return idenActiv;
    }

    /**
     * Define el valor de la propiedad idenActiv.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdenActiv(String value) {
        this.idenActiv = value;
    }

    /**
     * Obtiene el valor de la propiedad fechActiv.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechActiv() {
        return fechActiv;
    }

    /**
     * Define el valor de la propiedad fechActiv.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechActiv(String value) {
        this.fechActiv = value;
    }

    /**
     * Obtiene el valor de la propiedad fechTermino.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechTermino() {
        return fechTermino;
    }

    /**
     * Define el valor de la propiedad fechTermino.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechTermino(String value) {
        this.fechTermino = value;
    }

    /**
     * Obtiene el valor de la propiedad descActiv.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescActiv() {
        return descActiv;
    }

    /**
     * Define el valor de la propiedad descActiv.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescActiv(String value) {
        this.descActiv = value;
    }

    /**
     * Obtiene el valor de la propiedad descTitulo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescTitulo() {
        return descTitulo;
    }

    /**
     * Define el valor de la propiedad descTitulo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescTitulo(String value) {
        this.descTitulo = value;
    }

    /**
     * Obtiene el valor de la propiedad descAutor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescAutor() {
        return descAutor;
    }

    /**
     * Define el valor de la propiedad descAutor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescAutor(String value) {
        this.descAutor = value;
    }

    /**
     * Obtiene el valor de la propiedad fechDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechDocumento() {
        return fechDocumento;
    }

    /**
     * Define el valor de la propiedad fechDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechDocumento(String value) {
        this.fechDocumento = value;
    }

    /**
     * Obtiene el valor de la propiedad nombDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombDocumento() {
        return nombDocumento;
    }

    /**
     * Define el valor de la propiedad nombDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombDocumento(String value) {
        this.nombDocumento = value;
    }

    /**
     * Obtiene el valor de la propiedad contDocumento.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     *     
     */
    public JAXBElement<byte[]> getContDocumento() {
        return contDocumento;
    }

    /**
     * Define el valor de la propiedad contDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     *     
     */
    public void setContDocumento(JAXBElement<byte[]> value) {
        this.contDocumento = value;
    }

}
