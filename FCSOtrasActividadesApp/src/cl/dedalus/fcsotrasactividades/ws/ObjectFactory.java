
package cl.dedalus.fcsotrasactividades.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cl.dedalus.fcsotrasactividades.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _FaultInfo_QNAME = new QName("http://ws.fcsotrasactividades.dedalus.cl/", "faultInfo");
    private final static QName _TerminoactividadUpd_QNAME = new QName("http://ws.fcsotrasactividades.dedalus.cl/", "terminoactividadUpd");
    private final static QName _ActividadSetResponse_QNAME = new QName("http://ws.fcsotrasactividades.dedalus.cl/", "actividadSetResponse");
    private final static QName _DocactividadSet_QNAME = new QName("http://ws.fcsotrasactividades.dedalus.cl/", "docactividadSet");
    private final static QName _OtrasactivGet_QNAME = new QName("http://ws.fcsotrasactividades.dedalus.cl/", "otrasactivGet");
    private final static QName _ActividadSet_QNAME = new QName("http://ws.fcsotrasactividades.dedalus.cl/", "actividadSet");
    private final static QName _DocumentoGet_QNAME = new QName("http://ws.fcsotrasactividades.dedalus.cl/", "documentoGet");
    private final static QName _DocenteGetResponse_QNAME = new QName("http://ws.fcsotrasactividades.dedalus.cl/", "docenteGetResponse");
    private final static QName _DocumentoGetResponse_QNAME = new QName("http://ws.fcsotrasactividades.dedalus.cl/", "documentoGetResponse");
    private final static QName _ActividadesGetResponse_QNAME = new QName("http://ws.fcsotrasactividades.dedalus.cl/", "actividadesGetResponse");
    private final static QName _OtrasactivGetResponse_QNAME = new QName("http://ws.fcsotrasactividades.dedalus.cl/", "otrasactivGetResponse");
    private final static QName _ActividadesGet_QNAME = new QName("http://ws.fcsotrasactividades.dedalus.cl/", "actividadesGet");
    private final static QName _DocactividadSetResponse_QNAME = new QName("http://ws.fcsotrasactividades.dedalus.cl/", "docactividadSetResponse");
    private final static QName _DocenteGet_QNAME = new QName("http://ws.fcsotrasactividades.dedalus.cl/", "docenteGet");
    private final static QName _TerminoactividadUpdResponse_QNAME = new QName("http://ws.fcsotrasactividades.dedalus.cl/", "terminoactividadUpdResponse");
    private final static QName _DocactividadSetContDocumento_QNAME = new QName("http://ws.fcsotrasactividades.dedalus.cl/", "contDocumento");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cl.dedalus.fcsotrasactividades.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ActividadesGetResponse }
     * 
     */
    public ActividadesGetResponse createActividadesGetResponse() {
        return new ActividadesGetResponse();
    }

    /**
     * Create an instance of {@link OtrasactivGetResponse }
     * 
     */
    public OtrasactivGetResponse createOtrasactivGetResponse() {
        return new OtrasactivGetResponse();
    }

    /**
     * Create an instance of {@link ActividadesGet }
     * 
     */
    public ActividadesGet createActividadesGet() {
        return new ActividadesGet();
    }

    /**
     * Create an instance of {@link DocactividadSetResponse }
     * 
     */
    public DocactividadSetResponse createDocactividadSetResponse() {
        return new DocactividadSetResponse();
    }

    /**
     * Create an instance of {@link DocenteGet }
     * 
     */
    public DocenteGet createDocenteGet() {
        return new DocenteGet();
    }

    /**
     * Create an instance of {@link TerminoactividadUpdResponse }
     * 
     */
    public TerminoactividadUpdResponse createTerminoactividadUpdResponse() {
        return new TerminoactividadUpdResponse();
    }

    /**
     * Create an instance of {@link TerminoactividadUpd }
     * 
     */
    public TerminoactividadUpd createTerminoactividadUpd() {
        return new TerminoactividadUpd();
    }

    /**
     * Create an instance of {@link ActividadSetResponse }
     * 
     */
    public ActividadSetResponse createActividadSetResponse() {
        return new ActividadSetResponse();
    }

    /**
     * Create an instance of {@link DocactividadSet }
     * 
     */
    public DocactividadSet createDocactividadSet() {
        return new DocactividadSet();
    }

    /**
     * Create an instance of {@link OtrasactivGet }
     * 
     */
    public OtrasactivGet createOtrasactivGet() {
        return new OtrasactivGet();
    }

    /**
     * Create an instance of {@link ActividadSet }
     * 
     */
    public ActividadSet createActividadSet() {
        return new ActividadSet();
    }

    /**
     * Create an instance of {@link DocumentoGetResponse }
     * 
     */
    public DocumentoGetResponse createDocumentoGetResponse() {
        return new DocumentoGetResponse();
    }

    /**
     * Create an instance of {@link DocumentoGet }
     * 
     */
    public DocumentoGet createDocumentoGet() {
        return new DocumentoGet();
    }

    /**
     * Create an instance of {@link DocenteGetResponse }
     * 
     */
    public DocenteGetResponse createDocenteGetResponse() {
        return new DocenteGetResponse();
    }

    /**
     * Create an instance of {@link TerminoactividadUpdResult }
     * 
     */
    public TerminoactividadUpdResult createTerminoactividadUpdResult() {
        return new TerminoactividadUpdResult();
    }

    /**
     * Create an instance of {@link DocumentoGetResult }
     * 
     */
    public DocumentoGetResult createDocumentoGetResult() {
        return new DocumentoGetResult();
    }

    /**
     * Create an instance of {@link OtrasactivGetResult }
     * 
     */
    public OtrasactivGetResult createOtrasactivGetResult() {
        return new OtrasactivGetResult();
    }

    /**
     * Create an instance of {@link ActividadSetResult }
     * 
     */
    public ActividadSetResult createActividadSetResult() {
        return new ActividadSetResult();
    }

    /**
     * Create an instance of {@link OtrasactivGetActiv }
     * 
     */
    public OtrasactivGetActiv createOtrasactivGetActiv() {
        return new OtrasactivGetActiv();
    }

    /**
     * Create an instance of {@link DocenteGetResult }
     * 
     */
    public DocenteGetResult createDocenteGetResult() {
        return new DocenteGetResult();
    }

    /**
     * Create an instance of {@link ActividadesGetActiv }
     * 
     */
    public ActividadesGetActiv createActividadesGetActiv() {
        return new ActividadesGetActiv();
    }

    /**
     * Create an instance of {@link DocenteGetAsig }
     * 
     */
    public DocenteGetAsig createDocenteGetAsig() {
        return new DocenteGetAsig();
    }

    /**
     * Create an instance of {@link ActividadesGetResult }
     * 
     */
    public ActividadesGetResult createActividadesGetResult() {
        return new ActividadesGetResult();
    }

    /**
     * Create an instance of {@link DocactividadSetResult }
     * 
     */
    public DocactividadSetResult createDocactividadSetResult() {
        return new DocactividadSetResult();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsotrasactividades.dedalus.cl/", name = "faultInfo")
    public JAXBElement<String> createFaultInfo(String value) {
        return new JAXBElement<String>(_FaultInfo_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TerminoactividadUpd }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsotrasactividades.dedalus.cl/", name = "terminoactividadUpd")
    public JAXBElement<TerminoactividadUpd> createTerminoactividadUpd(TerminoactividadUpd value) {
        return new JAXBElement<TerminoactividadUpd>(_TerminoactividadUpd_QNAME, TerminoactividadUpd.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActividadSetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsotrasactividades.dedalus.cl/", name = "actividadSetResponse")
    public JAXBElement<ActividadSetResponse> createActividadSetResponse(ActividadSetResponse value) {
        return new JAXBElement<ActividadSetResponse>(_ActividadSetResponse_QNAME, ActividadSetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocactividadSet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsotrasactividades.dedalus.cl/", name = "docactividadSet")
    public JAXBElement<DocactividadSet> createDocactividadSet(DocactividadSet value) {
        return new JAXBElement<DocactividadSet>(_DocactividadSet_QNAME, DocactividadSet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OtrasactivGet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsotrasactividades.dedalus.cl/", name = "otrasactivGet")
    public JAXBElement<OtrasactivGet> createOtrasactivGet(OtrasactivGet value) {
        return new JAXBElement<OtrasactivGet>(_OtrasactivGet_QNAME, OtrasactivGet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActividadSet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsotrasactividades.dedalus.cl/", name = "actividadSet")
    public JAXBElement<ActividadSet> createActividadSet(ActividadSet value) {
        return new JAXBElement<ActividadSet>(_ActividadSet_QNAME, ActividadSet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocumentoGet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsotrasactividades.dedalus.cl/", name = "documentoGet")
    public JAXBElement<DocumentoGet> createDocumentoGet(DocumentoGet value) {
        return new JAXBElement<DocumentoGet>(_DocumentoGet_QNAME, DocumentoGet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocenteGetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsotrasactividades.dedalus.cl/", name = "docenteGetResponse")
    public JAXBElement<DocenteGetResponse> createDocenteGetResponse(DocenteGetResponse value) {
        return new JAXBElement<DocenteGetResponse>(_DocenteGetResponse_QNAME, DocenteGetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocumentoGetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsotrasactividades.dedalus.cl/", name = "documentoGetResponse")
    public JAXBElement<DocumentoGetResponse> createDocumentoGetResponse(DocumentoGetResponse value) {
        return new JAXBElement<DocumentoGetResponse>(_DocumentoGetResponse_QNAME, DocumentoGetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActividadesGetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsotrasactividades.dedalus.cl/", name = "actividadesGetResponse")
    public JAXBElement<ActividadesGetResponse> createActividadesGetResponse(ActividadesGetResponse value) {
        return new JAXBElement<ActividadesGetResponse>(_ActividadesGetResponse_QNAME, ActividadesGetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OtrasactivGetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsotrasactividades.dedalus.cl/", name = "otrasactivGetResponse")
    public JAXBElement<OtrasactivGetResponse> createOtrasactivGetResponse(OtrasactivGetResponse value) {
        return new JAXBElement<OtrasactivGetResponse>(_OtrasactivGetResponse_QNAME, OtrasactivGetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActividadesGet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsotrasactividades.dedalus.cl/", name = "actividadesGet")
    public JAXBElement<ActividadesGet> createActividadesGet(ActividadesGet value) {
        return new JAXBElement<ActividadesGet>(_ActividadesGet_QNAME, ActividadesGet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocactividadSetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsotrasactividades.dedalus.cl/", name = "docactividadSetResponse")
    public JAXBElement<DocactividadSetResponse> createDocactividadSetResponse(DocactividadSetResponse value) {
        return new JAXBElement<DocactividadSetResponse>(_DocactividadSetResponse_QNAME, DocactividadSetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocenteGet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsotrasactividades.dedalus.cl/", name = "docenteGet")
    public JAXBElement<DocenteGet> createDocenteGet(DocenteGet value) {
        return new JAXBElement<DocenteGet>(_DocenteGet_QNAME, DocenteGet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TerminoactividadUpdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsotrasactividades.dedalus.cl/", name = "terminoactividadUpdResponse")
    public JAXBElement<TerminoactividadUpdResponse> createTerminoactividadUpdResponse(TerminoactividadUpdResponse value) {
        return new JAXBElement<TerminoactividadUpdResponse>(_TerminoactividadUpdResponse_QNAME, TerminoactividadUpdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsotrasactividades.dedalus.cl/", name = "contDocumento", scope = DocactividadSet.class)
    public JAXBElement<byte[]> createDocactividadSetContDocumento(byte[] value) {
        return new JAXBElement<byte[]>(_DocactividadSetContDocumento_QNAME, byte[].class, DocactividadSet.class, ((byte[]) value));
    }

}
