
package cl.dedalus.fcsotrasactividades.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para OtrasactivGetActiv complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OtrasactivGetActiv">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cantHoras" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantSemana" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descActividad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="estdActividad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechActividad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechTermino" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="idenActividad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OtrasactivGetActiv", propOrder = {
    "cantHoras",
    "cantSemana",
    "descActividad",
    "estdActividad",
    "fechActividad",
    "fechTermino",
    "id",
    "idenActividad"
})
public class OtrasactivGetActiv {

    protected String cantHoras;
    protected String cantSemana;
    protected String descActividad;
    protected String estdActividad;
    protected String fechActividad;
    protected String fechTermino;
    protected long id;
    protected String idenActividad;

    /**
     * Obtiene el valor de la propiedad cantHoras.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantHoras() {
        return cantHoras;
    }

    /**
     * Define el valor de la propiedad cantHoras.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantHoras(String value) {
        this.cantHoras = value;
    }

    /**
     * Obtiene el valor de la propiedad cantSemana.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantSemana() {
        return cantSemana;
    }

    /**
     * Define el valor de la propiedad cantSemana.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantSemana(String value) {
        this.cantSemana = value;
    }

    /**
     * Obtiene el valor de la propiedad descActividad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescActividad() {
        return descActividad;
    }

    /**
     * Define el valor de la propiedad descActividad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescActividad(String value) {
        this.descActividad = value;
    }

    /**
     * Obtiene el valor de la propiedad estdActividad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstdActividad() {
        return estdActividad;
    }

    /**
     * Define el valor de la propiedad estdActividad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstdActividad(String value) {
        this.estdActividad = value;
    }

    /**
     * Obtiene el valor de la propiedad fechActividad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechActividad() {
        return fechActividad;
    }

    /**
     * Define el valor de la propiedad fechActividad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechActividad(String value) {
        this.fechActividad = value;
    }

    /**
     * Obtiene el valor de la propiedad fechTermino.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechTermino() {
        return fechTermino;
    }

    /**
     * Define el valor de la propiedad fechTermino.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechTermino(String value) {
        this.fechTermino = value;
    }

    /**
     * Obtiene el valor de la propiedad id.
     * 
     */
    public long getId() {
        return id;
    }

    /**
     * Define el valor de la propiedad id.
     * 
     */
    public void setId(long value) {
        this.id = value;
    }

    /**
     * Obtiene el valor de la propiedad idenActividad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdenActividad() {
        return idenActividad;
    }

    /**
     * Define el valor de la propiedad idenActividad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdenActividad(String value) {
        this.idenActividad = value;
    }

}
