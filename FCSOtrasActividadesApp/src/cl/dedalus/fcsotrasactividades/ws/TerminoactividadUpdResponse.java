
package cl.dedalus.fcsotrasactividades.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para terminoactividadUpdResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="terminoactividadUpdResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="terminoactividadUpdResult" type="{http://ws.fcsotrasactividades.dedalus.cl/}TerminoactividadUpdResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "terminoactividadUpdResponse", propOrder = {
    "terminoactividadUpdResult"
})
public class TerminoactividadUpdResponse {

    protected TerminoactividadUpdResult terminoactividadUpdResult;

    /**
     * Obtiene el valor de la propiedad terminoactividadUpdResult.
     * 
     * @return
     *     possible object is
     *     {@link TerminoactividadUpdResult }
     *     
     */
    public TerminoactividadUpdResult getTerminoactividadUpdResult() {
        return terminoactividadUpdResult;
    }

    /**
     * Define el valor de la propiedad terminoactividadUpdResult.
     * 
     * @param value
     *     allowed object is
     *     {@link TerminoactividadUpdResult }
     *     
     */
    public void setTerminoactividadUpdResult(TerminoactividadUpdResult value) {
        this.terminoactividadUpdResult = value;
    }

}
