
package cl.dedalus.fcsotrasactividades.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para actividadSetResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="actividadSetResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="actividadSetResult" type="{http://ws.fcsotrasactividades.dedalus.cl/}ActividadSetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "actividadSetResponse", propOrder = {
    "actividadSetResult"
})
public class ActividadSetResponse {

    protected ActividadSetResult actividadSetResult;

    /**
     * Obtiene el valor de la propiedad actividadSetResult.
     * 
     * @return
     *     possible object is
     *     {@link ActividadSetResult }
     *     
     */
    public ActividadSetResult getActividadSetResult() {
        return actividadSetResult;
    }

    /**
     * Define el valor de la propiedad actividadSetResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ActividadSetResult }
     *     
     */
    public void setActividadSetResult(ActividadSetResult value) {
        this.actividadSetResult = value;
    }

}
