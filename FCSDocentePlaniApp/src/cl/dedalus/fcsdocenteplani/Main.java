package cl.dedalus.fcsdocenteplani;

import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;

import cl.dedalus.fcsdocenteplani.ws.DocenteGetAsig;
import cl.dedalus.fcsdocenteplani.ws.DocenteGetResult;
import cl.dedalus.fcsdocenteplani.ws.DocentesasignaturaGetDocente;
import cl.dedalus.fcsdocenteplani.ws.DocentesasignaturaGetResult;
import cl.dedalus.fcsdocenteplani.ws.DocumentoGetResult;
import cl.dedalus.fcsdocenteplani.ws.FcsdocenteplaniWeb;
import cl.dedalus.fcsdocenteplani.ws.FcsdocenteplaniWebException;
import cl.dedalus.fcsdocenteplani.ws.FcsdocenteplaniWebService;
import cl.dedalus.fcsdocenteplani.ws.HorasdocenteGetResult;
import cl.dedalus.fcsdocenteplani.ws.XimprimirGetResult;
import cl.obcom.desktopfx.core.DesktopTask;
import cl.obcom.desktopfx.jfx.Dialog;
import cl.obcom.desktopfx.jfx.DialogAction;
import cl.obcom.desktopfx.jfx.FxmlPane;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.util.Callback;

public class Main extends FxmlPane
{
    @FXML TableView<asignatura> tb_asignaturas = new TableView<>();
    @FXML TableView<docente> tb_docentes = new TableView<>();
    @FXML private Slider sl_prep_ht;
    @FXML private Slider sl_corr_ht;
    @FXML private Slider sl_prep_hl;
    @FXML private Slider sl_corr_hl;
    @FXML private Slider sl_corr_in;
    @FXML private TextField tx_codigo;
    @FXML private TextField tx_facultad;
    @FXML private TextField tx_instituto;
    @FXML private TextField tx_nombre;
    @FXML private TextField tx_rut;
    @FXML private TextField tx_email;
    @FXML private TextField tx_horas;
    @FXML private TextField tx_total_c;
    @FXML private TextField tx_total_p;
    @FXML private TextField tx_ht;
    @FXML private TextField tx_hp;
    @FXML private TextField tx_hl;
    @FXML private TextField tx_pht;
    @FXML private TextField tx_net;
    @FXML private TextField tx_het;
    @FXML private TextField tx_hct;
    @FXML private TextField tx_nel;
    @FXML private TextField tx_hel;
    @FXML private TextField tx_hcl;
    @FXML private TextField tx_nil;
    @FXML private TextField tx_hci;
    @FXML private TextField tx_tota_hr_teo;
    @FXML private TextField tx_tota_hr_lab;
    @FXML private TextField tx_hrs_semana;
    @FXML private TextField tx_hrs_semestre;
    @FXML private TextField tx_grupos_teo;
    @FXML private TextField tx_grupos_lab;
    @FXML private TextField tx_ht_s;
    @FXML private TextField tx_hp_s;
    @FXML private TextField tx_hl_s;
    @FXML private TextField tx_alum_teo;
    @FXML private TextField tx_alum_lab;
    @FXML private Label lb_semanas;
    @FXML private Label lb_usuario;
    @FXML private Label lb_asignatura;
    @FXML private Button bo_salir;
    @FXML private Button bo_imprimir;
    @FXML private Button bo_subir_pgma;
    @FXML private Button bo_imp_docente;
    @FXML private Button bo_terminar;
    @FXML private Pane paneResponsable;

    final FcsdocenteplaniWeb port;

    final ObservableList<asignatura> dataAsignatura = FXCollections.observableArrayList ();
    final ObservableList<docente> dataDocente = FXCollections.observableArrayList ();

    public Main(final DesktopTask task) throws Exception
    {
        // Se carga fxml
        loadFxml();
        task.getStage().getScene().getStylesheets().add("resources/css/NuevoEstilo.css");

        final Callback<TableColumn<docente,String>, TableCell<docente,String>> cellFactory_3 =
            p -> new EditingCell_3();
        
        //Inicializar WS
        final FcsdocenteplaniWebService service = new FcsdocenteplaniWebService();
        port = service.getFcsdocenteplaniWebPort();
        task.initWebServicePort(port,service,task.getCodeBase());
       
        //PaneResponsable
        limpiarCampos();
        
        //Init Grillas tb_asignatura
        final TableColumn<asignatura, String> tc_codi_asig = new TableColumn<>("Codigo");
        tc_codi_asig.setCellValueFactory(new PropertyValueFactory<asignatura, String>("codi_asig"));
        tb_asignaturas.getColumns().add(tc_codi_asig);
        final TableColumn<asignatura, String> tc_desc_asig = new TableColumn<>("Descripcion");
        tc_desc_asig.setCellValueFactory(new PropertyValueFactory<asignatura, String>("desc_asig"));
        tb_asignaturas.getColumns().add(tc_desc_asig);
        final TableColumn<asignatura, String> tc_codi_seme = new TableColumn<>("Sem");
        tc_codi_seme.setCellValueFactory(new PropertyValueFactory<asignatura, String>("codi_seme"));
        tb_asignaturas.getColumns().add(tc_codi_seme);
        final TableColumn<asignatura, String> tc_codi_agno = new TableColumn<>("Año");
        tc_codi_agno.setCellValueFactory(new PropertyValueFactory<asignatura, String>("codi_agno"));
        tb_asignaturas.getColumns().add(tc_codi_agno);
        final TableColumn<asignatura, String> tc_flag_resp = new TableColumn<>("Coord");
        tc_flag_resp.setCellValueFactory(new PropertyValueFactory<asignatura, String>("flag_resp"));
        tb_asignaturas.getColumns().add(tc_flag_resp);
        final TableColumn<asignatura, String> tc_flag_prim = new TableColumn<>("Prim");
        tc_flag_prim.setCellValueFactory(new PropertyValueFactory<asignatura, String>("flag_prim"));
        tb_asignaturas.getColumns().add(tc_flag_prim);
        final TableColumn<asignatura, String> tc_cant_sem = new TableColumn<>("Sema");
        tc_cant_sem.setCellValueFactory(new PropertyValueFactory<asignatura, String>("cant_sem"));
        tb_asignaturas.getColumns().add(tc_cant_sem);
        final TableColumn<asignatura, String> tc_cant_hr_sema = new TableColumn<>("H.Sema");
        tc_cant_hr_sema.setCellValueFactory(new PropertyValueFactory<asignatura, String>("cant_hr_sema"));
        tb_asignaturas.getColumns().add(tc_cant_hr_sema);
        final TableColumn<asignatura, String> tc_cant_hr_seme = new TableColumn<>("H.Seme");
        tc_cant_hr_seme.setCellValueFactory(new PropertyValueFactory<asignatura, String>("cant_hr_seme"));
        tb_asignaturas.getColumns().add(tc_cant_hr_seme);
        final TableColumn<asignatura, String> tc_flag_prog = new TableColumn<>("Prog");
        tc_flag_prog.setCellValueFactory(new PropertyValueFactory<asignatura, String>("flag_prog"));
        tb_asignaturas.getColumns().add(tc_flag_prog);
        final TableColumn<asignatura, String> tc_flag_term = new TableColumn<>("Plani");
        tc_flag_term.setCellValueFactory(new PropertyValueFactory<asignatura, String>("flag_termino"));
        tb_asignaturas.getColumns().add(tc_flag_term);
        tb_asignaturas.setItems(dataAsignatura);
        tc_codi_asig.prefWidthProperty().bind(tb_asignaturas.widthProperty().multiply(0.13));
        tc_desc_asig.prefWidthProperty().bind(tb_asignaturas.widthProperty().multiply(0.20));
        tc_codi_seme.prefWidthProperty().bind(tb_asignaturas.widthProperty().multiply(0.07));
        tc_codi_agno.prefWidthProperty().bind(tb_asignaturas.widthProperty().multiply(0.07));
        tc_flag_resp.prefWidthProperty().bind(tb_asignaturas.widthProperty().multiply(0.07));
        tc_flag_prim.prefWidthProperty().bind(tb_asignaturas.widthProperty().multiply(0.07));
        tc_cant_sem.prefWidthProperty().bind(tb_asignaturas.widthProperty().multiply(0.07));
        tc_cant_hr_sema.prefWidthProperty().bind(tb_asignaturas.widthProperty().multiply(0.08));
        tc_cant_hr_seme.prefWidthProperty().bind(tb_asignaturas.widthProperty().multiply(0.08));
        tc_flag_prog.prefWidthProperty().bind(tb_asignaturas.widthProperty().multiply(0.07));
        tc_flag_term.prefWidthProperty().bind(tb_asignaturas.widthProperty().multiply(0.07));

        //tabla de docentes
        final TableColumn<docente, String> cCodigo = new TableColumn<>("Username");
        cCodigo.setCellValueFactory(new PropertyValueFactory<docente, String>("codi_docente"));
        tb_docentes.getColumns().add(cCodigo);
        //Horas Docencia==============================================================
        final TableColumn<docente, String> cPdocencia = new TableColumn<>("%D");
        cPdocencia.setCellValueFactory(new PropertyValueFactory<docente, String>("porc_doc"));
        cPdocencia.setCellFactory(cellFactory_3);
        cPdocencia.setOnEditCommit(
            t -> {
                t.getRowValue().setPorc_doc(t.getNewValue());
                final int v_hrs_doc = (int) Math.round(Integer.parseInt(t.getNewValue()) * Integer.parseInt(tx_total_c.getText()) / 100.0);
                t.getRowValue().setHora_doc(Integer.toString(v_hrs_doc));
                calculaHorasTotales();
                // id, % teo, hrs teo, % lab, hrs lab
                updateHorasDocente(t.getRowValue().getId(), t.getRowValue().getPorc_teo(), t.getRowValue().getHora_teo()
                        , t.getRowValue().getPorc_lab() , t.getRowValue().getHora_lab() 
                        , t.getNewValue() , Integer.toString(v_hrs_doc) );
                updateHorasEval();
            });
        tb_docentes.getColumns().add(cPdocencia);        
        final TableColumn<docente, String> cHdocencia = new TableColumn<>("H Doc");
        cHdocencia.setCellValueFactory(new PropertyValueFactory<docente, String>("hora_doc"));
        tb_docentes.getColumns().add(cHdocencia);
        
        //====Horas de Evaluaciones Teoricas
        final TableColumn<docente, String> cPteoria = new TableColumn<>("%T");
        cPteoria.setCellValueFactory(new PropertyValueFactory<docente, String>("porc_teo"));
        cPteoria.setCellFactory(cellFactory_3);
        cPteoria.setOnEditCommit(
            t -> {
                t.getRowValue().setPorc_teo(t.getNewValue());
                final int v_hrs_teo = (int) Math.round(Integer.parseInt(t.getNewValue()) * Integer.parseInt(tx_tota_hr_teo.getText()) / 100.0);
                t.getRowValue().setHora_teo(Integer.toString(v_hrs_teo));
                calculaHorasTotales();
                // id, % teo, hrs teo, % lab, hrs lab
                updateHorasDocente(t.getRowValue().getId(), t.getNewValue(), Integer.toString(v_hrs_teo)
                        , t.getRowValue().getPorc_lab() , t.getRowValue().getHora_lab()
                        , t.getRowValue().getPorc_doc() , t.getRowValue().getHora_doc() );
                updateHorasEval();
            });
        tb_docentes.getColumns().add(cPteoria);
        final TableColumn<docente, String> cHteoria = new TableColumn<>("H Teo");
        cHteoria.setCellValueFactory(new PropertyValueFactory<docente, String>("hora_teo"));
        tb_docentes.getColumns().add(cHteoria);
        
        //====Horas de Evaluaciones Laboratorio
        final TableColumn<docente, String> cPlabo = new TableColumn<>("%L");
        cPlabo.setCellValueFactory(new PropertyValueFactory<docente, String>("porc_lab"));
        cPlabo.setCellFactory(cellFactory_3);
        cPlabo.setOnEditCommit(
            t -> {
                t.getRowValue().setPorc_lab(t.getNewValue());
                final int v_hrs_lab = (int) Math.round(Integer.parseInt(t.getNewValue()) * Integer.parseInt(tx_tota_hr_lab.getText()) / 100.0);
                t.getRowValue().setHora_lab(Integer.toString(v_hrs_lab));
                calculaHorasTotales();
                // id, % teo, hrs teo, % lab, hrs lab
                updateHorasDocente(t.getRowValue().getId(), t.getRowValue().getPorc_teo(), t.getRowValue().getHora_teo()
                        , t.getNewValue(), Integer.toString(v_hrs_lab) 
                        , t.getRowValue().getPorc_doc() , t.getRowValue().getHora_doc() );
                updateHorasEval();
            });
        tb_docentes.getColumns().add(cPlabo);
        final TableColumn<docente, String> cHlabo = new TableColumn<>("H Lab");
        cHlabo.setCellValueFactory(new PropertyValueFactory<docente, String>("hora_lab"));
        tb_docentes.getColumns().add(cHlabo);
        //===Total
        final TableColumn<docente, String> cTotal = new TableColumn<>("Total");
        cTotal.setCellValueFactory(new PropertyValueFactory<docente, String>("tota_hrs"));
        tb_docentes.getColumns().add(cTotal);
       
        tb_docentes.setItems(dataDocente);
        cCodigo.prefWidthProperty().bind(tb_docentes.widthProperty().multiply(0.30));
        cPteoria.prefWidthProperty().bind(tb_docentes.widthProperty().multiply(0.10));
        cHteoria.prefWidthProperty().bind(tb_docentes.widthProperty().multiply(0.10));
        cPlabo.prefWidthProperty().bind(tb_docentes.widthProperty().multiply(0.10));
        cHlabo.prefWidthProperty().bind(tb_docentes.widthProperty().multiply(0.10));
        cPdocencia.prefWidthProperty().bind(tb_docentes.widthProperty().multiply(0.10));
        cHdocencia.prefWidthProperty().bind(tb_docentes.widthProperty().multiply(0.10));
        cTotal.prefWidthProperty().bind(tb_docentes.widthProperty().multiply(0.10));

        //Click on tb_asignaturas
        tb_asignaturas.setOnMouseClicked(event -> {
        	if (tb_asignaturas.getSelectionModel().getSelectedIndex() < 0 ) { return; }
            final asignatura t = dataAsignatura.get(tb_asignaturas.getSelectionModel().getSelectedIndex());
            activaPanelSiDocenteEsResponsable(t);
            lb_semanas.setText(t.getCant_sem());
            
            cargaHorasDocente(t.getId(), tx_codigo.getText().trim(), t.getCodi_asig(), t.getCodi_seme(), t.getCodi_agno(), t.getFlag_prim());
            cargaDocentesAsignatura(t.getCodi_asig(),t.getCodi_seme(),t.getCodi_agno());
            calculaHorasTotales();
        });

        //Doble Click en tb_asignaturas para ver el programa
        tb_asignaturas.setOnMousePressed(event -> {
            if (event.isPrimaryButtonDown() && event.getClickCount() == 2) {
                verDocumento(task);
            }
        });

        //lostfocus de tx_net ( nmro evaluaciones teoricas )
        tx_net.focusedProperty().addListener((ChangeListener<Boolean>) (arg0, oldPropertyValue, newPropertyValue) -> {
            if (!newPropertyValue)
            {
                final int v_net=Integer.valueOf(tx_net.getText());  // nmro de eval teo
                final int v_het = ((int) sl_prep_ht.getValue()) * v_net;  // Tiempo(slider) x numero de eval teo
                tx_het.setText(Integer.toString(v_het));              
                int v_hct = v_net * Integer.parseInt(tx_alum_teo.getText()) * ((int) sl_corr_ht.getValue());
                v_hct = (int)Math.round( v_hct*1.0/60 );           // total minutos / 60 = horas
                tx_hct.setText(Integer.toString(v_hct));
                calculaHorasTotales();
            }
        });

        //lostfocus de tx_net ( nmro evaluaciones laboratorio )
        tx_nel.focusedProperty().addListener((ChangeListener<Boolean>) (arg0, oldPropertyValue, newPropertyValue) -> {
            if (!newPropertyValue)
            {
                final int v_nel=Integer.valueOf(tx_nel.getText());  // nmro de eval lab
                final int v_hel = ((int) sl_prep_hl.getValue()) * v_nel;  // Tiempo(slider) x numero de eval lab
                tx_hel.setText(Integer.toString(v_hel));
                int v_hcl = v_nel * Integer.parseInt(tx_alum_lab.getText()) * ((int) sl_corr_hl.getValue());
                v_hcl = (int) Math.round( v_hcl*1.0/60 );         // total minutos / 60 = horas
                tx_hcl.setText(Integer.toString(v_hcl));
                calculaHorasTotales();
            }
        });

        //lostfocus de tx_nil ( nmro informes laboratorio )
        tx_nil.focusedProperty().addListener((ChangeListener<Boolean>) (arg0, oldPropertyValue, newPropertyValue) -> {
            if (!newPropertyValue)
            {
                final int v_nil=Integer.valueOf(tx_nil.getText());  // nmro de informes
                int v_hci = v_nil * Integer.parseInt(tx_alum_lab.getText()) * ((int) sl_corr_in.getValue());
                v_hci = (int)Math.round( v_hci*1.0/60 );     // total minutos / 60 = horas
                tx_hci.setText(Integer.toString(v_hci));
                calculaHorasTotales();
            }
        });

        //Slider para tiempo de preparacion de eval. teo. ( 2hrs - 5hrs )
        inicializarSlider(sl_prep_ht, 2 , 5, 2 );
        sl_prep_ht.valueProperty().addListener((ChangeListener<Number>) (ov, old_val, new_val) -> {
            tx_het.setText(String.format("%d", Integer.valueOf(tx_net.getText()) * new_val.intValue()));
            calculaHorasTotales();
         });

        ////Slider para tiempo de correccion de 1 eval. teo. ( 5min - 10min )
        inicializarSlider(sl_corr_ht, 5 , 10, 5 );
        sl_corr_ht.valueProperty().addListener((ChangeListener<Number>) (ov, old_val, new_val) -> {
            final int v_net=Integer.valueOf(tx_net.getText());  // nmro de eval teo
            int v_total=v_net * Integer.valueOf(tx_alum_teo.getText()) * new_val.intValue() ;
            v_total = (int) Math.round(v_total*1.0/60);  // Horas = minutos/60
            tx_hct.setText(String.format("%d", v_total));
            calculaHorasTotales();
        });

        //Slider para tiempo de preparacion de eval. lab. ( 2hrs - 5hrs )
        inicializarSlider(sl_prep_hl, 2 , 5, 2 );
        sl_prep_hl.valueProperty().addListener((ChangeListener<Number>) (ov, old_val, new_val) -> {
            tx_hel.setText(String.format("%d", Integer.valueOf(tx_nel.getText()) * new_val.intValue()));
            calculaHorasTotales();
        });

        ////Slider para tiempo de correccion de 1 eval. lab. ( 5min - 10min )
        inicializarSlider(sl_corr_hl, 5 , 10, 5);
        sl_corr_hl.valueProperty().addListener((ChangeListener<Number>) (ov, old_val, new_val) -> {
            final int v_nel=Integer.valueOf(tx_nel.getText());  // nmro de eval lab
            int v_total=v_nel * Integer.valueOf(tx_alum_lab.getText()) * new_val.intValue();
            v_total = (int) Math.round(v_total*1.0/60);       //Horas = Min/60
            tx_hcl.setText(String.format("%d", v_total ));
            calculaHorasTotales();
        });

        ////Slider para tiempo de correccion de 1 inf. lab. ( 5min - 10min )
        inicializarSlider(sl_corr_in, 5 , 10, 5);
        sl_corr_in.valueProperty().addListener((ChangeListener<Number>) (ov, old_val, new_val) -> {
            final int v_nil=Integer.valueOf(tx_nil.getText());  // nmro de informes
            int v_total=v_nil * Integer.valueOf(tx_alum_lab.getText()) * new_val.intValue();
            v_total = (int) Math.round(v_total*1.0/60);
            tx_hci.setText(String.format("%d", v_total));      //total en horas
            calculaHorasTotales();
        });
        
        cargaDatosDocente(task.getUser().getCode().trim());
        cargaToolTip();

        bo_salir.setOnAction(evt -> task.terminate(true));
        bo_imprimir.setOnAction(evt -> imprimirPrograma(task));
        bo_imp_docente.setOnAction(evt -> imprimirHrsDocente(task));
        bo_subir_pgma.setOnAction(evt -> ingresarPrograma(task));
        bo_terminar.setOnAction(evt -> updateTerminar(task));
    }
    
    public void imprimirHrsDocente(DesktopTask T){
 	   final DialogAction D=Dialog.showConfirm(this, "Esta seguro de generar el Informe ?");
       String v_cmd = null, v_file = null;
       String v_server=T.getCodeBase().toString().substring(7);          
       v_server=v_server.substring(0, v_server.indexOf(":"));                   
       
       if (D.toString() == "YES"){
    	   final asignatura t = dataAsignatura.get(tb_asignaturas.getSelectionModel().getSelectedIndex());
    	   v_cmd = "http://"+v_server+":8088/jasperserver/rest_v2/reports/reports/Fciencias/";
    	   v_cmd = v_cmd + "rep_docente.pdf?j_username=jasperadmin&j_password=jasperadmin&";
    	   v_cmd = v_cmd + "P_docente="+tx_codigo.getText().trim()+"&P_semestre="+t.getCodi_seme()+"&P_agnoasignatura="+t.getCodi_agno();
    	   v_file = T.getPluginDirectory().toString()+"/rep_docente_temp.pdf";
    	   
    	   //System.out.println("CMD:"+v_cmd);
    	   
           XimprimirGetResult r1 = null;
           byte[] P;
           try {
               //  Leo el .PDF (P) desde el procedimiento xinventarioget
        	   
               r1 = port.ximprimirGet(v_cmd);
               P = r1.getVReporte();
                                 
               //String ST= new String(P);
               //System.out.println("XXX->:"+P);
               //System.out.println("ST->:"+ST);
               //System.out.println("Largo->:"+P.length);

               // se define el archivo de salida para el .PDF
               OutputStream tt=null;
               try {
                   tt = new FileOutputStream(new File(v_file));
               } catch (final FileNotFoundException e) {
                   Dialog.showError(this, e);
               }

               // Escribo el contenido en el archivo .PDF
               try {
                   tt.write(P);
               } catch (final IOException e) {
                   Dialog.showError(this, e);
               }

               // Se abre el .PDF en el Cliente.
               final File myFile = new File(v_file);
               try {
                   Desktop.getDesktop().open(myFile);
               } catch (final IOException e) {
                   Dialog.showError(this, e);
               }

           } catch (final FcsdocenteplaniWebException e) {
               Dialog.showError(this, e);
           }
       }   // if=YES	
    }
    
    public void imprimirPrograma(DesktopTask T){
    	   final DialogAction D=Dialog.showConfirm(this, "Esta seguro de generar el Informe ?");
           String v_cmd = null, v_file = null;
           String v_server=T.getCodeBase().toString().substring(7);          
           v_server=v_server.substring(0, v_server.indexOf(":"));
           
           /*cl.obcom.desktopfx.core.Desktop TT;          
           try {
        	   System.out.println("Mail");
        	   TT.sendEmail("aldo.ulloa.c@gmail.com", "Hola", "Este es el mensaje");
           } catch (Exception e1) {
        	   Dialog.showError(this, e1);
           }*/                     
           
           if (D.toString() == "YES"){
        	   final asignatura t = dataAsignatura.get(tb_asignaturas.getSelectionModel().getSelectedIndex());
        	   v_cmd = "http://"+v_server+":8088/jasperserver/rest_v2/reports/reports/Fciencias/";
        	   v_cmd = v_cmd + "rep_asignatura.pdf?j_username=jasperadmin&j_password=jasperadmin&";
        	   v_cmd = v_cmd + "P_asignatura="+t.getCodi_asig()+"&P_semestre="+t.getCodi_seme()+"&P_agnoasignatura="+t.getCodi_agno();
        	   v_file = T.getPluginDirectory().toString()+"/rep_asignatura_temp.pdf";
        	   
               XimprimirGetResult r1 = null;
               byte[] P;
               try {
                   //  Leo el .PDF (P) desde el procedimiento xinventarioget
            	   
                   r1 = port.ximprimirGet(v_cmd);
                   P = r1.getVReporte();
                                     
                   //String ST= new String(P);
                   //System.out.println("XXX->:"+P);
                   //System.out.println("ST->:"+ST);
                   //System.out.println("Largo->:"+P.length);

                   // se define el archivo de salida para el .PDF
                   OutputStream tt=null;
                   try {
                       tt = new FileOutputStream(new File(v_file));
                   } catch (final FileNotFoundException e) {
                       Dialog.showError(this, e);
                   }

                   // Escribo el contenido en el archivo .PDF
                   try {
                       tt.write(P);
                   } catch (final IOException e) {
                       Dialog.showError(this, e);
                   }

                   // Se abre el .PDF en el Cliente.
                   final File myFile = new File(v_file);
                   try {
                       Desktop.getDesktop().open(myFile);
                   } catch (final IOException e) {
                       Dialog.showError(this, e);
                   }

               } catch (final FcsdocenteplaniWebException e) {
                   Dialog.showError(this, e);
               }
           }   // if=YES	
    }
    
    public void updateTerminar(DesktopTask T){
        DialogAction D;

        final asignatura t = dataAsignatura.get(tb_asignaturas.getSelectionModel().getSelectedIndex());
        D = Dialog.showConfirm(this, "Desea terminar la planificacion para asignatura:"+t.getCodi_asig()+" ?");
        if ( D.toString() == "YES" ) {
            try {
            	port.flagterminoUpd(t.getId());
                t.setFlag_termino("SI");
                //activaPanelSiDocenteEsResponsable(t);
                //limpiarCampos();
            } catch (final FcsdocenteplaniWebException e) {
                Dialog.showError(this,e);
            }
        }
    }

	private void activaPanelSiDocenteEsResponsable(final asignatura asignaturaSeleccionada) {
		if (asignaturaSeleccionada.getFlag_resp().equals("SI")) {
			paneResponsable.setDisable(false);
			tb_docentes.setDisable(false);
	        bo_subir_pgma.setDisable(false);
		} else {
			paneResponsable.setDisable(true);
			tb_docentes.setDisable(true);
	        bo_subir_pgma.setDisable(true);
		}
		/*if (asignaturaSeleccionada.getFlag_termino().equals("SI")) {
			paneResponsable.setDisable(true);
			//bo_terminar.setDisable(true);
			tb_docentes.setDisable(true);
		}else{
			paneResponsable.setDisable(false);
			//bo_terminar.setDisable(false);
			tb_docentes.setDisable(false);
		}*/
	}

    public void inicializarSlider(final Slider v_slider, final int v1, final int v2, final int v3){
        v_slider.setMin(v1); v_slider.setMax(v2);v_slider.setValue(v3);
        v_slider.setShowTickLabels(true); v_slider.setShowTickMarks(true); v_slider.setSnapToTicks(true);
        v_slider.setMajorTickUnit(1);
        v_slider.setMinorTickCount(0);
    }



    public void updateHorasDocente(final Long v_id, final String v_porc_teo, final String v_hrs_teo
    		, final String v_porc_lab, final String v_hrs_lab, String v_porc_doc, String v_hrs_doc){
        try {
            final String v_hr_semana   = tx_hrs_semana.getText().trim();
            final String v_hr_semestre = tx_hrs_semestre.getText().trim();
            port.horasdocenteUpd(v_id, v_porc_teo, v_hrs_teo, v_porc_lab, v_hrs_lab, v_porc_doc, v_hrs_doc, v_hr_semana, v_hr_semestre);
        } catch (final FcsdocenteplaniWebException e) {
            Dialog.showError(this,e);
        }
    }

    public void verDocumento(final DesktopTask T){
        final asignatura t = dataAsignatura.get(tb_asignaturas.getSelectionModel().getSelectedIndex());
        String v_archivo="";

        DocumentoGetResult res = null;
        byte[] P=null;

        try {
            res=port.documentoGet(t.getCodi_asig(), t.getCodi_seme(), t.getCodi_agno());
            P=res.getContPrograma();
            v_archivo=res.getNombArchivo();
        } catch (final NumberFormatException e) {
            Dialog.showError(this, e);
        } catch (final FcsdocenteplaniWebException e) {
            Dialog.showError(this, e);
        }
        final String v_file = T.getPluginDirectory().toString()+"/"+v_archivo;  // en /OBCOM creo el archivo

         //Se define archivo salida
        OutputStream tt=null;
        try {
            tt = new FileOutputStream(new File(v_file));
            try {
                tt.write(P);
            } catch (final IOException e) {
                Dialog.showError(this, e);
            }
        } catch (final FileNotFoundException e) {
            Dialog.showError(this, e);
        }

        // Se abre en el Cliente.
        final File myFile = new File(v_file);
        openFile(myFile);
    }

    public void openFile(final File myfile) {
        try {
            Desktop.getDesktop().open(myfile);
        } catch (final IOException ex) {
           Dialog.showError(this, ex);
        }
    }

    public void ingresarPrograma(final DesktopTask T){
        final FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        final File file = fileChooser.showOpenDialog(T.getPrimaryStage());
        byte[] ArchivoBytes = null;

        if (file != null) {
            final asignatura t = dataAsignatura.get(tb_asignaturas.getSelectionModel().getSelectedIndex());
            //System.out.println("Absolute Path:"+file.getAbsolutePath());
            //System.out.println("Name:"+file.getName());
            //System.out.println("Path"+file.getPath());

            //final byte[] ArchivoBytes = new byte[(int) file.length()];
            ArchivoBytes = new byte[(int) file.length()];
            try {
                final FileInputStream fileInputStream = new FileInputStream(file);
                try {
                    fileInputStream.read(ArchivoBytes);
                    fileInputStream.close();
                } catch (final IOException e) {
                    Dialog.showError(this, e);
                }
            } catch (final FileNotFoundException e) {
                Dialog.showError(this, e);
            }
            //-----El archivo esta en la variable ArchivoBytes,

            try {
                port.subirprogramaSet(t.getCodi_asig(), t.getCodi_seme(), t.getCodi_agno(), ArchivoBytes, file.getName());
                cargaDatosDocente(T.getUser().getCode().trim());
            } catch (final FcsdocenteplaniWebException e) {
                Dialog.showError(this,e);
            }

        }
    }

    public void cargaToolTip(){
        final Tooltip tooltip0 = new Tooltip();
        tooltip0.setText("Horas Teoricas por Semana.");
        tx_ht.setTooltip(tooltip0);

        final Tooltip tooltip1 = new Tooltip();
        tooltip1.setText("Horas Practicas por Semana.");
        tx_hp.setTooltip(tooltip1);

        final Tooltip tooltip2 = new Tooltip();
        tooltip2.setText("Horas Laboratorio por Semana.");
        tx_hl.setTooltip(tooltip2);

        final Tooltip tooltip3 = new Tooltip();
        tooltip3.setText("Horas Preparacion Horas Teoricas\n por Semestre.\nHT x 17 x (0.5 primerizo o por 1 ) ");
        tx_pht.setTooltip(tooltip3);

        final Tooltip tooltip4 = new Tooltip();
        tooltip4.setText("Cantidad de Evaluaciones Teoricas\n por Semestre");
        tx_net.setTooltip(tooltip4);

        final Tooltip tooltip5 = new Tooltip();
        tooltip5.setText("Horas de preparacion de las evaluaciones\n teoricas por semestre.");
        tx_het.setTooltip(tooltip5);

        final Tooltip tooltip6 = new Tooltip();
        tooltip6.setText("Horas de correccion de las evaluaciones\n teoricas por semestre.");
        tx_hct.setTooltip(tooltip6);

        final Tooltip tooltip7 = new Tooltip();
        tooltip7.setText("Cantidad de evaluaciones de laboratorio\n por semestre.");
        tx_nel.setTooltip(tooltip7);

        final Tooltip tooltip8 = new Tooltip();
        tooltip8.setText("Horas de preparacion de las evaluaciones de\n laboratorio por semestre.");
        tx_hel.setTooltip(tooltip8);

        final Tooltip tooltip9 = new Tooltip();
        tooltip9.setText("Horas de correccion de las evaluaciones de\n laboratorio por semestre.");
        tx_hcl.setTooltip(tooltip9);

        final Tooltip tooltip10 = new Tooltip();
        tooltip10.setText("Numero de informes en el semestre.");
        tx_nil.setTooltip(tooltip10);

        final Tooltip tooltip11 = new Tooltip();
        tooltip11.setText("Horas de correccion de las informes por semestre.");
        tx_hci.setTooltip(tooltip11);
        
        final Tooltip tooltip12 = new Tooltip();
        tooltip12.setText("Total Horas Teoricas por Semestre.");
        tx_ht_s.setTooltip(tooltip12);
    
        final Tooltip tooltip13 = new Tooltip();
        tooltip13.setText("Total de Horas Cronologicas por Semestre.");
        tx_total_c.setTooltip(tooltip13);
        
        final Tooltip tooltip14 = new Tooltip();
        tooltip14.setText("Total de Horas Pedagogicas por Semestre.");
        tx_total_p.setTooltip(tooltip13);
    }
    
    public void updateHorasEval(){
        final asignatura t = dataAsignatura.get(tb_asignaturas.getSelectionModel().getSelectedIndex());
        try {
            port.horasasignaturaUpd(t.getId(), tx_ht.getText().trim(), tx_hp.getText().trim(), tx_hl.getText().trim()
            , tx_ht_s.getText().trim(), tx_hp_s.getText().trim(), tx_hl_s.getText().trim()
            , tx_alum_teo.getText().trim(), tx_alum_lab.getText().trim()
            , tx_pht.getText().trim(), tx_net.getText().trim(), tx_het.getText().trim(), tx_hct.getText().trim()
            , tx_nel.getText().trim(), tx_hel.getText().trim(), tx_hcl.getText().trim()
            , tx_nil.getText().trim(), tx_hci.getText().trim()
            , tx_hrs_semana.getText().trim(), tx_hrs_semestre.getText().trim()
            , tx_tota_hr_teo.getText().trim(), tx_tota_hr_lab.getText().trim());
            //cargaDatosDocente(T.getUser().getCode().trim());
            //limpiarCampos();
        } catch (final FcsdocenteplaniWebException e) {
            Dialog.showError(this,e);
        }    	
    }

    public void updateHorasAsignatura(final DesktopTask T){
        DialogAction D;

        D = Dialog.showConfirm(this, "Desea actualizar horas de la Asignatura ?");
        if ( D.toString() == "YES" ) {
            updateHorasEval();
            Dialog.showMessage(this, "Horas actualizadas correctamente.");
        }
    }

    public void limpiarCampos(){
        paneResponsable.setDisable(true);
        //bo_terminar.setDisable(true);
        tb_docentes.setDisable(true);
        bo_subir_pgma.setDisable(true);
        tx_ht.setText("0");tx_hp.setText("0");tx_hl.setText("0");
        tx_ht_s.setText("0");tx_hp_s.setText("0");tx_hl_s.setText("0");
        tx_pht.setText("0");tx_net.setText("0");tx_het.setText("0");
        tx_hct.setText("0");tx_nel.setText("0");tx_hel.setText("0");
        tx_hcl.setText("0");tx_nil.setText("0");tx_hci.setText("0");
        tx_hrs_semana.setText("0");tx_hrs_semestre.setText("0");
        tx_tota_hr_teo.setText("0");tx_tota_hr_lab.setText("0");
        tx_alum_lab.setText("0");tx_alum_teo.setText("0");
        lb_semanas.setText("0");tx_total_c.setText("0");tx_total_p.setText("0");
        sl_prep_ht.setValue(2);
        sl_corr_ht.setValue(5);
        sl_prep_hl.setValue(2);
        sl_corr_hl.setValue(5);
        sl_corr_in.setValue(5);
    }

    public void calculaHorasTotales(){
        if (lb_semanas.equals("0")) { return; }
        int v_semestre = 0;

        // Obtengo las horas de dedicacion a las Eval. Teo y Lab.
        for (final docente t : dataDocente){
            if (t.getCodi_docente().equals(tx_codigo.getText().trim())){
                v_semestre =  Integer.parseInt(t.getHora_teo()) + Integer.parseInt(t.getHora_lab())+Integer.parseInt(t.getHora_doc());
            }
        }
        final Double v_semana = Math.round( (v_semestre *1.0/ Integer.parseInt(lb_semanas.getText()))*10.0 ) / 10.0;
        final int v_teo=Integer.parseInt(tx_het.getText()) + Integer.parseInt(tx_hct.getText()) ;
        final int v_lab=Integer.parseInt(tx_hel.getText()) + Integer.parseInt(tx_hcl.getText()) + Integer.parseInt(tx_hci.getText());
        tx_tota_hr_teo.setText(Integer.toString(v_teo));
        tx_tota_hr_lab.setText(Integer.toString(v_lab));
        tx_hrs_semana.setText(Double.toString( v_semana ));
        tx_hrs_semestre.setText(Integer.toString( v_semestre ));
        final asignatura t = dataAsignatura.get(tb_asignaturas.getSelectionModel().getSelectedIndex());
        t.setCant_hr_sema(tx_hrs_semana.getText().trim());
        t.setCant_hr_seme(tx_hrs_semestre.getText().trim());
    }

    public void cargaHorasDocente(final Long v_id, final String v_doc, final String v_asig
                                      , final String v_seme, final String v_agno, final String v_prim){
        HorasdocenteGetResult res = null;
        try {
            res=port.horasdocenteGet(v_id ,v_doc ,v_asig ,v_seme ,v_agno);
            tx_ht.setText(res.getCantHrTeo());
            tx_hp.setText(res.getCantHrPra());
            tx_hl.setText(res.getCantHrLab());
            tx_ht_s.setText(res.getCantTeoSemestre());
            tx_hp_s.setText(res.getCantPraSemestre());
            tx_hl_s.setText(res.getCantLabSemestre());
            tx_pht.setText(res.getCantHrPrepHt());
            tx_total_c.setText(res.getCantTotalC());
            tx_total_p.setText(res.getCantTotalP());
            tx_net.setText(res.getCantEvalTeo());
            tx_het.setText(res.getCantHrEvalTeo());
            if (!res.getCantEvalTeo().equals("0") ) {
            	sl_prep_ht.setValue( Double.parseDouble(res.getCantHrEvalTeo()) / Double.parseDouble(res.getCantEvalTeo()) );
            } else {
            	sl_prep_ht.setValue(2);
            }
            tx_hct.setText(res.getCantHrCorrTeo());
            tx_nel.setText(res.getCantEvalLab());
            tx_hel.setText(res.getCantHrEvalLab());
            if (!res.getCantEvalLab().equals("0") ) {
            	sl_prep_hl.setValue( Double.parseDouble(res.getCantHrEvalLab()) / Double.parseDouble(res.getCantEvalLab()) );
            } else {
                sl_prep_hl.setValue(2);
            }
            tx_hcl.setText(res.getCantHrCorrLab());
            tx_nil.setText(res.getCantInformes());
            tx_hci.setText(res.getCantHrCorrInf());
            tx_tota_hr_teo.setText(res.getTotaHrTeo());
            tx_tota_hr_lab.setText(res.getTotaHrLab());
            tx_alum_teo.setText(res.getTotaAlumnosTeo());
            tx_alum_lab.setText(res.getTotaAlumnosLab());
            tx_grupos_teo.setText(res.getCantGruposTeo());
            tx_grupos_lab.setText(res.getCantGruposLab());
            
            double tota_hr_cor=Double.parseDouble(res.getCantHrCorrTeo());
            double cant_ev_teo=Double.parseDouble(res.getCantEvalTeo());
            double tota_al_teo=Double.parseDouble(res.getTotaAlumnosTeo());          
            if ((cant_ev_teo!=0) && (tota_al_teo!=0)){
            	sl_corr_ht.setValue( tota_hr_cor*60.0/(cant_ev_teo*tota_al_teo) );
            } else {
            	sl_corr_ht.setValue(5);
            }
            
            double tota_hr_lab=Double.parseDouble(res.getCantHrCorrLab());
            double cant_ev_lab=Double.parseDouble(res.getCantEvalLab());
            double tota_al_lab=Double.parseDouble(res.getTotaAlumnosLab());
            if ((cant_ev_lab!=0) && (tota_al_lab!=0)){
            	sl_corr_hl.setValue( tota_hr_lab*60.0/(cant_ev_lab*tota_al_lab) );
            } else {
                sl_corr_hl.setValue(5);
            }
            
            double tota_hr_inf=Double.parseDouble(res.getCantHrCorrInf());
            double cant_inf=Double.parseDouble(res.getCantInformes());
            double tota_al_inf=Double.parseDouble(res.getTotaAlumnosLab());
            if ((cant_inf!=0) && (tota_al_inf!=0)){
            	sl_corr_in.setValue( tota_hr_inf*60.0/(cant_inf*tota_al_inf) );
            } else {
                sl_corr_in.setValue(5);
            }
            
        } catch (final Exception e1) {
            Dialog.showError(this, e1);
        }

    }

    public void cargaDocentesAsignatura(final String v_asig,final String v_seme, final String v_agno){
        dataDocente.clear();
        lb_asignatura.setText("Asignatura :"+v_asig+"   Semestre:"+v_seme+"   Año:"+v_agno);
        DocentesasignaturaGetResult res = null;
        try {
            res=port.docentesasignaturaGet(v_asig, v_seme, v_agno);
            for (final DocentesasignaturaGetDocente p : res.getDocentes()){
                dataDocente.add(new docente(p.getId(), p.getCodiDocente(), p.getPorcTeo(), p.getHoraTeo()
                        , p.getPorcLab(), p.getHoraLab() ,p.getPorcDoc() ,p.getHoraDoc() , p.getTotaHrs() ));
            }
            tb_docentes.setItems(dataDocente);
        } catch (final FcsdocenteplaniWebException e) {
            Dialog.showError(this,e);
        }
    }

    public void cargaDatosDocente(final String v_codi_doc){
    	int v_year = Calendar.getInstance().get(Calendar.YEAR);
        int v_mes = Calendar.getInstance().get(Calendar.MONTH);
        String v_semestre;
        v_mes=v_mes+1;
        if ((v_mes>=1) && (v_mes<=6)){ v_semestre="1"; } 
        else {	v_semestre="2"; }	
    	dataAsignatura.clear();
        DocenteGetResult res = null;
        try {
            res=port.docenteGet(v_codi_doc, v_semestre, Integer.toString(v_year));
            lb_usuario.setText("Total Horas Usuario :"+v_codi_doc);
            tx_codigo.setText(v_codi_doc);
            tx_facultad.setText(res.getDescFac());
            tx_instituto.setText(res.getDescIns());
            tx_nombre.setText(res.getDescNom());
            tx_rut.setText(res.getDescRut());
            tx_email.setText(res.getDescEma());
            for (final DocenteGetAsig p : res.getAsigs()){
                dataAsignatura.add(new asignatura(p.getId(), p.getCodiAsignatura(), p.getDescAsignatura(), p.getCodiSeme(), p.getCodiAgno()
                                , p.getFlagResp(), p.getFlagPrim(), p.getCantSem(), p.getTotaHrSema(), p.getTotaHrSeme()
                                , p.getFlagProg() , p.getFlagTermino() ));
            }
            tb_asignaturas.setItems(dataAsignatura);
        } catch (final Exception e1) {
            Dialog.showError(this, e1);
        }
    }

} // Main - FXMPane
