
package cl.dedalus.fcsdocenteplani.ws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.FaultAction;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebService(name = "FcsdocenteplaniWeb", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface FcsdocenteplaniWeb {


    /**
     * 
     * @param codiSeme
     * @param codiAsig
     * @param codiAgno
     * @return
     *     returns cl.dedalus.fcsdocenteplani.ws.DocumentoGetResult
     * @throws FcsdocenteplaniWebException
     */
    @WebMethod
    @WebResult(name = "documentoGetResult", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
    @RequestWrapper(localName = "documentoGet", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/", className = "cl.dedalus.fcsdocenteplani.ws.DocumentoGet")
    @ResponseWrapper(localName = "documentoGetResponse", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/", className = "cl.dedalus.fcsdocenteplani.ws.DocumentoGetResponse")
    @Action(input = "http://ws.fcsdocenteplani.dedalus.cl/FcsdocenteplaniWeb/documentoGetRequest", output = "http://ws.fcsdocenteplani.dedalus.cl/FcsdocenteplaniWeb/documentoGetResponse", fault = {
        @FaultAction(className = FcsdocenteplaniWebException.class, value = "http://ws.fcsdocenteplani.dedalus.cl/FcsdocenteplaniWeb/documentoGet/Fault/FcsdocenteplaniWebException")
    })
    public DocumentoGetResult documentoGet(
        @WebParam(name = "codiAsig", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        String codiAsig,
        @WebParam(name = "codiSeme", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        String codiSeme,
        @WebParam(name = "codiAgno", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        String codiAgno)
        throws FcsdocenteplaniWebException
    ;

    /**
     * 
     * @param cantAlumPra
     * @param cantHet
     * @param cantNel
     * @param totaHrTeo
     * @param cantHct
     * @param cantHt
     * @param cantNet
     * @param totaHrsSemana
     * @param cantPht
     * @param cantAlumTeo
     * @param cantHtS
     * @param cantHl
     * @param totaHrLab
     * @param cantHpS
     * @param cantHp
     * @param cantHci
     * @param cantHlS
     * @param cantHel
     * @param totaHrsSemestre
     * @param id
     * @param cantNin
     * @param cantHcl
     * @return
     *     returns cl.dedalus.fcsdocenteplani.ws.HorasasignaturaUpdResult
     * @throws FcsdocenteplaniWebException
     */
    @WebMethod
    @WebResult(name = "horasasignaturaUpdResult", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
    @RequestWrapper(localName = "horasasignaturaUpd", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/", className = "cl.dedalus.fcsdocenteplani.ws.HorasasignaturaUpd")
    @ResponseWrapper(localName = "horasasignaturaUpdResponse", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/", className = "cl.dedalus.fcsdocenteplani.ws.HorasasignaturaUpdResponse")
    @Action(input = "http://ws.fcsdocenteplani.dedalus.cl/FcsdocenteplaniWeb/horasasignaturaUpdRequest", output = "http://ws.fcsdocenteplani.dedalus.cl/FcsdocenteplaniWeb/horasasignaturaUpdResponse", fault = {
        @FaultAction(className = FcsdocenteplaniWebException.class, value = "http://ws.fcsdocenteplani.dedalus.cl/FcsdocenteplaniWeb/horasasignaturaUpd/Fault/FcsdocenteplaniWebException")
    })
    public HorasasignaturaUpdResult horasasignaturaUpd(
        @WebParam(name = "id", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        long id,
        @WebParam(name = "cantHt", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        String cantHt,
        @WebParam(name = "cantHp", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        String cantHp,
        @WebParam(name = "cantHl", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        String cantHl,
        @WebParam(name = "cantHtS", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        String cantHtS,
        @WebParam(name = "cantHpS", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        String cantHpS,
        @WebParam(name = "cantHlS", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        String cantHlS,
        @WebParam(name = "cantAlumTeo", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        String cantAlumTeo,
        @WebParam(name = "cantAlumPra", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        String cantAlumPra,
        @WebParam(name = "cantPht", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        String cantPht,
        @WebParam(name = "cantNet", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        String cantNet,
        @WebParam(name = "cantHet", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        String cantHet,
        @WebParam(name = "cantHct", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        String cantHct,
        @WebParam(name = "cantNel", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        String cantNel,
        @WebParam(name = "cantHel", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        String cantHel,
        @WebParam(name = "cantHcl", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        String cantHcl,
        @WebParam(name = "cantNin", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        String cantNin,
        @WebParam(name = "cantHci", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        String cantHci,
        @WebParam(name = "totaHrsSemana", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        String totaHrsSemana,
        @WebParam(name = "totaHrsSemestre", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        String totaHrsSemestre,
        @WebParam(name = "totaHrTeo", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        String totaHrTeo,
        @WebParam(name = "totaHrLab", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        String totaHrLab)
        throws FcsdocenteplaniWebException
    ;

    /**
     * 
     * @param vCmd
     * @return
     *     returns cl.dedalus.fcsdocenteplani.ws.XimprimirGetResult
     * @throws FcsdocenteplaniWebException
     */
    @WebMethod
    @WebResult(name = "ximprimirGetResult", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
    @RequestWrapper(localName = "ximprimirGet", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/", className = "cl.dedalus.fcsdocenteplani.ws.XimprimirGet")
    @ResponseWrapper(localName = "ximprimirGetResponse", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/", className = "cl.dedalus.fcsdocenteplani.ws.XimprimirGetResponse")
    @Action(input = "http://ws.fcsdocenteplani.dedalus.cl/FcsdocenteplaniWeb/ximprimirGetRequest", output = "http://ws.fcsdocenteplani.dedalus.cl/FcsdocenteplaniWeb/ximprimirGetResponse", fault = {
        @FaultAction(className = FcsdocenteplaniWebException.class, value = "http://ws.fcsdocenteplani.dedalus.cl/FcsdocenteplaniWeb/ximprimirGet/Fault/FcsdocenteplaniWebException")
    })
    public XimprimirGetResult ximprimirGet(
        @WebParam(name = "vCmd", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        String vCmd)
        throws FcsdocenteplaniWebException
    ;

    /**
     * 
     * @param codiSeme
     * @param codiDocente
     * @param codiAgno
     * @return
     *     returns cl.dedalus.fcsdocenteplani.ws.DocenteGetResult
     * @throws FcsdocenteplaniWebException
     */
    @WebMethod
    @WebResult(name = "docenteGetResult", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
    @RequestWrapper(localName = "docenteGet", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/", className = "cl.dedalus.fcsdocenteplani.ws.DocenteGet")
    @ResponseWrapper(localName = "docenteGetResponse", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/", className = "cl.dedalus.fcsdocenteplani.ws.DocenteGetResponse")
    @Action(input = "http://ws.fcsdocenteplani.dedalus.cl/FcsdocenteplaniWeb/docenteGetRequest", output = "http://ws.fcsdocenteplani.dedalus.cl/FcsdocenteplaniWeb/docenteGetResponse", fault = {
        @FaultAction(className = FcsdocenteplaniWebException.class, value = "http://ws.fcsdocenteplani.dedalus.cl/FcsdocenteplaniWeb/docenteGet/Fault/FcsdocenteplaniWebException")
    })
    public DocenteGetResult docenteGet(
        @WebParam(name = "codiDocente", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        String codiDocente,
        @WebParam(name = "codiSeme", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        String codiSeme,
        @WebParam(name = "codiAgno", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        String codiAgno)
        throws FcsdocenteplaniWebException
    ;

    /**
     * 
     * @param codiSeme
     * @param codiAsig
     * @param codiDocente
     * @param id
     * @param codiAgno
     * @return
     *     returns cl.dedalus.fcsdocenteplani.ws.HorasdocenteGetResult
     * @throws FcsdocenteplaniWebException
     */
    @WebMethod
    @WebResult(name = "horasdocenteGetResult", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
    @RequestWrapper(localName = "horasdocenteGet", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/", className = "cl.dedalus.fcsdocenteplani.ws.HorasdocenteGet")
    @ResponseWrapper(localName = "horasdocenteGetResponse", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/", className = "cl.dedalus.fcsdocenteplani.ws.HorasdocenteGetResponse")
    @Action(input = "http://ws.fcsdocenteplani.dedalus.cl/FcsdocenteplaniWeb/horasdocenteGetRequest", output = "http://ws.fcsdocenteplani.dedalus.cl/FcsdocenteplaniWeb/horasdocenteGetResponse", fault = {
        @FaultAction(className = FcsdocenteplaniWebException.class, value = "http://ws.fcsdocenteplani.dedalus.cl/FcsdocenteplaniWeb/horasdocenteGet/Fault/FcsdocenteplaniWebException")
    })
    public HorasdocenteGetResult horasdocenteGet(
        @WebParam(name = "id", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        long id,
        @WebParam(name = "codiDocente", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        String codiDocente,
        @WebParam(name = "codiAsig", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        String codiAsig,
        @WebParam(name = "codiSeme", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        String codiSeme,
        @WebParam(name = "codiAgno", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        String codiAgno)
        throws FcsdocenteplaniWebException
    ;

    /**
     * 
     * @param porcDoc
     * @param horaLab
     * @param totaHrSemestre
     * @param porcLab
     * @param porcTeo
     * @param totaHrSemana
     * @param horaDoc
     * @param id
     * @param horaTeo
     * @return
     *     returns cl.dedalus.fcsdocenteplani.ws.HorasdocenteUpdResult
     * @throws FcsdocenteplaniWebException
     */
    @WebMethod
    @WebResult(name = "horasdocenteUpdResult", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
    @RequestWrapper(localName = "horasdocenteUpd", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/", className = "cl.dedalus.fcsdocenteplani.ws.HorasdocenteUpd")
    @ResponseWrapper(localName = "horasdocenteUpdResponse", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/", className = "cl.dedalus.fcsdocenteplani.ws.HorasdocenteUpdResponse")
    @Action(input = "http://ws.fcsdocenteplani.dedalus.cl/FcsdocenteplaniWeb/horasdocenteUpdRequest", output = "http://ws.fcsdocenteplani.dedalus.cl/FcsdocenteplaniWeb/horasdocenteUpdResponse", fault = {
        @FaultAction(className = FcsdocenteplaniWebException.class, value = "http://ws.fcsdocenteplani.dedalus.cl/FcsdocenteplaniWeb/horasdocenteUpd/Fault/FcsdocenteplaniWebException")
    })
    public HorasdocenteUpdResult horasdocenteUpd(
        @WebParam(name = "id", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        long id,
        @WebParam(name = "porcTeo", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        String porcTeo,
        @WebParam(name = "horaTeo", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        String horaTeo,
        @WebParam(name = "porcLab", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        String porcLab,
        @WebParam(name = "horaLab", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        String horaLab,
        @WebParam(name = "porcDoc", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        String porcDoc,
        @WebParam(name = "horaDoc", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        String horaDoc,
        @WebParam(name = "totaHrSemana", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        String totaHrSemana,
        @WebParam(name = "totaHrSemestre", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        String totaHrSemestre)
        throws FcsdocenteplaniWebException
    ;

    /**
     * 
     * @param codiAsig
     * @param nombArchivo
     * @param codiSemestre
     * @param documento
     * @param codiAgno
     * @return
     *     returns cl.dedalus.fcsdocenteplani.ws.SubirprogramaSetResult
     * @throws FcsdocenteplaniWebException
     */
    @WebMethod
    @WebResult(name = "subirprogramaSetResult", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
    @RequestWrapper(localName = "subirprogramaSet", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/", className = "cl.dedalus.fcsdocenteplani.ws.SubirprogramaSet")
    @ResponseWrapper(localName = "subirprogramaSetResponse", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/", className = "cl.dedalus.fcsdocenteplani.ws.SubirprogramaSetResponse")
    @Action(input = "http://ws.fcsdocenteplani.dedalus.cl/FcsdocenteplaniWeb/subirprogramaSetRequest", output = "http://ws.fcsdocenteplani.dedalus.cl/FcsdocenteplaniWeb/subirprogramaSetResponse", fault = {
        @FaultAction(className = FcsdocenteplaniWebException.class, value = "http://ws.fcsdocenteplani.dedalus.cl/FcsdocenteplaniWeb/subirprogramaSet/Fault/FcsdocenteplaniWebException")
    })
    public SubirprogramaSetResult subirprogramaSet(
        @WebParam(name = "codiAsig", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        String codiAsig,
        @WebParam(name = "codiSemestre", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        String codiSemestre,
        @WebParam(name = "codiAgno", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        String codiAgno,
        @WebParam(name = "documento", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        byte[] documento,
        @WebParam(name = "nombArchivo", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        String nombArchivo)
        throws FcsdocenteplaniWebException
    ;

    /**
     * 
     * @param codiSemestre
     * @param codiAgno
     * @param codiAsignatura
     * @return
     *     returns cl.dedalus.fcsdocenteplani.ws.DocentesasignaturaGetResult
     * @throws FcsdocenteplaniWebException
     */
    @WebMethod
    @WebResult(name = "docentesasignaturaGetResult", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
    @RequestWrapper(localName = "docentesasignaturaGet", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/", className = "cl.dedalus.fcsdocenteplani.ws.DocentesasignaturaGet")
    @ResponseWrapper(localName = "docentesasignaturaGetResponse", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/", className = "cl.dedalus.fcsdocenteplani.ws.DocentesasignaturaGetResponse")
    @Action(input = "http://ws.fcsdocenteplani.dedalus.cl/FcsdocenteplaniWeb/docentesasignaturaGetRequest", output = "http://ws.fcsdocenteplani.dedalus.cl/FcsdocenteplaniWeb/docentesasignaturaGetResponse", fault = {
        @FaultAction(className = FcsdocenteplaniWebException.class, value = "http://ws.fcsdocenteplani.dedalus.cl/FcsdocenteplaniWeb/docentesasignaturaGet/Fault/FcsdocenteplaniWebException")
    })
    public DocentesasignaturaGetResult docentesasignaturaGet(
        @WebParam(name = "codiAsignatura", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        String codiAsignatura,
        @WebParam(name = "codiSemestre", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        String codiSemestre,
        @WebParam(name = "codiAgno", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        String codiAgno)
        throws FcsdocenteplaniWebException
    ;

    /**
     * 
     * @param id
     * @return
     *     returns cl.dedalus.fcsdocenteplani.ws.FlagterminoUpdResult
     * @throws FcsdocenteplaniWebException
     */
    @WebMethod
    @WebResult(name = "flagterminoUpdResult", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
    @RequestWrapper(localName = "flagterminoUpd", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/", className = "cl.dedalus.fcsdocenteplani.ws.FlagterminoUpd")
    @ResponseWrapper(localName = "flagterminoUpdResponse", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/", className = "cl.dedalus.fcsdocenteplani.ws.FlagterminoUpdResponse")
    @Action(input = "http://ws.fcsdocenteplani.dedalus.cl/FcsdocenteplaniWeb/flagterminoUpdRequest", output = "http://ws.fcsdocenteplani.dedalus.cl/FcsdocenteplaniWeb/flagterminoUpdResponse", fault = {
        @FaultAction(className = FcsdocenteplaniWebException.class, value = "http://ws.fcsdocenteplani.dedalus.cl/FcsdocenteplaniWeb/flagterminoUpd/Fault/FcsdocenteplaniWebException")
    })
    public FlagterminoUpdResult flagterminoUpd(
        @WebParam(name = "id", targetNamespace = "http://ws.fcsdocenteplani.dedalus.cl/")
        long id)
        throws FcsdocenteplaniWebException
    ;

}
