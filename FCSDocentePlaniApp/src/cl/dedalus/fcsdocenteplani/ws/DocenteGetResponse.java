
package cl.dedalus.fcsdocenteplani.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para docenteGetResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="docenteGetResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="docenteGetResult" type="{http://ws.fcsdocenteplani.dedalus.cl/}DocenteGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "docenteGetResponse", propOrder = {
    "docenteGetResult"
})
public class DocenteGetResponse {

    protected DocenteGetResult docenteGetResult;

    /**
     * Obtiene el valor de la propiedad docenteGetResult.
     * 
     * @return
     *     possible object is
     *     {@link DocenteGetResult }
     *     
     */
    public DocenteGetResult getDocenteGetResult() {
        return docenteGetResult;
    }

    /**
     * Define el valor de la propiedad docenteGetResult.
     * 
     * @param value
     *     allowed object is
     *     {@link DocenteGetResult }
     *     
     */
    public void setDocenteGetResult(DocenteGetResult value) {
        this.docenteGetResult = value;
    }

}
