
package cl.dedalus.fcsdocenteplani.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para horasdocenteUpdResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="horasdocenteUpdResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="horasdocenteUpdResult" type="{http://ws.fcsdocenteplani.dedalus.cl/}HorasdocenteUpdResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "horasdocenteUpdResponse", propOrder = {
    "horasdocenteUpdResult"
})
public class HorasdocenteUpdResponse {

    protected HorasdocenteUpdResult horasdocenteUpdResult;

    /**
     * Obtiene el valor de la propiedad horasdocenteUpdResult.
     * 
     * @return
     *     possible object is
     *     {@link HorasdocenteUpdResult }
     *     
     */
    public HorasdocenteUpdResult getHorasdocenteUpdResult() {
        return horasdocenteUpdResult;
    }

    /**
     * Define el valor de la propiedad horasdocenteUpdResult.
     * 
     * @param value
     *     allowed object is
     *     {@link HorasdocenteUpdResult }
     *     
     */
    public void setHorasdocenteUpdResult(HorasdocenteUpdResult value) {
        this.horasdocenteUpdResult = value;
    }

}
