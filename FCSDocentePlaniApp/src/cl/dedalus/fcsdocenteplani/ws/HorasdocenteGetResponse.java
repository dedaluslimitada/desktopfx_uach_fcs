
package cl.dedalus.fcsdocenteplani.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para horasdocenteGetResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="horasdocenteGetResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="horasdocenteGetResult" type="{http://ws.fcsdocenteplani.dedalus.cl/}HorasdocenteGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "horasdocenteGetResponse", propOrder = {
    "horasdocenteGetResult"
})
public class HorasdocenteGetResponse {

    protected HorasdocenteGetResult horasdocenteGetResult;

    /**
     * Obtiene el valor de la propiedad horasdocenteGetResult.
     * 
     * @return
     *     possible object is
     *     {@link HorasdocenteGetResult }
     *     
     */
    public HorasdocenteGetResult getHorasdocenteGetResult() {
        return horasdocenteGetResult;
    }

    /**
     * Define el valor de la propiedad horasdocenteGetResult.
     * 
     * @param value
     *     allowed object is
     *     {@link HorasdocenteGetResult }
     *     
     */
    public void setHorasdocenteGetResult(HorasdocenteGetResult value) {
        this.horasdocenteGetResult = value;
    }

}
