
package cl.dedalus.fcsdocenteplani.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para horasasignaturaUpd complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="horasasignaturaUpd">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="cantHt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantHp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantHl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantHtS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantHpS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantHlS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantAlumTeo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantAlumPra" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantPht" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantNet" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantHet" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantHct" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantNel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantHel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantHcl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantNin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantHci" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="totaHrsSemana" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="totaHrsSemestre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="totaHrTeo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="totaHrLab" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "horasasignaturaUpd", propOrder = {
    "id",
    "cantHt",
    "cantHp",
    "cantHl",
    "cantHtS",
    "cantHpS",
    "cantHlS",
    "cantAlumTeo",
    "cantAlumPra",
    "cantPht",
    "cantNet",
    "cantHet",
    "cantHct",
    "cantNel",
    "cantHel",
    "cantHcl",
    "cantNin",
    "cantHci",
    "totaHrsSemana",
    "totaHrsSemestre",
    "totaHrTeo",
    "totaHrLab"
})
public class HorasasignaturaUpd {

    protected long id;
    protected String cantHt;
    protected String cantHp;
    protected String cantHl;
    protected String cantHtS;
    protected String cantHpS;
    protected String cantHlS;
    protected String cantAlumTeo;
    protected String cantAlumPra;
    protected String cantPht;
    protected String cantNet;
    protected String cantHet;
    protected String cantHct;
    protected String cantNel;
    protected String cantHel;
    protected String cantHcl;
    protected String cantNin;
    protected String cantHci;
    protected String totaHrsSemana;
    protected String totaHrsSemestre;
    protected String totaHrTeo;
    protected String totaHrLab;

    /**
     * Obtiene el valor de la propiedad id.
     * 
     */
    public long getId() {
        return id;
    }

    /**
     * Define el valor de la propiedad id.
     * 
     */
    public void setId(long value) {
        this.id = value;
    }

    /**
     * Obtiene el valor de la propiedad cantHt.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantHt() {
        return cantHt;
    }

    /**
     * Define el valor de la propiedad cantHt.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantHt(String value) {
        this.cantHt = value;
    }

    /**
     * Obtiene el valor de la propiedad cantHp.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantHp() {
        return cantHp;
    }

    /**
     * Define el valor de la propiedad cantHp.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantHp(String value) {
        this.cantHp = value;
    }

    /**
     * Obtiene el valor de la propiedad cantHl.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantHl() {
        return cantHl;
    }

    /**
     * Define el valor de la propiedad cantHl.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantHl(String value) {
        this.cantHl = value;
    }

    /**
     * Obtiene el valor de la propiedad cantHtS.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantHtS() {
        return cantHtS;
    }

    /**
     * Define el valor de la propiedad cantHtS.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantHtS(String value) {
        this.cantHtS = value;
    }

    /**
     * Obtiene el valor de la propiedad cantHpS.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantHpS() {
        return cantHpS;
    }

    /**
     * Define el valor de la propiedad cantHpS.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantHpS(String value) {
        this.cantHpS = value;
    }

    /**
     * Obtiene el valor de la propiedad cantHlS.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantHlS() {
        return cantHlS;
    }

    /**
     * Define el valor de la propiedad cantHlS.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantHlS(String value) {
        this.cantHlS = value;
    }

    /**
     * Obtiene el valor de la propiedad cantAlumTeo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantAlumTeo() {
        return cantAlumTeo;
    }

    /**
     * Define el valor de la propiedad cantAlumTeo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantAlumTeo(String value) {
        this.cantAlumTeo = value;
    }

    /**
     * Obtiene el valor de la propiedad cantAlumPra.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantAlumPra() {
        return cantAlumPra;
    }

    /**
     * Define el valor de la propiedad cantAlumPra.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantAlumPra(String value) {
        this.cantAlumPra = value;
    }

    /**
     * Obtiene el valor de la propiedad cantPht.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantPht() {
        return cantPht;
    }

    /**
     * Define el valor de la propiedad cantPht.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantPht(String value) {
        this.cantPht = value;
    }

    /**
     * Obtiene el valor de la propiedad cantNet.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantNet() {
        return cantNet;
    }

    /**
     * Define el valor de la propiedad cantNet.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantNet(String value) {
        this.cantNet = value;
    }

    /**
     * Obtiene el valor de la propiedad cantHet.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantHet() {
        return cantHet;
    }

    /**
     * Define el valor de la propiedad cantHet.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantHet(String value) {
        this.cantHet = value;
    }

    /**
     * Obtiene el valor de la propiedad cantHct.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantHct() {
        return cantHct;
    }

    /**
     * Define el valor de la propiedad cantHct.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantHct(String value) {
        this.cantHct = value;
    }

    /**
     * Obtiene el valor de la propiedad cantNel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantNel() {
        return cantNel;
    }

    /**
     * Define el valor de la propiedad cantNel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantNel(String value) {
        this.cantNel = value;
    }

    /**
     * Obtiene el valor de la propiedad cantHel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantHel() {
        return cantHel;
    }

    /**
     * Define el valor de la propiedad cantHel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantHel(String value) {
        this.cantHel = value;
    }

    /**
     * Obtiene el valor de la propiedad cantHcl.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantHcl() {
        return cantHcl;
    }

    /**
     * Define el valor de la propiedad cantHcl.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantHcl(String value) {
        this.cantHcl = value;
    }

    /**
     * Obtiene el valor de la propiedad cantNin.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantNin() {
        return cantNin;
    }

    /**
     * Define el valor de la propiedad cantNin.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantNin(String value) {
        this.cantNin = value;
    }

    /**
     * Obtiene el valor de la propiedad cantHci.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantHci() {
        return cantHci;
    }

    /**
     * Define el valor de la propiedad cantHci.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantHci(String value) {
        this.cantHci = value;
    }

    /**
     * Obtiene el valor de la propiedad totaHrsSemana.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotaHrsSemana() {
        return totaHrsSemana;
    }

    /**
     * Define el valor de la propiedad totaHrsSemana.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotaHrsSemana(String value) {
        this.totaHrsSemana = value;
    }

    /**
     * Obtiene el valor de la propiedad totaHrsSemestre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotaHrsSemestre() {
        return totaHrsSemestre;
    }

    /**
     * Define el valor de la propiedad totaHrsSemestre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotaHrsSemestre(String value) {
        this.totaHrsSemestre = value;
    }

    /**
     * Obtiene el valor de la propiedad totaHrTeo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotaHrTeo() {
        return totaHrTeo;
    }

    /**
     * Define el valor de la propiedad totaHrTeo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotaHrTeo(String value) {
        this.totaHrTeo = value;
    }

    /**
     * Obtiene el valor de la propiedad totaHrLab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotaHrLab() {
        return totaHrLab;
    }

    /**
     * Define el valor de la propiedad totaHrLab.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotaHrLab(String value) {
        this.totaHrLab = value;
    }

}
