
package cl.dedalus.fcsdocenteplani.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para subirprogramaSetResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="subirprogramaSetResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="subirprogramaSetResult" type="{http://ws.fcsdocenteplani.dedalus.cl/}SubirprogramaSetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "subirprogramaSetResponse", propOrder = {
    "subirprogramaSetResult"
})
public class SubirprogramaSetResponse {

    protected SubirprogramaSetResult subirprogramaSetResult;

    /**
     * Obtiene el valor de la propiedad subirprogramaSetResult.
     * 
     * @return
     *     possible object is
     *     {@link SubirprogramaSetResult }
     *     
     */
    public SubirprogramaSetResult getSubirprogramaSetResult() {
        return subirprogramaSetResult;
    }

    /**
     * Define el valor de la propiedad subirprogramaSetResult.
     * 
     * @param value
     *     allowed object is
     *     {@link SubirprogramaSetResult }
     *     
     */
    public void setSubirprogramaSetResult(SubirprogramaSetResult value) {
        this.subirprogramaSetResult = value;
    }

}
