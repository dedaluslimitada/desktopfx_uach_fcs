
package cl.dedalus.fcsdocenteplani.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para horasasignaturaUpdResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="horasasignaturaUpdResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="horasasignaturaUpdResult" type="{http://ws.fcsdocenteplani.dedalus.cl/}HorasasignaturaUpdResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "horasasignaturaUpdResponse", propOrder = {
    "horasasignaturaUpdResult"
})
public class HorasasignaturaUpdResponse {

    protected HorasasignaturaUpdResult horasasignaturaUpdResult;

    /**
     * Obtiene el valor de la propiedad horasasignaturaUpdResult.
     * 
     * @return
     *     possible object is
     *     {@link HorasasignaturaUpdResult }
     *     
     */
    public HorasasignaturaUpdResult getHorasasignaturaUpdResult() {
        return horasasignaturaUpdResult;
    }

    /**
     * Define el valor de la propiedad horasasignaturaUpdResult.
     * 
     * @param value
     *     allowed object is
     *     {@link HorasasignaturaUpdResult }
     *     
     */
    public void setHorasasignaturaUpdResult(HorasasignaturaUpdResult value) {
        this.horasasignaturaUpdResult = value;
    }

}
