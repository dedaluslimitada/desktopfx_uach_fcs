
package cl.dedalus.fcsdocenteplani.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para horasdocenteUpd complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="horasdocenteUpd">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="porcTeo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="horaTeo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="porcLab" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="horaLab" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="porcDoc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="horaDoc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="totaHrSemana" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="totaHrSemestre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "horasdocenteUpd", propOrder = {
    "id",
    "porcTeo",
    "horaTeo",
    "porcLab",
    "horaLab",
    "porcDoc",
    "horaDoc",
    "totaHrSemana",
    "totaHrSemestre"
})
public class HorasdocenteUpd {

    protected long id;
    protected String porcTeo;
    protected String horaTeo;
    protected String porcLab;
    protected String horaLab;
    protected String porcDoc;
    protected String horaDoc;
    protected String totaHrSemana;
    protected String totaHrSemestre;

    /**
     * Obtiene el valor de la propiedad id.
     * 
     */
    public long getId() {
        return id;
    }

    /**
     * Define el valor de la propiedad id.
     * 
     */
    public void setId(long value) {
        this.id = value;
    }

    /**
     * Obtiene el valor de la propiedad porcTeo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPorcTeo() {
        return porcTeo;
    }

    /**
     * Define el valor de la propiedad porcTeo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPorcTeo(String value) {
        this.porcTeo = value;
    }

    /**
     * Obtiene el valor de la propiedad horaTeo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHoraTeo() {
        return horaTeo;
    }

    /**
     * Define el valor de la propiedad horaTeo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHoraTeo(String value) {
        this.horaTeo = value;
    }

    /**
     * Obtiene el valor de la propiedad porcLab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPorcLab() {
        return porcLab;
    }

    /**
     * Define el valor de la propiedad porcLab.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPorcLab(String value) {
        this.porcLab = value;
    }

    /**
     * Obtiene el valor de la propiedad horaLab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHoraLab() {
        return horaLab;
    }

    /**
     * Define el valor de la propiedad horaLab.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHoraLab(String value) {
        this.horaLab = value;
    }

    /**
     * Obtiene el valor de la propiedad porcDoc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPorcDoc() {
        return porcDoc;
    }

    /**
     * Define el valor de la propiedad porcDoc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPorcDoc(String value) {
        this.porcDoc = value;
    }

    /**
     * Obtiene el valor de la propiedad horaDoc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHoraDoc() {
        return horaDoc;
    }

    /**
     * Define el valor de la propiedad horaDoc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHoraDoc(String value) {
        this.horaDoc = value;
    }

    /**
     * Obtiene el valor de la propiedad totaHrSemana.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotaHrSemana() {
        return totaHrSemana;
    }

    /**
     * Define el valor de la propiedad totaHrSemana.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotaHrSemana(String value) {
        this.totaHrSemana = value;
    }

    /**
     * Obtiene el valor de la propiedad totaHrSemestre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotaHrSemestre() {
        return totaHrSemestre;
    }

    /**
     * Define el valor de la propiedad totaHrSemestre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotaHrSemestre(String value) {
        this.totaHrSemestre = value;
    }

}
