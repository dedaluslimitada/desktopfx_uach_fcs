
package cl.dedalus.fcsdocenteplani.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para HorasdocenteGetResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="HorasdocenteGetResult">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.fcsdocenteplani.dedalus.cl/}ProcedureResult">
 *       &lt;sequence>
 *         &lt;element name="cantEvalLab" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantEvalTeo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantGruposLab" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantGruposTeo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantHrCorrInf" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantHrCorrLab" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantHrCorrTeo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantHrEvalLab" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantHrEvalTeo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantHrLab" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantHrPra" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantHrPrepHt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantHrTeo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantInformes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantLabSemestre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantPraSemestre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantTeoSemestre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantTotalC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cantTotalP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="totaAlumnosLab" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="totaAlumnosTeo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="totaHrLab" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="totaHrTeo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HorasdocenteGetResult", propOrder = {
    "cantEvalLab",
    "cantEvalTeo",
    "cantGruposLab",
    "cantGruposTeo",
    "cantHrCorrInf",
    "cantHrCorrLab",
    "cantHrCorrTeo",
    "cantHrEvalLab",
    "cantHrEvalTeo",
    "cantHrLab",
    "cantHrPra",
    "cantHrPrepHt",
    "cantHrTeo",
    "cantInformes",
    "cantLabSemestre",
    "cantPraSemestre",
    "cantTeoSemestre",
    "cantTotalC",
    "cantTotalP",
    "totaAlumnosLab",
    "totaAlumnosTeo",
    "totaHrLab",
    "totaHrTeo"
})
public class HorasdocenteGetResult
    extends ProcedureResult
{

    protected String cantEvalLab;
    protected String cantEvalTeo;
    protected String cantGruposLab;
    protected String cantGruposTeo;
    protected String cantHrCorrInf;
    protected String cantHrCorrLab;
    protected String cantHrCorrTeo;
    protected String cantHrEvalLab;
    protected String cantHrEvalTeo;
    protected String cantHrLab;
    protected String cantHrPra;
    protected String cantHrPrepHt;
    protected String cantHrTeo;
    protected String cantInformes;
    protected String cantLabSemestre;
    protected String cantPraSemestre;
    protected String cantTeoSemestre;
    protected String cantTotalC;
    protected String cantTotalP;
    protected String totaAlumnosLab;
    protected String totaAlumnosTeo;
    protected String totaHrLab;
    protected String totaHrTeo;

    /**
     * Obtiene el valor de la propiedad cantEvalLab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantEvalLab() {
        return cantEvalLab;
    }

    /**
     * Define el valor de la propiedad cantEvalLab.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantEvalLab(String value) {
        this.cantEvalLab = value;
    }

    /**
     * Obtiene el valor de la propiedad cantEvalTeo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantEvalTeo() {
        return cantEvalTeo;
    }

    /**
     * Define el valor de la propiedad cantEvalTeo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantEvalTeo(String value) {
        this.cantEvalTeo = value;
    }

    /**
     * Obtiene el valor de la propiedad cantGruposLab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantGruposLab() {
        return cantGruposLab;
    }

    /**
     * Define el valor de la propiedad cantGruposLab.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantGruposLab(String value) {
        this.cantGruposLab = value;
    }

    /**
     * Obtiene el valor de la propiedad cantGruposTeo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantGruposTeo() {
        return cantGruposTeo;
    }

    /**
     * Define el valor de la propiedad cantGruposTeo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantGruposTeo(String value) {
        this.cantGruposTeo = value;
    }

    /**
     * Obtiene el valor de la propiedad cantHrCorrInf.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantHrCorrInf() {
        return cantHrCorrInf;
    }

    /**
     * Define el valor de la propiedad cantHrCorrInf.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantHrCorrInf(String value) {
        this.cantHrCorrInf = value;
    }

    /**
     * Obtiene el valor de la propiedad cantHrCorrLab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantHrCorrLab() {
        return cantHrCorrLab;
    }

    /**
     * Define el valor de la propiedad cantHrCorrLab.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantHrCorrLab(String value) {
        this.cantHrCorrLab = value;
    }

    /**
     * Obtiene el valor de la propiedad cantHrCorrTeo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantHrCorrTeo() {
        return cantHrCorrTeo;
    }

    /**
     * Define el valor de la propiedad cantHrCorrTeo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantHrCorrTeo(String value) {
        this.cantHrCorrTeo = value;
    }

    /**
     * Obtiene el valor de la propiedad cantHrEvalLab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantHrEvalLab() {
        return cantHrEvalLab;
    }

    /**
     * Define el valor de la propiedad cantHrEvalLab.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantHrEvalLab(String value) {
        this.cantHrEvalLab = value;
    }

    /**
     * Obtiene el valor de la propiedad cantHrEvalTeo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantHrEvalTeo() {
        return cantHrEvalTeo;
    }

    /**
     * Define el valor de la propiedad cantHrEvalTeo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantHrEvalTeo(String value) {
        this.cantHrEvalTeo = value;
    }

    /**
     * Obtiene el valor de la propiedad cantHrLab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantHrLab() {
        return cantHrLab;
    }

    /**
     * Define el valor de la propiedad cantHrLab.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantHrLab(String value) {
        this.cantHrLab = value;
    }

    /**
     * Obtiene el valor de la propiedad cantHrPra.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantHrPra() {
        return cantHrPra;
    }

    /**
     * Define el valor de la propiedad cantHrPra.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantHrPra(String value) {
        this.cantHrPra = value;
    }

    /**
     * Obtiene el valor de la propiedad cantHrPrepHt.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantHrPrepHt() {
        return cantHrPrepHt;
    }

    /**
     * Define el valor de la propiedad cantHrPrepHt.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantHrPrepHt(String value) {
        this.cantHrPrepHt = value;
    }

    /**
     * Obtiene el valor de la propiedad cantHrTeo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantHrTeo() {
        return cantHrTeo;
    }

    /**
     * Define el valor de la propiedad cantHrTeo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantHrTeo(String value) {
        this.cantHrTeo = value;
    }

    /**
     * Obtiene el valor de la propiedad cantInformes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantInformes() {
        return cantInformes;
    }

    /**
     * Define el valor de la propiedad cantInformes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantInformes(String value) {
        this.cantInformes = value;
    }

    /**
     * Obtiene el valor de la propiedad cantLabSemestre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantLabSemestre() {
        return cantLabSemestre;
    }

    /**
     * Define el valor de la propiedad cantLabSemestre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantLabSemestre(String value) {
        this.cantLabSemestre = value;
    }

    /**
     * Obtiene el valor de la propiedad cantPraSemestre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantPraSemestre() {
        return cantPraSemestre;
    }

    /**
     * Define el valor de la propiedad cantPraSemestre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantPraSemestre(String value) {
        this.cantPraSemestre = value;
    }

    /**
     * Obtiene el valor de la propiedad cantTeoSemestre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantTeoSemestre() {
        return cantTeoSemestre;
    }

    /**
     * Define el valor de la propiedad cantTeoSemestre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantTeoSemestre(String value) {
        this.cantTeoSemestre = value;
    }

    /**
     * Obtiene el valor de la propiedad cantTotalC.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantTotalC() {
        return cantTotalC;
    }

    /**
     * Define el valor de la propiedad cantTotalC.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantTotalC(String value) {
        this.cantTotalC = value;
    }

    /**
     * Obtiene el valor de la propiedad cantTotalP.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantTotalP() {
        return cantTotalP;
    }

    /**
     * Define el valor de la propiedad cantTotalP.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantTotalP(String value) {
        this.cantTotalP = value;
    }

    /**
     * Obtiene el valor de la propiedad totaAlumnosLab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotaAlumnosLab() {
        return totaAlumnosLab;
    }

    /**
     * Define el valor de la propiedad totaAlumnosLab.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotaAlumnosLab(String value) {
        this.totaAlumnosLab = value;
    }

    /**
     * Obtiene el valor de la propiedad totaAlumnosTeo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotaAlumnosTeo() {
        return totaAlumnosTeo;
    }

    /**
     * Define el valor de la propiedad totaAlumnosTeo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotaAlumnosTeo(String value) {
        this.totaAlumnosTeo = value;
    }

    /**
     * Obtiene el valor de la propiedad totaHrLab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotaHrLab() {
        return totaHrLab;
    }

    /**
     * Define el valor de la propiedad totaHrLab.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotaHrLab(String value) {
        this.totaHrLab = value;
    }

    /**
     * Obtiene el valor de la propiedad totaHrTeo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotaHrTeo() {
        return totaHrTeo;
    }

    /**
     * Define el valor de la propiedad totaHrTeo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotaHrTeo(String value) {
        this.totaHrTeo = value;
    }

}
