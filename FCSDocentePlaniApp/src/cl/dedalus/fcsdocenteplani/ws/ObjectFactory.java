
package cl.dedalus.fcsdocenteplani.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cl.dedalus.fcsdocenteplani.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _XimprimirGet_QNAME = new QName("http://ws.fcsdocenteplani.dedalus.cl/", "ximprimirGet");
    private final static QName _FaultInfo_QNAME = new QName("http://ws.fcsdocenteplani.dedalus.cl/", "faultInfo");
    private final static QName _DocentesasignaturaGetResponse_QNAME = new QName("http://ws.fcsdocenteplani.dedalus.cl/", "docentesasignaturaGetResponse");
    private final static QName _HorasdocenteGetResponse_QNAME = new QName("http://ws.fcsdocenteplani.dedalus.cl/", "horasdocenteGetResponse");
    private final static QName _DocentesasignaturaGet_QNAME = new QName("http://ws.fcsdocenteplani.dedalus.cl/", "docentesasignaturaGet");
    private final static QName _DocumentoGet_QNAME = new QName("http://ws.fcsdocenteplani.dedalus.cl/", "documentoGet");
    private final static QName _HorasdocenteGet_QNAME = new QName("http://ws.fcsdocenteplani.dedalus.cl/", "horasdocenteGet");
    private final static QName _DocenteGetResponse_QNAME = new QName("http://ws.fcsdocenteplani.dedalus.cl/", "docenteGetResponse");
    private final static QName _SubirprogramaSetResponse_QNAME = new QName("http://ws.fcsdocenteplani.dedalus.cl/", "subirprogramaSetResponse");
    private final static QName _DocumentoGetResponse_QNAME = new QName("http://ws.fcsdocenteplani.dedalus.cl/", "documentoGetResponse");
    private final static QName _HorasasignaturaUpdResponse_QNAME = new QName("http://ws.fcsdocenteplani.dedalus.cl/", "horasasignaturaUpdResponse");
    private final static QName _FlagterminoUpdResponse_QNAME = new QName("http://ws.fcsdocenteplani.dedalus.cl/", "flagterminoUpdResponse");
    private final static QName _HorasasignaturaUpd_QNAME = new QName("http://ws.fcsdocenteplani.dedalus.cl/", "horasasignaturaUpd");
    private final static QName _HorasdocenteUpd_QNAME = new QName("http://ws.fcsdocenteplani.dedalus.cl/", "horasdocenteUpd");
    private final static QName _DocenteGet_QNAME = new QName("http://ws.fcsdocenteplani.dedalus.cl/", "docenteGet");
    private final static QName _XimprimirGetResponse_QNAME = new QName("http://ws.fcsdocenteplani.dedalus.cl/", "ximprimirGetResponse");
    private final static QName _FlagterminoUpd_QNAME = new QName("http://ws.fcsdocenteplani.dedalus.cl/", "flagterminoUpd");
    private final static QName _SubirprogramaSet_QNAME = new QName("http://ws.fcsdocenteplani.dedalus.cl/", "subirprogramaSet");
    private final static QName _HorasdocenteUpdResponse_QNAME = new QName("http://ws.fcsdocenteplani.dedalus.cl/", "horasdocenteUpdResponse");
    private final static QName _SubirprogramaSetDocumento_QNAME = new QName("http://ws.fcsdocenteplani.dedalus.cl/", "documento");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cl.dedalus.fcsdocenteplani.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link FlagterminoUpdResponse }
     * 
     */
    public FlagterminoUpdResponse createFlagterminoUpdResponse() {
        return new FlagterminoUpdResponse();
    }

    /**
     * Create an instance of {@link HorasasignaturaUpd }
     * 
     */
    public HorasasignaturaUpd createHorasasignaturaUpd() {
        return new HorasasignaturaUpd();
    }

    /**
     * Create an instance of {@link HorasasignaturaUpdResponse }
     * 
     */
    public HorasasignaturaUpdResponse createHorasasignaturaUpdResponse() {
        return new HorasasignaturaUpdResponse();
    }

    /**
     * Create an instance of {@link DocenteGet }
     * 
     */
    public DocenteGet createDocenteGet() {
        return new DocenteGet();
    }

    /**
     * Create an instance of {@link HorasdocenteUpd }
     * 
     */
    public HorasdocenteUpd createHorasdocenteUpd() {
        return new HorasdocenteUpd();
    }

    /**
     * Create an instance of {@link FlagterminoUpd }
     * 
     */
    public FlagterminoUpd createFlagterminoUpd() {
        return new FlagterminoUpd();
    }

    /**
     * Create an instance of {@link SubirprogramaSet }
     * 
     */
    public SubirprogramaSet createSubirprogramaSet() {
        return new SubirprogramaSet();
    }

    /**
     * Create an instance of {@link HorasdocenteUpdResponse }
     * 
     */
    public HorasdocenteUpdResponse createHorasdocenteUpdResponse() {
        return new HorasdocenteUpdResponse();
    }

    /**
     * Create an instance of {@link XimprimirGetResponse }
     * 
     */
    public XimprimirGetResponse createXimprimirGetResponse() {
        return new XimprimirGetResponse();
    }

    /**
     * Create an instance of {@link XimprimirGet }
     * 
     */
    public XimprimirGet createXimprimirGet() {
        return new XimprimirGet();
    }

    /**
     * Create an instance of {@link DocentesasignaturaGetResponse }
     * 
     */
    public DocentesasignaturaGetResponse createDocentesasignaturaGetResponse() {
        return new DocentesasignaturaGetResponse();
    }

    /**
     * Create an instance of {@link HorasdocenteGetResponse }
     * 
     */
    public HorasdocenteGetResponse createHorasdocenteGetResponse() {
        return new HorasdocenteGetResponse();
    }

    /**
     * Create an instance of {@link DocentesasignaturaGet }
     * 
     */
    public DocentesasignaturaGet createDocentesasignaturaGet() {
        return new DocentesasignaturaGet();
    }

    /**
     * Create an instance of {@link DocumentoGetResponse }
     * 
     */
    public DocumentoGetResponse createDocumentoGetResponse() {
        return new DocumentoGetResponse();
    }

    /**
     * Create an instance of {@link DocumentoGet }
     * 
     */
    public DocumentoGet createDocumentoGet() {
        return new DocumentoGet();
    }

    /**
     * Create an instance of {@link HorasdocenteGet }
     * 
     */
    public HorasdocenteGet createHorasdocenteGet() {
        return new HorasdocenteGet();
    }

    /**
     * Create an instance of {@link DocenteGetResponse }
     * 
     */
    public DocenteGetResponse createDocenteGetResponse() {
        return new DocenteGetResponse();
    }

    /**
     * Create an instance of {@link SubirprogramaSetResponse }
     * 
     */
    public SubirprogramaSetResponse createSubirprogramaSetResponse() {
        return new SubirprogramaSetResponse();
    }

    /**
     * Create an instance of {@link DocumentoGetResult }
     * 
     */
    public DocumentoGetResult createDocumentoGetResult() {
        return new DocumentoGetResult();
    }

    /**
     * Create an instance of {@link SubirprogramaSetResult }
     * 
     */
    public SubirprogramaSetResult createSubirprogramaSetResult() {
        return new SubirprogramaSetResult();
    }

    /**
     * Create an instance of {@link DocentesasignaturaGetDocente }
     * 
     */
    public DocentesasignaturaGetDocente createDocentesasignaturaGetDocente() {
        return new DocentesasignaturaGetDocente();
    }

    /**
     * Create an instance of {@link HorasdocenteUpdResult }
     * 
     */
    public HorasdocenteUpdResult createHorasdocenteUpdResult() {
        return new HorasdocenteUpdResult();
    }

    /**
     * Create an instance of {@link FlagterminoUpdResult }
     * 
     */
    public FlagterminoUpdResult createFlagterminoUpdResult() {
        return new FlagterminoUpdResult();
    }

    /**
     * Create an instance of {@link DocenteGetResult }
     * 
     */
    public DocenteGetResult createDocenteGetResult() {
        return new DocenteGetResult();
    }

    /**
     * Create an instance of {@link XimprimirGetResult }
     * 
     */
    public XimprimirGetResult createXimprimirGetResult() {
        return new XimprimirGetResult();
    }

    /**
     * Create an instance of {@link DocentesasignaturaGetResult }
     * 
     */
    public DocentesasignaturaGetResult createDocentesasignaturaGetResult() {
        return new DocentesasignaturaGetResult();
    }

    /**
     * Create an instance of {@link DocenteGetAsig }
     * 
     */
    public DocenteGetAsig createDocenteGetAsig() {
        return new DocenteGetAsig();
    }

    /**
     * Create an instance of {@link HorasasignaturaUpdResult }
     * 
     */
    public HorasasignaturaUpdResult createHorasasignaturaUpdResult() {
        return new HorasasignaturaUpdResult();
    }

    /**
     * Create an instance of {@link HorasdocenteGetResult }
     * 
     */
    public HorasdocenteGetResult createHorasdocenteGetResult() {
        return new HorasdocenteGetResult();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XimprimirGet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsdocenteplani.dedalus.cl/", name = "ximprimirGet")
    public JAXBElement<XimprimirGet> createXimprimirGet(XimprimirGet value) {
        return new JAXBElement<XimprimirGet>(_XimprimirGet_QNAME, XimprimirGet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsdocenteplani.dedalus.cl/", name = "faultInfo")
    public JAXBElement<String> createFaultInfo(String value) {
        return new JAXBElement<String>(_FaultInfo_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocentesasignaturaGetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsdocenteplani.dedalus.cl/", name = "docentesasignaturaGetResponse")
    public JAXBElement<DocentesasignaturaGetResponse> createDocentesasignaturaGetResponse(DocentesasignaturaGetResponse value) {
        return new JAXBElement<DocentesasignaturaGetResponse>(_DocentesasignaturaGetResponse_QNAME, DocentesasignaturaGetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HorasdocenteGetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsdocenteplani.dedalus.cl/", name = "horasdocenteGetResponse")
    public JAXBElement<HorasdocenteGetResponse> createHorasdocenteGetResponse(HorasdocenteGetResponse value) {
        return new JAXBElement<HorasdocenteGetResponse>(_HorasdocenteGetResponse_QNAME, HorasdocenteGetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocentesasignaturaGet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsdocenteplani.dedalus.cl/", name = "docentesasignaturaGet")
    public JAXBElement<DocentesasignaturaGet> createDocentesasignaturaGet(DocentesasignaturaGet value) {
        return new JAXBElement<DocentesasignaturaGet>(_DocentesasignaturaGet_QNAME, DocentesasignaturaGet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocumentoGet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsdocenteplani.dedalus.cl/", name = "documentoGet")
    public JAXBElement<DocumentoGet> createDocumentoGet(DocumentoGet value) {
        return new JAXBElement<DocumentoGet>(_DocumentoGet_QNAME, DocumentoGet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HorasdocenteGet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsdocenteplani.dedalus.cl/", name = "horasdocenteGet")
    public JAXBElement<HorasdocenteGet> createHorasdocenteGet(HorasdocenteGet value) {
        return new JAXBElement<HorasdocenteGet>(_HorasdocenteGet_QNAME, HorasdocenteGet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocenteGetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsdocenteplani.dedalus.cl/", name = "docenteGetResponse")
    public JAXBElement<DocenteGetResponse> createDocenteGetResponse(DocenteGetResponse value) {
        return new JAXBElement<DocenteGetResponse>(_DocenteGetResponse_QNAME, DocenteGetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubirprogramaSetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsdocenteplani.dedalus.cl/", name = "subirprogramaSetResponse")
    public JAXBElement<SubirprogramaSetResponse> createSubirprogramaSetResponse(SubirprogramaSetResponse value) {
        return new JAXBElement<SubirprogramaSetResponse>(_SubirprogramaSetResponse_QNAME, SubirprogramaSetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocumentoGetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsdocenteplani.dedalus.cl/", name = "documentoGetResponse")
    public JAXBElement<DocumentoGetResponse> createDocumentoGetResponse(DocumentoGetResponse value) {
        return new JAXBElement<DocumentoGetResponse>(_DocumentoGetResponse_QNAME, DocumentoGetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HorasasignaturaUpdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsdocenteplani.dedalus.cl/", name = "horasasignaturaUpdResponse")
    public JAXBElement<HorasasignaturaUpdResponse> createHorasasignaturaUpdResponse(HorasasignaturaUpdResponse value) {
        return new JAXBElement<HorasasignaturaUpdResponse>(_HorasasignaturaUpdResponse_QNAME, HorasasignaturaUpdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FlagterminoUpdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsdocenteplani.dedalus.cl/", name = "flagterminoUpdResponse")
    public JAXBElement<FlagterminoUpdResponse> createFlagterminoUpdResponse(FlagterminoUpdResponse value) {
        return new JAXBElement<FlagterminoUpdResponse>(_FlagterminoUpdResponse_QNAME, FlagterminoUpdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HorasasignaturaUpd }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsdocenteplani.dedalus.cl/", name = "horasasignaturaUpd")
    public JAXBElement<HorasasignaturaUpd> createHorasasignaturaUpd(HorasasignaturaUpd value) {
        return new JAXBElement<HorasasignaturaUpd>(_HorasasignaturaUpd_QNAME, HorasasignaturaUpd.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HorasdocenteUpd }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsdocenteplani.dedalus.cl/", name = "horasdocenteUpd")
    public JAXBElement<HorasdocenteUpd> createHorasdocenteUpd(HorasdocenteUpd value) {
        return new JAXBElement<HorasdocenteUpd>(_HorasdocenteUpd_QNAME, HorasdocenteUpd.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocenteGet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsdocenteplani.dedalus.cl/", name = "docenteGet")
    public JAXBElement<DocenteGet> createDocenteGet(DocenteGet value) {
        return new JAXBElement<DocenteGet>(_DocenteGet_QNAME, DocenteGet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XimprimirGetResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsdocenteplani.dedalus.cl/", name = "ximprimirGetResponse")
    public JAXBElement<XimprimirGetResponse> createXimprimirGetResponse(XimprimirGetResponse value) {
        return new JAXBElement<XimprimirGetResponse>(_XimprimirGetResponse_QNAME, XimprimirGetResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FlagterminoUpd }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsdocenteplani.dedalus.cl/", name = "flagterminoUpd")
    public JAXBElement<FlagterminoUpd> createFlagterminoUpd(FlagterminoUpd value) {
        return new JAXBElement<FlagterminoUpd>(_FlagterminoUpd_QNAME, FlagterminoUpd.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubirprogramaSet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsdocenteplani.dedalus.cl/", name = "subirprogramaSet")
    public JAXBElement<SubirprogramaSet> createSubirprogramaSet(SubirprogramaSet value) {
        return new JAXBElement<SubirprogramaSet>(_SubirprogramaSet_QNAME, SubirprogramaSet.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HorasdocenteUpdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsdocenteplani.dedalus.cl/", name = "horasdocenteUpdResponse")
    public JAXBElement<HorasdocenteUpdResponse> createHorasdocenteUpdResponse(HorasdocenteUpdResponse value) {
        return new JAXBElement<HorasdocenteUpdResponse>(_HorasdocenteUpdResponse_QNAME, HorasdocenteUpdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fcsdocenteplani.dedalus.cl/", name = "documento", scope = SubirprogramaSet.class)
    public JAXBElement<byte[]> createSubirprogramaSetDocumento(byte[] value) {
        return new JAXBElement<byte[]>(_SubirprogramaSetDocumento_QNAME, byte[].class, SubirprogramaSet.class, ((byte[]) value));
    }

}
