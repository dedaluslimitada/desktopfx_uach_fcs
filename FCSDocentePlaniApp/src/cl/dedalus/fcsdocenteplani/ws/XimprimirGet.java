
package cl.dedalus.fcsdocenteplani.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ximprimirGet complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ximprimirGet">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="vCmd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ximprimirGet", propOrder = {
    "vCmd"
})
public class XimprimirGet {

    protected String vCmd;

    /**
     * Obtiene el valor de la propiedad vCmd.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVCmd() {
        return vCmd;
    }

    /**
     * Define el valor de la propiedad vCmd.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVCmd(String value) {
        this.vCmd = value;
    }

}
