
package cl.dedalus.fcsdocenteplani.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DocentesasignaturaGetResult complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DocentesasignaturaGetResult">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.fcsdocenteplani.dedalus.cl/}ProcedureResult">
 *       &lt;sequence>
 *         &lt;element name="docentes" type="{http://ws.fcsdocenteplani.dedalus.cl/}DocentesasignaturaGetDocente" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocentesasignaturaGetResult", propOrder = {
    "docentes"
})
public class DocentesasignaturaGetResult
    extends ProcedureResult
{

    @XmlElement(nillable = true)
    protected List<DocentesasignaturaGetDocente> docentes;

    /**
     * Gets the value of the docentes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the docentes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDocentes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DocentesasignaturaGetDocente }
     * 
     * 
     */
    public List<DocentesasignaturaGetDocente> getDocentes() {
        if (docentes == null) {
            docentes = new ArrayList<DocentesasignaturaGetDocente>();
        }
        return this.docentes;
    }

}
