
package cl.dedalus.fcsdocenteplani.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para DocenteGetAsig complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DocenteGetAsig">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cantSem" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codiAgno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codiAsignatura" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codiSeme" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descAsignatura" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="flagPrim" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="flagProg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="flagResp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="flagTermino" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="totaHrSema" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="totaHrSeme" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocenteGetAsig", propOrder = {
    "cantSem",
    "codiAgno",
    "codiAsignatura",
    "codiSeme",
    "descAsignatura",
    "flagPrim",
    "flagProg",
    "flagResp",
    "flagTermino",
    "id",
    "totaHrSema",
    "totaHrSeme"
})
public class DocenteGetAsig {

    protected String cantSem;
    protected String codiAgno;
    protected String codiAsignatura;
    protected String codiSeme;
    protected String descAsignatura;
    protected String flagPrim;
    protected String flagProg;
    protected String flagResp;
    protected String flagTermino;
    protected long id;
    protected String totaHrSema;
    protected String totaHrSeme;

    /**
     * Obtiene el valor de la propiedad cantSem.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantSem() {
        return cantSem;
    }

    /**
     * Define el valor de la propiedad cantSem.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantSem(String value) {
        this.cantSem = value;
    }

    /**
     * Obtiene el valor de la propiedad codiAgno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiAgno() {
        return codiAgno;
    }

    /**
     * Define el valor de la propiedad codiAgno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiAgno(String value) {
        this.codiAgno = value;
    }

    /**
     * Obtiene el valor de la propiedad codiAsignatura.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiAsignatura() {
        return codiAsignatura;
    }

    /**
     * Define el valor de la propiedad codiAsignatura.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiAsignatura(String value) {
        this.codiAsignatura = value;
    }

    /**
     * Obtiene el valor de la propiedad codiSeme.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiSeme() {
        return codiSeme;
    }

    /**
     * Define el valor de la propiedad codiSeme.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiSeme(String value) {
        this.codiSeme = value;
    }

    /**
     * Obtiene el valor de la propiedad descAsignatura.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescAsignatura() {
        return descAsignatura;
    }

    /**
     * Define el valor de la propiedad descAsignatura.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescAsignatura(String value) {
        this.descAsignatura = value;
    }

    /**
     * Obtiene el valor de la propiedad flagPrim.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlagPrim() {
        return flagPrim;
    }

    /**
     * Define el valor de la propiedad flagPrim.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlagPrim(String value) {
        this.flagPrim = value;
    }

    /**
     * Obtiene el valor de la propiedad flagProg.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlagProg() {
        return flagProg;
    }

    /**
     * Define el valor de la propiedad flagProg.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlagProg(String value) {
        this.flagProg = value;
    }

    /**
     * Obtiene el valor de la propiedad flagResp.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlagResp() {
        return flagResp;
    }

    /**
     * Define el valor de la propiedad flagResp.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlagResp(String value) {
        this.flagResp = value;
    }

    /**
     * Obtiene el valor de la propiedad flagTermino.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlagTermino() {
        return flagTermino;
    }

    /**
     * Define el valor de la propiedad flagTermino.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlagTermino(String value) {
        this.flagTermino = value;
    }

    /**
     * Obtiene el valor de la propiedad id.
     * 
     */
    public long getId() {
        return id;
    }

    /**
     * Define el valor de la propiedad id.
     * 
     */
    public void setId(long value) {
        this.id = value;
    }

    /**
     * Obtiene el valor de la propiedad totaHrSema.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotaHrSema() {
        return totaHrSema;
    }

    /**
     * Define el valor de la propiedad totaHrSema.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotaHrSema(String value) {
        this.totaHrSema = value;
    }

    /**
     * Obtiene el valor de la propiedad totaHrSeme.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotaHrSeme() {
        return totaHrSeme;
    }

    /**
     * Define el valor de la propiedad totaHrSeme.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotaHrSeme(String value) {
        this.totaHrSeme = value;
    }

}
