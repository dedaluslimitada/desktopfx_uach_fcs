
package cl.dedalus.fcsdocenteplani.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para documentoGetResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="documentoGetResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="documentoGetResult" type="{http://ws.fcsdocenteplani.dedalus.cl/}DocumentoGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "documentoGetResponse", propOrder = {
    "documentoGetResult"
})
public class DocumentoGetResponse {

    protected DocumentoGetResult documentoGetResult;

    /**
     * Obtiene el valor de la propiedad documentoGetResult.
     * 
     * @return
     *     possible object is
     *     {@link DocumentoGetResult }
     *     
     */
    public DocumentoGetResult getDocumentoGetResult() {
        return documentoGetResult;
    }

    /**
     * Define el valor de la propiedad documentoGetResult.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentoGetResult }
     *     
     */
    public void setDocumentoGetResult(DocumentoGetResult value) {
        this.documentoGetResult = value;
    }

}
