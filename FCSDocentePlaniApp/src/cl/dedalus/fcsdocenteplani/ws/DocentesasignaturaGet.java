
package cl.dedalus.fcsdocenteplani.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para docentesasignaturaGet complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="docentesasignaturaGet">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codiAsignatura" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codiSemestre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codiAgno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "docentesasignaturaGet", propOrder = {
    "codiAsignatura",
    "codiSemestre",
    "codiAgno"
})
public class DocentesasignaturaGet {

    protected String codiAsignatura;
    protected String codiSemestre;
    protected String codiAgno;

    /**
     * Obtiene el valor de la propiedad codiAsignatura.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiAsignatura() {
        return codiAsignatura;
    }

    /**
     * Define el valor de la propiedad codiAsignatura.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiAsignatura(String value) {
        this.codiAsignatura = value;
    }

    /**
     * Obtiene el valor de la propiedad codiSemestre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiSemestre() {
        return codiSemestre;
    }

    /**
     * Define el valor de la propiedad codiSemestre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiSemestre(String value) {
        this.codiSemestre = value;
    }

    /**
     * Obtiene el valor de la propiedad codiAgno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiAgno() {
        return codiAgno;
    }

    /**
     * Define el valor de la propiedad codiAgno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiAgno(String value) {
        this.codiAgno = value;
    }

}
