
package cl.dedalus.fcsdocenteplani.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para docentesasignaturaGetResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="docentesasignaturaGetResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="docentesasignaturaGetResult" type="{http://ws.fcsdocenteplani.dedalus.cl/}DocentesasignaturaGetResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "docentesasignaturaGetResponse", propOrder = {
    "docentesasignaturaGetResult"
})
public class DocentesasignaturaGetResponse {

    protected DocentesasignaturaGetResult docentesasignaturaGetResult;

    /**
     * Obtiene el valor de la propiedad docentesasignaturaGetResult.
     * 
     * @return
     *     possible object is
     *     {@link DocentesasignaturaGetResult }
     *     
     */
    public DocentesasignaturaGetResult getDocentesasignaturaGetResult() {
        return docentesasignaturaGetResult;
    }

    /**
     * Define el valor de la propiedad docentesasignaturaGetResult.
     * 
     * @param value
     *     allowed object is
     *     {@link DocentesasignaturaGetResult }
     *     
     */
    public void setDocentesasignaturaGetResult(DocentesasignaturaGetResult value) {
        this.docentesasignaturaGetResult = value;
    }

}
