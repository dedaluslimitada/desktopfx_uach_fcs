
package cl.dedalus.fcsdocenteplani.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para flagterminoUpdResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="flagterminoUpdResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="flagterminoUpdResult" type="{http://ws.fcsdocenteplani.dedalus.cl/}FlagterminoUpdResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "flagterminoUpdResponse", propOrder = {
    "flagterminoUpdResult"
})
public class FlagterminoUpdResponse {

    protected FlagterminoUpdResult flagterminoUpdResult;

    /**
     * Obtiene el valor de la propiedad flagterminoUpdResult.
     * 
     * @return
     *     possible object is
     *     {@link FlagterminoUpdResult }
     *     
     */
    public FlagterminoUpdResult getFlagterminoUpdResult() {
        return flagterminoUpdResult;
    }

    /**
     * Define el valor de la propiedad flagterminoUpdResult.
     * 
     * @param value
     *     allowed object is
     *     {@link FlagterminoUpdResult }
     *     
     */
    public void setFlagterminoUpdResult(FlagterminoUpdResult value) {
        this.flagterminoUpdResult = value;
    }

}
