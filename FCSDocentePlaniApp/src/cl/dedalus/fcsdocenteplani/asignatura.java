package cl.dedalus.fcsdocenteplani;

import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;

public class asignatura{
    private final SimpleLongProperty id;
    private final SimpleStringProperty codi_asig;
    private final SimpleStringProperty desc_asig;
    private final SimpleStringProperty codi_seme;
    private final SimpleStringProperty codi_agno;
    private final SimpleStringProperty flag_resp;
    private final SimpleStringProperty flag_prim;
    private final SimpleStringProperty cant_sem;
    private final SimpleStringProperty cant_hr_sema;
    private final SimpleStringProperty cant_hr_seme;
    private final SimpleStringProperty flag_prog;
    private final SimpleStringProperty flag_termino;

    public asignatura(final Long id, final String codi_asig,final String desc_asig, final String codi_seme, final String codi_agno
                      , final String flag_resp, final String flag_prim, final String cant_sem
                      ,final String cant_hr_sema, final String cant_hr_seme, final String flag_prog, String flag_termino){
        this.id = new SimpleLongProperty(id);
        this.codi_asig = new SimpleStringProperty(codi_asig);
        this.desc_asig = new SimpleStringProperty(desc_asig);
        this.codi_seme = new SimpleStringProperty(codi_seme);
        this.codi_agno = new SimpleStringProperty(codi_agno);
        this.flag_resp = new SimpleStringProperty(flag_resp);
        this.flag_prim = new SimpleStringProperty(flag_prim);
        this.cant_sem = new SimpleStringProperty(cant_sem);
        this.cant_hr_sema = new SimpleStringProperty(cant_hr_sema);
        this.cant_hr_seme = new SimpleStringProperty(cant_hr_seme);
        this.flag_prog = new SimpleStringProperty(flag_prog);
        this.flag_termino = new SimpleStringProperty(flag_termino);
    }

    public Long getId() {
        return id.get();
    }
    public String getCodi_asig() {
        return codi_asig.get();
    }
    public String getDesc_asig() {
        return desc_asig.get();
    }
    public String getCodi_seme() {
        return codi_seme.get();
    }
    public String getCodi_agno() {
        return codi_agno.get();
    }
    public String getFlag_resp() {
        return flag_resp.get();
    }
    public String getFlag_prim() {
        return flag_prim.get();
    }
    public String getCant_sem() {
        return cant_sem.get();
    }
  //-------------------------------------------------------
    public String getCant_hr_sema() {
        return cant_hr_sema.get();
    }
    public void setCant_hr_sema(final String ihoras) {
        cant_hr_sema.set(ihoras);
    }
    public SimpleStringProperty cant_hr_semaProperty(){
        return cant_hr_sema;
    }
//-------------------------------------------------------
    public String getCant_hr_seme() {
        return cant_hr_seme.get();
    }
    public void setCant_hr_seme(final String ihoras) {
        cant_hr_seme.set(ihoras);
    }
    public SimpleStringProperty cant_hr_semeProperty(){
        return cant_hr_seme;
    }
  //-------------------------------------------------------
    public String getFlag_prog() {
        return flag_prog.get();
    }
    //-------------------------------------------------------
    public String getFlag_termino() {
        return flag_termino.get();
    }
    public void setFlag_termino(final String iflag) {
        flag_termino.set(iflag);
    }
    public SimpleStringProperty flag_terminoProperty(){
        return flag_termino;
    }
}
