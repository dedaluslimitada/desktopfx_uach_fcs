package cl.dedalus.fcsdocenteplani;

import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;

public class docente{
    private final SimpleLongProperty id;
    private final SimpleStringProperty codi_docente;
    private final SimpleStringProperty porc_teo;
    private final SimpleStringProperty hora_teo;
    private final SimpleStringProperty porc_lab;
    private final SimpleStringProperty hora_lab;
    private final SimpleStringProperty porc_doc;
    private final SimpleStringProperty hora_doc;
    private final SimpleStringProperty tota_hrs;
    
    public docente(final Long id, final String codi_docente, final String porc_teo, final String hora_teo
                   , final String porc_lab, final String hora_lab, String porc_doc, String hora_doc, String tota_hrs){
        this.id = new SimpleLongProperty(id);
        this.codi_docente = new SimpleStringProperty(codi_docente);
        this.porc_teo = new SimpleStringProperty(porc_teo);
        this.hora_teo = new SimpleStringProperty(hora_teo);
        this.porc_lab = new SimpleStringProperty(porc_lab);
        this.hora_lab = new SimpleStringProperty(hora_lab);
        this.porc_doc = new SimpleStringProperty(porc_doc);
        this.hora_doc = new SimpleStringProperty(hora_doc);
        this.tota_hrs = new SimpleStringProperty(tota_hrs);
    }

    public Long getId() {
        return id.get();
    }
    public String getCodi_docente() {
        return codi_docente.get();
    }
//---------------------------porc_teo
    public String getPorc_teo() {
        return porc_teo.get();
    }
    public void setPorc_teo(final String iporc) {
        porc_teo.set(iporc);
    }
//---------------------------hora_teo
    public String getHora_teo() {
        return hora_teo.get();
    }
    public void setHora_teo(final String ihoras) {
        hora_teo.set(ihoras);
        calculaTotal();
    }
    public SimpleStringProperty hora_teoProperty(){
        return hora_teo;
    }
//---------------------------porc_lab
    public String getPorc_lab() {
        return porc_lab.get();
    }
    public void setPorc_lab(final String iporc) {
        porc_lab.set(iporc);
    }
//---------------------------hora_lab
    public String getHora_lab() {
        return hora_lab.get();
    }
    public void setHora_lab(final String ihoras) {
        hora_lab.set(ihoras);
        calculaTotal();
    }
    public SimpleStringProperty hora_labProperty(){
        return hora_lab;
    }
//---------------------------porc_doc    
    public String getPorc_doc() {
        return porc_doc.get();
    }
    public void setPorc_doc(final String iporc) {
        porc_doc.set(iporc);
    }
//---------------------------hora_doc    
    public String getHora_doc() {
        return hora_doc.get();
    }
    public void setHora_doc(final String ihoras) {
        hora_doc.set(ihoras);
        calculaTotal();
    }
    public SimpleStringProperty hora_docProperty(){
        return hora_doc;
    }
//---------------------------tota_hrs    
    public String getTota_hrs() {
        return tota_hrs.get();
    }
    public void setTota_hrs(final String ihoras) {
        tota_hrs.set(ihoras);
    }
    public SimpleStringProperty tota_hrsProperty(){
        return tota_hrs;
    }
    
    public void calculaTotal(){
    	Integer v_total=Integer.parseInt(getHora_teo()) + Integer.parseInt(getHora_lab()) + Integer.parseInt(getHora_doc());
    	setTota_hrs(Integer.toString(v_total));
    }
    
}
